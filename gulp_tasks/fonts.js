const path = require('path');
const gulp = require('gulp');
const conf = require('../conf/gulp.conf');

gulp.task('_fonts', _fonts);

function _fonts() {
  return gulp.src(['bower_components/fontawesome/fonts/FontAwesome.*',
  'bower_components/fontawesome/fonts/fontawesome-webfont.*',
  'bower_components/bootstrap/fonts/glyphicons-halflings-regular.*'])
    .pipe(gulp.dest(path.join(conf.paths.dist, 'fonts')));
}

gulp.task('_main_fonts', _main_fonts);

function _main_fonts() {
  return gulp.src(['bower_components/oasi-main-theme/dist/fonts/*.{eot,svg,ttf,woff,woff2}'])
      .pipe(gulp.dest(path.join(conf.paths.dist, 'styles/fonts')));
}

gulp.task('fonts', gulp.series('_fonts', '_main_fonts'));
