const path = require('path');
const gulp = require('gulp');
const conf = require('../conf/gulp.conf');

gulp.task('data', data);

function data() {
  return gulp.src(conf.path.src('data/*.json'))
    .pipe(gulp.dest(path.join(conf.paths.tmp, 'data')));
}

gulp.task('inspinia', inspinia);

function inspinia() {
  return gulp.src(conf.path.src('app/inspinia.js'))
    .pipe(gulp.dest(path.join(conf.paths.tmp, 'app')));
}

gulp.task('cryptoPro', cryptoPro);

function cryptoPro() {
  return gulp.src(conf.path.src('app/cadesplugin_api.js'))
  .pipe(gulp.dest(path.join(conf.paths.tmp, 'app')));
}

gulp.task('polyfill', polyfill);

function polyfill() {
  return gulp.src(conf.path.src('app/ie_eventlistner_polyfill.js'))
  .pipe(gulp.dest(path.join(conf.paths.tmp, 'app')));
}
