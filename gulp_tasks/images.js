const path = require('path');
const gulp = require('gulp');
const conf = require('../conf/gulp.conf');

gulp.task('_images', _images);

function _images() {
  return gulp.src(conf.path.src('styles/patterns/**/*.{png,jpg,gif}'))
    .pipe(gulp.dest(path.join(conf.paths.tmp, 'patterns')));
}

gulp.task('_images_icheck', _images_icheck);

function _images_icheck() {
  return gulp.src(['bower_components/iCheck/skins/square/green.png', 'bower_components/iCheck/skins/square/green@2x.png',
      'bower_components/iCheck/skins/square/orange.png', 'bower_components/iCheck/skins/square/orange@2x.png'])
    .pipe(gulp.dest(path.join(conf.paths.dist, 'styles')));
}

gulp.task('_images_chosen', _images_chosen);

function _images_chosen() {
  return gulp.src(['bower_components/chosen/chosen-sprite.png', 'bower_components/chosen/chosen-sprite@2x.png'])
    .pipe(gulp.dest(path.join(conf.paths.tmp, 'styles'))).pipe(gulp.dest(path.join(conf.paths.dist, 'styles')));
}

gulp.task('_icon_favicon', _icon_favicon);

function _icon_favicon() {
  return gulp.src(conf.path.src('favicon.ico'))
    .pipe(gulp.dest(conf.paths.tmp)).pipe(gulp.dest(conf.paths.dist));
}

gulp.task('_images_files', _images_files);

function _images_files() {
    return gulp.src(['bower_components/oasi-file-manager/dist/images/*.{png,jpg,gif}', 'src/images/*.gif'])
        .pipe(gulp.dest(path.join(conf.paths.tmp, 'images'))).pipe(gulp.dest(path.join(conf.paths.dist, 'styles/images')));
}

gulp.task('images', gulp.series('_images', '_images_icheck', '_icon_favicon', '_images_chosen', '_images_files'));
