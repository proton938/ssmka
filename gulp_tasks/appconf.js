const gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');
var gutil = require('gulp-util');

const conf = require('../conf/gulp.conf');

gulp.task("appconf", appconf);

function appconf() {
	var env = gutil.env.env || 'dev';
	return gulp.src('env/' + env + '/config.json')
				.pipe(gulpNgConfig(conf.ngModule, {
					createModule: false
				}))
				.pipe(gulp.dest(conf.path.tmp('app/consts')));
}