class AgendaDocumentWrapper {
    document: AgendaDocument;
}

class AgendaDocument {
    agenda: Agenda;
}

class Agenda {
    agendaID: string;
    meetingType: string;
    meetingTheme: string;
    meetingDate: Date;
    meetingNumber: string;
    meetingPlace: string;
    meetingTime: string;
    meetingTimeEnd: string;
    meetingHeld: boolean;
    agendaPublished: Date;
    fileAgenda: FileType;
    authorCreate?: AgendaUser;
    initiator?: AgendaUser;
    attended: AgendaAttended[];
    filesAttach: FileType[];
}

class AgendaAttended {
    attendedNum?: number;
    internal: boolean;
    accountName: string;
    post: string;
    iofShort: string;
    fioFull: string;
    name: string;
    code: string;
    //TODO:
    role: any;
    prefect: string;
}

class AgendaUser {
    post: string;
    iofShort: string;
    fioFull: string;
    accountName: string;
}

class AgendaQuestion {
    agendaId: string;
    questionId: string;
    itemNumber: string;
    status: AgendaQuestionStatus;
    dateChange: Date;
    reasonChange: string;
    fio: string;
    login: string;
}

class AgendaQuestionStatus {
    static EXCLUDED = "excluded";
    static INCLUDED = "included";
    static ADDED = "added"; 
}

class AgendaQuestionAuthor {
    accountName: string;
    fioFull: string;
}

class AgendaPrintBasis {
    basisType: string;
    number: string;
    date: Date;
    note: string;
}

class AgendaSearchFilter {
    meetingHeld: boolean;
    meetingTypes: string[];
    meetingNumber: string;
    meetingDate: DateRange;
}

class AgendaPayload {
    document: {
        agenda: Agenda;
    };
    questions?: Question[];
}

angular.module("app").constant("AgendaQuestionStatus", {
    EXCLUDED: AgendaQuestionStatus.EXCLUDED,
    INCLUDED: AgendaQuestionStatus.INCLUDED,
    ADDED: AgendaQuestionStatus.ADDED
});
