interface IProtocolTask extends oasiBpmRest.ITask {
    taskDescription: string;
    delegatorFio: string;
    isDelegated: boolean;
    foreign: boolean;
    delegationRuleId: number;
    processInstanceId: string;
}