class DelegationRule {
    id: number;
    fromUser: string;
    toUser: string;
    startDate: number;
    endDate: number;
    systemPrefix: string;
}