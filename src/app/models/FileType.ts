class FileType {
    idFile: string;
    nameFile: string;
    sizeFile: string;
    signed: boolean;
    dateFile: Date;
    typeFile: string;
    classFile: string;

    static create(idFile?: string, nameFile?: string, sizeFile?: string, signed?: boolean, dateFile?: Date, typeFile?: string, classFile?: string): FileType {
        let result: FileType = new FileType();

        result.idFile = (idFile) ? idFile : result.idFile;
        result.nameFile = (nameFile) ? nameFile : result.nameFile;
        result.sizeFile = (sizeFile) ? sizeFile : result.sizeFile;
        result.signed = (signed) ? signed : result.signed;
        result.dateFile = (dateFile) ? dateFile : result.dateFile;
        result.typeFile = (typeFile) ? typeFile : result.typeFile;
        result.classFile = (classFile) ? classFile : result.classFile;

        return result;
    }
}
