class ProtocolStatus {
    static REVISION = 'revision';
    static APPROVED = 'approved';
    code: string;
    name: string;
    color: string;
}
