class MeetingFilenetSettings {
    meetingType: string;
    questionFolderGuid: string;
    questionFolderClass: string;
    agendaFolderGuid: string;
    protocolFolderGuid: string;
}