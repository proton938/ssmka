class ApprovalType {
    static AGREED = 'agreed';
    static APPROVAL = 'approval';
    approvalTypeCode: string;
    approvalTypeDuration: string;
    approvalType: string;
    signingEDS: boolean;
    buttonYes: string;
    buttonNo: string;
    print: boolean;
    buttonYesColor: string;
    buttonNoColor: string;
}