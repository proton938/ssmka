class BaseType {
    basisType: string;
    basisTypeCode: string;
    active: boolean;
}