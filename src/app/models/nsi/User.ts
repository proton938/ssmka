class User {
    displayName: string;
    accountName: string;
    mail: string;
    departmentFullName: string;
    departmentCode: string;
    fioShort: string;
    iofShort: string;
    telephoneNumber: string;
    post: string;
}