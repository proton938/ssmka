class MeetingType {
    meetingType: string;
    meetingFullName: string;
    meetingShortName: string;
    meetingWeekly: boolean;
    meetingDay: string;
    meetingTimeStart: string;
    meetingTimeEnd: string;
    meetingPlace: [{
        code: string;
        name: string;
    }];
    meetingAgendaDay: string;
    meetingAgendaTime: string;
    startNumber: number;
    creatingOutside: boolean;
    planningMeeting: boolean;
    CreatingCopies: any;
    Express: boolean;
    meetingColor: string;
}
