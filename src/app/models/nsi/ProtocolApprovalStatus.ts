class ProtocolApprovalStatus {
    static WAIT = 'wait';
    static APPROVAL = 'approval';
    static REJECTED = 'rejected';
    static APPROVED = 'approved';
    code: string;
    name: string;
    color: string;
}
