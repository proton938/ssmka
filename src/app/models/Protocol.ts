class ProtocolDocumentWrapper {
    document: ProtocolDocument;
}

class ProtocolDocument {
    protocol: Protocol;
}

class Protocol {
    protocolID: string;
    agendaID: string;
    bpmProcess: ProtocolProcess;
    meetingType: string;
    meetingDate: Date;
    meetingNumber: string;
    meetingTime: string;
    protocolApproved: boolean;
    attended: ProtocolAttended[];
    attendedNum: number;
    draftingGroupMKA: ProtocolDraftingGroupMKA;
    draftingGroupInvest: ProtocolDraftingGroupInvest;
    protocolGroup: ProtocolProtocolGroup;
    approvedBy: ProtocolApprovedBy;
    fileProtocol: FileType[];
    fileConclusion: FileType;
    orderAttach: FileType[];
    questionsNum: number;
    question: ProtocolQuestion[];
    meetingFullName: string;
    secretary: ProtocolApprovedBy;
    secretaryResponsible: SecretaryResponsible;
    commentSecretaryResponsible: ProtocolComment[];
    commentChairman: ProtocolComment[];
    dsProtocolSecretaryResponsible: DsType;
    dsConclusionSecretaryResponsible: DsType;
    dsProtocolChairman: DsType;
    dsConclusionChairman: DsType;
    fileProtocolDraft: FileType;
    fileConclusionDraft: FileType;
    meetingHeld: boolean;
    protocolApprovedDate: Date;
    meetingTimeEnd: Date;
    meetingPlace: string;
    refusalQuestion: Refusal[];
    approval: ProtocolApproval;
    approvalHistory: ProtocolApprovalHistory;
    commissionMember: ProtocolCommissionMember[];
    invitee: ProtocolInvitee[];
    status: ProtocolStatus;
    protocolInfo: ProtocolInfo;

    public static closeApprovalCycle(protocol: Protocol, approvalStatus: ProtocolApprovalStatus): ProtocolApprovalHistoryCycle {
        let approvalCycle = protocol.approval.approvalCycle;
        protocol.approvalHistory = protocol.approvalHistory || new ProtocolApprovalHistory();
        let protocolApprovalHistoryCycle = ProtocolApprovalHistoryCycle.fromApprovalCycle(approvalCycle, protocol.fileProtocolDraft, protocol.fileConclusionDraft);
        protocol.approvalHistory.approvalCycle.push(protocolApprovalHistoryCycle);
        protocol.approval.approvalCycle = new ProtocolApprovalCycle('' + (parseInt(approvalCycle.approvalCycleNum) + 1));
        protocol.approval.approvalCycle.signOfApproval = approvalCycle.signOfApproval;
        protocol.approval.approvalCycle.agreed = approvalCycle.agreed.map(a => {
            let result = new ProtocolApprovalCycleAgreed();
            result.approvalType = a.approvalType;
            result.approvalDepartment = a.approvalDepartment;
            result.approvalTypeCode = a.approvalTypeCode;
            result.agreedBy = a.agreedBy;
            result.approvalNum = a.approvalNum;
            result.approvalResult = approvalStatus.name;
            result.approvalResultCode = approvalStatus.code;
            result.approvalResultColor = approvalStatus.color;
            return result;
        });
        return protocolApprovalHistoryCycle;
    }
}

class ProtocolCommissionMember {
    post: string;
    iofShort: string;
    fioFull: string;
    accountName: string;
}

class ProtocolInvitee {
    post: string;
    iofShort: string;
    fioFull: string;
    accountName: string;
    name: string;
    code: string;
}

class ProtocolApproval {
    approvalCycle: ProtocolApprovalCycle;

    constructor(approvalCycleNum: number) {
        this.approvalCycle = new ProtocolApprovalCycle('' + approvalCycleNum);
    }
}

class ProtocolApprovalCycle {
    approvalCycleNum: string;
    approvalCycleDate: Date;
    signOfApproval: boolean;
    agreed: ProtocolApprovalCycleAgreed[];

    constructor(approvalCycleNum: string) {
        this.approvalCycleNum = approvalCycleNum;
        this.approvalCycleDate = new Date();
        this.agreed = [];
    }
}

class ProtocolApprovalCycleAgreed {
    agreedStage: string;
    approvalNum: string;
    approvalDepartment: string;
    approvalTime: string;
    agreedBy: Approvee;
    approvalPlanDate: Date;
    approvalFactDate: Date;
    approvalResult: string;
    approvalResultCode: string;
    approvalResultColor: string;
    approvalType: string;
    approvalTypeCode: string;
    approvalNote: string;
    approvalComment: ProtocolApprovalCycleAgreedComment[];
    fileApproval: FileType[];
    agreedDs: ProtocolApprovalCycleAgreedDs;
    added: ProtocolApprovalCycleAgreedAdded[];

    static createFrom(approvalNum: string, approvalType: ApprovalType, status: ProtocolApprovalStatus, agreedBy?: User, approvalDepartment?: string) {
        const result = new ProtocolApprovalCycleAgreed();
        result.approvalNum = approvalNum;
        result.approvalType = approvalType.approvalType;
        result.approvalTypeCode = approvalType.approvalTypeCode;
        result.approvalResult = status.name;
        result.approvalResultCode = status.code;
        result.approvalResultColor = status.color;
        if (agreedBy) {
            result.agreedBy = Approvee.createFromUser(agreedBy);
        }
        result.approvalDepartment = approvalDepartment;
        return result;
    }
}

class ProtocolApprovalCycleAgreedComment {
    questionNumber: string;
    note: string;
    commentDate: Date;

    static fromComment(comment: Comment): ProtocolApprovalCycleAgreedComment {
        let result = new ProtocolApprovalCycleAgreedComment();
        result.questionNumber = comment.questionNumber;
        result.note = comment.comment;
        result.commentDate = new Date();
        return result;
    }

}

class ProtocolApprovalCycleAgreedDs {
    agreedDsLastName: string;
    agreedDsName: string;
    agreedDsPosition: string;
    agreedDsCN: string;
    agreedDsFrom: Date;
    agreedDsTo: Date;
}

class ProtocolApprovalCycleAgreedAdded {
    additionalAgreed: ProtocolApprovalCycleAgreedAddedAdditional;
    addedNote: string;

    static fromUser(user: User, comment: string) {
        let result = new ProtocolApprovalCycleAgreedAdded();
        result.additionalAgreed = ProtocolApprovalCycleAgreedAddedAdditional.fromUser(user);
        result.addedNote = comment;
        return result;
    }
}

class ProtocolApprovalCycleAgreedAddedAdditional {
    accountName: string;
    fio: string;
    iofShort: string;

    static fromUser(user: User) {
        let result = new ProtocolApprovalCycleAgreedAddedAdditional();
        result.accountName = user.accountName;
        result.fio = user.displayName;
        result.iofShort = user.iofShort;
        return result;
    }
}

class ProtocolApprovalHistory {
    constructor() {
        this.approvalCycle = [];
    }

    approvalCycle: ProtocolApprovalHistoryCycle[];
}

class ProtocolApprovalHistoryCycle {
    approvalCycleNum: string;
    approvalCycleDate: Date;
    signOfApproval: boolean;
    approvalCycleFile: FileType[];
    approvalCycleFileProtocol: FileType;
    approvalCycleFileConclusion: FileType;
    agreed: ProtocolApprovalHistoryCycleAgreed[];

    static fromApprovalCycle(approvalCycle: ProtocolApprovalCycle, approvalCycleFileProtocol: FileType, approvalCycleFileConclusion: FileType): ProtocolApprovalHistoryCycle {
        return {
            approvalCycleNum: approvalCycle.approvalCycleNum,
            approvalCycleDate: approvalCycle.approvalCycleDate,
            approvalCycleFile: [],
            approvalCycleFileProtocol: approvalCycleFileProtocol,
            approvalCycleFileConclusion: approvalCycleFileConclusion,
            signOfApproval: approvalCycle.signOfApproval,
            agreed: approvalCycle.agreed.map(a => ProtocolApprovalHistoryCycleAgreed.fromApprovalCycleAgreed(a))
        }
    }
}

class ProtocolApprovalHistoryCycleAgreed {
    agreedStage: string;
    approvalNum: string;
    approvalTime: string;
    agreedBy: Approvee;
    approvalPlanDate: Date;
    approvalFactDate: Date;
    approvalResult: string;
    approvalResultCode: string;
    approvalResultColor: string;
    approvalType: string;
    approvalTypeCode: string;
    approvalNote: string;
    approvalComment: ProtocolApprovalHistoryCycleAgreedComment[];
    fileApproval: FileType[];
    added: ProtocolApprovalHistoryCycleAgreedAdded[];

    static fromApprovalCycleAgreed(agreed: ProtocolApprovalCycleAgreed): ProtocolApprovalHistoryCycleAgreed {
        return {
            agreedStage: agreed.agreedStage,
            approvalNum: agreed.approvalNum,
            approvalTime: agreed.approvalTime,
            agreedBy: angular.copy(agreed.agreedBy),
            approvalPlanDate: agreed.approvalPlanDate,
            approvalFactDate: agreed.approvalFactDate,
            approvalResult: agreed.approvalResult,
            approvalResultCode: agreed.approvalResultCode,
            approvalResultColor: agreed.approvalResultColor,
            approvalType: agreed.approvalType,
            approvalTypeCode: agreed.approvalTypeCode,
            approvalNote: agreed.approvalNote,
            approvalComment: agreed.approvalComment ? agreed.approvalComment.map(c => ProtocolApprovalHistoryCycleAgreedComment.fromApprovalCycleAgreedComment(c)) : null,
            fileApproval: agreed.fileApproval,
            added: agreed.added ? agreed.added.map(a => ProtocolApprovalHistoryCycleAgreedAdded.fromApprovalCycleAgreedAdded(a)) : null
        }
    }
}


class ProtocolApprovalHistoryCycleAgreedComment {
    questionNumber: string;
    note: string;
    commentDate: Date;

    static fromApprovalCycleAgreedComment(comment: ProtocolApprovalCycleAgreedComment): ProtocolApprovalHistoryCycleAgreedComment {
        return {
            questionNumber: comment.questionNumber,
            note: comment.note,
            commentDate: comment.commentDate
        }
    }
}

class ProtocolApprovalHistoryCycleAgreedAdded {
    additionalAgreed: ProtocolApprovalHistoryCycleAgreedAddedAdditional;
    addedNote: string;

    static fromApprovalCycleAgreedAdded(added: ProtocolApprovalCycleAgreedAdded): ProtocolApprovalHistoryCycleAgreedAdded {
        return {
            additionalAgreed: ProtocolApprovalHistoryCycleAgreedAddedAdditional.fromApprovalCycleAgreedAddedAdditional(added.additionalAgreed),
            addedNote: added.addedNote
        }
    }
}

class ProtocolApprovalHistoryCycleAgreedAddedAdditional {
    accountName: string;
    fio: string;
    iofShort: string;

    static fromApprovalCycleAgreedAddedAdditional(addedAdditional: ProtocolApprovalCycleAgreedAddedAdditional): ProtocolApprovalHistoryCycleAgreedAddedAdditional {
        return {
            accountName: addedAdditional.accountName,
            fio: addedAdditional.fio,
            iofShort: addedAdditional.iofShort
        }
    }
}

class Refusal {
    executorDivisionCode: string;
    executorDivisionName: string;
    executor: RefusalExecutor[];
    question: RefusalQuestion[];
}

class RefusalExecutor {
    executorsLogin: string;
    executorFio: string;
    executorCheck: boolean;
}

class RefusalQuestion {
    questionNumber: string;
    refuseReason: RefusalRefuseReason[];
}

class RefusalRefuseReason {
    code: string;
    name: string;
}

class ProtocolComment {
    commentText: string;
    commentDate: string;
}

class ProtocolAttended {
    // fioShort: string;
    // role: string;
    accountName: string;
    post: string;
    iofShort: string;
    fioFull: string;
    name?: string;
    code?: string;
    role: {
        roleCode: string;
        roleName: string;
    };
    prefect: string;
    internal: boolean;

    static fromUser(user: oasiNsiRest.UserBean): ProtocolAttended {
        let res = new ProtocolAttended();
        res.accountName = user.accountName;
        res.post = user.post;
        res.iofShort = user.iofShort;
        res.fioFull = user.displayName;
        res.name = user.departmentFullName;
        res.code = user.departmentCode;
        res.internal = true;
        return res;
    }
}

class ProtocolSigner {
    iofShort: any;
    post: string;
    fioShort: string;
    accountName: string;
}

class ProtocolDraftingGroupMKA {
    member: ProtocolSigner[];
}

class ProtocolDraftingGroupInvest {
    member: ProtocolSigner[];
}

class ProtocolProtocolGroup {
    member: ProtocolSigner[];
}

class ProtocolPerson {
    post: string;
    iofShort: string;
    fioFull: string;
    accountName: string;
}

class ProtocolApprovedBy extends ProtocolPerson {
}

class SecretaryResponsible extends ProtocolPerson {
}

class ProtocolQuestion {
    questionID: string;
    questionNumber: string;
    itemNumber: string;
    departmentPrepareCode: string;
    questionCategory: string;
    questionCategoryPrint: string;
    refuseReasonPrint: string;
    excluded: boolean;
    basis: ProtocolQuestionBasis;
    resultsVote: ProtocolQuestionVoteResult;
    decisionDraft: string;
    decisionType: string;
    decisionTypeCode: string;
    decisionText: string;
    recommendationGK: string;
    parameters: QuestionParameters;
    prefect: string[];
    address: string;
    cadastralNumber: string;
    question: string;
    customer: string;
    designer: string;
    projectAuthor: string[];
    rightHolder: string;
    speaker: ProtocolUser;
    materials: ProtocolQuestionMaterialGroup[];
    requestSaveObject: boolean;
    urbanLimits: string;
    urbanLimitsOther: string;
    networkEngineering: string[];
    networkEngineeringOther: string;
    noteSquatter: string;
    repeatSquatter: boolean;
    activity: boolean;
    comment: ProtocolQuestionComment[];
}


class ProtocolQuestionComment {
    note: string;
    commentDate: Date;
    login: string;
    personFio: string;
    post: string;
}

class ProtocolQuestionBasis {
    basisType: string;
    number: string;
    numberCase: string;
    date: Date;
    note: string;
}

class ProtocolQuestionVoteResult {
    no: number;
    yes: number;
    abstain: number;
    total: number;
}

class ProtocolQuestionMaterialGroup {
    idBase: string;
    materialsFIO: string;
    accountName: string;
    materialType: string;
    document: FileType[];
}

class ProtocolProcess {
    bpmProcessId: string;
}

class ProtocolUser {
    organization: string;
    organizationShort: string;
    organizationCode: string;
    post: string;
    fioFull: string;
    fioShort: string;
    phone: string;
    email: string;
    accountName: string;
}

class ProtocolSearchFilter {
    protocolApproved: boolean;
    meetingTypes: string[];
    meetingNumber: string;
    meetingDate: DateRange;
    approvedBy: string[];
}

class ProtocolInfo {
    durationMeeting: string;
    considerationTopic: string;
    other: string;
}

class ConclusionSecretaryResponsible {
    dsLastName: string;
    dsName: string;
    dsPosition: string;
    dsCN: string;
    dsFrom: Date;
    dsTo: Date;
}

class DsType {
    dsLastName: string;
    dsName: string;
    dsPosition: string;
    dsCN: string;
    dsFrom: Date;
    dsTo: Date;
}

class Approvee {
    post: string;
    fioFull: string;
    accountName: string;
    fioShort: string;
    iofShort: string;
    phone: string;

    static createFromUser(user: User) {
        let result = new Approvee();
        result.accountName = user.accountName;
        result.fioFull = user.displayName;
        result.fioShort = user.fioShort;
        result.iofShort = user.iofShort;
        result.post = user.post;
        result.phone = user.telephoneNumber;
        return result;
    }
}