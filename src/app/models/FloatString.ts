class FloatString {
    private string: string;

    constructor(string = "0") {
        if (typeof string !== "string") {
            throw new TypeError()
        }

        string = string === "" ? "0" : string;

        let match = string.match(/([0-9\-+\.])+/g);

        if (match) {
            string = match.join('');
            this.string = FloatString.onlyFloat(string);
        }
    }

    static normalize(string): any {
        // let parse = parseFloat(string);
        // let value = isNaN(parse) || !isFinite(parse) ? 0 : parse;
        let value = string ? new BigNumber(string) : "";
        return value.toString();
    }

    toString() {
        return this.string;
    }

    toJSON() {
        return this.string;
    }

    static onlyFloat(input): string {
        return input.match(/[+-]?(\d+)?(\.)?(\d+)?/g)[0];
    }
}
