class QuestionDocumentWrapper {
    document: QuestionDocument;
}

class QuestionDocument {
    question: Question;
}

class Question {
    questionID: string;
    questionPrimaryID: string;
    create: string;
    deleted: boolean;
    questionIDHistory: string[];
    meetingType: string;
    meetingFullName: string;
    meetingShortName: string;
    questionConsider: QuestionConsider;
    planningMeeting: boolean;
    comments: QuestionComment[];
    questionRG: QuestionRG;
    Renovation: boolean | string;
}

class QuestionParameters {
    vriVersion: VriVersionEnum;
    mainVRI: QuestionParameterType;
    relativelyVRI: QuestionParameterType;
    subVRI: QuestionParameterType;
    tep: QuestionParameterTep[];

    constructor() {
        this.vriVersion = VriVersionEnum.Dictionary_120;
        this.mainVRI = new QuestionParameterType();
        this.relativelyVRI = new QuestionParameterType();
        this.subVRI = new QuestionParameterType();
        this.tep = [];
    }
}

class VriVersionEnum {
    static Dictionary_120 = '120-ПП';
    static Dictionary_540 = '540-ПП';
}

class NSIType {
    public code: string|number;
    public name: string;

    constructor({code = null, name = null} = {code: null, name: null}) {
        this.code = code;
        this.name = name;
    }
}

class QuestionParameterTep {
    tepName: {
        code: string;
        name: string;
        type: string;
        sortNumber: string;
    };
    currentValue: string;
    newValue: string;
    recommendedValue: string;
    sortNumber: string;
}

class QuestionParameterType {
    current: NSIType[];
    'new': NSIType[];
    recommended: NSIType[];
    currentComment: string;
    newComment: string;
    recommendedComment: string;

    constructor() {
        this.current = [];
        this['new'] = [];
        this.recommended = [];
        this.currentComment = '';
        this.newComment = '';
        this.recommendedComment = '';
    }
}

class QuestionComment {
    date: Date;
    author: QuestionUserType;
    comment: string;
}

class QuestionRG {
    materialsSent: Date;
    materials: QuestionRGMaterial[];
}

class QuestionRGMaterial {
    idBase: string;
    materialType: string;
    contractorNote: QuestionUserType;
    contractorPresentation: QuestionUserType;
    materialsFIO: string;
    accountName: string;
    document: FileType;
    approved: boolean;
    approvedDate: Date;
    approvedCancelDate: Date;
    remark: QuestionRGRemark[];
}

class QuestionRGRemark {
    remarkAuthor: QuestionUserType;
    remarkDate: Date;
    remarkContent: string;
    remarkFixed: boolean;
    remarkFixedDate: Date;
}

class Consideration {
    static ACCEPTED = "Принято к рассмотрению";
    static REVOKED = "Отозвано";
}

class QuestionConsider {
    address: string;
    prefect: string[];
    district: string[];
    passDate: Date;
    cadastralNumber: string;
    consideration: Consideration;
    question: string;
    questionStatus: boolean;
    questionCategory: string;
    questionCategoryCode: string;
    basis: QuestionBasis;
    responsiblePrepare: QuestionResponsible;
    responsiblePrepareExpress: QuestionResponsible[];
    customer: string;
    rightHolder: string;
    planDate: Date;
    planDateEditor: string;
    parameters: QuestionParameters;
    meetingDate: Date;
    meetingNumber: string;
    itemNumber: string;
    gpzu: string;
    speaker: QuestionUserType;
    cospeaker: QuestionUserType[];
    decisionDraft: string;
    decisionType: string;
    decisionTypeCode: string;
    decisionText: string;
    presentationPrepare: QuestionUserType;
    materialsChecked: boolean;
    materials: QuestionMaterialGroup[];
    urbanLimitsCode: string;
    urbanLimits: string;
    urbanLimitsOther: string;
    networkEngineering: string[];
    networkEngineeringCode: string[];
    networkEngineeringOther: string;
    requestNumber: string;
    externalProtocol: QuestionExternalProtocol;
    externalAgree: OrgType[];
    nameObject: string;
    totalArea: string;
    designer: string;
    projectAuthor: string[];
    expertFio: string;
}

class OrgType {
    organization?: string;
    organizationShort?: string;
    organizationCode?: string;
}

class QuestionExternalProtocol {
    comission: NsiShortType;
    protocolDate: Date;
    protocolNumber: string;
    pointNumber: string;
    decision: string;
}

class QuestionBasis {
    basisType: string;
    number: string;
    numberCase: string;
    date: Date;
    note: string;
    documentId: string;
}

class QuestionResponsible {
    departmentPrepareFull: string;
    departmentPrepareShort: string;
    departmentPrepareCode: string;
    contractorPrepare: string;
    contractorPrepareFIO: string;
    contractorPreparePhone: string;
}

class QuestionUserType {
    organization: string;
    organizationShort: string;
    organizationCode: string;
    post: string;
    fioFull: string;
    fioShort: string;
    phone: string;
    email: string;
    accountName: string;
}

class QuestionMaterialGroup {
    idBase: string;
    materialsFIO: string;
    accountName: string;
    materialType: string;
    document: FileType[];
}

class NsiShortType {
    code: string;
    name: string;
    shortName: string;
}

angular.module("app").constant("Consideration", {
    ACCEPTED: Consideration.ACCEPTED,
    REVOKED: Consideration.REVOKED
});
