angular.module("app").constant("MaterialType", {
    INQUIRY_SSMKA: 'Справка СС МКА',
    INQUIRY_PZZ: 'Справка ПЗЗ',
    INQUIRY: 'Справка',
    INQUIRIES: ['Справка СС МКА', 'Справка ПЗЗ', 'Справка'],
    PRESENTATION: 'Презентация',
    GPZU_SCHEME: 'Чертеж ГПЗУ',
    OTHER: 'Дополнительные материалы'
});