angular.module("app").constant("MeetingType", {
    SSMKA: 'SS MKA',
    PZZ: 'PZZ',
    GK_EOO: 'GK_EOO',
    AS: 'AS'
});