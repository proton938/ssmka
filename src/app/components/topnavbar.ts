class TopNavBarComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = TopNavBarController;
        this.templateUrl = 'app/components/topnavbar.html';
    this.bindings = {};
    }
}

interface TopNavBarStateParams extends ng.ui.IStateParamsService {
    query: string;
}

class TopNavBarController {
    static $inject = ['$rootScope', 'session', '$state', '$stateParams', 'authenticationService', 'ssAuthorizationService',
        'userActionsStack', 'eventListener', 'meetingTypesService'];
    userLogin: string;
    query: string;
    post: string;
    constructor(private $rootScope: any,
        private session: oasiSecurity.SessionStorage,
        private $state: ng.ui.IStateService,
        private $stateParams: TopNavBarStateParams,
        private authenticationService: IAuthenticationService,
        private ssAuthorizationService: IAuthorizationService,
        private userActionsStack: UserActionsStack, private eventListener: IEventListener,
        private meetingTypesService: IMeetingTypesService
    ) {
        this.userLogin = this.session.name();
        this.post = this.session.post();
    }

    $onDestroy() {
    }
    
    $onInit() {
        this.query = this.$stateParams.query;
        var listener = (event: ng.IAngularEvent, toState: ng.ui.IState, toParams: any, fromState: ng.ui.IState, fromParams: any) => {
            this.query = toParams.query;
        };
        this.$rootScope.$on('$stateChangeStart', listener);        
    }

    public logout() {
        this.userActionsStack.register('logout');

        this.authenticationService.destroySession(() => {
            this.meetingTypesService.setUserMeetingTypes([]);
            this.ssAuthorizationService.setUserPermissions([]);
            this.session.cleanSession();
            this.$state.go('login', {}, { reload: true });
        }, (params) => {
            console.error(params);
        });
    }

    public search() {
        this.$state.go('app.ssmka.questions', { query: this.query, filter: null }, { reload: false });
    }
}

angular.module('app').component('topnavbar', new TopNavBarComponent());
