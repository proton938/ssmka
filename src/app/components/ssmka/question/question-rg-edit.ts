class QuestionRGEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionRGEditController;
        this.templateUrl = 'app/components/ssmka/question/question-rg-edit.html';
        this.bindings = {
            questionRG: "<"
        };
    }
}

interface IQuestionRGEditComponentBindings {
    questionRG: QuestionRG;
}

class QuestionRGEditController implements IQuestionRGEditComponentBindings {
    static $inject = [];

    public questionRG: QuestionRG; 
    
    constructor() {
    }

    $onInit() {
    }

}

angular.module('app').component('questionRgEdit', new QuestionRGEditComponent());