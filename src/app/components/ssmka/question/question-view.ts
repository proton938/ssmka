class QuestionViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionViewController;
        this.templateUrl = 'app/components/ssmka/question/question-view.html';
        this.bindings = {
            questionPromise: "<",
            edit: '&'
        };
    }
}

class QuestionAddMaterialType {
    materialType: string;
    maxFiles: number;
    fileType: string;
    meetingType: string;
    editable: boolean;
}

class QuestionViewController {
    static $inject = [
        '$q', '$uibModal', '$stateParams', '$uiViewScroll',
        'fileResource', 'questionResource', 'toastr',
        'ssAuthorizationService', 'MaterialType', 'MeetingType', 'gpzu_url', 'agr_url',
        'meetingTypesService', 'oasiSolarRestService'
    ];

    private loadingStatus: LoadingStatus;

    private canDeleteMaterials: boolean = false;

    private questionPromise: ng.IPromise<Question>;
    private question: Question;
    private questionHistory: Question[];
    private protocol: Protocol;
    private loadingMaterialsStatus: LoadingStatus;
    public edit: (val: any) => {};
    
    private materialTypes: QuestionAddMaterialType[];
    private basisLink: string = null;

    constructor(
        private $q: ng.IQService, private $uibModal: any, private $stateParams: any, private $uiViewScroll: any,
        private fileResource: IFileResource, private questionResource: IQuestionResource, private toastr: Toastr,
        private authorizationService: IAuthorizationService, private MaterialType: any, private MeetingType: any, private gpzuUrl: string, private agrUrl: string,
        private meetingTypesService: IMeetingTypesService, private oasiSolarRestService: oasiSolarRest.OasiSolarRestService
    ) {
    }

    $onInit() {

        this.loadingStatus = LoadingStatus.LOADING;
        this.canDeleteMaterials = this.authorizationService.check("OASI_MEETING_EDITBUTTON_MATERIALS");

        this.$q.all([
            this.questionPromise,
            this.meetingTypesService.getMeetingTypes()
        ]).then((data: any[]) => {
            const possiblePzzNumber = _.get(data[0], 'question.questionConsider.basis.number');
            if (!possiblePzzNumber) {
                return [...data, {}];
            }
            return this.$q.all([
                ...data,
                this.oasiSolarRestService.searchExt({
                    type: 'PZZ',
                    fields: [{
                        name: 'docNumberPzz',
                        value: possiblePzzNumber,
                    }]
                })
            ]);
        }).then((data: any[]) => {
            this.question = data[0].question;
            this.questionHistory = data[0].questionHistory;
            this.protocol = data[0].protocol;

            const meetingTypes: MeetingType[] = data[1];
            const meetingType = _.find(meetingTypes, mt => mt.meetingType === this.question.meetingType);
            this.question.planningMeeting = meetingType.planningMeeting;

            this.materialTypes = [{
                materialType: this.MaterialType.INQUIRY_SSMKA,
                maxFiles: 1, fileType: 'MkaDocInfoSsMka',
                meetingType: this.MeetingType.SSMKA,
                editable: false
            }, {
                materialType: this.MaterialType.INQUIRY,
                maxFiles: 1, fileType: 'MkaDocInfoPzz',
                meetingType: this.MeetingType.PZZ,
                editable: null
            }, {
                materialType: this.MaterialType.PRESENTATION,
                maxFiles: 1, fileType: 'MkaDocPresentation',
                meetingType: null,
                editable: null
            }, {
                materialType: this.MaterialType.GPZU_SCHEME,
                maxFiles: null,
                fileType: 'MkaDocDrowGpzu',
                meetingType: null,
                editable: false
            }, {
                materialType: this.MaterialType.OTHER,
                maxFiles: null,
                fileType: 'MkaDocOther',
                meetingType: null,
                editable: null
            }];

            if (data[2].docs && data[2].docs.length === 1) {
                this.basisLink = data[2].docs[0].link;
            } else {
                let documentId = this.question.questionConsider.basis.documentId;
                if (this.question.meetingType === this.MeetingType.AS && documentId) {
                    this.basisLink = this.getAgrLink(documentId);
                }
            }

            this.loadingStatus = LoadingStatus.SUCCESS;
            this.loadingMaterialsStatus = LoadingStatus.SUCCESS;

        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        }).finally(() => {
            this.$uiViewScroll(angular.element('ui-view'));
        });

    }
    
    editCard() {
        this.edit({ val: true });
    }
    
    reloadMaterials() {
        this.questionResource.materials({
            id: this.question.questionID
        }, (materials: QuestionMaterialGroup[]) => {
            this.question.questionConsider.materials = materials;
            this.loadingMaterialsStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingMaterialsStatus = LoadingStatus.ERROR;
        })         
    }
    
    deleteDocument(fileInfo: FileType): any {
        this.loadingMaterialsStatus = LoadingStatus.LOADING;
        this.questionResource.deleteMaterial({
            id: this.question.questionID,
            fileGuid: fileInfo.idFile
        }, () => {
            this.reloadMaterials();
        });
    }
    
    showAddMaterialsDialog() {
        if (this.authorizationService.check('OASI_MEETING_FORM_MATERIALS')) {
            let meetingType = this.question.meetingType;
            let materialTypes: QuestionAddMaterialType[] = _.filter(this.materialTypes, (mt) => {
                return (!mt.meetingType || mt.meetingType === meetingType) && this.isEditableMaterialType(mt);
            });
            this.$uibModal.open({
                component: 'question-add-materials',
                resolve: {
                    question: () => {
                        return this.question
                    },
                    questionHistory: () => {
                        return this.questionHistory
                    },
                    materialTypes: () => {
                        return materialTypes;
                    }
                },
                size: 'lg'
            }).result.then(() => {
                this.loadingMaterialsStatus = LoadingStatus.LOADING;
                this.reloadMaterials();
            }); 
        } else {
            this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
        }                
    }
    
    getUrbanLimitsString() {
    	if (this.question && this.question.questionConsider) {
	    	let urbanLimitsCode = this.question.questionConsider.urbanLimitsCode;
	    	return urbanLimitsCode === "OTHER" ? this.question.questionConsider.urbanLimitsOther
	    		: this.question.questionConsider.urbanLimits;
		}
    }
    
    getNetworkEngineeringsString() {
    	if (this.question && this.question.questionConsider) {
	    	let networkEngineering = this.question.questionConsider.networkEngineering;
	    	if (networkEngineering) {
	    		let arr = _.clone(networkEngineering);
	    		let ind = this.question.questionConsider.networkEngineeringCode.indexOf("OTHER");
	    		if (ind >= 0) {
	    			arr[ind] = this.question.questionConsider.networkEngineeringOther;
	    		}
	    		return arr.join(', ');
	    	}
    	}
    }
    
    isEditableMaterialGroup(materialGroup: string) {
        if (this.question.create !== '0') {
            return true;
        }
        let materialType: QuestionAddMaterialType = _.find(this.materialTypes, (mt) => {
            return mt.materialType === materialGroup;
        })
        return this.isEditableMaterialType(materialType);
    }    
    
    isEditableMaterialType(materialType: QuestionAddMaterialType) {
        if (this.question.create !== '0') {
            return true;
        }
        if (!materialType) {
            return true;
        }
        return _.isNull(materialType.editable) ? true : materialType.editable; 
    }

    isUnauthBuild() {
        return this.question && this.question.meetingType === 'UNAUTHBUILD';
    }
    /**
     *  Признак, что вопрос касается ГПЗУ
     *  @return {boolean}
     */
    isGpzu() {
        return this.question && this.question.questionConsider && this.question.questionConsider.basis.basisType === 'Заявка на ГПЗУ';
    }
    /**
     *  Ссылка на ГПЗУ
     *  @return {string | null}
     */
    getGpzuLink() {
        let gpzuNum = this.question.questionConsider.basis.number;
        if (gpzuNum) {
            let templ = _.template(this.gpzuUrl);
            return templ({gpzuNum: gpzuNum.replace('ГПЗУ', 'GPZU').replace('/', '-').replace('(', '').replace(')', '')});
        }
        return null;
    }
    /**
     *  Ссылка на АГР
     *  @return {string | null}
     */
    getAgrLink(documentId: string) {
        let templ = _.template(this.agrUrl);
        return templ({documentId: documentId});
    }

    isShowExternalProtocol() {
        let externalProtocol = this.question.questionConsider.externalProtocol;
        return externalProtocol && (
            externalProtocol.comission || externalProtocol.protocolNumber || externalProtocol.protocolDate ||
            externalProtocol.pointNumber || externalProtocol.decision
        )
    }
}

angular.module('app').component('questionView', new QuestionViewComponent());
