class QuestionInstructionsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionInstructionsController;
        this.templateUrl = 'app/components/ssmka/question/question-instructions.html';
        this.bindings = {
            questionPromise: "<",
            edit: '&'
        };
    }
}

class QuestionInstructionsController {
    static $inject = [
        '$q', 'instructionsService', 'nsiRestService'
    ];

    private questionPromise: ng.IPromise<any>;
    private question: Question;

    lists: any = {
        conditions: [],
        priority: []
    };

    private loadingStatus: LoadingStatus;
    private documents: oasiSolarRest.SearchResultDocument[];

    constructor(
        private $q: ng.IQService,
        private instructionsService: IInstructionsService,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;

        this.questionPromise.then((data: any) => {
            this.question = data.question;
            return this.nsiRestService.get('OasiInstructionStatus');
        }).then((conditions) => {
            this.lists.conditions = conditions;
            return this.nsiRestService.get('OasiInstructionPriority');
        }).then((priority) => {
            this.lists.priority = priority;
            return this.instructionsService.getQuestionInstructions(this.question);
        }).then((result: oasiSolarRest.SearchResult) => {
            this.documents = result.docs;
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }

}

angular.module('app').component('questionInstructions', new QuestionInstructionsComponent());