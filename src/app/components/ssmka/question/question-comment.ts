class QuestionCommentComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionCommentController;
        this.templateUrl = 'app/components/ssmka/question/question-comment.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class QuestionCommentController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = [];
    
    private resolve: any;
    
    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;
    
    private comment: string;
    
    constructor() {
    }

    save() {
        this.close({
            $value: this.comment
        });
    }
    
    cancel() {
        this.dismiss();
    }

    $onInit() {
        this.comment = this.resolve.comment;
    }

}

angular.module('app').component('questionComment', new QuestionCommentComponent());
