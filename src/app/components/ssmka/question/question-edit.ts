class QuestionEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionEditController;
        this.templateUrl = 'app/components/ssmka/question/question-edit.html';
        this.bindings = {
            questionPromise: "<",
            saveQuestion: '&',
            cancel: '&'
        };
    }
}

class QuestionEditController {
    static $inject = [
        '$timeout', '$state', '$scope', '$q',
        '$uibModal', 'session', 'questionResource',
        'meetingTypesService', 'nsiRestService',
        'toastr', 'ssAuthorizationService', 'Consideration', 'userUtils',
        'MeetingType'
    ];

    private loadingStatus: LoadingStatus;

    private questionPromise: ng.IPromise<Question>;
    private question: Question;
    private saveQuestion: (data: any) => {};
    private cancel: () => {};

    private meetingTypes: MeetingType[];
    private questionCategories: CategoryQuestion[];
    private origPrefects: Prefect[];
    private prefects: string[];
    private origDistricts: District[];
    private districts: string[];
    private basisTypes: BaseType[];
    private responsibleDepartments: ResponsibleDepartment[];
    private meetingResponsibles: oasiNsiRest.UserBean[];
    private presentationResponsibles: QuestionUserType[];
    private speakers: Speaker[];
    private urbanLimits: UrbanLimit[];
    private networkEngineerings: NetworkEngineering[];
    private isPlanDateEditable: Boolean = true;
    private hasOtherUrbanLimits: Boolean = false;
    private hasOtherNetworkEngineering: Boolean = false;
    private restrictionCreationMeetings: any[];
    private pzzExternalAgree: PzzExternalAgree[];
    private externalAgrees: { [key: string] : boolean } = {};

    private projectAuthor: string[];

    private mandatoryCadastralNumber: boolean;

    private updatingUsersListInProgress: Boolean = false;

    constructor(
        private $timeout: any, private $state: any, private $scope: any, private $q: ng.IQService,
        private $uibModal: any, private session: oasiSecurity.ISessionStorage, private questionResource: IQuestionResource,
        private meetingTypesService: IMeetingTypesService, private nsiRestService: oasiNsiRest.NsiRestService,
        private toastr: Toastr, private authorizationService: IAuthorizationService, private Consideration: any, private userUtils: IUserUtils,
        private  MeetingType: any) {
    }

    $onInit() {
        this.questionPromise.then((data: any) => {
            if (this.authorizationService.check('OASI_MEETING_FORM_QUESTION')) {
                this.question = angular.copy(data.question);
                this.projectAuthor = this.question.questionConsider.projectAuthor || [];
                if (this.question.questionID) {
                    this.checkPlanDate(this.question.questionID);
                } else {
                    this.loadDictionaries();
                }
                if (this.question.questionConsider.externalAgree) {
                    this.question.questionConsider.externalAgree.forEach(agr => {
                        this.externalAgrees[agr.organizationCode] = true;
                    })
                }
            } else {
                this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
                this.loadingStatus = LoadingStatus.ERROR;
            }
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDictionaries() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('Prefect'),
            this.nsiRestService.get('District'),
            this.nsiRestService.get('BaseTypes'),
            this.nsiRestService.get('ResponsibleDepartments'),
            this.nsiRestService.ldapUsers('OASI_MEETING_PRESENTATION'),
            this.nsiRestService.get('speakers'),
            this.nsiRestService.get('UrbanLimits'),
            this.nsiRestService.get('NetworkEngineering'),
            this.nsiRestService.get('RestrictionCreationMeetings'),
            this.nsiRestService.get('pzzExternalAgree')
        ]).then((values: any[]) => {

            this.meetingTypes = values[0].filter((el) => !el.Express);
            this.restrictionCreationMeetings = values[9];

            this.meetingTypes = _.filter(this.meetingTypes, (el) => {
                let elInRestriction = _.find(this.restrictionCreationMeetings, (i) => (i.MeetingTypeCode === el.meetingType));
                if (elInRestriction && elInRestriction.CategoryQuestionCode || !elInRestriction) {
                    return el;
                }
            });

            this.origPrefects = values[1];
            this.prefects = _.map(this.origPrefects, (prefect) => {
                return prefect.name;
            });
            this.origDistricts = values[2];
            this.districts = [];
            this.basisTypes = values[3];
            this.responsibleDepartments = values[4];
            this.presentationResponsibles = this.convertUsers(values[5]);
            this.speakers = values[6];
            this.urbanLimits = values[7];
            this.networkEngineerings = values[8];
            this.pzzExternalAgree = values[10];
            this.loadingStatus = LoadingStatus.SUCCESS;

            this.meetingTypeChanged();
            this.prefectChanged(false);
            this.departmentChanged();
            
            this.checkHasOtherNetworkEngineering();
            this.checkHasOtherUrbanLimits();

            if (this.question.hasOwnProperty('Renovation') &&
                this.question.Renovation === true) {
                this.question.Renovation = true;
            }
            else {
                this.question.Renovation = false;
            }


        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    checkPlanDate(questionID: string) {
        this.questionResource
            .isPlanDateEditable({id: questionID}, (response) => {
                this.isPlanDateEditable = response.editable;
                this.loadDictionaries();
            });
    }

    convertUsers(users: oasiNsiRest.UserBean[]): QuestionUserType[] {
        return _.map(users, (user: oasiNsiRest.UserBean) => {
            return this.userUtils.toQuestionUserType(user);
        });
    }

    meetingTypeChanged(createQuestion?) {
        let meetingTypeName: string = this.question.meetingType;

        if (meetingTypeName) {

            let meetingType: MeetingType = _.find(this.meetingTypes, mt => mt.meetingType === meetingTypeName);
            this.question.meetingShortName = meetingType.meetingShortName;
            this.question.meetingFullName = meetingType.meetingFullName;
            this.question.planningMeeting = meetingType.planningMeeting;

            if (this.question.meetingType === 'RENOV' && createQuestion) {
                this.question.Renovation = true;
            }
            else if (createQuestion){
                this.question.Renovation = false;
            }

            this.nsiRestService.getBy('CategoryQuestion', {
                nickAttr: 'meetingType',
                values: [meetingTypeName]
            }).then((questionCategories: CategoryQuestion[]) => {
                this.questionCategories = _.filter(questionCategories, (category) => {
                    let meeting = _.find(this.restrictionCreationMeetings, (i) => i.MeetingTypeCode === this.question.meetingType);
                    if (meeting && meeting.CategoryQuestionCode.indexOf(category.questionCode) === -1 ) {
                        return category;
                    }
                    else if (!meeting) {
                        return category;
                    }
                });
            });
            this.mandatoryCadastralNumber = this.question.meetingType !== 'GK_EOO';
        } else {

            this.question.meetingShortName = null;
            this.question.meetingFullName = null;
            this.question.planningMeeting = false;

            this.questionCategories = [];
            this.mandatoryCadastralNumber = false;
        }

    }

    questionCategoryChanged() {
        let questionCategory: string = this.question.questionConsider.questionCategory;
        this.question.questionConsider.question = questionCategory;
    }

    prefectChanged(nullDistricts: boolean) {
        let prefectsNames: string[] = this.question.questionConsider ? this.question.questionConsider.prefect : [];
        this.districts = _.chain(this.origDistricts).filter((district) => {
            return _.intersection(prefectsNames, _.map(district.perfectId, pref => pref.name)).length > 0;
        }).map((district) => {
            return district.name;
        }).value();
        if (nullDistricts && this.question.questionConsider) {
            this.question.questionConsider.district = [];
        }
    }

    departmentChanged() {
        let departmentPrepareCode = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.departmentPrepareCode : null;
        this.updatingUsersListInProgress = true;
        if (departmentPrepareCode) {
            let department: ResponsibleDepartment = _.find(this.responsibleDepartments, function(department: ResponsibleDepartment) {
                return department.departmentPrepareCode === departmentPrepareCode;
            });
            this.question.questionConsider.responsiblePrepare.departmentPrepareShort = department.departmentPrepareShort;
            this.question.questionConsider.responsiblePrepare.departmentPrepareFull = department.departmentPrepareFull;

            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE', department.departmentPrepareCode).then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });

        } else {
            if (this.question.questionConsider.responsiblePrepare) {
                this.question.questionConsider.responsiblePrepare.departmentPrepareShort = null;
                this.question.questionConsider.responsiblePrepare.departmentPrepareFull = null;
            }
            this.meetingResponsibles = [];
            this.updatingUsersListInProgress = false;
        }

    }

    departmentContractorChanged() {
        let contractorPrepareLogin = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.contractorPrepare : null;
        if (contractorPrepareLogin) {
            let responsible = _.find(this.meetingResponsibles, (meetingResponsible) => {
                return meetingResponsible.accountName === contractorPrepareLogin;
            });
            this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = responsible.displayName;
            this.question.questionConsider.responsiblePrepare.contractorPreparePhone = responsible.telephoneNumber;
        } else {
            if (this.question.questionConsider.responsiblePrepare) {
                this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = null;
                this.question.questionConsider.responsiblePrepare.contractorPreparePhone = null;
            }
        }

    }
    
    isUnauthBuild() {
        return this.question && this.question.meetingType === 'UNAUTHBUILD';
    }    
    
    checkHasOtherUrbanLimits() {
        let urbanLimitsCode = this.question.questionConsider.urbanLimitsCode;
        this.hasOtherUrbanLimits = urbanLimitsCode === "OTHER";    
    }    

    urbanLimitsChanged() {
        let urbanLimitsCode = this.question.questionConsider.urbanLimitsCode;
        this.checkHasOtherUrbanLimits();
        if (!this.hasOtherUrbanLimits) {
        	delete this.question.questionConsider.urbanLimitsOther;
        }
        if (urbanLimitsCode) {
            this.question.questionConsider.urbanLimits = _.find(this.urbanLimits, (ul) => {
                return ul.limitCode === urbanLimitsCode;
            }).limitName;
        } else {
            delete this.question.questionConsider.urbanLimits; 
        }
    }

    checkHasOtherNetworkEngineering() {
        let networkEngineeringCode = this.question.questionConsider.networkEngineeringCode;
        this.hasOtherNetworkEngineering = networkEngineeringCode &&
	        _.some(networkEngineeringCode, (ne) => {
	            return ne === "OTHER";
	        })    
    }

    networkEngineeringChanged() {
        let networkEngineeringCode = this.question.questionConsider.networkEngineeringCode;
        this.checkHasOtherNetworkEngineering();
        if (!this.hasOtherNetworkEngineering) {
        	delete this.question.questionConsider.networkEngineeringOther;
        }        
        this.question.questionConsider.networkEngineering = networkEngineeringCode ? _.map(networkEngineeringCode, (code) => {
            return _.find(this.networkEngineerings, (ne) => {
                return ne.engCode === code;
            }).engName;
        }) : [];
    }

    planDateChanged(alue: Date) {
        this.question.questionConsider.planDateEditor = this.session.fullName();
    }

    save() {
        let hasProjectAuthor = this.question.questionConsider.projectAuthor && this.question.questionConsider.projectAuthor.length > 0;
        if (this.question.meetingType === this.MeetingType.AS && !hasProjectAuthor) {
            this.toastr.warning('Необходимо указать хотя бы одного автора проекта!');
            return;
        }
        if (!this.question.questionConsider.passDate) {
            this.question.questionConsider.passDate = new Date();
        }
        let self: QuestionEditController = this;
        self.saveQuestion({ question: self.question });
    }

    cancelEdit() {
        this.cancel();
    }

    isActiveBaseType(baseType: BaseType) {
        return baseType.active;
    }

    clearSpeaker() {
        delete this.question.questionConsider.speaker;
    }

    chooseSpeaker() {
        let tree = this.buildSelectedTree(this.speakers);
        this.$uibModal.open({
            component: 'dictionary-modal',
            size: 'lg',
            resolve: {
                name: () => {
                    return 'Выбор докладчика';
                },
                multiple: () => {
                    return false;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let speaker = this.question.questionConsider.speaker;
                    return speaker ? [this.question.questionConsider.speaker.organizationCode] : [];
                }
            }
        }).result.then((result: string[]) => {
            if (result && result.length > 0) {
                let speaker: Speaker = this.findInDictByCode(this.speakers, result[0]);
                this.question.questionConsider.speaker = this.convertSpeakerOrganizationToQuestionUserType(speaker);
            }
        });
    }

    chooseCospeakers() {
        let tree = this.buildSelectedTree(this.speakers);
        this.$uibModal.open({
            component: 'dictionary-modal',
            size: 'lg',
            resolve: {
                name: () => {
                    return 'Выбор содокладчиков';
                },
                multiple: () => {
                    return true;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let cospeakers = this.question.questionConsider.cospeaker;
                    return cospeakers ? _.map(cospeakers, (cs) => {
                        return cs.organizationCode;
                    }) : [];
                }
            }
        }).result.then((result: string[]) => {
            if (result) {
                this.question.questionConsider.cospeaker = _.map(result, (val) => {
                    let cospeaker = this.findInDictByCode(this.speakers, val);
                    return this.convertSpeakerOrganizationToQuestionUserType(cospeaker);
                })
            } else {
                delete this.question.questionConsider.cospeaker;
            }
        });
    }

    convertSpeakerOrganizationToQuestionUserType(org: Speaker): QuestionUserType {
        let result = new QuestionUserType();
        result.organization = org.name;
        result.organizationShort = org.nameShort;
        result.organizationCode = org.code;
        return result;
    }

    findInDictByCode(dict: Speaker[], code: string): Speaker {
        let result: Speaker = null;

        dict.forEach(d => {
            if (d.code === code) {
                result = d;
            }

            if (d.children !== undefined && d.children !== null) {
                let r = this.findInDictByCode(d.children, code);
                if (r !== null) {
                    result = r;
                }
            }
        });

        return result;
    }

    isHasPrefect() {
        return !_.isEmpty(this.question.questionConsider.prefect);
    }

    buildSelectedTree(dict: Speaker[]): oasiWidgets.ITreeViewItemChecked[] {
        let tree: oasiWidgets.ITreeViewItemChecked[] = null;

        dict.forEach((item: Speaker) => {
            if (tree === null) {
                tree = [];
            }

            let children: oasiWidgets.ITreeViewItemChecked[] = (item.children === undefined || item.children === null) ? null : this.buildSelectedTree(item.children);
            let selectable = children ? false : true;
            let template = children ? item.group : item.name;
            let result: oasiWidgets.ITreeViewItemChecked = {
                parent: null,
                id: item.code,
                template: template,
                children: children,
                expanded: false,
                checked: false,
                disabled: false,
                selectable: selectable
            };
            if (children) {
                children.forEach(ch => {
                    ch.parent = result;
                })
            }
            tree.push(result);
        });

        return tree;
    }

    switchExternalAgree(code, val) {
        if (val) {
            let pzzExternalAgree = _.find(this.pzzExternalAgree, agr => agr.code === code);
            this.question.questionConsider.externalAgree = this.question.questionConsider.externalAgree || [];
            this.question.questionConsider.externalAgree.push({
                organization: pzzExternalAgree.name,
                organizationShort: pzzExternalAgree.shortName,
                organizationCode: pzzExternalAgree.code
            });
        } else {
            _.remove(this.question.questionConsider.externalAgree, agr => agr.organizationCode === code);
        }
    }

    onChangeProjectAuthor = function (projectAuthor) {
        this.question.questionConsider.projectAuthor = projectAuthor;
    }.bind(this);

}

angular.module('app').component('questionEdit', new QuestionEditComponent());
