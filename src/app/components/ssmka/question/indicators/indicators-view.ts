class IndicatorsViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = IndicatorsViewController;
        this.templateUrl = 'app/components/ssmka/question/indicators/indicators-view.html';
        this.bindings = {
            questionPromise: "<",
            edit: '&'
        };
    }
}

class IndicatorsViewController {
    static $inject = ['$q', 'questionResource', 'nsiRestService'];

    private questionPromise: ng.IPromise<Question>;
    private question: Question;
    private loadingQuestionStatus: LoadingStatus;
    public edit: (val: any) => {};

    constructor(private $q: ng.IQService, private questionResource: IQuestionResource, private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.loadingQuestionStatus = LoadingStatus.LOADING;
        this.questionPromise.then((data: any) => {
            this.question = data.question;

            if (!this.question.questionConsider.parameters) {
                this.question.questionConsider.parameters = new QuestionParameters();
            }

            this.$q.all([
                this.nsiRestService.get('PzzParameters')
            ]).then((values: any[]) => {
                const pzzParameters: PzzParameter[] = values[0];
                if (!this.question.questionConsider.parameters.tep.length) {
                    this.question.questionConsider.parameters.tep = [];
                    pzzParameters.forEach((pzzParameter) => {
                        this.question.questionConsider.parameters.tep.push({
                            tepName: pzzParameter,
                            currentValue: '',
                            newValue: '',
                            recommendedValue: '',
                            sortNumber: '',
                        });
                    });
                }
                this.question.questionConsider.parameters.tep.forEach((tep: any) => {
                    const code = tep.tepName;
                    if (!_.isString(code)) {
                        return;
                    }
                    const find = _.find(pzzParameters, (item: any) => item.code === code);
                    if (find){
                        tep.tepName = find;
                    }
                });
                this.loadingQuestionStatus = LoadingStatus.SUCCESS;
            }, () => {
                this.loadingQuestionStatus = LoadingStatus.ERROR;
            });
        });
    }

    editCard() {
        this.edit({ val: true });
    }
}

angular.module('app').component('indicatorsView', new IndicatorsViewComponent());