class IndicatorsEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = IndicatorsEditController;
        this.templateUrl = 'app/components/ssmka/question/indicators/indicators-edit.html';
        this.bindings = {
            questionPromise: "<",
            saveQuestion: '&',
            cancel: '&'
        };
    }
}

class IndicatorsEditController {
    static $inject = ['$q', 'questionResource', 'nsiRestService', '$uibModal'];

    private saveQuestion: (data: any) => {};
    private questionPromise: ng.IPromise<Question>;
    private question: Question;
    private loadingQuestionStatus: LoadingStatus;
    private vriZu120Types: VriZu120[] = [];

    public edit: (val: any) => {};

    constructor(private $q: ng.IQService,
                private questionResource: IQuestionResource,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private $uibModal: any) {
    }

    $onInit() {
        this.loadingQuestionStatus = LoadingStatus.LOADING;
        this.questionPromise.then((data: any) => {
            this.question = angular.copy(data.question);

            if (!this.question.questionConsider.parameters) {
                this.question.questionConsider.parameters = new QuestionParameters();
            }

            if (!this.question.questionConsider.parameters.vriVersion) {
                this.question.questionConsider.parameters.vriVersion = VriVersionEnum.Dictionary_120;
            }

            this.$q.all([
                this.nsiRestService.get('vriZu120'),
                this.nsiRestService.get('PzzParameters')
            ]).then((values: any[]) => {
                this.vriZu120Types = values[0];
                const pzzParameters: PzzParameter[] = values[1];

                if (!this.question.questionConsider.parameters.tep) {
                    this.question.questionConsider.parameters.tep = [];
                    pzzParameters.forEach((pzzParameter) => {
                        this.question.questionConsider.parameters.tep.push({
                            tepName: pzzParameter,
                            currentValue: '',
                            newValue: '',
                            recommendedValue: '',
                            sortNumber: '',
                        });
                    });
                }
                this.question.questionConsider.parameters.tep.forEach((tep: any) => {
                    const code = tep.tepName;
                    if (!_.isString(code)) {
                        return;
                    }
                    const find = _.find(pzzParameters, (item: any) => item.code === code);
                    if (find){
                        tep.tepName = find;
                    }
                });
                this.loadingQuestionStatus = LoadingStatus.SUCCESS;
            }, () => {
                this.loadingQuestionStatus = LoadingStatus.ERROR;
            });
        });
    }

    /**
     * @param {boolean} customizable - для новых значений (.new) используется только новая версия справочника
     * */
    onSelectFromReferenceClick(models: any[], customizable: boolean){
        let usedDictionary;

        if (customizable){
            switch (this.question.questionConsider.parameters.vriVersion){
                case VriVersionEnum.Dictionary_120: usedDictionary = this.vriZu120Types; break;
            }
        } else {
            usedDictionary = this.vriZu120Types;
        }
        let tree = this._buildSelectedTree(usedDictionary, this.question.questionConsider.parameters.vriVersion);

        let modalInstance = this.$uibModal.open({
            component: 'dictionaryModal',
            animation: true,
            controllerAs: "$ctrl",
            size: 'lg',
            resolve: {
                multiple: () => {
                    return true;
                },
                name: () => {
                    return name;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let codes: string[] = models.map( item => item.code );
                    return codes;
                }
            }
        });

        return modalInstance.result.then((codes) => {
            let dicts: any[] = [];
            codes.forEach(code => {
                let dict = this._findDictByCode(usedDictionary, code);

                if (dict){
                    dicts.push(dict);
                }
            });
            return dicts;
        });
    }

    onSelectFromReferenceClick1(){
        this.onSelectFromReferenceClick(this.question.questionConsider.parameters.mainVRI.recommended || [], true)
            .then((codes) => {
                this.question.questionConsider.parameters.mainVRI.recommended = codes;
            });
    }
    onSelectFromReferenceClick2(){
        this.onSelectFromReferenceClick(this.question.questionConsider.parameters.relativelyVRI.recommended || [], false)
            .then((codes) => {
                this.question.questionConsider.parameters.relativelyVRI.recommended = codes;
            });
    }
    onSelectFromReferenceClick3(){
        this.onSelectFromReferenceClick(this.question.questionConsider.parameters.subVRI.recommended || [], true)
            .then((codes) => {
                this.question.questionConsider.parameters.subVRI.recommended = codes;
            });
    }

    save() {
        this.saveQuestion({ question: this.question });
    }

    /**
     * Адаптировать данные из справочника к интерфейсу
     * */
    _buildSelectedTree(dict: any[] = [], dictName: VriVersionEnum, parent?: oasiWidgets.ITreeViewItemChecked): oasiWidgets.ITreeViewItemChecked[] {
        let tree: oasiWidgets.ITreeViewItemChecked[] = null;
        dict.forEach(item => {
            if ( !tree ) {
                tree = [];
            }
            let template = `${item.value} [${item.code}]`;
            let result: oasiWidgets.ITreeViewItemChecked = {
                parent: parent || null,
                id: item.code,
                template,
                selectable: true,
                children: null,
                expanded: true,
                checked: false,
                disabled: false
            };

            if (item.children){
                let children = this._buildSelectedTree(item.children, dictName, result);
                result.children = children;
            } else {
                result.children = null;
            }
            tree.push(result);
        });
        return tree;
    }

    _findDictByCode(dicts: any[], code: string): string {
        let result: any = null;

        dicts.some( dict => {
            if (dict.code === code) {
                dict.name = dict.value;
                result = dict;
            }
            if (dict.children) {
                let subDict = this._findDictByCode(dict.children, code);
                if ( subDict ) {
                    result = subDict;
                }
            }
            return result;
        });
        return result;
    }

    normalize(string){
        return FloatString.normalize(string);
    }
}

angular.module('app').component('indicatorsEdit', new IndicatorsEditComponent());