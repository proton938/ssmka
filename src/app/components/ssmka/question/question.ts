class QuestionComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionController;
        this.templateUrl = 'app/components/ssmka/question/question.html';
        this.bindings = {
        };
    }
}

interface IQuestionController {
    addComment(comment: string): void;
}

class QuestionController {
    static $inject = [
        '$q', '$state', '$stateParams', 'session',
        '$uibModal', 'questionResource', 'protocolResource',
        'nsiRestService', 'wordReporterPreviewService',
        'toastr', 'ssAuthorizationService', 'userUtils', 'MeetingType',
        'meetingTypesService'
    ];

    private loadingAppStatus: LoadingStatus;

    private questionPromise: ng.IPromise<any>;
    private question: Question;   
    private questionHistory: Question[];
    private protocol: Protocol;

    private hasEditCommentPermission: boolean;
    private showIndicatorsTab: boolean = false;

    private editingCard: boolean;
    private editingRG: boolean;
    private editingIndicators: boolean = false;
    private availableToAddInstruction: any;
    private availableToCopy: any;

    private accountName: string;

    private activeTab: any;
    private externalAgreeCodes: string[];
    private pzzExternalAgree: any;

    constructor(
        private $q: ng.IQService, private $state: any, private $stateParams: any, private session: oasiSecurity.ISessionStorage,
        private $uibModal: any, private questionResource: IQuestionResource, private protocolResource: IProtocolResource,
        private nsiRestService: oasiNsiRest.NsiRestService, private wordReporterPreviewService: oasiWordreporterRest.WordReporterPreviewService,
        private toastr: Toastr, private authorizationService: IAuthorizationService, private userUtils: IUserUtils, private MeetingType: any,
        private meetingTypesService: IMeetingTypesService
    ) {
    }

    $onInit() {
        let self: QuestionController = this;

        this.editingCard = this.$stateParams.editingCard;
        this.editingRG = false;

        this.accountName = this.session.login();

        this.hasEditCommentPermission = this.authorizationService.check('OASI_MEETING_EDITBUTTON_COMMENT');
        
        let id: string = this.$stateParams.id;
        this.activeTab = this.$stateParams.tab;
        let question: Question = this.$stateParams.question;

        if (!id) {            
            this.questionPromise = this.newQuestion();
            this.editingCard = true;
        } else {
            this.questionPromise = this.loadQuestion(id);
        }

    }

    newQuestion() {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.question = new Question();
        this.question.questionIDHistory = [];
        this.question.questionConsider = new QuestionConsider();
        this.question.questionConsider.passDate = undefined;
        this.question.questionConsider.consideration = Consideration.ACCEPTED;
        this.question.questionConsider.basis = new QuestionBasis();
        this.question.questionConsider.basis.date = undefined;
        this.question.questionConsider.planDate = undefined;
        this.question.questionConsider.meetingDate = undefined;
        this.question.questionConsider.materials = [];
        this.question.comments = [];
        this.loadingAppStatus = LoadingStatus.SUCCESS;
        deferred.resolve({
            question: this.question
        });
        return deferred.promise;
    }
    
    loadProtocol(question: Question): ng.IPromise<Protocol> {
        let deferred: ng.IDeferred<Protocol> = this.$q.defer();
        let meetingType: string = question.meetingType;
        let meetingNumber: string = question.questionConsider.meetingNumber;
        let meetingDate: Date = question.questionConsider.meetingDate;
        if (meetingNumber && meetingDate) {
            this.protocolResource.getByMeetingNumber({
                meetingType: meetingType,
                meetingNumber: meetingNumber,
                year: meetingDate.getFullYear()
            }, (wrapper: ProtocolDocumentWrapper) => {
                deferred.resolve(wrapper.document.protocol);
            });
        } else {
            deferred.resolve(null);
        }
        return deferred.promise;        
    }
    
    loadHistory(question: Question): ng.IPromise<Question[]> {
        let deferred: ng.IDeferred<Question[]> = this.$q.defer();
        let questionIDHistory = question.questionIDHistory || [];
        this.questionResource.getQuestionsIdsWithHistoryContaining({id: question.questionID}, (ids: string[]) => {
            if (ids) {
                questionIDHistory = questionIDHistory.concat(ids);
            }
            if (questionIDHistory.length > 0) {
                let promises: any = _.map(questionIDHistory, (id) => {
                    return this.questionResource.getById({ id: id }).$promise;
                });
                this.$q.all(promises).then((questionHistory: QuestionDocumentWrapper[]) => {
                    deferred.resolve(_.map(questionHistory, function(data: QuestionDocumentWrapper) {
                        return data.document.question;
                    }));
                });
            } else {
                deferred.resolve([]);
            }
        }, () => {
            deferred.reject();
        });

        return deferred.promise;        
    }
    
    loadQuestion(id: string) {
        this.loadingAppStatus = LoadingStatus.LOADING;
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.questionResource.getById({ id: id }, (data: QuestionDocumentWrapper) => {
            if (this.meetingTypesService.checkMeetingType(data.document.question.meetingType)) {
                this.question = data.document.question;
                this.showIndicatorsTab = this.question.meetingType === 'PZZ' ||
                    (this.question.meetingType.replace(/\s+/g,'') === 'SSMKA' &&
                    this.question.questionConsider.questionCategory.replace(/\s\s+/g,' ') == 'Рассмотрение вопроса для вынесения на ГК');

                this.$q.all([
                    this.loadHistory(this.question),
                    this.loadProtocol(this.question),
                    this.nsiRestService.get('OasiInstructionExecutors'),
                    this.meetingTypesService.getMeetingTypes(),
                    this.nsiRestService.get('pzzExternalAgree'),
                ]).then((result: any) => {
                    this.questionHistory = result[0];
                    this.protocol = result[1];
                    this.availableToAddInstruction = _.find(result[2], el => {
                        return el.creator == this.session.login() || (el.assistant && el.assistant.indexOf(this.session.login()) >= 0);
                    });

                    this.loadingAppStatus = LoadingStatus.SUCCESS;

                    deferred.resolve({
                        question: this.question,
                        questionHistory: this.questionHistory,
                        protocol: this.protocol
                    });

                    this.availableToCopy = _.find(result[3], (mt: MeetingType) => mt.meetingType === this.question.meetingType).CreatingCopies;

                    this.pzzExternalAgree = result[4];
                    this.externalAgreeCodes = _.map(this.question.questionConsider.externalAgree, (el) => el.organizationCode);

                })
            } else {
                deferred.reject();
                this.loadingAppStatus = LoadingStatus.ERROR;
                this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
            }
        });
        return deferred.promise;
    }

    copy() {
        this.questionResource.copy({ id: this.question.questionID }).$promise.then(({id}) => {
            this.toastr.success('Копия вопроса успешно создана');
            this.$state.go('app.ssmka.question', {id: id, editingCard: true});
        }).catch(() => {
            this.toastr.warning('Ошибка при копировании вопроса');
        })
    }

    private createQuestion(question: Question): ng.IPromise<string> {
        let deferred = this.$q.defer<string>();
        this.questionResource.create({
            document: {
                question: question
            }
        }, (data: QuestionCreateResult) => {
            deferred.resolve(data.id);
        });
        return deferred.promise;
    }

    private updateQuestion(question: Question): ng.IPromise<boolean> {
        let id: string = question.questionID;
        let deferred = this.$q.defer<boolean>();
        this.questionResource.update({ id: id }, {
            document: {
                question: question
            }
        }, function() {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    saveQuestion(question: Question): void {
        let self: QuestionController = this;
        let q: Question = angular.copy(this.question);
        _.extend(q, {
            Renovation: question.Renovation,
            meetingType: question.meetingType,
            meetingFullName: question.meetingFullName,
            meetingShortName: question.meetingShortName,
            questionConsider: question.questionConsider
        });
        if (q.questionID) {
            this.updateQuestion(q).then(function() {
                _.extend(self.question, q);
                self.editingCard = false;
                self.editingIndicators = false;
            });
        } else {
            this.createQuestion(q).then((id: string) => {
                self.$state.go('app.ssmka.question', {id: id, editingCard: false});
            });
        }
    }

    saveComment(comment: string, author: QuestionUserType): void {
        this.question.comments = this.question.comments || [];
        this.question.comments.push({
            date: new Date(),
            author: author,
            comment: comment
        });
        this.updateQuestion(this.question);

    }
    
    showAddCommentDialog(): void {
        if (this.authorizationService.check('OASI_MEETING_FORM_COMMENT')) {
            this.$uibModal.open({
                component: 'question-comment',
                size: 'lg'
            }).result.then((function(comment) {
                let user: oasiNsiRest.UserBean = this.getCurrentUser();
                this.saveComment(comment, this.userUtils.toQuestionUserType(user, user.departmentCode));
            }).bind(this));
        } else {
            this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
        }                
    }
    
    showEditCommentDialog(ind: number): void {
        let comment: QuestionComment = this.question.comments[ind];
        if (this.authorizationService.check('OASI_MEETING_FORM_COMMENT')) {
            this.$uibModal.open({
                component: 'question-comment',
                size: 'lg',
                resolve: {
                    comment: () => {
                        return comment.comment;
                    }
                }
            }).result.then((value) => {
                comment.comment = value;
                comment.date = new Date();
                comment.author = this.userUtils.toQuestionUserType(this.getCurrentUser());
                this.updateQuestion(this.question);                
            });
        } else {
            this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
        }                
    }

    getCurrentUser(): oasiNsiRest.UserBean {
        return {
            accountName: this.session.login(),
            displayName: this.session.fullName(),
            fioShort: this.session.fioShort(),
            iofShort: this.session.iofShort(),
            telephoneNumber: this.session.telephoneNumber(),
            mail: this.session.mail(),
            post: this.session.post(),
            departmentCode: this.session.departmentCode(),
            departmentFullName: this.session.departmentFullName(),
            authenticated: 'true',
            distinguishedName: null
        }
    }

    editCard(edit: boolean) {
        this.editingCard = edit;
    }

    editRG(edit: boolean) {
        this.editingRG = edit;
    }

    editIndicators(edit: boolean) {
        this.editingIndicators = edit;
    }

    cancelEditIndicators() {
        if (this.question.questionID) {
            this.editIndicators(false);
        } else {
            this.$state.go('app.ssmka.questions');
        }
    }
    
    cancelEditCard() {
        if (this.question.questionID) {
            this.editCard(false);
        } else {
            this.$state.go('app.ssmka.questions');
        }
    }

    previewExcerpt() {
        this.wordReporterPreviewService.nsi2Preview({
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать выписку",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: "Выписка по вопросу.docx",
                xwpfTemplateDocumentDescr: ''
            },
            jsonTxt: JSON.stringify({document: {question: this.question}}),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'ssSSMKAExcerpt'
            }
        });
    }

    saveQuestionPrimaryId(): ng.IPromise<any> {
        this.question.questionPrimaryID = this.question.questionID;
        return this.updateQuestion(this.question);
    }

    createInstruction() {
        const saveQuestionPrimaryId = this.question.questionPrimaryID ? this.$q.resolve() : this.saveQuestionPrimaryId();
        saveQuestionPrimaryId.then(() => {
            location.assign('/oasi/#/app/instruction/newFromSSMKA/' + this.question.questionID + '/' + this.question.questionPrimaryID);
        })
    }

    getCommentOrganization(comment) {
        let organizationCode = comment.author.organizationCode;
        if (organizationCode && this.externalAgreeCodes.indexOf(organizationCode) !== -1) {
            return _.find(this.pzzExternalAgree, (el) => el.code === organizationCode).shortName;
        }
        else {
            return comment.author.organization;
        }
    }

}

angular.module('app').component('question', new QuestionComponent());
