class QuestionAddMaterialsComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionAddMaterialsController;
        this.templateUrl = 'app/components/ssmka/question/question-add-materials.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class QuestionEditMaterialGroup {
    materialType: string;
    maxFiles: number;
    group: QuestionMaterialGroup;
}

class QuestionAddMaterialsController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = ['$timeout', '$q', '$scope', 'toastr', 'questionResource', 'fileResource'];
    
    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;
    
    private resolve: any;
    private questionID: string;
    private questionHistory: Question[];
    
    private materialTypes: any[];
    private dropzones: Dropzone[];
    private copies: any;
    
    constructor(private $timeout: any, private $q: ng.IQService, private $scope: any, private toastr: any, private questionResource: IQuestionResource,
        private fileResource: IFileResource) {
    }
    
    upload(dropzone: Dropzone) {
        let deferred: ng.IDeferred<void> = this.$q.defer<void>();
        if (dropzone.files.length > 0) {
            dropzone.on('queuecomplete', () => {
                deferred.resolve();
            });
            dropzone.processQueue();
        } else {
            deferred.resolve();
        }
        return deferred.promise;        
    }
    
    copy(copy: any) {
        let deferred: ng.IDeferred<void> = this.$q.defer<void>();
        if (copy.val) {
            this.questionResource.copyMaterials({
                id: this.questionID,
                fileGuid: copy.idFile
            }, function() {
                deferred.resolve();    
            })
        } else {
            deferred.resolve();
        }
        return deferred.promise;        
    }
    
    saveDropzoneAndQueue(dropzones: Dropzone[], ind: number, final: any) {
        if (ind < dropzones.length) {
            let dropzone: Dropzone = dropzones[ind];
            this.upload(dropzone).then(() => {
                this.saveDropzoneAndQueue(dropzones, ind + 1, final);
            });           
        } else {
            final();
        }
    }
    
    copyMaterialAndQueue(copies: any[], ind: number, final: any) {
        if (ind < copies.length) {
            let copy: any = copies[ind];
            this.copy(copy).then(() => {
                this.copyMaterialAndQueue(copies, ind + 1, final);
            });
        } else {
            final();
        }
    }

    save() {
        this.saveDropzoneAndQueue(this.dropzones, 0, () => {
            let copies: any[] = [];
            _.each(this.copies, (val, idFile) => {
                copies.push({
                    idFile: idFile,
                    val: val
                });
            });
            this.copyMaterialAndQueue(copies, 0, () => {
                this.close();
            })
        });
    }
    
    cancel() {
        this.dismiss();
    }
    
    $onInit() {
        let self: QuestionAddMaterialsController = this;
        this.dropzones = [];
        this.copies = {};
        this.questionID = this.resolve.question.questionID;
        this.questionHistory = this.resolve.questionHistory;
        this.materialTypes = this.resolve.materialTypes;
        this.$timeout(() => {
            _.each(this.materialTypes, (materialType, index) => {
                let id: string = 'edit-materials-' + index + '-dropzone';
                let options: any = {
                    autoProcessQueue: false,
                    withCredentials: true,
                    parallelUploads: 1,
                    paramName: "file",
                    url: this.questionResource.getUploadMaterialUrl(this.questionID, materialType.fileType),
                    init: function() {
                        this.on("error", function(file, errorMessage) {
                            self.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                            this.removeFile(file);
                        });
                        this.on("maxfilesexceeded", function(file, errorMessage) {
                            self.toastr.warning('Можно загрузить всего один файл.', 'Ошибка');
                            this.removeFile(file);
                        });
                        this.on("success", function() {
                            this.processQueue();
                        });
                    }
                };
                if (materialType.maxFiles) {
                    options = _.extend(options, { maxFiles: materialType.maxFiles });
                }                
                this.dropzones.push(new Dropzone("#" + id, options));                
            });
        });
    }
    
}

angular.module('app').component('questionAddMaterials', new QuestionAddMaterialsComponent());
