class QuestionExternalAgreeComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionExternalAgreeController;
        this.templateUrl = 'app/components/ssmka/question/question-external-agree.html';
        this.bindings = {
            questionPromise: "<"
        };
    }
}

class QuestionExternalAgreeController {
    static $inject = [
        '$q', 'nsiRestService'
    ];

    private questionPromise: ng.IPromise<any>;
    private question: Question;

    private pzzExternalAgree: { [key: string] : PzzExternalAgree };

    private loadingStatus: LoadingStatus;

    constructor(
        private $q: ng.IQService,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;

        this.questionPromise.then((data: any) => {
            this.question = data.question;
            return this.nsiRestService.get('pzzExternalAgree');
        }).then((pzzExternalAgree: PzzExternalAgree[]) => {
            this.pzzExternalAgree = {};
            _.forEach(pzzExternalAgree, (p) => {
                this.pzzExternalAgree[p.code] = p;
            })
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }

    getComment(externalAgree: OrgType): QuestionComment {
        return _.find(this.question.comments, (comment) => {
            return comment.author.organizationCode === externalAgree.organizationCode;
        });
    }

    getRepresentativeFioFull(externalAgree: OrgType) {
        let comment = this.getComment(externalAgree);
        return comment ? comment.author.fioFull : null;
    }

}

angular.module('app').component('questionExternalAgree', new QuestionExternalAgreeComponent());