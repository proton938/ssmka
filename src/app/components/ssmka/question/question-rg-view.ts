class QuestionRGViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionRGViewController;
        this.templateUrl = 'app/components/ssmka/question/question-rg-view.html';
        this.bindings = {
            questionRG: "<",
            edit: '&'
        };
    }
}

interface IQuestionRGViewComponentBindings {
    questionRG: QuestionRG;
    edit(val: any): any;
}

class QuestionRGViewController implements IQuestionRGViewComponentBindings {
    static $inject = [];

    public questionRG: QuestionRG;
    public edit: (val: any) => {};

    constructor() {
    }

    $onInit() {
    }

    editRG() {
        this.edit({ val: true });
    }

}

angular.module('app').component('questionRgView', new QuestionRGViewComponent());