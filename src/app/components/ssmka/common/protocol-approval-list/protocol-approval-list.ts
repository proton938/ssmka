class ProtocolApprovalListController {
    static $inject = [
        'approvalCycleUtils'
    ];

    private approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle;
    approvees: any[];

    constructor(private approvalCycleUtils: ApprovalCycleUtils) {
    }

    $onInit() {
        this.approvees = this.approvalCycleUtils.getSupervisors(this.approvalCycle).concat([
            this.approvalCycleUtils.getStroyUser(this.approvalCycle),
            this.approvalCycleUtils.getResponsibleSecretary(this.approvalCycle),
            this.approvalCycleUtils.getChairman(this.approvalCycle)
        ]);
    }

}

class ProtocolApprovalListComponent {
    public controller: any = ProtocolApprovalListController;
    public templateUrl: string = 'app/components/ssmka/common/protocol-approval-list/protocol-approval-list.html';
    public bindings: any = {
        approvalCycle: "<"
    };
}

angular.module('app').component('protocolApprovalList', new ProtocolApprovalListComponent());