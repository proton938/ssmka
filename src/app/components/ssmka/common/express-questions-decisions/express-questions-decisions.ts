class ExpressQuestionsDecisionsController {
    private protocol: Protocol;
    private editing: boolean;
}

class ExpressQuestionsDecisionsComponent {
    public controller: any = ExpressQuestionsDecisionsController;
    public templateUrl: string = 'app/components/ssmka/common/express-questions-decisions/express-questions-decisions.html';
    public bindings: any = {
        protocol: "=",
        editing: "<"
    };
}

angular.module('app').component('expressQuestionsDecisions', new ExpressQuestionsDecisionsComponent());