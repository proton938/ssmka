class QuestionsCommentsController {
    static $inject = [];

    private questions: ProtocolQuestion[];
    private decisions: Decision[];

    questionsDecisions: Dictionary<Decision>;

    constructor() {
    }

    $onInit() {
        this.questionsDecisions = {};
        this.questions.forEach(q => {
            this.questionsDecisions[q.questionNumber] = _.find(this.decisions, d => d.decisionTypeCode === q.decisionTypeCode);
        });
    }

    imageClass(code) {
        return {
            'fa-check-circle question-decision_approved': code === 'APPROVE',
            'fa-times-circle question-decision_declined': code === 'REJECT' || code === 'REWORK'
        };
    }
}

class QuestionsCommentsComponent {
    public controller: any = QuestionsCommentsController;
    public templateUrl: string = 'app/components/ssmka/common/questions-comments/questions-comments.html';
    public bindings: any = {
        questions: "<",
        decisions: "<"
    };
}

angular.module('app').component('questionsComments', new QuestionsCommentsComponent());
