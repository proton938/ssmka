class QuestionParametersController {
    static $inject = [
        '$uibModal',
        '$stateParams',
        'protocolResource'
    ];
    private questionParameters: QuestionParameters;
    private questionId: string;
    private editable: boolean;
    private parametersCollapse: boolean = false;
    constructor(
        private $uibModal: any, private $stateParams: any,
        private protocolResource: IProtocolResource
    ) {
    }
    haveParameters() {
        return !!this.questionParameters &&
            ( this.mainVriVisible() || this.relativelyVriVisible() || this.subVriVisible() || this.tepVisible() );
    }
    mainVriVisible() {
        return !!this.questionParameters.mainVRI && !!this.questionParameters.mainVRI.recommended && !!this.questionParameters.mainVRI.recommended.length;
    }
    relativelyVriVisible() {
        return !!this.questionParameters.relativelyVRI && !!this.questionParameters.relativelyVRI.recommended && !!this.questionParameters.relativelyVRI.recommended.length;
    }
    subVriVisible() {
        return !!this.questionParameters.subVRI &&
            !!this.questionParameters.subVRI.recommended &&
            !!this.questionParameters.relativelyVRI &&
            !!this.questionParameters.relativelyVRI.recommended.length ||
            (!!this.questionParameters.subVRI && !!this.questionParameters.subVRI.recommendedComment);
    }
    tepVisible() {
        return _.some(this.questionParameters.tep, tepParameter => !!tepParameter.recommendedValue);
    }
    parametersChevronClass() {
        return {
            'fa-chevron-down': !this.parametersCollapse,
            'fa-chevron-right': this.parametersCollapse
        };
    }
    chevronClickHandler() {
        this.parametersCollapse = !this.parametersCollapse;
    }
    addParametersHandler() {
        this.$uibModal.open({
            templateUrl: 'app/components/ssmka/my-tasks/protocol-and-conclusion/questionParametersForm.html',
            controller: 'QuestionParametersFormController as $ctrl',
            backdrop: 'static',
            size: 'md',
            resolve: {
                questionParameters: () => angular.copy(this.questionParameters)
            }
        }).result.then((result) => {
            this.questionParameters = {
                ...result
            };
            this.saveParameters();
        });
    }
    saveParameters() {
        this.protocolResource.questionUpdate({
            id: this.$stateParams.documentId,
            questionId: this.questionId
        }, this.questionParameters);
    }
    editButtonHandler() {
        this.addParametersHandler();
    }
}

class QuestionParametersComponent {
    public controller: any = QuestionParametersController;
    public templateUrl: string = 'app/components/ssmka/common/question-table-content/question-parameters.html';
    public bindings: any = {
        questionParameters: "=parameters",
        questionId: "<",
        editable: "<"
    };
}

angular.module('app').component('questionParameters', new QuestionParametersComponent());
