class QuestionTableContentController {
    static $inject = [];
    constructor() {
    }
}

class QuestionTableContentComponent {
    public controller: any = QuestionTableContentController;
    public templateUrl: string = 'app/components/ssmka/common/question-table-content/question-table-content.html';
    public bindings: any = {
        question: "<",
        addressLink: "<",
        showCategory: "<",
        showParameters: "<",
        editableParameters: "<"
    };
}

angular.module('app').component('questionTableContent', new QuestionTableContentComponent());
