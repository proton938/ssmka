class QuestionDecisionComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionDecisionController;
        this.templateUrl = 'app/components/ssmka/common/question-decision.html';
        this.bindings = {
            decision: '=',
            decisionCode: '=',
            small: '='
        };
    }
}

class QuestionDecisionController {
    static $inject = [];

    decisionCode: string;
    decisionClass: string;
    decisionIcon: string;

    constructor() {
    }

    $onInit() {
        this.decisionClass = this.getDecisionClass();
        this.decisionIcon = this.getDecisionIcon();
    }

    getDecisionClass() {
        switch(this.decisionCode) {
            case 'DOR-GZK':
            case 'TO-RGGZK':
            case 'TO-RGGZK2':
                return 'text-info';
            case 'OFORM':
            case 'DONE':
                return 'text-success';
            default:
                return 'text-default';
        }
    }

    getDecisionIcon() {
        switch(this.decisionCode) {
            case 'DOR-GZK':
            case 'TO-RGGZK':
            case 'TO-RGGZK2':
                return 'fa-arrow-up';
            case 'OFORM':
            case 'DONE':
                return 'fa-check';
            default:
                return 'fa-repeat';
        }
    }
}

angular.module('app').component('questionDecision', new QuestionDecisionComponent());
