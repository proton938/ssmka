class ProtocolAttendeeEditorController {
    static $inject = [
        '$uibModal',
        'nsiRestService'
    ];

    private attended: ProtocolAttended[];
    private organizations: ProtocolAttendeeEditorOrganization[];

    availOrganizations: ProtocolAttendeeEditorOrganization[];
    groups: AttendeeGroup[];


    internals: ProtocolAttended[] = [];
    searchingInternals: boolean = false;

    constructor(private $uibModal: any,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.groups = this.initGroups(this.attended);
    }

    refreshAvailOrganizations(query: string) {
        this.availOrganizations = this.organizations.filter(o => {
            return !_.some(this.groups, gr => gr.org != null && gr.org.code === o.code);
        }).filter(o => {
            return !o || o.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
        })
    }

    initGroups(attended: ProtocolAttended[]): AttendeeGroup[] {
        let mka = {
            org: _.find(this.organizations, o => o.code === 'MKA'),
            internal: true,
            participants: attended.filter(a => a.internal)
        };

        let external: AttendeeGroup[] = [];
        attended.filter(a => !a.internal).forEach(a => {
            let ext = _.find(external, e => e.org.code === a.code);
            if (!ext) {
                let org = _.find(this.organizations, o => o.code === a.code) || {code: a.code, name: a.name};
                ext = {org: org, internal: false, participants: []};
                external.push(ext);
            }
            ext.participants.push(a);
        });
        return [mka].concat(external);
    }

    addGroup() {
        this.groups.push({org: null, internal: false, participants: []});
    }

    deleteGroup(group: AttendeeGroup) {
        let index = this.groups.indexOf(group);
        this.groups.splice(index, 1);
    }

    orgChanged(group: AttendeeGroup) {
        group.participants.forEach(p => {
            p.code = group.org.code;
            p.name = group.org.name;
        })
    }

    addParticipant(group: AttendeeGroup) {
        this.$uibModal.open({
            component: 'add-external-attendee',
            backdrop: 'static',
            keyboard: false,
            size: 'lg'
        }).result.then((result: ProtocolAttended) => {
            if (group.org) {
                result.code = group.org.code;
                result.name = group.org.name;
            }
            group.participants.push(result);
            this.attended.push(result);
        });
    }

    deleteParticipant(group: AttendeeGroup, participant: ProtocolAttended) {
        let index = group.participants.indexOf(participant);
        group.participants.splice(index, 1);
        index = this.attended.indexOf(participant)
        this.attended.splice(index, 1);
    }

    searchInternals(query: string) {
        this.searchingInternals = true;
        this.nsiRestService.searchUsers({fio: query, department: ['MKA']}).then((users: oasiNsiRest.UserBean[]) => {
            this.internals = users.map(u => ProtocolAttended.fromUser(u));
            console.log(users);
            this.searchingInternals = false;
        }, () => {
            this.searchingInternals = false;
        });
    }

}

class ProtocolAttendeeEditorComponent {
    public controller: any = ProtocolAttendeeEditorController;
    public templateUrl: string = 'app/components/ssmka/common/protocol-attendee-editor/protocol-attendee-editor.html';
    public bindings: any = {
        attended: "=",
        organizations: '<'
    };
}

class AttendeeGroup {
    org: ProtocolAttendeeEditorOrganization;
    internal: boolean;
    participants: ProtocolAttended[]
}

class ProtocolAttendeeEditorOrganization {
    code: string;
    name: string;

    constructor(code: string, name: string) {
        this.code = code;
        this.name = name;
    }
}

angular.module('app').component('protocolAttendeeEditor', new ProtocolAttendeeEditorComponent());