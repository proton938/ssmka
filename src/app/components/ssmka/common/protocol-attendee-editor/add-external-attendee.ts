class AddExternalAttendeeController {
    static $inject = ['nsiRestService'];

    private modalInstance: angular.ui.bootstrap.IModalServiceInstance;

    private searchingUsers: boolean;
    private users: ProtocolAttended[] = [];

    private attendee: ProtocolAttended;
    private fioFull: String;
    private iofShort: String;

    constructor(private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
    }

    searchUsers(query: string) {
        this.searchingUsers = true;
        this.nsiRestService.searchUsers({fio: query}).then((users: oasiNsiRest.UserBean[]) => {
            this.users = users.map(u => ProtocolAttended.fromUser(u));
            this.searchingUsers = false;
        }, () => {
            this.searchingUsers = false;
        });
    }

    add() {
        if (this.attendee) {
            this.modalInstance.close(this.attendee);
        } else {
            let res = new ProtocolAttended();
            res.fioFull = this.fioFull;
            res.iofShort = this.iofShort;
            res.internal = false;
            this.modalInstance.close(res);
        }
    }

    cancel() {
        this.modalInstance.dismiss();
    }

}

class AddExternalAttendeeComponent {
    public controller: any = AddExternalAttendeeController;
    public templateUrl: string = 'app/components/ssmka/common/protocol-attendee-editor/add-external-attendee.html';
    public bindings: any = {
        modalInstance: '<',
        resolve: '<',
    };
}

angular.module('app').component('addExternalAttendee', new AddExternalAttendeeComponent());