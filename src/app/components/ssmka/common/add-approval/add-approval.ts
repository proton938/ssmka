class AddApprovalController {
    static $inject = [
        '$q',
        'nsiRestService'
    ];

    private approvers: User[];
    private searchingApprovers: boolean = false;
    private result: { approvalType: ApprovalType, user: User, comment: string }[] = [];

    private modalInstance: angular.ui.bootstrap.IModalServiceInstance;
    private resolve: any;

    private approvalCycle: ProtocolApprovalCycle;
    private exclude: string[];
    private showApprovalType: boolean;
    private approvalTypes: ApprovalType[];
    private showComment: boolean;

    constructor(private $q: ng.IQService,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.approvalCycle = this.resolve.approvalCycle;
        this.exclude = this.resolve.exclude;
        this.approvalTypes = this.resolve.approvalTypes;
        this.showApprovalType = this.approvalTypes && this.approvalTypes.length > 0;
        this.showComment = this.resolve.showComment;
    }

    addApprover() {
        this.result.push({approvalType: ApprovalType, user: new User(), comment: null});
    }

    onChangeApprover(index, item) {
        this.result[index].user = _.find(this.approvers, a => a.accountName === this.result[index].user.accountName);
    }

    canAdd() {
        return this.result.length && this.result.filter(u => {
            return !u.user.accountName || (this.showApprovalType && !u.approvalType.approvalTypeCode)
        }).length === 0;
    }

    add() {
        this.modalInstance.close(this.result);
    }

    cancel() {
        this.modalInstance.dismiss();
    }

    searchApprovers(query: string) {
        const params = {fio: query};
        this.searchingApprovers = true;
        this.nsiRestService.searchUsers(params).then((users: User[]) => {
            this.approvers = users.filter(a =>
                _.indexOf(this.exclude, a.accountName) === -1 && !_.some(this.result, u => u.accountName === a.accountName)
            );
            this.searchingApprovers = false;
        }, () => {
            this.searchingApprovers = false;
        });
    }

}

class AddApprovalComponent {
    public controller: any = AddApprovalController;
    public templateUrl: string = 'app/components/ssmka/common/add-approval/add-approval.html';
    public bindings: any = {
        modalInstance: '<',
        resolve: '<'
    };
}

angular.module('app').component('addApproval', new AddApprovalComponent());