class ExpressProtocolApprovalListController {
    static $inject = [
    ];

    private approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle;

    constructor() {
    }

    $onInit() {
    }

}

class ExpressProtocolApprovalListComponent {
    public controller: any = ExpressProtocolApprovalListController;
    public templateUrl: string = 'app/components/ssmka/common/express-protocol-approval-list/express-protocol-approval-list.html';
    public bindings: any = {
        approvalCycle: "<"
    };
}

angular.module('app').component('expressProtocolApprovalList', new ExpressProtocolApprovalListComponent());