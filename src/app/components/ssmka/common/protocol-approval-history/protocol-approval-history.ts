class ProtocolApprovalHistoryController {
    static $inject = [
        'approvalCycleUtils'
    ];

    private approvalCycle: ProtocolApprovalHistoryCycle[];
    currentCycle: number;

    constructor(private approvalCycleUtils: ApprovalCycleUtils) {
    }

    $onInit() {
    }

}

class ProtocolApprovalHistoryComponent {
    public controller: any = ProtocolApprovalHistoryController;
    public templateUrl: string = 'app/components/ssmka/common/protocol-approval-history/protocol-approval-history.html';
    public bindings: any = {
        approvalCycle: "<"
    };
}

angular.module('app').component('protocolApprovalHistory', new ProtocolApprovalHistoryComponent());