class ExpressProtocolApprovalHistoryController {
    static $inject = [
    ];

    private history: ProtocolApprovalHistoryCycle[];
    approvalHistoryListExpanded: _.Dictionary<boolean> = {};

    constructor() {
    }

    $onInit() {
    }

}

class ExpressProtocolApprovalHistoryComponent {
    public controller: any = ExpressProtocolApprovalHistoryController;
    public templateUrl: string = 'app/components/ssmka/common/express-protocol-approval-history/express-protocol-approval-history.html';
    public bindings: any = {
        history: "<"
    };
}

angular.module('app').component('expressProtocolApprovalHistory', new ExpressProtocolApprovalHistoryComponent());