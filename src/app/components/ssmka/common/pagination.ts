class PaginationController {
    static $inject = [];

    data: Pagination;
    onChange: () => void;

    constructor() {
    }

    onPaginationChange() {
        this.onChange();
    }
}

class PaginationComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {

        this.controller = PaginationController;
        this.templateUrl = 'app/components/ssmka/common/pagination.html';
        this.bindings = {
            data: "<",
            onChange: "&"
        };
    }
}

angular.module('app').component('pagination', new PaginationComponent()); 