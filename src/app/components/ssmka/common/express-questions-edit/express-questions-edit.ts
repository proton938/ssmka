class ExpressQuestionsEditController {
    static $inject = [
        'instructionsService',
        'alertService'
    ];

    private agenda: Agenda;
    private questions: Question[];

    private responsibles: QuestionResponsible[] = [];

    constructor(private instructionsService: IInstructionsService,
                private alertService: oasiWidgets.IAlertService) {
    }

    $onInit() {
        this.setQuestionsNumeration();
    }

    addQuestion() {
        this.questions.push(new Question());
        this.setQuestionsNumeration();
    }

    deleteQuestion(index: number) {

        let question: Question = this.questions[index];

        if (this.questions[index].questionID) {
            return this.instructionsService.getQuestionInstructions(question).then((response) => {
                if (response.numFound > 0) {
                    this.alertService.message({
                        message: 'Удалите поручения, созданные по данному вопросу',
                        type: 'warning'
                    }).then(() => {
                        this.questions.splice(index, 1);
                        this.setQuestionsNumeration();
                    });
                } else {
                    this.alertService.confirm({
                        message: 'Вы действительно хотите удалить данный вопрос?',
                        okButtonText: 'Ок',
                        type: 'warning'
                    }).then(() => {
                        this.questions.splice(index, 1);
                        this.setQuestionsNumeration();
                    });
                }
            });
        }

        this.questions.splice(index, 1);
        this.setQuestionsNumeration();
    }

    swapQuestions(ind1: number, ind2: number) {
        let question1 = this.questions[ind1];
        let question2 = this.questions[ind2];
        this.questions[ind1] = question2;
        this.questions[ind2] = question1;
        this.setQuestionsNumeration();
    }

    setQuestionsNumeration() {
        _.each(this.questions, (item, index) => {
            if (!item.questionConsider) {
                item.questionConsider = new QuestionConsider();
            }
            item.questionConsider.itemNumber = "" + (index + 1);
        });
    }

    updateResponsibles(index) {
        this.responsibles = this.agenda.attended ? this.agenda.attended.map(u => ({
            departmentPrepareFull: null,
            departmentPrepareShort: null,
            departmentPrepareCode: null,
            contractorPrepare: u.accountName,
            contractorPrepareFIO: u.fioFull,
            contractorPreparePhone: null
        })) : [];
        let responsiblePrepareExpress = this.questions[index].questionConsider.responsiblePrepareExpress;
        if (responsiblePrepareExpress) {
            this.responsibles = _.differenceWith(this.responsibles, responsiblePrepareExpress, (a, b) => a.contractorPrepare === b.contractorPrepare);
        }
    }

}

class ExpressQuestionsEditComponent {
    public controller: any = ExpressQuestionsEditController;
    public templateUrl: string = 'app/components/ssmka/common/express-questions-edit/express-questions-edit.html';
    public bindings: any = {
        questions: "=",
        agenda: '<'
    };
}

angular.module('app').component('expressQuestionsEdit', new ExpressQuestionsEditComponent());