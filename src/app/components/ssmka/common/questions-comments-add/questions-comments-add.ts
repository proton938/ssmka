class QuestionsCommentsAddController {
    static $inject = [];

    private questionsComments: QuestionCommentToAdd[];
    private questions: ProtocolQuestion[];

    constructor() {
    }

    $onInit() {
    }

    addComment() {
        this.questionsComments.push(new QuestionCommentToAdd(null));
    }

    removeComment(index) {
        this.questionsComments.splice(index, 1);
    }
}

class QuestionsCommentsAddComponent {
    public controller: any = QuestionsCommentsAddController;
    public templateUrl: string = 'app/components/ssmka/common/questions-comments-add/questions-comments-add.html';
    public bindings: any = {
        questionsComments: "=",
        questions: "<"
    };
}

class QuestionCommentToAdd {
    questionNumber: string;
    comment: string;

    constructor(questionNumber: string) {
        this.questionNumber = questionNumber;
        this.comment = '';
    }
}

angular.module('app').component('questionsCommentsAdd', new QuestionsCommentsAddComponent())
    .filter('notCommentedQuestions', [() => {
        return function (options: _.List<ProtocolQuestion>, comments: QuestionCommentToAdd[], questionNumber: string) {
            return _.filter(options, o => {
                return !_.some(comments, c => c.questionNumber === o.questionNumber) || o.questionNumber === questionNumber;
            });
        };
    }]);
