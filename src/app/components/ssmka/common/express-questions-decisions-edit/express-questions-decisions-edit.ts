class ExpressQuestionsDecisionsEditController {
    static $inject = [
    ];

    private questions: Question[];
    private decisions: string[];
    private beforeAddQuestion: () => ng.IPromise<any>;
    private beforeDeleteQuestion: (val: {question: Question}) => ng.IPromise<any>;
    private afterChangeQuestions: () => any;

    constructor() {
    }

    $onInit() {
    }

    addQuestion() {
        if (!this.beforeAddQuestion) {
            this._addQuestion();
            return;
        }
        this.beforeAddQuestion().then(() => this._addQuestion());
    }

    private _addQuestion() {
        this.questions.push(new Question());
        this.decisions.push("");
        if (this.afterChangeQuestions) {
            this.afterChangeQuestions();
        }
    }

    deleteQuestion(index: number) {
        if (!this.beforeDeleteQuestion) {
            this._deleteQuestion(index);
            return;
        }
        this.beforeDeleteQuestion({question: this.questions[index]}).then(() => this._deleteQuestion(index));
    }

    private _deleteQuestion(index: number) {
        this.questions.splice(index, 1);
        this.decisions.splice(index, 1);
        if (this.afterChangeQuestions) {
            this.afterChangeQuestions();
        }
    }

    swapQuestions(ind1: number, ind2: number) {
        this.swapArr(this.questions, ind1, ind2);
        this.swapArr(this.decisions, ind1, ind2);
    }

    swapArr<T>(arr: T[], ind1: number, ind2: number) {
        let el1 = arr[ind1], el2 = arr[ind2];
        arr[ind1] = el2;
        arr[ind2] = el1;
    }

}

class ExpressQuestionsDecisionsComponent {
    public controller: any = ExpressQuestionsDecisionsEditController;
    public templateUrl: string = 'app/components/ssmka/common/express-questions-decisions-edit/express-questions-decisions-edit.html';
    public bindings: any = {
        questions: "=",
        decisions: '=',
        beforeAddQuestion: '&',
        beforeDeleteQuestion: '&',
        afterChangeQuestions: '&',
    };
}

angular.module('app').component('expressQuestionsDecisionsEdit', new ExpressQuestionsDecisionsComponent());