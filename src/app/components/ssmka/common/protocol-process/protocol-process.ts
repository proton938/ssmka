class CardProcessController {
    static $inject = [
        '$log',
        '$state',
        '$timeout',
        'session',
        'activityProcessHistoryManager',
        'toastr'
    ];

    private protocolId: string;
    private meetingType: string;

    private init: oasiBpmHistory.IInfoAboutProcessInit;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    private admin: boolean;

    constructor(private $log: any,
                private $state: any,
                private $timeout: ng.ITimeoutService,
                private session: oasiSecurity.ISessionStorage,
                private activityProcessHistoryManager: oasiBpmHistory.IProcessHistoryManager,
                private toastr: Toastr) {
    }

    $onInit() {
        this.admin = this.session.permissions().some(p => p === 'PROTOCOL_PROCESS_EDIT');
        let categories: oasiBpmHistory.IProcessCategory[] = [];
        let key = this.meetingType === 'GK_EOO' ? 'gkoo' : 'ss';
        categories.push({
            name: "Электронное голосование",
            key: `${key}_`,
            compareType: oasiBpmHistory.CompareType.startsWith
        });
        this.activityProcessHistoryManager.init(key, categories, this.protocolId, "EntityIdVar").then(result => {
            this.init = result;
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(error => {
            this.$log.error(error);
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    isAdmin() {
        return this.admin;
    }

    finishTask(taskId: number, vars: any[], reqNum: string) {
        this.$timeout(1500).then(response => {
            return this.activityProcessHistoryManager.finishTask(taskId, vars)
        }).then(response => {
            this.toastr.success("Задача успешно завершена!");
            this.$state.reload();
        }).catch(error => {
            this.toastr.error("Ошибка при завершении задачи!");
            console.log(error);
        });
    }

    formFinishTask(taskId: string): ng.IPromise<any[]> {
        return this.activityProcessHistoryManager.formFinishTask(taskId);
    }

    formStartProcess(processId: string): ng.IPromise<any> {
        return this.activityProcessHistoryManager.formStartProcess(processId);
    }

    openDiagram(id: string, type: string, name: string) {
        let image = this.activityProcessHistoryManager.getDiagram(id, type);
        let url = this.$state.href('diagram', {title: name, image: image});
        window.open(url, '_blank');
    }

    deleteProcess(id: any, name: any, reqNum: any) {
        this.$timeout(1500).then(response => {
            return this.activityProcessHistoryManager.deleteProcess(id);
        }).then(response => {
            this.toastr.success("Процесс успешно удален!");
            this.$state.reload();
        }).catch(error => {
            this.toastr.error("Ошибка при удалении процесса!");
            console.log(error);
        });
    }

    startProcess(id: any, name: any, vars: any, reqNum: any) {
        this.$timeout(1500).then(response => {
            return this.activityProcessHistoryManager.startProcess(id, <any[]>vars);
        }).then(response => {
            this.toastr.success("Процесс успешно запущен!");
            this.$state.reload();
        }).catch(error => {
            this.toastr.error("Ошибка при запуске процесса!");
            console.log(error);
        });
    }
}

angular.module('app').component('protocolProcess', {
    controller: CardProcessController,
    templateUrl: 'app/components/ssmka/common/protocol-process/protocol-process.html',
    bindings: {
        protocolId: "<",
        meetingType: "<"
    }
});
