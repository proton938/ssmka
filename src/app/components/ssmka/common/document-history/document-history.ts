class DocumentHistoryController {
    static $inject = [
        'documentLogResource'
    ];

    private documentId: string;
    private history: oasiDocumentLogs.HistoryModel;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(private documentLogResource: IDocumentLogResource) {
    }

    $onInit() {
        this.documentLogResource.get({id: this.documentId}).$promise.then(data => {
            let logs: oasiDocumentLogs.HistoryLogModel[] = [];
            data.forEach(r => {
                let date = new Date(r.dateEdit);
                let ms = date.getTime();
                if (r.jsonPatch) {
                    r.jsonPatch.forEach(a => {
                        logs.push(new oasiDocumentLogs.HistoryLogModel(ms, r.userName, a.op, a.path, a.value));
                    });
                } else {
                    logs.push(new oasiDocumentLogs.HistoryLogModel(ms, r.userName, 'add', '/document', r.jsonNew));
                }
            });
            this.history = {
                logs: logs,
                allLogs: logs,
                sortings: oasiDocumentLogs.HistoryLogSorting.fillSortings(),
                filter: new oasiDocumentLogs.HistoryLogFilter(logs),
                pagination: new oasiDocumentLogs.HistoryLogPagination(logs.length)
            };
        }).then(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, error => {
            this.loadingStatus = LoadingStatus.ERROR;
            console.log(error);
        });
    }

}

angular.module('app').component('documentHistory', {
    controller: DocumentHistoryController,
    templateUrl: 'app/components/ssmka/common/document-history/document-history.html',
    bindings: {
        documentId: "<",
    }
});
