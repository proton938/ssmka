class AgendasPublishedComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendasPublishedController;
        this.templateUrl = 'app/components/ssmka/agendas/agendas-published.html';
        this.bindings = {};
    }
}

class SortingAll {
    static MEETING_HELD: string = 'meetingHeld';
    static MEETING_DATE: string = 'meetingDate';
    static MEETING_TYPE_ORDER: string = 'meetingTypeOrder';
    static MEETING_NUMBER: string = 'meetingNumber';
}

class AgendasPublishedController {
    static $inject = ['$q', 'agendaResource', 'meetingTypesService', 'ssAuthorizationService'];
    
    private hasViewPermission: boolean;

    private searchResult: SearchResult<Agenda>;
    private meetingTypes: { [key: string]: String };
    private questionsCounts: { [key: string]: number };
    private agendasChangeDates: { [key: string]: Date };
    private agendasChanges: { [key: string]: any };
    private loadingStatus: LoadingStatus;
    private loadingDataStatus: LoadingStatus;
    private viewExtSearch: boolean = false;
    private extSearchFilter: AgendaSearchFilter = new AgendaSearchFilter();
    
    private sortingAsc: boolean;
    private sorting: SortingAll = SortingAll.MEETING_DATE;
    private pagination: Pagination;
    private meetingTypesData: any[];

    constructor(private $q: any, private agendaResource: IAgendaResource, private meetingTypesService: IMeetingTypesService, private authorizationService: IAuthorizationService) {
    }
    
    reload() {

        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingDataStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            }
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);

        this.loadingDataStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_AGENDA');

        let sort = this.sorting;
        if (sort === SortingAll.MEETING_TYPE_ORDER) {
            sort = [
                `${SortingAll.MEETING_TYPE_ORDER},${this.sortingAsc ? 'asc' : 'desc'}`,
                `${SortingAll.MEETING_NUMBER},${this.sortingAsc ? 'desc' : 'asc'}`
            ]
        }
        else {
            sort += this.sortingAsc ? ',asc' : ',desc';
        }
        
        let params: any = {
            sort: sort,
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };

        let error = () => {
            this.loadingDataStatus = LoadingStatus.ERROR;
        };

        if (!this.extSearchFilter.meetingTypes) {
            this.extSearchFilter.meetingTypes = availableMeetingTypes;
        }

        this.agendaResource.searchPublished(params, this.extSearchFilter, (result: any) => {
            this.searchResult = {
                data: _.map(result.content, (wrapper: AgendaDocumentWrapper) => {
                    return wrapper.document.agenda;
                }),
                total: result.totalElements
            }
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            }   
            let requests: ng.IPromise<any>[] = [];
            _.each(this.searchResult.data, (agenda) => {
                requests.push(
                    this.agendaResource.includedQuestionsCount({
                        id: agenda.agendaID
                    }).$promise
                );
                requests.push(
                    this.agendaResource.changes({
                        id: agenda.agendaID
                    }).$promise
                );                
            })        
            this.$q.all(
                requests
            ).then((result: any) => {
                _.each(this.searchResult.data, (agenda, ind) => {
                    this.questionsCounts[agenda.agendaID] = result[2 * ind].includedTotal;
                    this.agendasChanges[agenda.agendaID] = {
                        added: 
                            _.chain(result[2 * ind + 1]).filter((question) => {
                                return question.status === AgendaQuestionStatus.ADDED;
                            }).map((question) => {
                                return question.itemNumber;
                            }).value(),
                        excluded: 
                            _.chain(result[2 * ind + 1]).filter((question) => {
                                return question.status === AgendaQuestionStatus.EXCLUDED;
                            }).map((question) => {
                                return question.itemNumber;
                            }).value()
                    };
                    this.agendasChangeDates[agenda.agendaID] = _.chain(result[2 * ind + 1]).map((question) => {
                        return question.dateChange;
                    }).max().value();
                });
                this.loadingDataStatus = LoadingStatus.SUCCESS;                
            }, error);
        }, error);
    }

    isSorted(field: SortingAll): boolean {
        return field === this.sorting;
    }
    
    toggleSorting(sorting: SortingAll) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }
        this.reload();
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.meetingTypes = {};
        this.questionsCounts = {};
        this.agendasChangeDates = {}; 
        this.agendasChanges = {};
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            _.each(meetingTypes, (meetingType) => {
                this.meetingTypes[meetingType.meetingType] = meetingType.meetingShortName;
            });
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.reload();
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }

    setFilter(filter: AgendaSearchFilter) {
        this.extSearchFilter = filter;
        this.reload();
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key];
        });
        this.reload();
    }

}

angular.module('app').component('agendasPublished', new AgendasPublishedComponent());
