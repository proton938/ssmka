class AgendasProjectsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendasProjectsController;
        this.templateUrl = 'app/components/ssmka/agendas/agendas-projects.html';
        this.bindings = {};
    }
}

class AgendasProjectsController {
    static $inject = ['$q', 'agendaResource', 'meetingTypesService', 'ssAuthorizationService'];
    
    private hasViewPermission: boolean;

    private searchResult: SearchResult<Agenda>;
    private meetingTypes: { [key: string]: String };
    private questionsCounts: { [key: string]: number };
    private loadingStatus: LoadingStatus;
    private loadingDataStatus: LoadingStatus;
    
    private sortingAsc: boolean;
    private pagination: Pagination;
    private meetingTypesData: any[];

    constructor(private $q: any, private agendaResource: IAgendaResource, private meetingTypesService: IMeetingTypesService, private authorizationService: IAuthorizationService) {
    }
    
    reload() {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingDataStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            };
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingDataStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_AGENDA');
        
        let params: any = {
            sort: 'meetingDate,' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize,
            meetingTypes: availableMeetingTypes
        };        
        let error = () => {
            this.loadingDataStatus = LoadingStatus.ERROR;
        };        
        this.agendaResource.searchProjects(params, (result: any) => {
            this.searchResult = {
                data: _.map(result.content, (wrapper: AgendaDocumentWrapper) => {
                    return wrapper.document.agenda;
                }),
                total: result.totalElements
            }
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                 
            }
            this.$q.all(
                _.map(this.searchResult.data, (agenda) => {
                    return this.agendaResource.includedQuestionsCount({
                        id: agenda.agendaID
                    }).$promise
                })
            ).then((result: any) => {
                _.each(this.searchResult.data, (agenda, ind) => {
                    this.questionsCounts[agenda.agendaID] = result[ind].includedTotal;
                });
                this.loadingDataStatus = LoadingStatus.SUCCESS;                
            }, error);            
        }, error);
    }
    
    toggleSorting() {
        this.sortingAsc = !this.sortingAsc;
        this.reload();
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.meetingTypes = {};        
        this.questionsCounts = {};
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            _.each(meetingTypes, (meetingType) => {
                this.meetingTypes[meetingType.meetingType] = meetingType.meetingShortName;
            });
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.reload();
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

}

angular.module('app').component('agendasProjects', new AgendasProjectsComponent());
