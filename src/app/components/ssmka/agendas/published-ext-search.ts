class PublishedExtSearchComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = PublishedExtSearchController;
        this.templateUrl = 'app/components/ssmka/agendas/published-ext-search.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
    }
}

class PublishedExtSearchController {
    static $inject = ['meetingTypesService'];

    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: AgendaSearchFilter;

    private loadingAppStatus: LoadingStatus;

    private meetingTypes: MeetingType[];
    private meetingHeldOptions: string[] = ['Планируется', 'Проведено'];

    constructor(private meetingTypesService: IMeetingTypesService) {
    }

    $onInit() {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypes = meetingTypes.filter((el) => !el.Express);
            this.loadingAppStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingAppStatus = LoadingStatus.ERROR;
        });
    }

    setFilter(extSearchFilter: AgendaSearchFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    apply() {
        let filter: any = _.clone(this.searchFilter);
        if (filter.meetingHeld) {
            filter.meetingHeld = filter.meetingHeld === 'Проведено';
        }
        if (filter.meetingTypes && filter.meetingTypes.length < 1) {
            delete filter.meetingTypes;
        }
        if (filter.meetingDate) {
            if (!filter.meetingDate.startDate) {
                delete filter.meetingDate.startDate;
            }
            if (!filter.meetingDate.endDate) {
                delete filter.meetingDate.endDate;
            }
        }
        this.applyFilter({
            value: filter
        });
    }

    cancel() {
        _.each(_.keys(this.searchFilter), (key) => {
            delete this.searchFilter[key];
        });
        this.clearFilter();
    }
}

angular.module('app').component('publishedExtSearch', new PublishedExtSearchComponent());
