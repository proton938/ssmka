class AgendaEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendaEditController;
        this.templateUrl = 'app/components/ssmka/agenda/agenda-edit.html';
        this.bindings = {
            agendaPromise: "<",
            edit: '&'
        };
    }
}

class AgendaEditController {
    static $inject = [
        '$timeout', '$state', '$scope', '$q', '$uibModal', 'session',
        'agendaResource', 'questionResource', 'meetingTypesService', 'nsiRestService',
        'dateUtils', 'toastr', 'ssAuthorizationService','alertService', 'mggtService',
        'AgendaQuestionStatus', 'MaterialType', 'protocolResource', 'MeetingType'
    ];

    private hasQuestionViewPermission: boolean;

    private loadingStatus: LoadingStatus;
    private loadingQuestionsStatus: LoadingStatus;

    private agendaPromise: ng.IPromise<Agenda>;
    private agenda: Agenda;
    private questions: { [key: string]: Question };
    private included: AgendaQuestion[];
    private includedCount: number;
    private candidates: string[];
    private restorables: { [key: string]: boolean };
    private edit: (val: any) => {};

    private meetingTypes: MeetingType[];
    private meetingPlaces: MeetingPlace[];

    private departments: any[];
    private internalAttendees: any[];
    private externalAttendees: any[];
    private updatingUsersListInProgress: boolean = false;
    private meetingAttendees: any[];

    private availableToCopy: any;

    constructor(
        private $timeout: any, private $state: any, private $scope: any, private $q: ng.IQService, private $uibModal: any, private session: oasiSecurity.ISessionStorage,
        private agendaResource: IAgendaResource, private questionResource: IQuestionResource, private meetingTypesService: IMeetingTypesService, private nsiRestService: oasiNsiRest.NsiRestService,
        private dateUtils: IDateUtils, private toastr: Toastr, private authorizationService: IAuthorizationService, private alertService: oasiWidgets.IAlertService,
        private mggtService: MggtService, private AgendaQuestionStatus: any, private MaterialType: any, private protocolResource: IProtocolResource, private MeetingType: any
    ) {
    }

    $onInit() {
        this.questions = {};
        this.restorables = {};
        this.hasQuestionViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
        this.agendaPromise.then((data: any) => {
            if (this.authorizationService.check('OASI_MEETING_FORM_AGENDA')) {
                this.agenda = angular.copy(data.agenda);
                this.loadDictionaries();
            } else {
                this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
                this.loadingStatus = LoadingStatus.ERROR;
            }
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDictionaries() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('MeetingPlace'),
            this.nsiRestService.departments(),
            this.nsiRestService.get('RolesParticipants')
        ]).then((values: any[]) => {
            this.meetingTypes = values[0].filter((el) => !el.Express);
            this.meetingPlaces = values[1];
            this.departments = values[2];

            this.meetingAttendees = values[3].filter((el) => {
                if (el.LevelRoleCode === 'organization') {
                    return el;
                }
            }).map((el) => {
                el.name = el.RoleName;
                el.code = el.RoleCode;
                el.role = {
                    roleCode: el.RoleCode,
                    roleName: el.RoleName
                };
                return el;
            });

            this.meetingTypeChanged();
            this.prepareAttendees();

            this.loadingStatus = LoadingStatus.SUCCESS;

        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    meetingTypeChanged() {
        if (this.agenda.meetingType && !this.agenda.agendaID) {
            let meetingType: MeetingType = _.find(this.meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.agenda.meetingType;
            });
            if (meetingType) {
                if (!this.agenda.meetingTime) {
                    this.agenda.meetingTime = meetingType.meetingTimeStart;
                }
                if (!this.agenda.meetingPlace) {
                    this.agenda.meetingPlace = meetingType.meetingPlace[0].name;
                }
                this.availableToCopy = meetingType.CreatingCopies;
            }
        }
        else if (this.agenda.meetingType && this.agenda.agendaID) {
            let meetingType: MeetingType = _.find(this.meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.agenda.meetingType;
            });
            if (meetingType) {
                this.availableToCopy = meetingType.CreatingCopies;
            }
        }
        this.loadQuestions(this.agenda.meetingType, this.agenda.meetingDate);
    }

    meetingDateChanged(meetingDate) {
        this.loadQuestions(this.agenda.meetingType, meetingDate);
    }

    loadIncluded(agendaID: string, meetingType: string, meetingDate: Date): ng.IPromise<any> {
        if (agendaID) {
            return this.agendaResource.includedQuestions({
                id: agendaID
            }).$promise.then(agendaQuestions => {
                this.included = agendaQuestions;
                return this.$q.all(agendaQuestions.map(aq => this.questionResource.getById({id: aq.questionId}).$promise))
            }).then(questions => {
                questions.forEach((q: QuestionDocumentWrapper) => this.questions[q.document.question.questionID] = q.document.question);
            });
        } else {
            return this.agendaResource.searchIncludedQuestions({
                meetingType: meetingType,
                meetingDate: this.dateUtils.formatDate(meetingDate)
            }).$promise.then(questions => {
                this.included = questions.map((q: QuestionDocumentWrapper) => {
                    let res = new AgendaQuestion();
                    res.questionId = q.document.question.questionID;
                    return res;
                });
                questions.forEach((q: QuestionDocumentWrapper) => this.questions[q.document.question.questionID] = q.document.question);
            });
        }
    }

    loadCandidates(agendaID: string, meetingType: string, meetingDate: Date): ng.IPromise<void> {
        if (agendaID) {
            return this.agendaResource.candidateQuestions({
                id: agendaID,
                meetingType: meetingType,
                meetingDate: this.dateUtils.formatDate(meetingDate)
            }).$promise.then(questions => {
                this.candidates = questions.map((q: QuestionDocumentWrapper) => q.document.question.questionID);
                questions.forEach((q: QuestionDocumentWrapper) => this.questions[q.document.question.questionID] = q.document.question);
            });
        } else {
            return this.agendaResource.searchCandidateQuestions({
                meetingType: meetingType,
                meetingDate: this.dateUtils.formatDate(meetingDate)
            }).$promise.then(questions => {
                this.candidates = questions.map((q: QuestionDocumentWrapper) => q.document.question.questionID);
                questions.forEach((q: QuestionDocumentWrapper) => this.questions[q.document.question.questionID] = q.document.question);
            });
        }
    }

    getIncludedCount(): ng.IPromise<number> {
        let deferred: ng.IDeferred<number> = this.$q.defer();
        if (this.agenda.agendaID) {
            this.agendaResource.includedQuestionsCount({
                id: this.agenda.agendaID
            }, (result) => {
                deferred.resolve(result.includedTotal);
            },() => {
                deferred.reject();
            })
        } else {
            deferred.resolve(this.included.length);
        }
        return deferred.promise;
    }

    loadQuestions(meetingType, meetingDate) {
        let error = () => {
            this.included = this.candidates = [];
            this.loadingQuestionsStatus = LoadingStatus.ERROR;
        };
        if (meetingType && meetingDate) {
            this.loadingQuestionsStatus = LoadingStatus.LOADING;
            let getIncluded = this.loadIncluded(this.agenda.agendaID, meetingType, meetingDate);
            let getCandidates = this.loadCandidates(this.agenda.agendaID, meetingType, meetingDate);
            this.$q.all([ getIncluded, getCandidates ]).then((result: any) => {
                return this.getIncludedCount();
            }, error).then((includedCount) => {
                this.includedCount = includedCount;
            }, error).then((result) => {
                let excludedQuestionsIds: string[] = _.chain(this.included).filter((question: AgendaQuestion) => {
                    return question.status === AgendaQuestionStatus.EXCLUDED;
                }).map((question: AgendaQuestion) => {
                    return question.questionId;
                }).value();
                return this.$q.all(
                    _.map(excludedQuestionsIds, (questionID: string) => {
                        return this.agendaResource.isRestorable({
                            id: this.agenda.agendaID,
                            questionId: questionID
                        }).$promise
                    })
                )
            }, error).then((result: any[]) => {
                this.restorables = {};
                _.each(result, (item: any) => {
                    _.extend(this.restorables, item);
                });
                this.loadingQuestionsStatus = LoadingStatus.SUCCESS;
            }, error);
        } else {
            this.loadingQuestionsStatus = LoadingStatus.SUCCESS;
        }
    }

    getMaterialGroup(question: Question, materialType: string) {
        return _.find(question.questionConsider.materials, (materialGroup) => {
            return materialGroup.materialType === materialType;
        });
    }

    getMaterialGroups(question: Question, materialTypes: string[]) {
        return _.find(question.questionConsider.materials, (materialGroup) => {
            return _.indexOf(materialTypes, materialGroup.materialType) >= 0;
        });
    }

    exclude(ind: number) {
        if (this.agenda.agendaPublished) {
            this.$uibModal.open({
                component: 'agenda-edit-question-reason',
                size: 'lg'
            }).result.then((reason) => {
                this.included[ind].status = AgendaQuestionStatus.EXCLUDED;
                this.included[ind].reasonChange = reason;
                this.included[ind].fio = this.session.fullName();
                this.included[ind].login = this.session.login();
                this.cleanFields(this.included[ind]);
                this.includedCount--;
            });
        } else {
            this.candidates.push(this.included[ind].questionId);
            this.included.splice(ind, 1);
            this.includedCount--;
        }
    }

    include(ind: number) {
        let question: AgendaQuestion = new AgendaQuestion();
        question.questionId = this.candidates[ind];
        if (this.agenda.agendaPublished) {
            this.$uibModal.open({
                component: 'agenda-edit-question-reason',
                size: 'lg'
            }).result.then((reason) => {
                question.status = AgendaQuestionStatus.ADDED;
                question.reasonChange = reason;
                question.fio = this.session.fullName();
                question.login = this.session.login();
                this.cleanFields(question);
                this.included.push(question);
                this.candidates.splice(ind, 1);
                this.includedCount++;
            });
        } else {
            this.included.push(question);
            this.candidates.splice(ind, 1);
            this.includedCount++;
        }
    }

    isUpdate(question: AgendaQuestion) {
        return question.status === AgendaQuestionStatus.ADDED || question.status === AgendaQuestionStatus.EXCLUDED;
    }

    isPrepared(questionId: string) {
        let question = this.questions[questionId];
        return this.hasMaterials(question, this.MaterialType.INQUIRIES) &&
            this.hasMaterial(question, this.MaterialType.PRESENTATION) && question.questionConsider.materialsChecked;
    }

    hasMaterial(question: Question, materialType: string) {
        return _.some(question.questionConsider.materials, (group: QuestionMaterialGroup) => {
            return group.materialType === materialType && group.document && group.document.length > 0;
        });
    }

    hasMaterials(question: Question, materialTypes: string[]) {
        return _.some(materialTypes, (materialType) => {
            return _.some(question.questionConsider.materials, (group: QuestionMaterialGroup) => {
                return group.materialType === materialType && group.document && group.document.length > 0;
            });
        });
    }

    canRestore(questionID: string): boolean {
        return this.restorables[questionID];
    }

    restore(ind: number) {
        this.$uibModal.open({
            component: 'agenda-edit-question-reason',
            size: 'lg'
        }).result.then((reason) => {
            this.included[ind].status = AgendaQuestionStatus.ADDED;
            this.included[ind].reasonChange = reason;
            this.included[ind].fio = this.session.fullName();
            this.included[ind].login = this.session.login();
            this.cleanFields(this.included[ind]);
            this.includedCount++;
        });
    }

    cleanFields(question: AgendaQuestion) {
        question.dateChange = null;
    }

    save(questionsToCopy?: string[], stayInEditMode?: boolean){
        this.saveAttendees();
        let a: Agenda = angular.copy(this.agenda);
        let newAgenda = !a.agendaID;
        let agendaId = a.agendaID;
        let promise: ng.IPromise<any> = newAgenda ? this.createAgenda(a, this.included, questionsToCopy) : this.updateAgenda(a, this.included, questionsToCopy);
        promise.then((result) => {
            if (newAgenda) {
                agendaId = result;
                this.agenda.agendaID = result;
            } else {
                _.extend(this.agenda, a);
            }
            return this.loadQuestions(this.agenda.meetingType, this.agenda.meetingDate);
        }).then(() => {
            if (!newAgenda && a.agendaPublished) {
                this.mggtService.sendAgenda(a, this.included.filter(q => q.status !== AgendaQuestionStatus.EXCLUDED).map(q => q.questionId));
            } else {
                return this.$q.resolve();
            }
        }).then(() => {
            if (newAgenda) {
                this.$state.go('app.ssmka.agenda', {id: agendaId, editing: !!stayInEditMode});
            } else {
                if (!stayInEditMode) {
                    this.edit({ val: false });
                }
            }
        });
    }

    private createAgenda(agenda: Agenda, questions: any[], questionsToCopy?: string[]): ng.IPromise<string> {
        let deferred = this.$q.defer<string>();
        this.agendaResource.create({
            agenda: {
                document: {
                    agenda: agenda
                }
            },
            questions: questions,
            questionsToCopy: questionsToCopy
        }, (data: AgendaCreateResult) => {
            deferred.resolve(data.id);
        });
        return deferred.promise;
    }

    private updateAgenda(agenda: Agenda, questions: any[], questionsToCopy?: string[]): ng.IPromise<boolean> {
        let id: string = agenda.agendaID;
        let deferred = this.$q.defer<boolean>();
        this.agendaResource.update({ id: id }, {
            agenda: {
                document: {
                    agenda: agenda
                }
            },
            questions: questions,
            questionsToCopy: questionsToCopy
        }, function() {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    publish() {
        // Проверка остается на бэкенде, но зависит от значения флага MeetingTypes.planningMeeting
        /* let emptyPrefect: boolean = _.some(this.included, (question: AgendaQuestion) => {
            return !this.questions[question.questionId].questionConsider.prefect;
        });
        if (emptyPrefect) {
            this.alertService.message({
                message: 'Для одного или более вопросов не указан округ'
            });
        } else {
            this.publishAgenda({
                id: this.agenda.agendaID
            });
        }*/

        this.agendaResource.publish({id: this.agenda.agendaID}).$promise.then(() => {
            this.mggtService.sendAgenda(this.agenda, this.included.map(q => q.questionId)).then(() => {

                if (this.agenda.meetingType === 'AS') {
                    this.agendaResource.createProtocol({id: this.agenda.agendaID}).$promise.then((response: ProtocolDocumentWrapper) => {
                        this.agenda.meetingNumber = response.document.protocol.meetingNumber;
                        this.agenda.meetingDate = response.document.protocol.meetingDate;
                        return this.protocolResource.create(response).$promise.then((response) => {
                            this.protocolResource.sendDecision({id: response.id});
                        });
                    }).then(() => {
                        this.$state.go('app.ssmka.agendas.published');
                    });
                } else {
                    this.$state.go('app.ssmka.agendas.published');
                }


            });
        });
    }

    cancelEdit() {
        this.edit({ val: false });
    }

    showCopyQuestionsDialog() {
        this.agendaResource.getLastHeldQuestion({
            meetingType: this.agenda.meetingType,
            planDate: this.dateUtils.formatDate(this.agenda.meetingDate)
        }).$promise.then(questions => {
            return this.$uibModal.open({
                component: 'agenda-edit-copy-questions-dialog',
                size: "md",
                resolve: {
                    questions: () => {
                        return questions;
                    }
                }
            }).result;
        }).then((result: LastHeldQuestion[]) => {
            const questionsToCopy = _.map(result, q => q.questionId);
            return this.save(questionsToCopy, true);
        });
    }

    prepareAttendees() {

        this.internalAttendees = [];
        this.externalAttendees = [];

        if (this.agenda.attended && this.agenda.attended.length) {

            this.agenda.attended.map((el) => {
                if (el.internal && el.internal === true) {
                    this.internalAttendees.push({result: el, department: el.name});
                }
                else {
                    this.externalAttendees.push(el);
                }
            });

            if (!this.internalAttendees.length) {
                this.internalAttendees.push({});
            }

            if (!this.externalAttendees.length && !this.agenda.agendaID) {
                this.externalAttendees = this.meetingAttendees;
            }

        }
        else {
            this.internalAttendees.push({});
            if (!this.agenda.agendaID) {
                this.externalAttendees = this.meetingAttendees;
            }
        }

    }

    departmentsChange(department, index) {

        this.updatingUsersListInProgress = true;
        this.internalAttendees[index].users = [];

        this.nsiRestService.ldapUsers(null, department.description).then((users: any) => {
            this.internalAttendees[index].users = users;
            this.internalAttendees[index].result = {};
            this.updatingUsersListInProgress = false;
        });

    }

    internalAttendeeChange(user, index) {
        user.name = user.departmentFullName;
        user.code = user.departmentCode;
        user.internal = true;
        user.fioFull = user.displayName;
        this.internalAttendees[index].result = user;
    }

    addInternalAttendee() {

        this.internalAttendees.push({});

    }

    removeInternalAttendee(index) {

        if (this.internalAttendees.length === 1) {
            this.internalAttendees[0] = {};
            return;
        }

        this.internalAttendees.splice(index, 1);

    }

    saveAttendees() {

        this.agenda.attended = [];

        let internalAttendees = this.internalAttendees.filter((el) => {
            if (el.result) return el;
        }).map((el) => {
            return el.result;
        });

        this.agenda.attended = this.agenda.attended.concat(internalAttendees).concat(this.externalAttendees);

    }

    clearAttendees() {
        this.externalAttendees = [];
    }

}

angular.module('app').component('agendaEdit', new AgendaEditComponent());
