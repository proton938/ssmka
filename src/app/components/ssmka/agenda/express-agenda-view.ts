class ExpressAgendaViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ExpressAgendaViewController;
        this.templateUrl = 'app/components/ssmka/agenda/express-agenda-view.html';
        this.bindings = {
            agendaPromise: "<",
            reload: '&'
        };
    }
}

class ExpressAgendaViewController {
    static $inject = [
        '$q',
        '$timeout',
        '$state',
        '$stateParams',
        '$window',
        'agendaResource',
        'questionResource',
        'expressResource',
        'protocolResource',
        'meetingTypesService',
        'nsiRestService',
        'session',
        'instructionsService',
        'mggtService',
        'wordReporterPreviewService',
        'toastr',
        'protocolTaskService',
        'activityRestService'
    ];

    private activeTab: any;

    private availableToAddInstruction: boolean;
    private sendingMailNotifications: boolean;
    private creatingProtocolPrintForm: boolean;
    private creatingProtocol: boolean;
    private markingMeetingPassed: boolean;
    private savingProtocol: boolean;

    private loadingStatus: LoadingStatus;

    private agendaPromise: ng.IPromise<Agenda>;
    private agenda: Agenda;
    private protocol: Protocol;

    private editingProtocol: boolean = false;

    private meetingType: MeetingType;
    private included: AgendaQuestion[];
    private questions: { [key: string]: Question };
    private includedCount: number;
    private filesFolderGuid: string;

    private instructions: { question: Question, instructions: oasiSolarRest.SearchResultDocument[] }[];
    private statuses: any[];
    private availableToCopy: boolean = false;
    private showJsonEditor: boolean = false;
    private showHistory: boolean = false;

    public reload: () => ng.IPromise<any>;

    public canCreateProtocol: boolean;
    public canCreateProtocolWithProcess: boolean;

    public hasProcessHistory: boolean;

    constructor(private $q: ng.IQService,
                private $timeout: any,
                private $state: any,
                private $stateParams: any,
                private $window: any,
                private agendaResource: IAgendaResource,
                private questionResource: IQuestionResource,
                private expressResource: IExpressResource,
                private protocolResource: IProtocolResource,
                private meetingTypesService: IMeetingTypesService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private instructionsService: IInstructionsService,
                private mggtService: MggtService,
                private wordReporterPreviewService: oasiWordreporterRest.WordReporterPreviewService, 
                private toastr: Toastr,
                private protocolTaskService: IProtocolTaskService,
                private activityRestService: oasiBpmRest.ActivityRestService) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };
        this.questions = {};
        const groups = this.session.groups();
        const permissions = this.session.permissions();
        this.showJsonEditor = groups.some(gr => gr === 'OASI_PROTOCOL_ADMIN');
        this.showHistory = groups.some(gr => gr === 'OASI_PROTOCOL_ADMIN');

        if (this.$stateParams.activeTab) {
            this.activeTab = this.$stateParams.activeTab;
        }

        this.agendaPromise.then((data: any) => {
            this.agenda = data.agenda;
            this.$q.all([
                this.loadDictionaries(),
                this.loadIncludedQuestions(),
                this.loadProtocol()
            ]).then(() => {
                return this.loadInstructions();
            }).then(() => {
                return this.getProcessHistory();
            }).then((result: any[]) => {
                this.hasProcessHistory = result.length > 0;
                this.canCreateProtocol = permissions.some(p => p === 'OASI_PROTOCOL_CREATE');
                this.canCreateProtocolWithProcess = permissions.some(p => p === 'OASI_PROTOCOL_PROCESS_CREATE');
                this.loadingStatus = LoadingStatus.SUCCESS;
            }, error);
        }, error);
    }

    loadProtocol(): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.protocolResource.getByAgendaId({
            agendaId: this.agenda.agendaID
        }, (wrapper: ProtocolDocumentWrapper) => {
            if (wrapper && wrapper.document) {
                this.protocol = wrapper.document.protocol;
            }
            deferred.resolve();
        }, () => {
            deferred.resolve()
        });
        return deferred.promise;
    }

    getProcessHistory(): ng.IPromise<any[]> {
        if (!this.protocol) {
            return this.$q.resolve([]);
        }
        return this.activityRestService.getProcessHistoryByReqNum(this.protocol.protocolID, 'EntityIdVar').then(result => {
            return result.data;
        });
    }

    loadDictionaries(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('OasiInstructionExecutors'),
            this.nsiRestService.get('OasiInstructionStatus'),
            this.nsiRestService.get('MeetingFilenetSettings')
        ]).then((result: any) => {
            let meetingTypes: MeetingType[] = result[0];
            this.meetingType = _.find(meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.agenda.meetingType;
            });
            this.availableToAddInstruction = _.find(result[1], (el: any) => {
                return el.creator == this.session.login() || (el.assistant && el.assistant.indexOf(this.session.login()) >= 0);
            });
            this.availableToCopy = this.meetingType.CreatingCopies || false;
            this.statuses = result[2];
            this.filesFolderGuid = result[3].find(f => f.meetingType === this.agenda.meetingType).agendaFolderGuid;
            deferred.resolve();
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    loadIncludedQuestions(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.$q.all([
            this.agendaResource.includedQuestions({ id: this.agenda.agendaID }).$promise,
            this.agendaResource.includedQuestionsCount({ id: this.agenda.agendaID }).$promise
        ]).then((result: any) => {

            this.included = result[0].filter(question => question.status !== AgendaQuestionStatus.EXCLUDED);
            this.$q.all(
                _.map(this.included, (question: AgendaQuestion) => {
                    return this.questionResource.getById({
                        id: question.questionId
                    }).$promise
                })
            ).then((result) => {
                _.each(result, (wrapper: QuestionDocumentWrapper) => {
                    let question: Question = wrapper.document.question;
                    this.questions[question.questionID] = question;
                });
                deferred.resolve();
            }, () => {
                deferred.reject();
            });

            this.includedCount = result[1].includedTotal;

        }, () => {
            deferred.reject()
        });
        return deferred.promise;
    }

    loadInstructions() : ng.IPromise<any> {
        const primaryQuestionsIds = _.values(this.questions).map(q => q.questionPrimaryID);
        if (!primaryQuestionsIds || !primaryQuestionsIds.length) {
            return this.$q.resolve({docs: []});
        }

        let solrFieldsFilter = [{
            name: 'questionPrimaryID',
            value: primaryQuestionsIds
        }];

        let questionMap: {[key: string] : Question} = {};
        _.values(this.questions).forEach(q => questionMap[q.questionPrimaryID] = q);

        let date = new Date(this.agenda.meetingDate.getTime());
        date.setDate(date.getDate() + 1);
        return this.instructionsService.getAgendaInstructions(this.agenda, this.meetingType, primaryQuestionsIds).then((result: oasiSolarRest.SearchResult) => {
            this.instructions = _.chain(result.docs).groupBy(doc => doc.questionPrimaryID).toPairs().map((el: any) => {
                return {
                    question: questionMap[el[0]],
                    instructions: el[1]
                };
            }).value();
            this.loadingStatus = LoadingStatus.SUCCESS;
        });
    }

    edit() {
        this.$state.go('app.ssmka.express-agenda-edit', { id: this.agenda.agendaID });
    }

    copyAgenda(agenda: Agenda, questions: { [key: string]: Question }): ng.IPromise<any> {
        let deferred = this.$q.defer<any>();
        const copiedQuestions = Object.keys(questions).map(key => {
            const question = questions[key];
            return {
                questionConsider: question.questionConsider,
                questionPrimaryID: question.questionPrimaryID
            };
        });
        const copiedAgenda = angular.copy(agenda);
        delete copiedAgenda.agendaID;
        delete copiedAgenda.agendaPublished;
        delete copiedAgenda.authorCreate;
        delete copiedAgenda.meetingNumber;
        delete copiedAgenda.meetingDate;
        copiedAgenda.filesAttach = [];

        const payload = {
            document: {
                agenda: copiedAgenda
            },
            questions: copiedQuestions
        };
        deferred.resolve(payload);
        return deferred.promise;
    }

    copy() {
        this.copyAgenda(this.agenda, this.questions).then(payload => {
            this.$state.go('app.ssmka.express-agenda-edit', {payload, copy: true});
        }).catch(() => {
            this.toastr.warning('Ошибка при копировании совещания');
        });
    }

    mailNotification() {
        this.sendingMailNotifications = true;
        this.agendaResource.mailNotification({id: this.agenda.agendaID}).$promise.then(() => {
            this.sendingMailNotifications = false;
        }).catch(() => {
            this.sendingMailNotifications = false;
        });
    }


    meetingHeld() {
        this.markingMeetingPassed = true;
        this.expressResource.markPassed({id: this.agenda.agendaID}).$promise.then(() => {
            return this.reload();
        }).then((data: any) => {
            this.agenda = data.agenda;
            this.loadIncludedQuestions();
        }).then(() => {
            return this.mggtService.sendAgenda(this.agenda, this.included.map(q => q.questionId));
        }).then(() => {
            this.markingMeetingPassed = false;
        }).catch(() => {
            this.markingMeetingPassed = false;
        })
    }

    createProtocol(startProcess: boolean) {
        this.creatingProtocol = true;
        this.expressResource.generateProtocol({
            id: this.agenda.agendaID
        }).$promise.then(() => {
            return this.reload();
        }).then((data: any) => {
            this.agenda = data.agenda;
            return this.loadProtocol()
        }).then(() => {
            if (startProcess) {
                return this.protocolTaskService.getProcessDefinition('ss_approvalProtocolDraft').then(process => {
                    let properties = this.protocolTaskService.getInitProcessProperties(this.protocol, this.session.login());
                    return this.protocolTaskService.initProcess(process.id, properties);
                })
            } else {
                return this.mggtService.sendProtocol(this.protocol, this.agenda);
            }
        }).then(() => {
            this.$state.go('app.ssmka.agenda', {id: this.agenda.agendaID, activeTab: 2}, {reload: true});
        }).catch(() => {
            this.creatingProtocol = false;
        })
    }

    createProtocolPrintForm() {
        this.creatingProtocolPrintForm = true;
        this.protocolResource.createReport({
            id: this.protocol.protocolID
        }).$promise.then((file: FileType) => {
            this.protocol.fileProtocol = this.protocol.fileProtocol || [];
            this.protocol.fileProtocol.push(file)
            this.creatingProtocolPrintForm = false;
        }).catch(() => {
            this.creatingProtocolPrintForm = false;
        });
    }

    editProtocol() {
        this.editingProtocol = true;
    }

    saveProtocol() {
        this.savingProtocol = true;
        this.protocolResource.update({
            id: this.protocol.protocolID
        }, {document: { protocol: this.protocol }}).$promise.then(() => {
            this.loadProtocol();
        }).then(() => {
            return this.mggtService.sendProtocol(this.protocol, this.agenda);
        }).then(() => {
            this.savingProtocol = false;
            this.editingProtocol = false;
        })
    }

    createInstruction(questionId: string) {
        const question = this.questions[questionId];
        const saveQuestionPrimaryId = question.questionPrimaryID ? this.$q.resolve() : this.saveQuestionPrimaryId(question);
        saveQuestionPrimaryId.then(() => {
            const url = this.$window.location.protocol + '//' + this.$window.location.host +
                '/oasi/#/app/instruction/newFromSSMKA/' + questionId + '/' + question.questionPrimaryID +
                '?expressMeetingID=' + this.agenda.agendaID;
            this.$window.open(url, '_blank');
        })
    }

    saveQuestionPrimaryId(question: Question): ng.IPromise<any> {
        question.questionPrimaryID = question.questionID;
        let id: string = question.questionID;
        let deferred = this.$q.defer<boolean>();
        this.questionResource.update({ id: id }, {
            document: {
                question: question
            }
        }, function() {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    createProtocolWithInstructionsPrintForm() {
        const json = this.instructionsService.getProtocolWithInstructions(this.protocol, this.meetingType, this.instructions.map(instr => {
            return {
                questionId: instr.question.questionID,
                instructions: instr.instructions
            }
        }));
        this.wordReporterPreviewService.nsi2Preview({
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать протокол с поручениями",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: "Протокол с поручениями.docx",
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({
                document: {
                    protocol: json
                }
            }),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'ssProtocolInstr'
            }
        });
    }

}

angular.module('app').component('expressAgendaView', new ExpressAgendaViewComponent());
