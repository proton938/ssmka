class AgendaViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendaViewController;
        this.templateUrl = 'app/components/ssmka/agenda/agenda-view.html';
        this.bindings = {
            agendaPromise: "<",
            edit: '&',
            reload: '&'
        };
    }
}

class AgendaViewController {
    static $inject = [
        '$q', '$uibModal', '$state',
        '$stateParams', 'fileResource', 'agendaResource',
        'questionResource', 'meetingTypesService', 'wordReporterPreviewService',
        'toastr', 'ssAuthorizationService',
        'AgendaQuestionStatus', 'MaterialType', 'MeetingType',
        'protocolResource', 'protocolTaskService',
        'session', 'activityRestService', 'dateFilter', 'nsiRestService'];

    private hasQuestionViewPermission: boolean;

    private loadingStatus: LoadingStatus;

    private agendaPromise: ng.IPromise<Agenda>;
    private agenda: Agenda;
    private fileAgenda: FileType;
    private meetingType: MeetingType;
    private included: AgendaQuestion[];
    private showChanges: boolean;
    private includedCount: number;
    private added: string[];
    private excluded: string[];
    private lastChangeDate: Date;
    private questions: { [key: string]: Question };
    public edit: (val: any) => {};
    public reload: () => ng.IPromise<any>;
    private pzzExternalAgree: { [key: string]: PzzExternalAgree };
    private externalAgree: any;
    private availableToCopy: boolean = false;

    constructor(private $q: ng.IQService, private $uibModal: any, private $state: any,
                private $stateParams: any, private fileResource: IFileResource,
                private agendaResource: IAgendaResource, private questionResource: IQuestionResource,
                private meetingTypesService: IMeetingTypesService, private wordReporterPreviewService: oasiWordreporterRest.WordReporterPreviewService,
                private toastr: Toastr,
                private authorizationService: IAuthorizationService, private AgendaQuestionStatus: any,
                private MaterialType: any, private MeetingType: any,
                private protocolResource: IProtocolResource, private protocolTaskService: IProtocolTaskService,
                private session: oasiSecurity.ISessionStorage, public activityRestService: oasiBpmRest.ActivityRestService, private dateFilter: any,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;
        this.hasQuestionViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };
        this.questions = {};
        this.showChanges = true;
        this.agendaPromise.then((data: any) => {
            this.agenda = data.agenda;
            this.$q.all([
                this.loadDictionaries(),
                this.loadIncludedQuestions()
            ]).then((result: any) => {
                this.loadingStatus = LoadingStatus.SUCCESS;

                _.forEach(this.questions, (question) => {
                    _.forEach(question.questionConsider.externalAgree, (externalAgree) => {
                        let hasComment = _.find(question.comments, {'author': {'organizationCode': externalAgree.organizationCode}});
                        externalAgree.hasComment = hasComment ? true : false;
                    });
                });

            }, error);
        }, error);
    }

    loadDictionaries(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('pzzExternalAgree')
        ]).then((result: any) => {
            let meetingTypes: MeetingType[] = result[0];
            this.meetingType = _.find(meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.agenda.meetingType;
            });
            this.availableToCopy = this.meetingType.CreatingCopies || false;

            this.pzzExternalAgree = {};
            _.forEach(result[1], (p) => {
                this.pzzExternalAgree[p.code] = p;
            });

            deferred.resolve();
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    loadIncludedQuestions(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.$q.all([
            this.agendaResource.includedQuestions({id: this.agenda.agendaID}).$promise,
            this.agendaResource.includedQuestionsCount({id: this.agenda.agendaID}).$promise
        ]).then((result: any) => {

            this.included = result[0];
            this.$q.all(
                _.map(this.included, (question: AgendaQuestion) => {
                    return this.questionResource.getById({
                        id: question.questionId
                    }).$promise
                })
            ).then((result) => {
                _.each(result, (wrapper: QuestionDocumentWrapper) => {
                    let question: Question = wrapper.document.question;
                    this.questions[question.questionID] = question;
                });
                deferred.resolve();
            }, () => {
                deferred.reject();
            });
            this.lastChangeDate = _.chain(this.included).map((question) => {
                return question.dateChange;
            }).max().value();
            this.added = _.chain(this.included).filter((question) => {
                return question.status === AgendaQuestionStatus.ADDED;
            }).map((question) => {
                return question.itemNumber;
            }).value();
            this.excluded = _.chain(this.included).filter((question) => {
                return question.status === AgendaQuestionStatus.EXCLUDED;
            }).map((question) => {
                return question.itemNumber;
            }).value();

            this.includedCount = result[1].includedTotal;

        }, () => {
            deferred.reject()
        });
        return deferred.promise;
    }

    createReport() {
        this.agendaResource.createReport({}, {
            id: this.agenda.agendaID
        }, () => {
            this.reload().then((data: any) => {
                this.agenda = data.agenda;
            });
        });
    }

    getMaterialGroup(question: Question, materialType: string) {
        return _.find(question.questionConsider.materials, (materialGroup) => {
            return materialGroup.materialType === materialType;
        });
    }

    getMaterialGroups(question: Question, materialTypes: string[]) {
        return _.find(question.questionConsider.materials, (materialGroup) => {
            return _.indexOf(materialTypes, materialGroup.materialType) >= 0;
        });
    }

    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }

    previewPzzApproval() {
        let deferred = this.$q.defer();
        this.$q.all([
            this.agendaResource.getByIdFull({id: this.agenda.agendaID}).$promise,
        ]).then((result: any) => {
            let agendaFull = result[0];
            let fileName = 'Лист согласования ПЗЗ на ' + this.dateFilter(this.agenda.meetingDate, 'dd.MM.yyyy') + '.docx';
            this.wordReporterPreviewService.nsi2Preview({
                options: {
                    jsonSourceDescr: "",
                    onProcess: "Сформировать лист согласования",
                    placeholderCode: "",
                    strictCheckMode: true,
                    xwpfResultDocumentDescr: fileName,
                    xwpfTemplateDocumentDescr: ""
                },
                jsonTxt: JSON.stringify(agendaFull),
                rootJsonPath: "$",
                nsiTemplate: {
                    templateCode: 'ssPZZApproval'
                }
            });

            deferred.resolve();
        }, () => {
            deferred.reject()
        });
        return deferred.promise;
    }

    editAgenda() {
        this.edit({val: true});
    }

    copyAgenda(agenda: Agenda, questions: { [key: string]: Question }): ng.IPromise<string> {
        let deferred = this.$q.defer<string>();
        const copiedQuestions = Object.keys(questions).map(key => {
            const question = questions[key];
            return {
                questionConsider: question.questionConsider
            };
        });
        const copiedAgenda = angular.copy(agenda);
        delete copiedAgenda.agendaID;
        delete copiedAgenda.agendaPublished;
        // delete copiedAgenda.attended;
        delete copiedAgenda.authorCreate;
        delete copiedAgenda.meetingNumber;
        delete copiedAgenda.meetingDate;

        this.agendaResource.create({
            agenda: {
                document: {
                    agenda: copiedAgenda
                }
            },
            questions: copiedQuestions
        }, (data: AgendaCreateResult) => {
            deferred.resolve(data.id);
        });
        return deferred.promise;
    }

    copy() {
        this.copyAgenda(this.agenda, this.questions).then(id => {
            this.$state.go('app.ssmka.express-agenda-edit', {id});
        }).catch(() => {
            this.toastr.warning('Ошибка при копировании совещания');
        });
    }

    hideChanges() {
        this.showChanges = false;
    }

    createProtocol() {
        this.$state.go('app.ssmka.protocol', {
            agendaID: this.agenda.agendaID,
            editing: true
        });
    }

    renumberQuestions() {
        this.agendaResource.renumberQuestions({}, {
            id: this.agenda.agendaID
        }, () => {
            this.loadIncludedQuestions();
        });
    }

    createGkVoting() {
        this.createVoting(
            (agendaId) => this.agendaResource.generateGkVoting({}, {id: agendaId}).$promise,
            'ss_approval_gk',
            (protocol) => this.protocolTaskService.getInitProcessProperties(protocol, this.session.login()),
            'app.ssmka.process.gkPzzRegisterSS'
        );
    }

    createGkEOOVoting() {
        this.createVoting(
            (agendaId) => this.agendaResource.generateGkVoting({}, {id: agendaId}).$promise,
            'gkoo_voting',
            (protocol) => this.protocolTaskService.getInitProcessProperties(protocol, this.session.login()),
            'app.ssmka.process.gkEooRegMembersAddQuestions'
        );
    }

    createVoting(getProtocol: (string) => ng.IPromise<ProtocolDocumentWrapper>,
                 processId: string, getInitProcessProperties: (Protocol) => oasiBpmRest.ITaskVariable[],
                 state: string) {
        getProtocol(this.agenda.agendaID).then((protocolDocumentWrapper: ProtocolDocumentWrapper) => {
            let protocol: Protocol = protocolDocumentWrapper.document.protocol;
            this.protocolTaskService.getProcessDefinition(processId).then((process: oasiBpmRest.IProcessDefinition) => {
                return this.protocolTaskService.initProcess(process.id, getInitProcessProperties(protocol));
            }).then((id) => {
                protocol.bpmProcess = new ProtocolProcess();
                protocol.bpmProcess.bpmProcessId = id;
                return this.protocolResource.update({id: protocol.protocolID}, {document: {protocol}});
            }).then(() => {
                return this.getTaskId(protocol.bpmProcess.bpmProcessId);
            }).then((taskId) => {
                this.$state.go(state, {documentId: protocol.protocolID, taskId});
            });
        });
    }

    getTaskId(processId: any) {
        let deferred = this.$q.defer();
        this.activityRestService.getTasks({
            processInstanceId: processId
        }).then((result: any) => {
            deferred.resolve(result.data[0].id);
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    // getInitProcessProperties(protocol: Protocol): any[] {
    //     return [{
    //         "id": "EntityIdVar",
    //         "value": protocol.protocolID
    //     },{
    //         "id": "EntityDescriptionVar",
    //         "value": `${protocol.meetingFullName}` +
    //         ` №${protocol.meetingNumber} от ${this.dateFilter(protocol.meetingDate, 'dd.MM.yyyy')}`
    //     }, {
    //         "id": "ContractorVar",
    //         "value": this.session.login()
    //     }];
    // }

}

angular.module('app').component('agendaView', new AgendaViewComponent());
