class AgendaComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendaController;
        this.templateUrl = 'app/components/ssmka/agenda/agenda.html';
        this.bindings = {};
    }
}

class AgendaController {
    static $inject = ['$q', '$state', '$stateParams', '$rootScope', 'agendaResource', 'ssAuthorizationService', 'toastr',
        'meetingTypesService'
    ];

    private agendaPromise: ng.IPromise<any>;
    private agenda: Agenda;
    private express: boolean;
    private loadingStatus: LoadingStatus;
    
    private editing: boolean;

    constructor(private $q: ng.IQService, private $state: any, private $stateParams: any, private $rootScope: any, private agendaResource: IAgendaResource,
        private authorizationService: IAuthorizationService, private toastr: Toastr, private meetingTypesService: IMeetingTypesService) {
        $rootScope.$on('navigatorLoaded', this.updateBreadcrumbs.bind(this));
    }

    $onInit() {

        this.editing = this.$stateParams.editing;

        let id: string = this.$stateParams.id;
        let agenda: Agenda = this.$stateParams.agenda;
        if (!id) {            
            this.agendaPromise = this.newAgenda();
            this.editing = true;
        } else {
            this.agendaPromise = this.loadAgenda(id);
        }
        this.agendaPromise.then((val: {agenda: Agenda, express: boolean}) => {
            this.agenda = val.agenda;
            this.express = val.express;
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.updateBreadcrumbs();
        })

    }
    
    reload(): ng.IPromise<any> {
        this.agendaPromise = this.loadAgenda(this.agenda.agendaID);
        return this.agendaPromise; 
    }

    newAgenda(): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        deferred.resolve({
            agenda: new Agenda(),
            express: false
        });
        return deferred.promise;
    }

    loadAgenda(id: string): ng.IPromise<any> {
        this.loadingStatus = LoadingStatus.LOADING;
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.$q.all([
            this.agendaResource.getById({ id: id }).$promise,
            this.meetingTypesService.getMeetingTypes()
        ]).then((values: any[]) => {
            let agenda: Agenda = values[0].document.agenda;
            let meetingTypes: MeetingType[] = values[1];
            let meetingType = _.find(meetingTypes, (meetingType) => {
                return meetingType.meetingType === agenda.meetingType;
            });
            if (meetingType) {
                deferred.resolve({
                    agenda: agenda,
                    express: meetingType.Express
                });
            } else {
                deferred.reject();
                this.loadingStatus = LoadingStatus.ERROR;
                this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
            }
        });
        return deferred.promise;
    }
    
    edit(edit: boolean) {
        this.editing = edit;
    }

    updateBreadcrumbs() {

        let newBreadcrumbs = [];
        let navigatorCode = this.express ? 'meetengs-special' : 'publishedCard';
        let sidebar = this.$rootScope["sidebar"];

        if (sidebar) {

            sidebar.forEach(item => {
                if (item["code"] === navigatorCode) {
                    newBreadcrumbs.push({
                        title: item["name"],
                        url: item["url"]
                    });
                }
                else {
                    item["subItems"].forEach(subItem => {
                        if (subItem["code"] === navigatorCode) {
                            newBreadcrumbs.push({
                                title: item["name"],
                                url: item["url"]
                            });
                            newBreadcrumbs.push({
                                title: subItem["name"],
                                url: subItem["url"]
                            });
                        }
                        else {
                            subItem["subItems"].forEach(subSubItem => {
                                if (subSubItem["code"] === navigatorCode) {
                                    newBreadcrumbs.push({
                                        title: item["name"],
                                        url: item["url"]
                                    });
                                    newBreadcrumbs.push({
                                        title: subItem["name"],
                                        url: subItem["url"]
                                    });
                                    newBreadcrumbs.push({
                                        title: subSubItem["name"],
                                        url: subSubItem["url"]
                                    });
                                }
                            });
                        }
                    });
                }
            });

            if (!this.$rootScope["breadcrumbs"]) {
                this.$rootScope["breadcrumbs"] = [];
            }

            this.$rootScope["breadcrumbs"].length = 0;
            this.$rootScope["breadcrumbs"] = this.$rootScope["breadcrumbs"].concat(newBreadcrumbs);
        }

    }
}

angular.module('app').component('agenda', new AgendaComponent());
