class ExpressAgendaEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ExpressAgendaEditController;
        this.templateUrl = 'app/components/ssmka/agenda/express-agenda-edit.html';
        this.bindings = {};
    }
}

class ExpressAgendaEditController {
    static $inject = [
        '$q',
        '$state',
        '$stateParams',
        '$scope',
        '$window',
        '$filter',
        '$timeout',
        'agendaResource',
        'meetingTypesService',
        'alertService',
        'nsiRestService',
        'oasiSolarRestService',
        'expressResource',
        'questionResource',
        'toastr',
        'fileHttpService',
        'mggtService',
        'ssFileRenameService',
        'timeUtils',
        'instructionResource'
    ];

    private loadingStatus: LoadingStatus;

    private agenda: Agenda;
    private questions: Question[];

    private meetingTypes: MeetingType[];
    private meetingPlaces: MeetingPlace[];
    private foldersSettings: MeetingFilenetSettings[];
    private parentFolderGuid: string;
    private filesFolderGuid: string;
    private dropzone: Dropzone;
    private fileQueue: File[];
    private fileQueueToDisplay: FileType[];

    private emailGroups: EmailGroups[];
    private emailGroupsList: EmailGroups[];

    private updatingAttendeeListInProgress: boolean;
    private updatingResponsiblesListInProgress: number = -1;
    private attendeeList: AgendaAttended[];
    private responsiblesList: QuestionResponsible[];
    private responsibles1 = [];
    private freeResponsibles = [];
    private initiatorsList: AgendaUser[];

    private saving: boolean;
    private uploading: boolean;
    private isCopied: boolean;

    private intersectionsWarnings: string[];

    constructor(private $q: ng.IQService,
        private $state: any,
        private $stateParams: any,
        private $scope: any,
        private $window: ng.IWindowService,
        private $filter: any,
        private $timeout: any,
        private agendaResource: IAgendaResource,
        private meetingTypesService: IMeetingTypesService,
        private alertService: oasiWidgets.IAlertService,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private oasiSolarRestService: oasiSolarRest.OasiSolarRestService,
        private expressResource: IExpressResource,
        private questionResource: IQuestionResource,
        private toastr: Toastr,
        private fileHttpService: FileHttpService,
        private mggtService: MggtService,
        private ssFileRenameService: ssFileRenameService,
        private timeUtils: ITimeUtils,
        private instructionResource: IInstructionResource) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;

        let id: string = this.$stateParams.id;
        this.isCopied = this.$stateParams.copy;
        let agendaPromise = id ? this.loadAgenda(id) : this.newAgenda();

        agendaPromise.then((agenda) => {
            this.agenda = agenda;
            if (!this.agenda.filesAttach) {
                this.agenda.filesAttach = [];
            }
            if (this.agenda.attended) {
                this.updateResponsibles();
            }
            this.fileQueueToDisplay = this.agenda.filesAttach;
            this.fileQueue = [];

            this.$scope.$watch(() => {
                return this.agenda.meetingDate;
            }, () => {
                this.updateIntersectionsWarnings();
            });
            this.$scope.$watch(() => {
                return this.agenda.meetingTime;
            }, () => {
                this.updateIntersectionsWarnings();
            });
            this.$scope.$watch(() => {
                return this.agenda.meetingTimeEnd;
            }, () => {
                this.updateIntersectionsWarnings();
            });
            return this.$q.all([
                this.loadDictionaries()
            ]);
        }).then((result: any) => {
            this.updateEmailGroupsAfterAttended();
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.$timeout(() => {
                const self = this;
                self.dropzone = new Dropzone("#filesAttach", {
                    autoProcessQueue: false,
                    withCredentials: true,
                    parallelUploads: 1,
                    paramName: "file",
                    url: self.fileHttpService.root,
                    init: function () {
                        this.on("addedfile", self.addFileToQueue.bind(self));
                    }
                });
            })
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    newAgenda(): ng.IPromise<Agenda> {
        let agenda: Agenda = new Agenda();
        this.questions = [new Question()];
        if (this.$stateParams.payload) {
            agenda = this.$stateParams.payload.document.agenda;
            this.questions = this.$stateParams.payload.questions;
        }
        return this.$q.resolve(agenda);
    }

    loadAgenda(id: string): ng.IPromise<Agenda> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.agendaResource.getById({ id: id }, (data: AgendaDocumentWrapper) => {
            if (this.meetingTypesService.checkMeetingType(data.document.agenda.meetingType)) {
                this.loadIncludedQuestions(id).then(() => {
                    deferred.resolve(data.document.agenda);
                }).catch(() => {
                    deferred.reject();
                    this.toastr.warning("Не удалось загрузить вопросы совещания.", "Ошибка");
                });
            } else {
                deferred.reject();
                this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
            }
        });
        return deferred.promise;
    }

    loadIncludedQuestions(agendaId: string): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.agendaResource.includedQuestions({ id: agendaId }).$promise.then((included: AgendaQuestion[]) => {
            included = included.filter(question => question.status !== AgendaQuestionStatus.EXCLUDED);
            this.$q.all(
                _.map(included, (question: AgendaQuestion) => {
                    return this.questionResource.getById({
                        id: question.questionId
                    }).$promise;
                })
            ).then((result: QuestionDocumentWrapper[]) => {
                if (result.length > 0) {
                    this.questions = result.map(wr => wr.document.question);
                } else {
                    this.questions = [new Question()];
                }
                deferred.resolve();
            }, () => {
                deferred.reject();
            });

        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    loadDictionaries(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('MeetingPlace'),
            this.nsiRestService.get('emailGroups'),
            this.nsiRestService.get('MeetingFilenetSettings')
        ]).then((values: any[][]) => {
            this.meetingTypes = values[0].filter((el) => el.Express);
            this.meetingPlaces = values[1];
            this.emailGroupsList = values[2].filter(emailGroup => {
                return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
                    return sendSystem.code === 'ss';
                });
            });
            // Настройка папки для сохранения файлов.
            this.foldersSettings = values[3];

            if (!this.agenda.meetingType) {
                let express = _.find(this.meetingTypes, (el) => el.meetingType === 'EXPRESS');
                this.agenda.meetingType = express.meetingType;
            }
            this._setFolderGuid();

            deferred.resolve();
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    /**
     *  Устанавливаем идентификатор папки в filenet в зависимости от agenda.meetingType или пустую строку, которая
     *  заблокирует дропзону для файлов.
     *  @private
     */
    private _setFolderGuid() {
        if (this.agenda.meetingType) {
            this.parentFolderGuid = this.foldersSettings.find(f => f.meetingType === this.agenda.meetingType).agendaFolderGuid;
        } else {
            this.parentFolderGuid = '';
        }
    }

    meetingTypeChanged() {
        this._setFolderGuid();
        let meetingType: MeetingType = _.find(this.meetingTypes, (meetingType) => {
            return meetingType.meetingType === this.agenda.meetingType;
        });
        if (this.agenda.meetingType && !this.agenda.agendaID) {
            if (meetingType) {
                if (!this.agenda.meetingTime) {
                    this.agenda.meetingTime = meetingType.meetingTimeStart;
                }
                if (!this.agenda.meetingPlace && meetingType.meetingPlace) {
                    this.agenda.meetingPlace = meetingType.meetingPlace[0].name;
                }
            }
        }
    }

    searchAttendee(query: string) {
        const params: oasiNsiRest.ISerachUsersParams = {
            fio: query,
            department: ['MKA', 'MKA_MGGT', 'MKA_OTHER_GAUNIPIGP', 'MKA_GENPLAN', 'MKA_GLAVAPU']
        };
        this.updatingAttendeeListInProgress = true;
        this.nsiRestService.searchUsers(params, 'OR').then((users: oasiNsiRest.UserBean[]) => {
            this.attendeeList = users.map(this._convertServerUser);
            this.updatingAttendeeListInProgress = false;
        }, () => {
            this.updatingAttendeeListInProgress = false;
        });
    }

    clearAttendeeList() {
        this.attendeeList = [];
    }

    updateResponsibles() {
        this.responsibles1 = this.agenda.attended ? this.agenda.attended.map((u: any) => ({
            departmentPrepareFull: u.departmentFullName,
            departmentPrepareShort: null,
            departmentPrepareCode: u.departmentCode,
            contractorPrepare: u.accountName,
            contractorPrepareFIO: u.fioFull,
            contractorPreparePhone: u.telephoneNumber
        })) : [];
        this.freeResponsibles = [...this.responsibles1];
        console.log(this.freeResponsibles);
    }

    updateSavedResponsibles(item) {
        this.questions.forEach((question, index) => {
            let personIndex = this._checkSavedResponsibles(question, item);
            if (personIndex !== -1 && !_.isNull(personIndex)) {
                _.pullAt(question.questionConsider.responsiblePrepareExpress, personIndex);
            }
        });
    }

    private _checkSavedResponsibles(question: Question, item: any) {
        return !_.isUndefined(question.questionConsider.responsiblePrepareExpress) ?
            _.findIndex(question.questionConsider.responsiblePrepareExpress, (responsible) => {
                return responsible.contractorPrepareFIO === item.fioFull;
            }) : null;
    }

    searchResponsibles(query: string, questionIndex: number) {
        this.responsiblesList = this.agenda.attended.map((u: any) => {
            return {
                departmentPrepareFull: u.departmentFullName,
                departmentPrepareShort: null,
                departmentPrepareCode: u.departmentCode,
                contractorPrepare: u.accountName,
                contractorPrepareFIO: u.fioFull,
                contractorPreparePhone: u.telephoneNumber
            };
        });
    }

    /**
     *  Поиск инициатора экспресс-совещания
     *  @param  {string}    query   Строка поиска
     */
    searchInitiator(query: string) {
        const params: oasiNsiRest.ISerachUsersParams = {
            fio: query,
        };
        this.nsiRestService.searchUsers(params).then((users: oasiNsiRest.UserBean[]) => {
            this.initiatorsList = users.filter(user => !user.mkaLock).map(user => ({
                accountName: user.accountName,
                post: user.post,
                iofShort: user.iofShort,
                fioFull: user.displayName,
            }));
        });
    }

    clearResponsiblesList() {
        this.responsiblesList = [];
    }

    cancel() {
        this.alertService.confirm({
            message: 'Данные не будут сохранены, продолжить?',
            okButtonText: 'Продолжить',
            type: 'danger'
        }).then(() => {
            this.$window.history.back();
        });
    }

    save() {
        const iPromise = this.validate();
        iPromise.catch((msg) => {
            this.toastr.error(msg);
            return this.$q.reject();
        }).then(() => {
            this.saving = true;
            let promise = null;
            const payload: AgendaPayload = this._getAgendaPayload();
            if (!this.agenda.agendaID) {
                promise = this.expressResource.createAgenda(payload).$promise;
            } else {
                promise = this.expressResource.updateAgenda({ id: this.agenda.agendaID }, payload).$promise.then(() => {
                    return { id: this.agenda.agendaID };
                });
            }
            return promise;
        }).then((result: { id: string }) => {
            return this.agendaResource.getById({ id: result.id }).$promise;
        }).then((wrapper) => {
            this.agenda = wrapper.document.agenda;
            return this.loadIncludedQuestions(this.agenda.agendaID);
        }).then(() => {
            return this.uploadFiles();
        }).then(() => {
            const payload: AgendaPayload = this._getAgendaPayload();
            return this.expressResource.updateAgenda({ id: this.agenda.agendaID }, payload).$promise;
        }).then(() => {
            return this.processInstructionsForCopiedQuestions(this.agenda.agendaID, this.questions);
        }).then(() => {
            return this.mggtService.sendAgenda(this.agenda, this._getQuestionsToSave().map(q => q.questionID))
        }).then(() => {
            this.saving = false;
            this.$state.go('app.ssmka.agenda', { id: this.agenda.agendaID, editing: false });
        }).catch(() => {
            this.saving = false;
        });
    }

    processInstructionsForCopiedQuestions(agendaId: string, questions: Question[]): ng.IPromise<any> {
        if (!this.isCopied || !questions || this.questions.length === 0) {
            return this.$q.resolve();
        }
        return this.$q.all(questions.map(q => this.processInstructionsForCopiedQuestion(agendaId, q)));
    }

    processInstructionsForCopiedQuestion(agendaId: string, question: Question): ng.IPromise<any> {
        return this.oasiSolarRestService.searchExt({
            type: 'INSTRUCTION',
            pageSize: 200,
            fields: [{name: 'questionPrimaryID', value: [question.questionPrimaryID]}]
        }).then(result => {
            if (!result.docs || result.docs.length === 0) {
                return this.$q.resolve();
            }
            return this.$q.all(result.docs.map(doc => {
                const id = doc.documentId;
                return this.instructionResource.getInstruction({id: id}).$promise.then(wrapper => {
                    let showExpressMeetingID = wrapper.document.instruction.showExpressMeetingID;
                    let payload;
                    if (showExpressMeetingID) {
                        payload = [{ "op": "add", "path": "/document/instruction/showExpressMeetingID/-", "value": agendaId}];
                    } else {
                        payload = [{ "op": "add", "path": "/document/instruction/showExpressMeetingID", "value": [agendaId]}];
                    }
                    return this.instructionResource.updateInstruction({id: doc.documentId}, payload);
                });

            }));
        });
    }

    validate(): ng.IPromise<any> {
        if (!this.agenda.meetingType) {
            return this.$q.reject('Не указан вид совещания');
        }
        if (!this.agenda.initiator) {
            return this.$q.reject('Не указан инициатор');
        }
        if (!this.agenda.meetingPlace) {
            return this.$q.reject('Не указано место проведения');
        }
        if (!this.agenda.meetingTheme) {
            return this.$q.reject('Не указана тема совещания');
        }
        if (!this.agenda.meetingDate) {
            return this.$q.reject('Не указана дата совещания');
        }
        if (!this.agenda.meetingTime) {
            return this.$q.reject('Не указано время начала');
        }
        if (!this.agenda.meetingTimeEnd) {
            return this.$q.reject('Не указано время окончания');
        }
        if (!this._validateMeetingTimeEnd()) {
            return this.$q.reject('Введенное время окончания совещания меньше времени начала совещания');
        }
        if (!this.agenda.attended || !this.agenda.attended.length) {
            return this.$q.reject('Не указаны участники');
        }
        return this.$q.resolve();
    }

    attendedAdded(attended: AgendaAttended) {
        if (!this.agenda.meetingDate || !this.agenda.meetingTime || !this.agenda.meetingTimeEnd) {
            this.alertService.message({
                message: "Для проверки занятости участников, введите время начала и окончания совещания"
            });
            return;
        }
        let meetingDateTime = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTime);
        let meetingDateTimeEnd = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTimeEnd);
        this.checkAttended([attended], meetingDateTime, meetingDateTimeEnd).then((messages) => {
            if (messages.length > 0) {
                this.alertService.message({ message: messages.join("<br/>") });
            }
        });
    }

    updateIntersectionsWarnings() {
        this.intersectionsWarnings = [];
        if (this.agenda.attended && this.agenda.attended.length && (!this.agenda.meetingDate || !this.agenda.meetingTime || !this.agenda.meetingTimeEnd)) {
            this.intersectionsWarnings = ["Для проверки занятости участников, введите дату, время начала и окончания совещания"];
            return;
        }
        if (this.agenda.attended && this.agenda.attended.length) {
            let meetingDateTime = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTime);
            let meetingDateTimeEnd = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTimeEnd);

            this.checkAttended(this.agenda.attended, meetingDateTime, meetingDateTimeEnd).then((messages) => {
                this.intersectionsWarnings = messages || [];
            });
        }
    }

    checkAttended(attended: AgendaAttended[], startDateTime: string, endDateTime: string): ng.IPromise<string[]> {
        return this.$q.all(
            attended.map(att => {
                return this.oasiSolarRestService.searchExt({
                    type: 'SS_AGENDA',
                    pageSize: 200,
                    fields: [
                        { name: 'attendedAccountSs', value: att.accountName },
                        { name: 'meetingDateTimeSs', value: { to: endDateTime } },
                        { name: 'meetingDateTimeEndSs', value: { from: startDateTime } }
                    ]
                });
            })
        ).then((results: oasiSolarRest.SearchResult[]) => {
            let messages: string[] = [];
            results.forEach((result, index) => {
                let att = attended[index];
                result.docs.forEach(doc => {
                    if (!this.agenda.agendaID || doc.documentId !== this.agenda.agendaID) {
                        let message = 'Внимание: <span class="semibold">' + att.fioFull + '</span> с ' + this.formatTime(doc.meetingTimeSS) +
                            ' до ' + this.formatTime(doc.meetingTimeEndSS) + ' участвует в совещании "<u>' + doc.meetingShortNameSs + '</u>"';
                        messages.push(message);
                    }
                });
            });
            return this.$q.resolve(messages);
        });
    }

    onRemoveEmailGroups(emailGroup: EmailGroups) {
        this.agenda.attended = this.agenda.attended.filter(user => {
            return emailGroup.users.indexOf(user.accountName) === -1;
        });
    }

    updateAttendedAfterEmailGroups(emailGroup: EmailGroups) {
        this.updatingAttendeeListInProgress = true;

        let usersDef = emailGroup.users.map(user => {
            return this.nsiRestService.ldapUser(user);
        });

        this.$q.all(usersDef)
            .then(users => {
                let usersInEmailGroups = users.map(this._convertServerUser);
                if (!_.isArray(this.agenda.attended)) {
                    this.agenda.attended = [];
                }

                this.agenda.attended = _.uniqBy(_.union(this.agenda.attended, usersInEmailGroups), 'accountName');
                this.updatingAttendeeListInProgress = false;
            }, () => {
                this.updatingAttendeeListInProgress = false;
            });
    }

    updateEmailGroupsAfterAttended() {
        let attendedAccounts: string[] = this.agenda.attended ? this.agenda.attended.map(user => {
            return user.accountName;
        }) : [];
        this.emailGroups = _
            .chain(this.emailGroupsList)
            .filter(emailGroup => {
                return _.every(emailGroup.users, user => {
                    return attendedAccounts.indexOf(user) > 0;
                });
            })
            .value();
    }

    formatTime(time: string): string {
        let t = this.timeUtils.parse(time);
        return this.$filter('date')(t, 'HH:mm');
    }

    getDateTime(date: Date, time: string): string {
        return this.$filter('date')(this._buildDate(date, time), 'yyyy-MM-ddTHH:mm:ss');
    }

    /**
     *  Показываем сообщение об ошибке, что выбрано начало совещания позже конца
     */
    showMeetingTimeEndError() {
        if (!this._validateMeetingTimeEnd()) {
            this.toastr.error('Введенное время окончания совещания меньше времени начала совещания');
        }
    }

    private _convertServerUser(user: any): AgendaAttended {
        return {
            internal: true,
            accountName: user.accountName,
            post: user.post,
            iofShort: user.iofShort,
            fioFull: user.displayName,
            name: user.departmentFullName,
            code: user.departmentCode,
            role: {
                name: user.departmentFullName,
                code: user.departmentCode
            },
            prefect: null
        };
    }

    /**
     *  Составляю объект для сохранения повестки
     *  @returns    {AgendaPayload}
     *  @private
     */
    _getAgendaPayload(): AgendaPayload {
        const payload: AgendaPayload = {
            document: { agenda: this.agenda },
        };
        const questionsToSave: Question[] = this._getQuestionsToSave();
        if (questionsToSave.length > 0) {
            payload.questions = questionsToSave;
        }
        return payload;
    }

    _getQuestionsToSave(): Question[] {
        return this.questions.filter(q => _.get(q, 'questionConsider.question', false) || _.get(q, 'questionConsider.responsiblePrepareExpress', false));
    }

    /**
     *  Получаем объект Date из параметоров
     *  @param  {Date}    date  Выбранная дата
     *  @param  {string}  time  Выбранное время
     *  @returns    {Date}
     *  @private
     */
    private _buildDate(date: Date, time: string): Date {
        const t = this.timeUtils.parse(time);
        const result = new Date(date.getTime());
        result.setHours(t.getHours(), t.getMinutes());
        return result;
    }

    /**
     *  Проверка времени, что начало раньше конца
     *  @returns    {boolean}
     *  @private
     */
    private _validateMeetingTimeEnd(): boolean {
        const meetingDateTime: Moment = moment(this._buildDate(this.agenda.meetingDate, this.agenda.meetingTime));
        const meetingDateTimeEnd: Moment = moment(this._buildDate(this.agenda.meetingDate, this.agenda.meetingTimeEnd));
        return meetingDateTime.isBefore(meetingDateTimeEnd);
    }

    /**
     *  Добавляем файл в очередь, имитируя загрузку
     *  @param  {File}  file    файл для постановки в очередь
     */
    addFileToQueue(file) {
        this.fileQueue.push(file);
        const fileType: FileType = FileType.create(undefined, file.name, file.size, false, moment().toISOString(), file.type, 'MkaDocOther');
        this.fileQueueToDisplay.push(fileType);
        this.dropzone.removeFile(file);
    }

    /**
     *  Создаем папку для файлов с именем agendaID, загружаем файлы, если файлов в совещании больше нет,
     *  то папку удаляем
     *  @returns    {Promise}
     */
    uploadFiles() {
        const folderData: ICreateFolderPostData = {
            folderName: this.agenda.agendaID,
            parentGuid: this.parentFolderGuid,
            folderType: 'MkaFldCaseContainer',
            folderAttrs: [],
        };
        return this.fileHttpService.createFolder(folderData).then((result: any) => {
            this.filesFolderGuid = result.guid;
            if (this.fileQueueToDisplay.length === 0) {
                return this.fileHttpService.deleteFolder(this.filesFolderGuid);
            }
            return this.$q.all(
                this.fileQueue
                    .map(f => this.validateAndUploadFile(f))
            ).then((fileList: FileType[]) => {
                const existingFiles = this.fileQueueToDisplay
                    .filter(f => f.idFile);

                this.agenda.filesAttach = [...existingFiles, ...fileList];
            });

        })
    }

    /**
     *  Загрузка файла с проверкой имени
     *  @param  {File}  file    Файл для загрузки
     *  @returns    {FileType}    guid нового файла
     */
    validateAndUploadFile(file) {
        this.uploading = true;
        let fd = new FormData();
        fd.append('file', file);
        fd.append('folderGuid', this.filesFolderGuid);
        fd.append('fileType', 'MkaDocOther');
        return this.fileHttpService.handleFileUploadNew(fd)
            .then(res => {
                const fileType: FileType = FileType.create(res.guid, file.name, file.size, false, moment().toISOString(), file.type, 'MkaDocOther');
                this.removeFileQueued(file);
                this.uploading = false;
                return fileType;
            })
            .catch(err => {
                const { data } = err;
                let reason = '';
                // Иногда приходит HTML, например, при ошибке большого файла
                try {
                    reason = JSON.parse(data).message;
                } catch (error) {
                }
                if (~reason.indexOf('File already exists')) {
                    return this.ssFileRenameService.renameFile(file.name).then(newName => {
                        let _file = new File([file], newName, {
                            type: file.type,
                        });
                        return this.validateAndUploadFile(_file);
                    }).catch(cancel => {
                        this.removeFileQueued(file);
                        this.uploading = false;
                    });
                }
                this.removeFileQueued(file);
                this.uploading = false;
                return this.$q.resolve();
            });
    }

    /**
     *  Удаление файла из filenet или из очереди
     *  @param  {File}  file    Удаляемый файл
     */
    removeFile(file) {
        if (file.idFile) {
            this.fileHttpService.deleteFile(file.idFile)
                .then(() => {
                    const ind = this.agenda.filesAttach.findIndex(el => el.idFile === file.idFile);
                    this.agenda.filesAttach.splice(ind, 1);
                });
        } else {
            let ind = this.fileQueue.findIndex(el => el.name === file.name);
            this.fileQueue.splice(ind, 1);
            ind = this.fileQueueToDisplay.findIndex(el => el.name === file.name);
            this.fileQueueToDisplay.splice(ind, 1);
        }
    }

    /**
     *  Удаление файлa из очереди после загрузки или после отмены переименования
     *  @param  {File}  file  Удаляемый файл
     */
    removeFileQueued(file) {
        let ind = this.fileQueue.findIndex(el => el.name === file.name);
        this.fileQueue.splice(ind, 1);
    }

}

angular.module('app').component('expressAgendaEdit', new ExpressAgendaEditComponent());
