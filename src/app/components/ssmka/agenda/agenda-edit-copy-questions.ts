class AgendaEditCopyQuestionsController implements ng.ui.bootstrap.IModalServiceInstance  {
    static $inject = [
    ];

    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;

    private resolve: {
        questions: LastHeldQuestion[]
    };

    private checked: boolean[];

    constructor() {
    }

    $onInit() {
        this.checked = new Array(this.resolve.questions.length);
    }

    getCheckedCount() {
        const result = _.countBy(this.checked, ch => ch).true;
        return result ? result : 0;
    }

    save() {
        this.close({$value: _.filter(this.resolve.questions, (q, index) => this.checked[index])});
    };

    cancel() {
        this.dismiss();
    };

}

class AgendaEditCopyQuestionsComponent {
    public controller: any = AgendaEditCopyQuestionsController;
    public bindings: any = {
        resolve: '<',
        close: '&',
        dismiss: '&'
    };
    public templateUrl: string = 'app/components/ssmka/agenda/agenda-edit-copy-questions.html';
}

angular.module('app').component('agendaEditCopyQuestionsDialog', new AgendaEditCopyQuestionsComponent());
