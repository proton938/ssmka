class AgendaJsonEditorController {
	static $inject = ['toastr', '$state', 'agendaResource', '$q'];
	constructor(
		private toastr: Toastr,
		private $state,
		private agendaResource: IAgendaResource,
		private $q: ng.IQService
	) {
	}
	private document: Agenda;
	private jsonEditorOptions: {
		mode: string
	};
	$onInit() {
		console.info("INIT JSON EDITOR");
		this.jsonEditorOptions = {
			mode: 'tree'
		};
	}

	save() {
		this.updateAgenda(this.document);
	}

	private updateAgenda(agenda: Agenda): ng.IPromise<boolean> {
		let id: string = agenda.agendaID;
		let deferred = this.$q.defer<boolean>();
		this.agendaResource.update({ id: id }, {
			agenda: {
				document: {
					agenda: agenda
				}
			}
		}, function () {
			deferred.resolve(true);
		});
		return deferred.promise;
	}

}

class AgendaJsonEditorComponent {
	public bindings: any;
	public controller: any;
	public templateUrl: string;

	constructor() {
		this.controller = AgendaJsonEditorController;
		this.templateUrl = 'app/components/ssmka/agenda/agenda-json-editor.html';
		this.bindings = {
			document: "<"
		};
	}
}

angular.module('app').component('agendaJsonEditor', new AgendaJsonEditorComponent());
