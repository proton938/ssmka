class AgendaEditQuestionReasonComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AgendaEditQuestionReasonController;
        this.templateUrl = 'app/components/ssmka/agenda/agenda-edit-question-reason.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class AgendaEditQuestionReasonController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = [];
    
    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;
    
    private reason: string;
    
    constructor() {
    }

    save() {
        this.close({
            $value: this.reason
        });
    }
    
    cancel() {
        this.dismiss();
    }

    $onInit() {
    }

}

angular.module('app').component('agendaEditQuestionReason', new AgendaEditQuestionReasonComponent());
