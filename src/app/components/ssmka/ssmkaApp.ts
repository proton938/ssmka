class SsmkaAppComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = SsmkaAppController;
        this.templateUrl = 'app/components/ssmka/ssmkaApp.html';
        this.bindings = {};
    }
}

class SsmkaAppController {
}

angular.module('app').component('ssmkaApp', new SsmkaAppComponent()); 