class PlannedQuestionsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = PlannedQuestionsController;
        this.templateUrl = 'app/components/ssmka/planned_questions/planned-questions.html';
        this.bindings = {};
    }
}

class PlannedQuestionsController {
    static $inject = ['$q', 'questionResource', 'ssAuthorizationService', 'meetingTypesService'];
    
    private hasViewPermission: boolean;

    private viewExtSearch: boolean;

    private extSearchFilter: QuestionSearchFilter;
    
    private pagination: Pagination;
    
    private loadingStatus: LoadingStatus;
    private searchResult: SearchResult<MeetingGroup<string>>;
    
    private questions: { [key: string]: Question };
    
    private expanded: any;

    private meetingTypesData: any[];

    constructor(private $q: ng.IQService, private questionResource: IQuestionResource, private authorizationService: IAuthorizationService,
                private meetingTypesService: IMeetingTypesService) {
    }
    
    reload() {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            }            
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
        
        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };        
        this.expanded = {};

        const params: any = {
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };

        if (!this.extSearchFilter.meetingTypes) {
            this.extSearchFilter.meetingTypes = availableMeetingTypes;
        }

        this.questionResource.searchPlan(params, this.extSearchFilter, (result: PageData<MeetingGroup<string>>) => {
            this.searchResult = {
                data: result.content,
                total: result.totalElements
            }            
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            }
            let questionsIds: string[] = _.chain(result.content).map(meetingGroup => {
                return meetingGroup.content;
            }).flatten().value();
            this.$q.all(
                _.map(questionsIds, (questionId: string) => {
                    return this.questionResource.getById({
                        id: questionId
                    }).$promise
                })
            ).then((result) => {
                _.each(result, (wrapper: QuestionDocumentWrapper) => {
                    let question: Question = wrapper.document.question;
                    this.questions[question.questionID] = question;
                });
                this.loadingStatus = LoadingStatus.SUCCESS;
            }, error)            
        }, error);
    }

    $onInit() {
        this.viewExtSearch = false;
        this.extSearchFilter = new QuestionSearchFilter();
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.expanded = {};
        this.questions = {};

        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            this.reload();
        }).catch(() => { });
    }
    
    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }
    
    toggle(index: number) {
        this.expanded[index] = !this.expanded[index];
    }  
    
    setFilter(filter: QuestionSearchFilter) {
        _.extend(this.extSearchFilter, filter);
        this.reload();
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key]; 
        });
        this.reload();
    }    

}

angular.module('app').component('plannedQuestions', new PlannedQuestionsComponent());
