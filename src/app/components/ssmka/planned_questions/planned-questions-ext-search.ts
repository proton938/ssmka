class PlannedQuestionsExtSearchComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = PlannedQuestionsExtSearchController;
        this.templateUrl = 'app/components/ssmka/planned_questions/planned-questions-ext-search.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
    }
}

class PlannedQuestionsExtSearchController {
    static $inject = ['$q', 'nsiRestService'];

    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: QuestionSearchFilter;

    private loadingAppStatus: LoadingStatus;

    private prefects: Prefect[];
    private responsibleDepartments: ResponsibleDepartment[];
    private meetingResponsibles: oasiNsiRest.UserBean[];
    private presentationResponsibles: oasiNsiRest.UserBean[];
    private questionCategories: CategoryQuestion[];

    private updatingUsersListInProgress: boolean = false;

    constructor(private $q: ng.IQService, private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.$q.all([
            this.nsiRestService.get('Prefect'),
            this.nsiRestService.get('ResponsibleDepartments'),
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE'),
            this.nsiRestService.ldapUsers('OASI_MEETING_PRESENTATION'),
            this.nsiRestService.get('CategoryQuestion')
        ]).then((values: any[]) => {
            this.prefects = values[0];
            this.responsibleDepartments = values[1];
            this.meetingResponsibles = values[2];
            this.presentationResponsibles = values[3];
            this.questionCategories = values[4];
            this.loadingAppStatus = LoadingStatus.SUCCESS;
        });
    }
    
    setFilter(extSearchFilter: QuestionSearchFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    departmentChanged() {
        let departmentPrepareShort: string = this.searchFilter.departmentPrepare;
        this.updatingUsersListInProgress = true;
        if (departmentPrepareShort) {
            let department = _.find(this.responsibleDepartments, (department: ResponsibleDepartment) => {
                return department.departmentPrepareShort === departmentPrepareShort;
            });
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE', department.departmentPrepareCode).then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });
        } else {
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE').then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });
        }
    }
    
    apply() {
        this.applyFilter({
            value: this.searchFilter
        });
    }
    
    cancel() {
        _.each(_.keys(this.searchFilter), (key) => {
            delete this.searchFilter[key]; 
        });
        this.clearFilter();
    }

}

angular.module('app').component('plannedQuestionsExtSearch', new PlannedQuestionsExtSearchComponent());
