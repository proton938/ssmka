import IModalServiceInstance = angular.ui.bootstrap.IModalServiceInstance;

class AddQuestionDialogController {
    static $inject = [
        'meetingTypesService',
        'nsiRestService',
        'userUtils',
        'Consideration',
        '$uibModal',
        'questionResource',
        'protocolResource',
        '$stateParams'
    ];
    public close: (result?: any) => {};
    public dismiss: (reason?: any) => {};
    // public result: any;
    private resolve: {
        protocolInfo: Protocol
    };
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    private question: Question = new Question();
    private questionCategories: CategoryQuestion[];
    private origPrefects: Prefect[];
    private prefects: string[];
    private origDistricts: District[];
    private districts: string[];
    private basisTypes: BaseType[];
    private responsibleDepartments: ResponsibleDepartment[];
    private meetingResponsibles: oasiNsiRest.UserBean[];
    private presentationResponsibles: QuestionUserType[];
    private speakers: Speaker[];
    private urbanLimits: UrbanLimit[];
    private networkEngineerings: NetworkEngineering[];
    private protocolInfo: Protocol;

    private mandatoryCadastralNumber: boolean;

    private updatingUsersListInProgress: Boolean = false;

    constructor(
        private meetingTypesService: IMeetingTypesService, private nsiRestService: oasiNsiRest.NsiRestService,
        private userUtils: IUserUtils,
        private Consideration: any, private $uibModal: any,
        private questionResource: IQuestionResource, private protocolResource: IProtocolResource,
        private $stateParams: any
    ) {
    }
    private $onInit() {
        Promise.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('Prefect'),
            this.nsiRestService.get('District'),
            this.nsiRestService.get('BaseTypes'),
            this.nsiRestService.get('ResponsibleDepartments'),
            this.nsiRestService.ldapUsers('OASI_MEETING_PRESENTATION'),
            this.nsiRestService.get('speakers'),
            this.nsiRestService.get('UrbanLimits'),
            this.nsiRestService.get('NetworkEngineering'),
            this.protocolResource.getById({ id: this.$stateParams.protocolId }).$promise
        ]).then((response: any[]) => {
            this.protocolInfo = response[9].document.protocol;
            const meetingTypes: MeetingType[] = response[0];
            const meetingType = _.find(meetingTypes, type => type.meetingType === this.$stateParams.meetingType);
            if (meetingType) {
                this.question.meetingFullName = meetingType.meetingFullName;
                this.question.meetingShortName = meetingType.meetingShortName;
            }
            this.question.meetingType = this.$stateParams.meetingType;
            this.meetingTypeChangeHandler();
            this.origPrefects = response[1];
            this.prefects = _.map(this.origPrefects, (prefect) => {
                return prefect.name;
            });
            this.origDistricts = response[2];
            this.districts = [];
            this.basisTypes = response[3];
            this.responsibleDepartments = response[4];
            this.presentationResponsibles = this.convertUsers(response[5]);
            this.speakers = response[6];
            this.urbanLimits = response[7];
            this.networkEngineerings = response[8];
            this.setDefaultData();
            this.loadingStatus = LoadingStatus.SUCCESS;
        });
    }
    private setDefaultData() {
        this.question.questionIDHistory = [];
        this.question.questionConsider = new QuestionConsider();
        this.question.questionConsider.passDate = undefined;
        this.question.questionConsider.consideration = Consideration.ACCEPTED;
        this.question.questionConsider.basis = new QuestionBasis();
        this.question.questionConsider.basis.date = undefined;
        this.question.questionConsider.planDate = this.protocolInfo.meetingDate;
        this.question.questionConsider.meetingDate = this.protocolInfo.meetingDate;
        this.question.questionConsider.materials = [];
        this.question.questionConsider.question = this.question.questionConsider.questionCategory;
        this.question.comments = [];
    }
    private meetingTypeChangeHandler() {
        this.nsiRestService.getBy('CategoryQuestion', {
            nickAttr: 'meetingType',
            values: [this.question.meetingType]
        }).then((questionCategories: CategoryQuestion[]) => {
            this.questionCategories = questionCategories;
        });
        this.mandatoryCadastralNumber = this.question.meetingType !== 'GK_EOO';
    }

    private questionCategoryChangeHandler() {
        let questionCategory: string = this.question.questionConsider.questionCategory;
        this.question.questionConsider.question = questionCategory;
    }

    private prefectChangeHandler(nullDistricts: boolean) {
        let prefectsNames: string[] = this.question.questionConsider ? this.question.questionConsider.prefect : [];
        this.districts = _.chain(this.origDistricts).filter((district) => {
            return _.intersection(prefectsNames, _.map(district.perfectId, pref => pref.name)).length > 0;
        }).map((district) => {
            return district.name;
        }).value();
        if (nullDistricts && this.question.questionConsider) {
            this.question.questionConsider.district = [];
        }
    }
    private departmentChangeHandler() {
        let departmentPrepareCode = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.departmentPrepareCode : null;
        this.updatingUsersListInProgress = true;
        if (departmentPrepareCode) {
            let department: ResponsibleDepartment = _.find(this.responsibleDepartments, (department: ResponsibleDepartment) => {
                return department.departmentPrepareCode === departmentPrepareCode;
            });
            this.question.questionConsider.responsiblePrepare.departmentPrepareShort = department.departmentPrepareShort;
            this.question.questionConsider.responsiblePrepare.departmentPrepareFull = department.departmentPrepareFull;

            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE', department.departmentPrepareCode).then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });

        } else {
            if (this.question.questionConsider.responsiblePrepare) {
                this.question.questionConsider.responsiblePrepare.departmentPrepareShort = null;
                this.question.questionConsider.responsiblePrepare.departmentPrepareFull = null;
            }
            this.meetingResponsibles = [];
            this.updatingUsersListInProgress = false;
        }

    }
    private convertUsers(users: oasiNsiRest.UserBean[]): QuestionUserType[] {
        return _.map(users, (user: oasiNsiRest.UserBean) => {
            return this.userUtils.toQuestionUserType(user);
        });
    }
    private departmentContractorChangeHandler() {
        let contractorPrepareLogin = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.contractorPrepare : null;
        if (contractorPrepareLogin) {
            let responsible: oasiNsiRest.UserBean = _.find(this.meetingResponsibles, function(meetingResponsible: oasiNsiRest.UserBean) {
                return meetingResponsible.accountName === contractorPrepareLogin;
            });
            this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = responsible.displayName;
            this.question.questionConsider.responsiblePrepare.contractorPreparePhone = responsible.telephoneNumber;
        } else {
            if (this.question.questionConsider.responsiblePrepare) {
                this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = null;
                this.question.questionConsider.responsiblePrepare.contractorPreparePhone = null;
            }
        }

    }

    private save() {
        if (!this.question.questionConsider.passDate) {
            this.question.questionConsider.passDate = new Date();
        }
        this.questionResource.create({
            document: {
                question: this.question
            }
        }).$promise.then((question) => {
            this.protocolResource.addQuestionToProtocol({
                protocolId: this.$stateParams.protocolId,
                questionId: question.id
            }).$promise.then(() => {
                window.close();
            });
        });
    }

    private cancelHandler() {
        this.dismiss();
    }

    isActiveBaseType(baseType: BaseType) {
        return baseType.active;
    }

    clearSpeaker() {
        delete this.question.questionConsider.speaker;
    }

    chooseSpeaker() {
        let tree = this.buildSelectedTree(this.speakers);
        this.$uibModal.open({
            component: 'dictionary-modal',
            size: 'lg',
            resolve: {
                name: () => {
                    return 'Выбор докладчика';
                },
                multiple: () => {
                    return false;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let speaker = this.question.questionConsider.speaker;
                    return speaker ? [this.question.questionConsider.speaker.organizationCode] : [];
                }
            }
        }).result.then((result: string[]) => {
            if (result && result.length > 0) {
                let speaker: Speaker = this.findInDictByCode(this.speakers, result[0]);
                this.question.questionConsider.speaker = this.convertSpeakerOrganizationToQuestionUserType(speaker);
            }
        });
    }

    findInDictByCode(dict: Speaker[], code: string): Speaker {
        let result: Speaker = null;

        dict.forEach(d => {
            if (d.code === code) {
                result = d;
            }

            if (d.children !== undefined && d.children !== null) {
                let r = this.findInDictByCode(d.children, code);
                if (r !== null) {
                    result = r;
                }
            }
        });

        return result;
    }


    chooseCospeakers() {
        let tree = this.buildSelectedTree(this.speakers);
        this.$uibModal.open({
            component: 'dictionary-modal',
            size: 'lg',
            resolve: {
                name: () => {
                    return 'Выбор содокладчиков';
                },
                multiple: () => {
                    return true;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let cospeakers = this.question.questionConsider.cospeaker;
                    return cospeakers ? _.map(cospeakers, (cs) => {
                        return cs.organizationCode;
                    }) : [];
                }
            }
        }).result.then((result: string[]) => {
            if (result) {
                this.question.questionConsider.cospeaker = _.map(result, (val) => {
                    let cospeaker = this.findInDictByCode(this.speakers, val);
                    return this.convertSpeakerOrganizationToQuestionUserType(cospeaker);
                })
            } else {
                delete this.question.questionConsider.cospeaker;
            }
        });
    }

    convertSpeakerOrganizationToQuestionUserType(org: Speaker): QuestionUserType {
        let result = new QuestionUserType();
        result.organization = org.name;
        result.organizationShort = org.nameShort;
        result.organizationCode = org.code;
        return result;
    }
    buildSelectedTree(dict: Speaker[]): oasiWidgets.ITreeViewItemChecked[] {
        let tree: oasiWidgets.ITreeViewItemChecked[] = null;

        dict.forEach(item => {
            if (tree === null) {
                tree = [];
            }

            let children: oasiWidgets.ITreeViewItemChecked[] = (item.children === undefined || item.children === null) ? null : this.buildSelectedTree(item.children);
            let selectable = children ? false : true;
            let template = children ? item.group : item.name;
            let result: oasiWidgets.ITreeViewItemChecked = {
                parent: null,
                id: item.code,
                template: template,
                children: children,
                expanded: false,
                checked: false,
                disabled: false,
                selectable: selectable
            };
            if (children) {
                children.forEach(ch => {
                    ch.parent = result;
                })
            }
            tree.push(result);
        });

        return tree;
    }

    isHasPrefect() {
        return !_.isEmpty(this.question.questionConsider.prefect);
    }
}

class AddQuestionDialogComponent {
    public controller: any = AddQuestionDialogController;
    public bindings: any = {
        resolve: '<',
        close: '&',
        dismiss: '&'
    };
    public templateUrl: string = 'app/components/ssmka/my-tasks/gk-registration-form/addQuestionDialog.html';
}

angular.module('app').component('addQuestionDialog', new AddQuestionDialogComponent());
