class GkEooRegistrationFormController extends GkRegistrationFormController {
    static $inject = [
        '$q',
        '$stateParams',
        'protocolResource',
        'nsiRestService',
        '$timeout',
        '$state',
        'protocolTaskService',
        'toastr',
        '$uibModal',
        'dateUtils'
    ];

    constructor(public $q: ng.IQService, private $stateParams: any,
                public protocolResource: IProtocolResource,
                private nsiRestService: oasiNsiRest.NsiRestService,
                public $timeout: any, private $state: any,
                public protocolTaskService: IProtocolTaskService,
                private toastr: Toastr,
                private $uibModal: any,
                private dateUtils: IDateUtils,) {
        super('Городская комиссия по ЭОО', 'gkoo_quizVote',
            $q, $stateParams, protocolResource, nsiRestService, $timeout, $state, protocolTaskService,
            toastr, $uibModal, dateUtils);
    }
}

class GkEooRegistrationFormComponent {
    public controller: any = GkEooRegistrationFormController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/gk-registration-form/gkRegistrationForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooRegistrationForm', new GkEooRegistrationFormComponent());
