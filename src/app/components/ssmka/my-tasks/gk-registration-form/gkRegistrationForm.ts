class GkRegistrationFormController {
    tableData: any = {};
    private rolesCount: number;
    private taskWrapper: any;
    private taskInfo: any = {
        description: '',
        meetingDate: null,
        secretaryName: 'не указан'
    };
    private loadingStatus = LoadingStatus.LOADING;
    private responseTaskInfo: TaskInfo;
    private updateInProgress: boolean = false;
    private saveInProgress: boolean = false;
    private submitInProgress: boolean = false;
    private isQuorum: boolean = false;
    private countOfRegistered: number = 0;
    private responseSecretary: any;

    constructor(public meetingType: string, public voteProcessId: string,
                public $q: ng.IQService, private $stateParams: any,
                public protocolResource: IProtocolResource,
                private nsiRestService: oasiNsiRest.NsiRestService,
                public $timeout: any, private $state: any,
                public protocolTaskService: IProtocolTaskService, private toastr: Toastr,
                private $uibModal: any,
                private dateUtils: IDateUtils) {
    }

    $onInit() {
        this.initData();
    };

    initData() {
        Promise.all([
            this.nsiRestService.get('RolesParticipants'),
            this.getTaskInfo(
                this.$stateParams.documentId,
                this.$stateParams.formKey,
                this.$stateParams.taskId
            )
        ]).then((responses: [RoleParticipant[], TaskInfo]) => {
            this.setTaskInfoData(responses[1]);
            this.normalizeParticipantList(responses[0]).then((normalizedParticipantList) => {
                this.$timeout(() => {
                    this.tableData = normalizedParticipantList;
                    this.rolesCount = _.keys(normalizedParticipantList).length;
                    this.updateInProgress = false;
                    this.saveInProgress = false;
                    this.loadingStatus = LoadingStatus.SUCCESS;
                    this.updateIsQuorum();
                });
            });

            let secretary = _.find(responses[0], (item) => item.LevelRoleCode === 'responsSecretary').account_name;
            this.nsiRestService.ldapUser(secretary).then((result) => {
                this.responseSecretary = result;
            });
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    getTaskInfo(id: string, formKey: string, taskId: number): ng.IPromise<TaskInfo> {
        let deferred: ng.IDeferred<TaskInfo> = this.$q.defer();
        let taskInfo: TaskInfo = {
            document: null,
            task: null,
            taskParams: {
                formKey: formKey,
                documentId: id,
                taskId: taskId
            }
        };
        this.loadDocument(id).then((document) => {
            taskInfo.task = this.taskWrapper.task;
            taskInfo.document = document;
            deferred.resolve(taskInfo);
        });
        return deferred.promise;
    }

    loadDocument(id: string) {
        let deferred: ng.IDeferred<ProtocolDocumentWrapper> = this.$q.defer();

        this.protocolResource.getById({id: id}, (data: ProtocolDocumentWrapper) => {
            deferred.resolve(data);
        }, () => {
            deferred.reject();
        });

        return deferred.promise;
    }

    setTaskInfoData(taskInfo: TaskInfo) {
        this.responseTaskInfo = taskInfo;
        this.taskInfo.description = taskInfo.task.description ? taskInfo.task.description : this.taskInfo.description;
        this.taskInfo.secretaryName = taskInfo.document.document.protocol.secretary.fioFull;
        this.taskInfo.meetingDate = taskInfo.document.document.protocol.meetingDate;
    }

    updateTableHandler() {
        this.updateInProgress = true;
        this.initData();
    }

    saveHandler() {
        this.saveInProgress = true;
        this.saveForm().then(() => {
            this.toastr.info('Данные сохранены.');
            this.$timeout(() => {
                this.saveInProgress = false;
            });
        });
    }

    async normalizeParticipantList(participantsList: RoleParticipant[]) {
        let attendedParticipants: ProtocolAttended[] = this.responseTaskInfo.document.document.protocol.attended || [],
            normalizedParticipantList: any = {};

        for (let participant of participantsList) {
            if (!participant.account_name) {
                continue;
            }

            let participantAccount: oasiNsiRest.UserBean;

            await this.nsiRestService.ldapUser(participant.account_name).then((user: any) => {
                participantAccount = user;
            }).catch((error) => {
                //console.error(error);
            });

            if (participantAccount) {
                let normalizedParticipant: NormalizedParticipant = {
                    shortFullName: participantAccount.fioShort || participantAccount.displayName,
                    fullName: participantAccount.displayName,
                    role: participant.RoleName,
                    checked: attendedParticipants.some((attendedParticipant) => {
                        return attendedParticipant.role.roleCode === participant.RoleCode
                            && attendedParticipant.accountName === participant.account_name;
                    }),
                    disabled: false,
                    levelRoleCode: participant.LevelRoleCode,
                    mask: participant.mask,
                    post: participant.PostName,
                    accountName: participant.account_name,
                    roleCode: participant.RoleCode,
                    quorum: participant.quorum
                };
                let participanKey = participant.groupRoles + participant.LevelRoleCode;
                if (!normalizedParticipantList.hasOwnProperty(participanKey)) {
                    let emptyCase = {
                        shortFullName: '',
                        accountName: null
                    };
                    normalizedParticipantList[participanKey] = {
                        participantsList: {
                            emptyCase,
                            [normalizedParticipant.accountName]: normalizedParticipant
                        },
                        selectedParticipant: emptyCase,
                        roleName: normalizedParticipant.role
                    };
                } else {
                    normalizedParticipantList[participanKey].participantsList[normalizedParticipant.accountName] = normalizedParticipant;
                }
                if (normalizedParticipant.checked) {
                    normalizedParticipantList[participanKey].selectedParticipant = normalizedParticipant;
                }
            }
        }
        return normalizedParticipantList;
    }

    /**
     * @deprecated
     * @param changedParticipant
     */
    changeParticipantChecked(changedParticipant: any) {
        let changedParticipantChecked = changedParticipant.checked,
            isChangedParticipantDeputy: boolean = !!/депутат/.exec(changedParticipant.role.toLowerCase());
        if (!isChangedParticipantDeputy) {
            _.forEach(this.tableData, (participant) => {
                let isCurrentParticipantDeputy: boolean = !!/депутат/.exec(participant.role.toLowerCase());
                if (!isCurrentParticipantDeputy &&
                    (participant !== changedParticipant) &&
                    (participant.role === changedParticipant.role)) {
                    participant.disabled = changedParticipantChecked;
                    participant.checked = false;
                }
            });
        }
    }

    checkRole(participant: IParticipantTableRow, roleName: string) {
        return participant.levelRoleCode === roleName;
    }

    updateIsQuorum() {
        let cntSelected = 0;
        let cntQuorum = 0;
        _.forEach(this.tableData, (role) => {
            if (role.selectedParticipant.accountName) {
                cntSelected++;
                if (role.selectedParticipant.quorum) {
                    cntQuorum++;
                }
            }
        });
        this.countOfRegistered = cntSelected;
        this.isQuorum = cntSelected / this.rolesCount > 1 / 3 && cntQuorum >= 9;
    }

    attendParticipants() {
        this.responseTaskInfo.document.document.protocol.attended = [];
        for (let roleCode in this.tableData) {
            if (this.tableData[roleCode].selectedParticipant.accountName !== null) {
                let selectedParticipant = this.tableData[roleCode].selectedParticipant;
                this.responseTaskInfo.document.document.protocol.attended.push({
                    accountName: selectedParticipant.accountName,
                    post: selectedParticipant.post,
                    iofShort: selectedParticipant.shortFullName,
                    fioFull: selectedParticipant.fullName,
                    role: {
                        roleCode: selectedParticipant.roleCode,
                        roleName: selectedParticipant.role
                    },
                    prefect: selectedParticipant.mask
                });
            }
        }
    }

    getAttendedCount() {
        return this.responseTaskInfo.document.document.protocol.attended.length;
    }

    addQuestionHandler() {
        this.saveForm().then(() => {
            let url = this.$state.href('app.ssmka.process.addQuestion', {
                meetingType: this.responseTaskInfo.document.document.protocol.meetingType,
                protocolId: this.responseTaskInfo.document.document.protocol.protocolID,
                taskId: this.$stateParams.taskId
            });
            window.open(url, '_blank');
        });
    }

    async saveForm() {
        this.attendParticipants();
        return await this.protocolResource.attendParticipants(
            {id: this.$stateParams.documentId},
            this.responseTaskInfo.document.document.protocol.attended
        ).$promise;
    }

    submitHandler() {
        let chiefOrViceParticipant: IParticipantTableRow = null,
            secretaryParticipant: IParticipantTableRow = null;

        let origProtocol = angular.copy(this.responseTaskInfo.document.document.protocol);

        if (!this.responseTaskInfo.document.document.protocol.secretaryResponsible) {
            this.responseTaskInfo.document.document.protocol.secretaryResponsible = new SecretaryResponsible();
            this.responseTaskInfo.document.document.protocol.secretaryResponsible.accountName = this.responseSecretary.accountName;
            this.responseTaskInfo.document.document.protocol.secretaryResponsible.post = this.responseSecretary.post;
            this.responseTaskInfo.document.document.protocol.secretaryResponsible.iofShort = this.responseSecretary.iofShort;
            this.responseTaskInfo.document.document.protocol.secretaryResponsible.fioFull = this.responseSecretary.displayName;
        }

        for (let roleCode in this.tableData) {
            let selectedParticipant = this.tableData[roleCode].selectedParticipant;
            if (selectedParticipant.accountName !== null) {
                switch (true) {
                    case this.checkRole(selectedParticipant, 'chief'):
                        chiefOrViceParticipant = selectedParticipant;
                        break;
                    case this.checkRole(selectedParticipant, 'vice'):
                        chiefOrViceParticipant = chiefOrViceParticipant || selectedParticipant;
                        break;
                    case this.checkRole(selectedParticipant, 'responsSecretary'):
                        secretaryParticipant = selectedParticipant;
                        break;
                }
            }
        }

        if (chiefOrViceParticipant) {
            this.submitInProgress = true;
            this.saveProtocol(origProtocol, this.responseTaskInfo.document.document.protocol).then(() => {
                this.responseTaskInfo.document.document.protocol.approvedBy = {
                    post: chiefOrViceParticipant.post,
                    iofShort: chiefOrViceParticipant.shortFullName,
                    fioFull: chiefOrViceParticipant.fullName,
                    accountName: chiefOrViceParticipant.accountName
                };

                if (secretaryParticipant) {
                    this.responseTaskInfo.document.document.protocol.secretaryResponsible = {
                        post: secretaryParticipant.post,
                        iofShort: secretaryParticipant.shortFullName,
                        fioFull: secretaryParticipant.fullName,
                        accountName: secretaryParticipant.accountName
                    };
                }
                this.attendParticipants();
                this.responseTaskInfo.document.document.protocol.attendedNum = this.getAttendedCount();

                this.saveForm().then(this.submit.bind(this));
            });
        } else {
            this.toastr.error('Не выбран председатель или его заместитель');
        }

    }

    /**
     * @deprecated
     * @param {ProtocolAttended} attended
     * @param {string} levelRoleCode
     */
    checkAttendedRole(attended: ProtocolAttended, levelRoleCode: string) {
        return this.tableData.some((participant: IParticipantTableRow) => {
            return (participant.accountName === attended.accountName) &&
                (participant.levelRoleCode === levelRoleCode);
        });
    }

    async submit() {
        let protocol: Protocol = this.responseTaskInfo.document.document.protocol,
            process: oasiBpmRest.IProcessDefinition = await this.protocolTaskService.getProcessDefinition(
                this.voteProcessId
            );

        for (let attendedParticipant of protocol.attended) {
            await this.protocolTaskService.initProcess(
                process.id,
                this.protocolTaskService.getInitProcessProperties(protocol, attendedParticipant.accountName)
            );
        }

        let params: oasiBpmRest.ITaskVariable[] = [{
            name: "secretaryVar",
            value: protocol.secretary.accountName
        }, {
            name: "comissionHeadVar",
            value: protocol.approvedBy.accountName
        }, {
            name: "responsSecretaryVar",
            value: protocol.secretaryResponsible.accountName
        }];

        return this.protocolTaskService.updateProcess(this.$stateParams.taskId, params).then(this.goBack.bind(this));
    }

    goBack() {
        this.submitInProgress = false;
        location.assign('/oasi/#/app/my-tasks');
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.responseTaskInfo.document.document.protocol.protocolID}, diff).$promise;
    }
}

class ITaskParams {
    formKey: string;
    documentId: string;
    taskId: number;
}

class TaskInfo {
    document: ProtocolDocumentWrapper;
    task: oasiBpmRest.ITask;
    taskParams: ITaskParams;
}

class IParticipantTableRow {
    shortFullName: string;
    fullName: string;
    role: string;
    checked: boolean;
    disabled: boolean;
    levelRoleCode: string;
    mask: string;
    post: string;
    accountName: string;
    roleCode: string;
}