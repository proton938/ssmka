class ProcessComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProcessController;
        this.templateUrl = 'app/components/ssmka/my-tasks/process.html';
        this.bindings = {};
    }
}

class ProcessController {
    static $inject = [
        '$q',
        '$stateParams',
        'session'
    ];

    private taskId: string;
    private login: string;
    private fio: string;

    constructor(
        private $q: ng.IQService,
        private $stateParams: any,
        private session: oasiSecurity.ISessionStorage
    ) {
        this.taskId = this.$stateParams.taskId;
        this.login = this.session.login();
        this.fio = this.session.fullName();
    }

    $onInit() {
    }

    goToMyTasks() {
        location.assign('/oasi/#/app/my-tasks');
    }

}

angular.module('app').component('process', new ProcessComponent());