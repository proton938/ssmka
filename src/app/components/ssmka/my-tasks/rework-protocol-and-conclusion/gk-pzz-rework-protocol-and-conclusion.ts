class GkPzzReworkProtocolAndConclusionController extends ReworkProtocolAndConclusionController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'protocolTaskService',
        'nsiRestService',
        'session',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'fileHttpService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private fileHttpService: oasiFileRest.FileHttpService) {
        super('Городская комиссия по вопросам градостроительной деятельности','Сформировать Протокол и Заключение ГК', true, true, 'Рекомендовать', 'Не рекомендовать', true,
            protocolResource, $stateParams, $q, $uibModal, toastr, protocolTaskService,
            nsiRestService, session, blockUI, dateUtils, approvalCycleUtils, fileHttpService);
    }

}

class GkPzzReworkProtocolAndConclusion {
    public controller: any = GkPzzReworkProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/rework-protocol-and-conclusion/rework-protocol-and-conclusion.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzReworkProtocolAndConclusion', new GkPzzReworkProtocolAndConclusion());
