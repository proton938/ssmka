class GkEooReworkProtocolAndConclusionController extends ReworkProtocolAndConclusionController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'protocolTaskService',
        'nsiRestService',
        'session',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'fileHttpService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private fileHttpService: oasiFileRest.FileHttpService) {
        super('Городская комиссия по ЭОО', 'Сформировать Протокол', false, false, 'Утвердить', 'Доработать', false,
            protocolResource, $stateParams, $q, $uibModal, toastr, protocolTaskService,
            nsiRestService, session, blockUI, dateUtils, approvalCycleUtils, fileHttpService);
    }

    getUpdateProcessVars() {
        let stroyUser = this.approvalCycleUtils.getStroyUser(this.protocol.approval.approvalCycle).agreedBy.accountName;
        return [
            {name: "UkdUserVar", value: stroyUser}
        ];
    }

}

class GkEooReworkProtocolAndConclusion {
    public controller: any = GkEooReworkProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/rework-protocol-and-conclusion/rework-protocol-and-conclusion.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooReworkProtocolAndConclusion', new GkEooReworkProtocolAndConclusion());
