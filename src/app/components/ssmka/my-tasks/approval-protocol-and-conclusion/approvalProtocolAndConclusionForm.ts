class ApprovalProtocolAndConclusionController {
    private taskWrapper: any;
    private origProtocol: Protocol;
    private protocol: Protocol;

    private protocolDraftFiles: FileType[] = [];
    private conclusionDraftFiles: FileType[] = [];

    public checkSign: string;

    public clickMobileSign: boolean;

    public openProtocolDraftSignDialog: () => void;
    public openProtocolDraftSignDialogMobile: () => void;

    public openConclusionDraftSignDialog: () => void;
    public openConclusionDraftSignDialogMobile: () => void;

    private decisions: Decision[];
    private approvalStatuses: ProtocolApprovalStatus[];

    private questionsComments: QuestionCommentToAdd[];
    private comment: string;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(public meetingType: string,
                public showConclusion: boolean,
                public signConclusion: boolean,
                public signButtonTitle: string,
                private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private session: oasiSecurity.ISessionStorage,
                private dateUtils: IDateUtils,
                private blockUI: any,
                private approvalCycleUtils: ApprovalCycleUtils,
                private signService: oasiFileManager.SignService) {
    }

    $onInit() {
        this.questionsComments = [];
        this.loadDocument(this.$stateParams.documentId).then(() => {
            return this.initializeSecretaryResponsible();
        }).then(() => {
            return this.processDelegate();
        }).then(() => {
            return this.loadDictionaries();
        }).then(() => {
            return this.signService.checkSert();
        }).then((res) => {
            this.checkSign = res;
            this.protocolDraftFiles = [this.protocol.fileProtocolDraft];
            this.conclusionDraftFiles = [this.protocol.fileConclusionDraft];
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDocument(id: string): ng.IPromise<void> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => {
            this.protocol = angular.copy(data.document.protocol);
            this.origProtocol = angular.copy(data.document.protocol);
            return this.$q.resolve();
        });
    }

    processDelegate() {
        let responsibleSecretary = this.approvalCycleUtils.getResponsibleSecretary(this.protocol.approval.approvalCycle);
        if (this.session.login() === responsibleSecretary.agreedBy.accountName) {
            return this.$q.resolve();
        }
        responsibleSecretary.agreedBy = {
            accountName: this.session.login(),
            phone: this.session.telephoneNumber(),
            post: this.session.post(),
            iofShort: this.session.iofShort(),
            fioShort: this.session.fioShort(),
            fioFull: this.session.fullName()
        };
        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            return this.loadDocument(this.protocol.protocolID);
        })
    }

    initializeSecretaryResponsible(): ng.IPromise<void> {
        if (this.protocol.secretaryResponsible) {
            return this.$q.resolve();
        }
        this.protocol.secretaryResponsible = {
            post: this.session.post(),
            iofShort: this.session.iofShort(),
            fioFull: this.session.fullName(),
            accountName: this.session.login()
        };
        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            return this.loadDocument(this.protocol.protocolID);
        });
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]}),
            this.nsiRestService.get('OasiStatusResultProtocolPM')
        ]).then((result: [Decision[], ProtocolApprovalStatus[]]) => {
            this.decisions = result[0];
            this.approvalStatuses = result[1];
        })
    }

    submitHandler() {
        if (this.clickMobileSign) {
            this.openProtocolDraftSignDialogMobile();
        } else {
            this.openProtocolDraftSignDialog();
        }
    }

    beforeProtocolDraftSign = function (fileInfo, folderGuid, certEx): ng.IPromise<any> {
        let ds = this.getDs(certEx);
        this.protocol.dsProtocolSecretaryResponsible = ds;
        if (this.signConclusion) {
            this.protocol.dsConclusionSecretaryResponsible = ds;
        }
        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            return fileInfo;
        })
    }.bind(this);

    afterProtocolDraftSign = function (result: { error?: any, result?: { cert: oasiCryptopro.ICertificateInfo, certEx: oasiCryptopro.ICertificateInfoEx, guid: string } }) {
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.protocol.fileProtocolDraft.signed = true;

        if (this.signConclusion) {
            if (this.clickMobileSign) {
                this.openConclusionDraftSignDialogMobile();
            } else {
                this.openConclusionDraftSignDialog();
            }
        } else {
            this.saveAndFinishTask();
        }
    }.bind(this);

    afterConclusionDraftSign = function (result: { error?: any, result?: { cert: oasiCryptopro.ICertificateInfo, certEx: oasiCryptopro.ICertificateInfoEx, guid: string } }) {
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.protocol.fileConclusionDraft.signed = true;

        this.saveAndFinishTask();
    }.bind(this);

    saveAndFinishTask() {
        let block = this.blockUI.instances.get('approvalProtocolAndConclusion');
        block.start();

        this.protocol.commentSecretaryResponsible = this.protocol.commentSecretaryResponsible || [];
        this.protocol.commentSecretaryResponsible.push({
            commentText: this.comment,
            commentDate: new Date().toISOString()
        });

        let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.APPROVED);

        this.applyResult(status, true).then(() => {
            this.toastr.success('Электронные документы успешно созданы и подписаны');
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, this.getUpdateProcessVars(true));
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    getDs(certificateEx): DsType {
        let lastName = '';
        let firstName = '';

        if (certificateEx.lastName === '' && certificateEx.firstName === '') {
            lastName = certificateEx.subject
        } else {
            lastName = certificateEx.lastName;
            firstName = certificateEx.firstName;
        }

        return {
            dsLastName: lastName,
            dsName: firstName,
            dsPosition: certificateEx.position,
            dsCN: certificateEx.serialNumber,
            dsFrom: moment(certificateEx.from, 'DD.MM.YYYY HH:mm:ss').startOf('day').toDate(),
            dsTo: moment(certificateEx.till, 'DD.MM.YYYY HH:mm:ss').startOf('day').toDate()
        }
    }

    sendBackHandler() {
        let block = this.blockUI.instances.get('approvalProtocolAndConclusion');
        block.start();

        this.protocol.commentSecretaryResponsible = this.protocol.commentSecretaryResponsible || [];
        this.protocol.commentSecretaryResponsible.push({
            commentText: this.comment,
            commentDate: new Date().toISOString()
        });

        let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.REJECTED);

        this.applyResult(status, false, this.comment, this.questionsComments).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, this.getUpdateProcessVars(false));
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    getUpdateProcessVars(approve: boolean) {
        let result: any[] = [{name: "ProtocolAndConclusionSigned", value: approve}];
        if (approve && this.protocol.secretaryResponsible) {
            result.push({name: "responsSecretaryVar", value: this.protocol.secretaryResponsible.accountName});
        }
        return result;
    }

    private applyResult(status: ProtocolApprovalStatus, signOfApproval: boolean, approvalNote?: string, questionsComments?: QuestionCommentToAdd[]) {
        let agreed = this.approvalCycleUtils.getResponsibleSecretary(this.protocol.approval.approvalCycle);
        agreed.approvalResult = status.name;
        agreed.approvalResultCode = status.code;
        agreed.approvalResultColor = status.color;
        if (approvalNote) {
            agreed.approvalNote = approvalNote;
        }
        agreed.approvalFactDate = new Date();

        this.protocol.approval.approvalCycle.signOfApproval = signOfApproval;

        if (questionsComments) {
            questionsComments.filter(q => q.questionNumber && q.comment).forEach(comment => {
                let question = _.find(this.protocol.question, q => q.questionNumber === comment.questionNumber);
                question.comment = question.comment || [];
                question.comment.push(this.createComment(comment.comment));
            });
            agreed.approvalComment = questionsComments.map(c => ProtocolApprovalCycleAgreedComment.fromComment(c));
        }

        if (!signOfApproval) {
            let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code = ProtocolApprovalStatus.WAIT);
            Protocol.closeApprovalCycle(this.protocol, waitApprovalStatus);
        }
        return this.saveProtocol(this.origProtocol, this.protocol);
    }

    private createComment(commentText: string): ProtocolQuestionComment {
        let comment = new ProtocolQuestionComment();
        comment.commentDate = new Date();
        comment.login = this.session.login();
        comment.personFio = this.session.fullName();
        comment.post = this.session.post();
        comment.note = commentText;
        return comment;
    }

    hasQuestionsComments() {
        return this.questionsComments.filter(q => q.questionNumber && q.comment).length > 0;
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.protocol.protocolID}, diff).$promise;
    }
}

class GkPzzApprovalProtocolAndConclusionController extends ApprovalProtocolAndConclusionController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'nsiRestService',
        'toastr',
        'protocolTaskService',
        'signService',
        'session',
        'dateUtils',
        'blockUI',
        'approvalCycleUtils'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private signService: ISignService,
                private session: oasiSecurity.ISessionStorage,
                private dateUtils: IDateUtils,
                private blockUI: any,
                private approvalCycleUtils: ApprovalCycleUtils) {
        super('Городская комиссия по вопросам градостроительной деятельности', true, true, 'Утвердить Протокол и Заключение ГК',
            protocolResource, $stateParams, $q, $uibModal, nsiRestService, toastr, protocolTaskService,
            session, dateUtils, blockUI, approvalCycleUtils, signService);
    }
}

class GkPzzApprovalProtocolAndConclusionForm {
    public controller: any = GkPzzApprovalProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/approval-protocol-and-conclusion/approvalProtocolAndConclusionForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzApprovalProtocolAndConclusionForm', new GkPzzApprovalProtocolAndConclusionForm());


class GkEooApprovalProtocolAndConclusionController extends ApprovalProtocolAndConclusionController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'nsiRestService',
        'toastr',
        'protocolTaskService',
        'signService',
        'session',
        'dateUtils',
        'blockUI',
        'approvalCycleUtils'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private signService: ISignService,
                private session: oasiSecurity.ISessionStorage,
                private dateUtils: IDateUtils,
                private blockUI: any,
                private approvalCycleUtils: ApprovalCycleUtils) {
        super('Городская комиссия по ЭОО', false, false, 'Утвердить протокол ЭОО ГК',
            protocolResource, $stateParams, $q, $uibModal, nsiRestService, toastr, protocolTaskService,
            session, dateUtils, blockUI, approvalCycleUtils, signService);
    }

    getUpdateProcessVars(approve: boolean) {
        return [{ name: "IsApproved", value: approve }];
    }
}

class GkEooApprovalProtocolAndConclusionForm {
    public controller: any = GkEooApprovalProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/approval-protocol-and-conclusion/approvalProtocolAndConclusionForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooApprovalProtocolAndConclusionForm', new GkEooApprovalProtocolAndConclusionForm());
