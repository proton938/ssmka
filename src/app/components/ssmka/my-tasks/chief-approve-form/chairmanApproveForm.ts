declare var cadesplugin: any;

class ChiefApproveFormController {
    private taskWrapper: any;
    private origProtocol: Protocol;
    private protocol: Protocol;
    private agendaQuestions: AgendaQuestion[];

    private protocolDraftFiles: FileType[] = [];
    private conclusionDraftFiles: FileType[] = [];

    private folderGuid;

    public checkSign: string;

    public clickMobileSign: boolean;

    public openProtocolDraftSignDialog: () => void;
    public openProtocolDraftSignDialogMobile: () => void;

    public openConclusionDraftSignDialog: () => void;
    public openConclusionDraftSignDialogMobile: () => void;

    public protocolFile: FileType;
    public conclusionFile: FileType;

    private decisions: Decision[];
    private approvalStatuses: ProtocolApprovalStatus[];
    private protocolStatuses: ProtocolStatus[];
    private templates: Template[];

    private questionsComments: QuestionCommentToAdd[];
    private comment: string;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(public meetingType: string,
                public showConclusion: boolean,
                public signConclusion: boolean,
                public signButtonTitle: string,
                private protocolResource: IProtocolResource,
                private agendaResource: IAgendaResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private blockUI: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private mggtService: MggtService,
                private approvalCycleUtils: ApprovalCycleUtils,
                private dateUtils: IDateUtils,
                private session: any,
                private wordReporterRestService: oasiWordreporterRest.WordReporterRestService,
                private signService: oasiFileManager.SignService) {
    }

    $onInit() {
        this.questionsComments = [];
        this.loadDocument(this.$stateParams.documentId).then(() => {
            return this.processDelegate();
        }).then(() => {
            return this.agendaResource.includedQuestions({id: this.protocol.agendaID});
        }).then(questions => {
            this.agendaQuestions = questions;
            return this.loadDictionaries();
        }).then(() => {
            return this.signService.checkSert();
        }).then((res) => {
            this.checkSign = res;

            this.protocolDraftFiles = [this.protocol.fileProtocolDraft];
            this.conclusionDraftFiles = [this.protocol.fileConclusionDraft];

            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDocument(id: string): ng.IPromise<void> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => {
            this.protocol = angular.copy(data.document.protocol);
            this.origProtocol = angular.copy(data.document.protocol);
            return this.$q.resolve();
        });
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]}),
            this.nsiRestService.get('OasiStatusResultProtocolPM'),
            this.nsiRestService.get('OasiStatusProtocolPM'),
            this.nsiRestService.get('Templates'),
            this.nsiRestService.get('MeetingFilenetSettings')
        ]).then((result: [Decision[], ProtocolApprovalStatus[], ProtocolStatus[], Template[], MeetingFilenetSettings[]]) => {
            this.decisions = result[0];
            this.approvalStatuses = result[1];
            this.protocolStatuses = result[2];
            this.templates = result[3];
            this.folderGuid = _.find(result[4], f => f.meetingType === this.protocol.meetingType).protocolFolderGuid;
        })
    }

    processDelegate() {
        let chairman = this.approvalCycleUtils.getChairman(this.protocol.approval.approvalCycle);
        if (this.session.login() === chairman.agreedBy.accountName) {
            return this.$q.resolve();
        }
        chairman.agreedBy = {
            accountName: this.session.login(),
            phone: this.session.telephoneNumber(),
            post: this.session.post(),
            iofShort: this.session.iofShort(),
            fioShort: this.session.fioShort(),
            fioFull: this.session.fullName()
        };
        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            return this.loadDocument(this.protocol.protocolID);
        })
    }

    submitHandler() {
        if (this.clickMobileSign) {
            this.openProtocolDraftSignDialogMobile();
        } else {
            this.openProtocolDraftSignDialog();
        }
    }

    beforeProtocolDraftSign = function (fileInfo, folderGuid, certEx): ng.IPromise<any> {
        let template = _.find(this.templates, t => t.meetingType.some(mt => mt.meetingType === this.protocol.meetingType));
        let protocolName = `ЭО Протокол ${template.meetingType[0].meetingShortName} № ${this.protocol.meetingNumber} от ${this.dateUtils.formatDate(this.protocol.meetingDate, 'dd.MM.yyyy')}`;
        let fileName = protocolName + '.docx';

        let ds = this.getDs(certEx);
        this.protocol.dsProtocolChairman = ds;
        if (this.signConclusion) {
            this.protocol.dsConclusionChairman = ds;
        }

        this.protocol.commentChairman = this.protocol.commentChairman || [];
        this.protocol.commentChairman.push({
            commentText: this.comment,
            commentDate: new Date().toISOString()
        });

        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            return this.wordReporterRestService.filenet2Filenet({
                options: {
                    jsonSourceDescr: protocolName,
                    onProcess: "Create Protocol Report Docx",
                    placeholderCode: "sn",
                    strictCheckMode: true,
                    xwpfResultDocumentDescr: fileName,
                    xwpfTemplateDocumentDescr: ""
                },
                jsonTxt: JSON.stringify({
                    document: {
                        protocol: this.protocol
                    }
                }),
                rootJsonPath: "$",
                filenetTemplate: {
                    templateVersionSeriesGuid: fileInfo.idFile
                },
                filenetDestination: {
                    reportFileName: fileName,
                    reportFileType: template.meetingProtocolClass,
                    reportFolderGuid: folderGuid
                }
            })
        }).then((result) => {
            return this.convertToPdf(folderGuid, result.versionSeriesGuid, result.fileName, result.fileType);
        }).then(file => {
            this.protocolFile = file;
            return file;
        });

    }.bind(this);

    afterProtocolDraftSign = function (result: { error?: any, result?: { cert: oasiCryptopro.ICertificateInfo, certEx: oasiCryptopro.ICertificateInfoEx, guid: string } }) {
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.protocolFile.signed = true;
        if (this.signConclusion) {
            if (this.clickMobileSign) {
                this.openConclusionDraftSignDialogMobile();
            } else {
                this.openConclusionDraftSignDialog();
            }
        } else {
            this.saveAndFinishTask();
        }
    }.bind(this);

    beforeConclusionDraftSign = function (fileInfo, folderGuid, certEx): ng.IPromise<any> {
        let template = _.find(this.templates, t => t.meetingType.some(mt => mt.meetingType === this.protocol.meetingType));
        let protocolName = `ЭО Заключение ГК ${template.meetingType[0].meetingShortName} № ${this.protocol.meetingNumber} от ${this.dateUtils.formatDate(this.protocol.meetingDate, 'dd.MM.yyyy')}`;
        let fileName = protocolName + '.docx';

        return this.wordReporterRestService.filenet2Filenet({
            options: {
                jsonSourceDescr: protocolName,
                onProcess: "Create Protocol Report Pdf",
                placeholderCode: "sn",
                strictCheckMode: true,
                xwpfResultDocumentDescr: fileName,
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({
                document: {
                    protocol: this.protocol
                }
            }),
            rootJsonPath: "$",
            filenetTemplate: {
                templateVersionSeriesGuid: fileInfo.idFile
            },
            filenetDestination: {
                reportFileName: fileName,
                reportFileType: template.meetingProtocolClass,
                reportFolderGuid: folderGuid
            }
        }).then((result) => {
            return this.convertToPdf(folderGuid, result.versionSeriesGuid, result.fileName, result.fileType);
        }).then(file => {
            this.conclusionFile = file;
            return file;
        });
    }.bind(this);

    afterConclusionDraftSign = function (result: { error?: any, result?: { cert: oasiCryptopro.ICertificateInfo, certEx: oasiCryptopro.ICertificateInfoEx, guid: string } }) {
        if (result.error) {
            console.error(result.error);
            return;
        }
        this.conclusionFile.signed = true;

        this.saveAndFinishTask();
    }.bind(this);

    private convertToPdf(folderGuid: string, idFile: string, nameFile: string, typeFile: string): ng.IPromise<FileType> {
        let fileExtension = nameFile.substring(nameFile.lastIndexOf(".") + 1);
        let convertRequest = {
            pdfFileName: nameFile.replace(`.${fileExtension}`, '.pdf'),
            pdfFileType: typeFile,
            pdfFolderGuid: folderGuid,
            wordVersionSeriesGuid: idFile
        };
        return this.wordReporterRestService.word2pdf(convertRequest).then(result => {
            let file: FileType = FileType.create(result.versionSeriesGuid, result.fileName, "" + result.fileSize,
                false, new Date(), result.mimeType, result.fileType);
            return this.$q.resolve(file);
        });
    }

    saveAndFinishTask() {
        let block = this.blockUI.instances.get('chairmanApprove');
        block.start();

        this.protocol.fileProtocol = this.protocol.fileProtocol || [];
        this.protocol.fileProtocol = this.protocol.fileProtocol.filter(f => f.idFile !== this.protocolFile.idFile);
        this.protocol.fileProtocol = this.protocol.fileProtocol.filter(f => f.classFile !== this.protocolFile.classFile);
        this.protocol.fileProtocol.push(this.protocolFile);

        if (this.signConclusion) {
            this.protocol.fileConclusion = this.conclusionFile;
        }

        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.success('Электронные документы успешно созданы и подписаны');
            return this.protocolResource.sendDecision({
                id: this.protocol.protocolID
            });
        }).then(() => {
            this.protocol.protocolApproved = true;
            this.protocol.protocolApprovedDate = new Date();
            this.protocol.status = _.find(this.protocolStatuses, st => st.code === ProtocolStatus.APPROVED);
            let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.APPROVED);
            return this.applyResult(status, true);
        }).then(() => {
            return this.mggtService.sendProtocolAgendaAndQuestions(this.protocol, this.protocol.agendaID, this.agendaQuestions.map(q => q.questionId));
        }).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, this.getUpdateProcessVars(true))
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    getUpdateProcessVars(approve: boolean) {
        return [{name: "ProtocolAndConclusionSigned", value: approve}];
    }

    getDs(certificateEx) {
        let lastName = '';
        let firstName = '';

        if (certificateEx.lastName === '' && certificateEx.firstName === '') {
            lastName = certificateEx.subject
        } else {
            lastName = certificateEx.lastName;
            firstName = certificateEx.firstName;
        }

        return {
            dsLastName: lastName,
            dsName: firstName,
            dsPosition: certificateEx.position,
            dsCN: certificateEx.serialNumber,
            dsFrom: moment(certificateEx.from, 'DD.MM.YYYY HH:mm:ss').startOf('day').toDate(),
            dsTo: moment(certificateEx.till, 'DD.MM.YYYY HH:mm:ss').startOf('day').toDate()
        }
    }

    sendBackHandler() {
        let block = this.blockUI.instances.get('chairmanApprove');
        block.start();

        this.protocol.commentSecretaryResponsible = this.protocol.commentSecretaryResponsible || [];
        this.protocol.commentSecretaryResponsible.push({
            commentText: this.comment,
            commentDate: new Date().toISOString()
        });

        this.protocol.status = _.find(this.protocolStatuses, st => st.code === ProtocolStatus.REVISION);

        let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.REJECTED);
        this.applyResult(status, false, this.comment, this.questionsComments).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, this.getUpdateProcessVars(false));
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    private applyResult(status: ProtocolApprovalStatus, signOfApproval: boolean, approvalNote?: string, questionsComments?: QuestionCommentToAdd[]) {
        let agreed = this.approvalCycleUtils.getChairman(this.protocol.approval.approvalCycle);
        agreed.approvalResult = status.name;
        agreed.approvalResultCode = status.code;
        agreed.approvalResultColor = status.color;
        if (approvalNote) {
            agreed.approvalNote = approvalNote;
        }
        agreed.approvalFactDate = new Date();

        this.protocol.approval.approvalCycle.signOfApproval = signOfApproval;

        if (questionsComments) {
            questionsComments.filter(q => q.questionNumber && q.comment).forEach(comment => {
                let question = _.find(this.protocol.question, q => q.questionNumber === comment.questionNumber);
                question.comment = question.comment || [];
                question.comment.push(this.createComment(comment.comment));
            });
            agreed.approvalComment = questionsComments.map(c => ProtocolApprovalCycleAgreedComment.fromComment(c));
        }

        if (!signOfApproval) {
            let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code = ProtocolApprovalStatus.WAIT);
            Protocol.closeApprovalCycle(this.protocol, waitApprovalStatus);
        }
        return this.saveProtocol(this.origProtocol, this.protocol);
    }

    private createComment(commentText: string): ProtocolQuestionComment {
        let comment = new ProtocolQuestionComment();
        comment.commentDate = new Date();
        comment.login = this.session.login();
        comment.personFio = this.session.fullName();
        comment.post = this.session.post();
        comment.note = commentText;
        return comment;
    }

    hasQuestionsComments() {
        return this.questionsComments.filter(q => q.questionNumber && q.comment).length > 0;
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.protocol.protocolID}, diff).$promise.then(wrapper => {
            this.protocol = angular.copy(wrapper.document.protocol);
            this.origProtocol = angular.copy(wrapper.document.protocol);
            return this.$q.resolve();
        });
    }

}

class GkPzzChiefApproveFormController extends ChiefApproveFormController {
    static $inject = [
        'protocolResource',
        'agendaResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'blockUI',
        'protocolTaskService',
        'nsiRestService',
        'mggtService',
        'signService',
        'approvalCycleUtils',
        'dateUtils',
        'session',
        'wordReporterRestService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private agendaResource: IAgendaResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private blockUI: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private mggtService: MggtService,
                private signService: ISignService,
                private approvalCycleUtils: ApprovalCycleUtils,
                private dateUtils: IDateUtils,
                private session: any,
                private wordReporterRestService: oasiWordreporterRest.WordReporterRestService) {
        super('Городская комиссия по вопросам градостроительной деятельности', true, true, 'Утвердить Протокол и Заключение ГК', protocolResource, agendaResource, $stateParams, $q, $uibModal, toastr, blockUI, protocolTaskService,
            nsiRestService, mggtService, approvalCycleUtils, dateUtils, session, wordReporterRestService, signService);
    }
}

class GkPzzChiefApproveFormComponent {
    public controller: any = GkPzzChiefApproveFormController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/chief-approve-form/chairmanApproveForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzChairmanApproveForm', new GkPzzChiefApproveFormComponent());


class GkEooChiefApproveFormController extends ChiefApproveFormController {
    static $inject = [
        'protocolResource',
        'agendaResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'blockUI',
        'protocolTaskService',
        'nsiRestService',
        'mggtService',
        'signService',
        'approvalCycleUtils',
        'dateUtils',
        'session',
        'wordReporterRestService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private agendaResource: IAgendaResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private blockUI: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private mggtService: MggtService,
                private signService: ISignService,
                private approvalCycleUtils: ApprovalCycleUtils,
                private dateUtils: IDateUtils,
                private session: any,
                private wordReporterRestService: oasiWordreporterRest.WordReporterRestService) {
        super('Городская комиссия по ЭОО', false, false, 'Утвердить протокол ЭОО ГК',
            protocolResource, agendaResource, $stateParams, $q, $uibModal, toastr, blockUI, protocolTaskService,
            nsiRestService, mggtService, approvalCycleUtils, dateUtils, session, wordReporterRestService, signService);
    }

    getUpdateProcessVars(approve: boolean) {
        return [{name: "IsApproved", value: approve}];
    }
}

class GkEooChiefApproveFormComponent {
    public controller: any = GkEooChiefApproveFormController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/chief-approve-form/chairmanApproveForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooChairmanApproveForm', new GkEooChiefApproveFormComponent());
