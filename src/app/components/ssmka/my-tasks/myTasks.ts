class MyTasksController {
    static $inject = [
        'toastr', 
        'session',
        '$rootScope',
        'myTasksService',
        'protocolResource',
        '$uibModal', 
        '$state'
    ];
    
    private tasks: IProtocolTask[] = [];
    private numberOfTasks: number;
    private tasksLoadStatus: LoadingStatus = LoadingStatus.LOADING;    
    
    sortName: string = 'dueDate';
    sortReverse: boolean = false;
    currentDate: any = new Date();
    currentDateTime: any;    
    
    viewType: ViewType;

    constructor(
        private toastr: Toastr,
        private session: oasiSecurity.ISessionStorage,
        private $rootScope: ng.IRootScopeService,
        private myTasksService: MyTasksService,
        private protocolResource: IProtocolResource,
        private $uibModal: angular.ui.bootstrap.IModalService,
        private $state: ng.ui.IStateService
    ) {
        this.viewType = new ViewType('LIST',
            [new ViewValue('LIST', 'списком', 'fa fa-list'), new ViewValue('GROUP', 'по задачам', 'fa fa-indent')]);
    }

    $onInit() {
        this.currentDateTime = this.currentDate.getTime();
        this.$rootScope['title'] = "Мои задачи";
        
        this.myTasksService.getAllTaskWithDelegated(this.session.login()).then(tasks => {
            this.tasks = tasks;
            this.numberOfTasks = this.tasks.length;
            this.$rootScope.$broadcast('updateMyTasksCount', {
            	count: this.numberOfTasks
            });
            if (this.tasks.length !== 0) {
            	let promises = [];
            	_.forEach(this.tasks, t => {
            		t.taskDescription = <string>_.find(t.variables, v => {
            			return v.name === 'EntityDescriptionVar'
            		}).value;
            	});
            	this.tasksLoadStatus = LoadingStatus.SUCCESS;
            } else {
            	this.tasksLoadStatus = LoadingStatus.SUCCESS;
            }
        }).catch(error => {
            this.tasksLoadStatus = LoadingStatus.ERROR;
            console.error(error);
        });       
        
    }
    
    private goToTask(task: IProtocolTask) {
        if (task.formKey) {
            //todo add .process
        	let stateName = `app.ssmka.process.${task.formKey}`;
            this.$state.go(stateName, {
            	formKey: task.formKey,
                documentId: this.getDocumentId(task),
                taskId: task.id
            });
        } else {
            this.toastr.error('Задача является автоматической!');
        }
    }
    
	  sortBy(propName: string, ascOrder: boolean) {
	    let _ascOrder = ascOrder ? 'asc' : 'desc';
	    this.tasks = _.orderBy(this.tasks, [propName], [_ascOrder]);
	  }
    
    getDocumentId(task: IProtocolTask): string {
		if (task.variables && _.isArray(task.variables)) {
			let variables: any[] = <any[]> task.variables;
			let idVar = _.find(variables, variable => {
				return variable.name && variable.name === "EntityIdVar";
			})
			return idVar ? idVar.value : null;
		} else {
			return null;
		}
    }

}

class MyTasksComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MyTasksController;
        this.templateUrl = 'app/components/ssmka/my-tasks/myTasks.html';
        this.bindings = {};
    }
}

angular.module('app').component('myTasks', new MyTasksComponent());
