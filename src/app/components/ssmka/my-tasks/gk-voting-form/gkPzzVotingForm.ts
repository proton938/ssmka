class GkPzzVotingFormController extends GkVotingFormController {
    static $inject = [
        '$q',
        '$sce',
        '$state',
        '$stateParams',
        '$scope',
        '$stomp',
        '$uibModal',
        '$injector',
        'protocolResource',
        'protocolTaskService',
        'ssAlertService',
        'alertService',
        'fileResource',
        'session',
        '$timeout',
        '$interval',
        'toastr',
        'base_url',
        'dateUtils',
        '$window',
        'ssAuthorizationService'
    ];

    constructor(public $q: ng.IQService, private $sce: any, private $state: any, private $stateParams: any, private $scope: any,
                private $stomp: any, private $uibModal: any, private $injector: any,
                public protocolResource: IProtocolResource,
                private protocolTaskService: IProtocolTaskService, private ssAlertService: IAlertService, private alertService: oasiWidgets.IAlertService,
                private fileResource: IFileResource, private session: oasiSecurity.ISessionStorage,
                public $timeout: any, private $interval: any, private toastr: Toastr, private baseUrl: string,
                private dateUtils: IDateUtils, private $window: any, private authorizationService: IAuthorizationService) {
        super('Городская комиссия по вопросам градостроительной деятельности', 'pzzVotingResource', () => {
                return this.authorizationService.check('OASI_MEETING_VIEW_PZZ_VOTING_RESULTS') ||
                    this.authorizationService.check('OASI_MEETING_PERFORM_GK_PZZ_VOTING')
            }, 'Рекомендовать', 'Не рекомендовать',
            $q, $sce, $state, $stateParams, $scope, $stomp, $uibModal, $injector, protocolResource,
            protocolTaskService, ssAlertService, alertService, fileResource, session,
            $timeout, $interval, toastr, baseUrl, dateUtils, $window);
    }

}

class GkPzzVotingFormComponent {
    public controller: any = GkPzzVotingFormController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/gk-voting-form/gkVotingForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzVotingForm', new GkPzzVotingFormComponent());
