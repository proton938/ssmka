class GkVotingFormQuestionVoteController {
    static $inject = [
    	'$uibModalInstance',
        'ssAlertService',
        'voteYesTitle',
        'voteNoTitle'
    ];
    
    private resultsVoteCode: string;
    
    constructor(
    	private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance, private alertService: IAlertService,
        private voteYesTitle: string, private voteNoTitle: string
    ) {
    }

    voteRadioPressHandler(value) {
        this.resultsVoteCode = value;
        this.alertService.confirm({
            className: 'our-modal--second our-voting-confirm-modal',
            message: 'После сохранения, решение по вопросу нельзя будет изменить. Вы уверены в своем решении?',
            ok: {
                label: 'Да',
                className: 'btn-primary'
            }
        }).then(() => {
            this.$uibModalInstance.close(this.resultsVoteCode);
        });
    }
    
    cancel() {
        this.$uibModalInstance.dismiss();
    }    
    
}

angular.module('app').controller('GkVotingFormQuestionVoteController', GkVotingFormQuestionVoteController);