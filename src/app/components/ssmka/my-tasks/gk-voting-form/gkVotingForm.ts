class GkVotingFormController {

    votingResource: IVotingResource;

    private taskWrapper: any;
    private taskInfo : any = {
        description: '',
        meetingDate: null
    };
    private protocol: Protocol;
    private ballotQuestions: { [key: string]: BallotPaperQuestion };
    private ballotPaper: BallotPaper;
    private notVoted: {[key: string]: string[]} = {};
    private voters: {[key: string]: any[]} = {};
    private loadingStatus = LoadingStatus.LOADING;

    private wsConnectNumber = 0;
    private maxWsReconnectNumber = 5;
    private wsReconnectNumber = 0;
    private wsRetrying = false;

    private updateAllQuestions: boolean = false;
    private updatedVotesQuestions: string[] = [];

    private creatingFile: boolean = false;
    private creatingFileForSign: boolean = false;
    private canViewVotingResults: boolean;
    private signingFile: boolean = false;
    private finishingVoting: boolean = false;
    private questionVoting: any = {};
    private showToActiveButton: boolean = false;

    private currentQuestionElemTop: number;
    private blockerInstance: any;

    private openSignDialog: () => void;

    constructor(
        public meetingType: string, public votingResourceId: string, public canViewVotingResults: () => boolean,
        public voteYesTitle: string, public voteNoTitle: string,
        public $q: ng.IQService, private $sce: any, private $state: any, private $stateParams: any, private $scope: any,
        private $stomp: any, private $uibModal: any, private $injector: any,
        public protocolResource: IProtocolResource,
        private protocolTaskService: IProtocolTaskService, private ssAlertService: IAlertService, private alertService: oasiWidgets.IAlertService,
        private fileResource: IFileResource, private session: oasiSecurity.ISessionStorage,
        public $timeout: any, private $interval: any, private toastr: Toastr, private baseUrl: string,
        private dateUtils: IDateUtils, private $window: any
    ) {
        this.votingResource = this.$injector.get(this.votingResourceId);
    }

    $onInit() {
        this.initData();
    }

    applyActiveQuestionId(activeQuestionId?: string) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.refreshCurrentResultsVoteIfNecessary().then(() => {
                    this.protocol.question.forEach(q => {
                        if (_.isBoolean(q.activity) && q.activity) {
                            q.activity = false;
                        }
                        if (activeQuestionId && activeQuestionId === q.questionID) {
                            q.activity = true;
                        }
                    });

                    let currentQuestion = document.getElementsByClassName('current-question')[0];
                    if (currentQuestion) {
                        this.currentQuestionElemTop = $(currentQuestion).offset().top;
                        this.updateShowActiveButtonVisibility();
                    }
                    else { //in case of element render takes too long (for too long lists on slow devices)
                        setTimeout(() => {
                            currentQuestion = document.getElementsByClassName('current-question')[0];
                            if (currentQuestion) {
                                this.currentQuestionElemTop = $(currentQuestion).offset().top;
                            }
                            this.updateShowActiveButtonVisibility()
                        }, 300);
                    }
                });


            });
        });
    };
    refreshCurrentResultsVoteIfNecessary(): ng.IPromise<void> {
        let currentActiveQuestion = _.find(this.protocol.question, q => q.activity);
        if (!currentActiveQuestion) {
            return this.$q.resolve();
        }
        return this.protocolResource.getResultsVote({
            id: this.protocol.protocolID,
            questionId: currentActiveQuestion.questionID
        }).$promise.then((resultsVote: ProtocolQuestionVoteResult) => {
            currentActiveQuestion.resultsVote = resultsVote;
        })
    }
    applyExcluded(exludedQuestionsIds: string[]) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.protocol.question.forEach(q => {
                    if (_.indexOf(exludedQuestionsIds, q.questionID) >= 0) {
                        q.excluded = true;
                    }
                });
            });
        });
    };
    applyMeetingTimeEnd(meetingTimeEnd: Date) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.protocol.meetingTimeEnd = meetingTimeEnd;
            });
        });
    };
    applyVotesUpdated(questionsIds: string[]) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                if (questionsIds.length) {
                    if (!this.updateAllQuestions) {
                        this.updatedVotesQuestions = _.union(this.updatedVotesQuestions ? this.updatedVotesQuestions : [], questionsIds);
                    }
                } else {
                    this.updatedVotesQuestions = [];
                    this.updateAllQuestions = true;
                }
            });
        })
    }
    handleSuccess() {
        let subscriptions = [];
        subscriptions.push(this.$stomp.subscribe('/user/queue/voting/' + this.protocol.protocolID + '/ballotPaper', (payload) => {
            this.$scope.$apply(() => {
                this.setBallotPaper(payload);
            })
        }, {}));
        subscriptions.push(this.$stomp.subscribe('/user/queue/voting/' + this.protocol.protocolID + '/deleted', () => {
            this.$scope.$apply(() => {
                this.toastr.warning('Голосование по вопросам Городской комиссии №' + this.protocol.meetingNumber + ' от ' + this.dateUtils.formatDate(this.taskInfo.meetingDate) + ' завершено.');
                console.log('asdad');
                this.$timeout(() => {
                    this.$window.location.href = this.$window.location.protocol + '//' + this.$window.location.host + '/oasi/#/app/my-tasks';
                }, 3000);
            })
        }, {}));
        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/active', (result: any) => {
            this.applyActiveQuestionId(result.active);
        }, {}));
        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/excluded', (exludedQuestionsIds: string[]) => {
            this.applyExcluded(exludedQuestionsIds);
        }, {}));
        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/end', (meetingTimeEnd: Date) => {
            this.applyMeetingTimeEnd(meetingTimeEnd);
        }, {}));
        if (this.canViewVotingResults) {
            subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/votesUpdated', (questionsIds: string[]) => {
                this.applyVotesUpdated(questionsIds);
            }, {}));
        }
        this.$scope.$on("$destroy", () => {
            subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
            this.$stomp.disconnect();
        });
        this.updateAllQuestions = true;
        this.updatedVotesQuestions = [];
        if (this.wsConnectNumber > 0) {
            this.loadDocument(this.protocol.protocolID).then((wrapper) => {
                const protocol = wrapper.document.protocol;
                this.protocol.meetingTimeEnd = protocol.meetingTimeEnd;
                for (let i = 0; i < this.protocol.question.length; i++) {
                    this.protocol.question[i] = _.extend(this.protocol.question[i],
                        _.pick(protocol.question[i], ['activity', 'excluded']));
                }
            })
        }
    }
    handleRetry(successCallback: (any) => void, errorCallback?: () => void) {
        if (this.wsConnectNumber > 0 && !this.wsRetrying) {
            this.toastr.warning("Произошла ошибка веб-сокет соединения, происходит попытка переподключения.");
        }
        this.wsRetrying = true;
        if (++this.wsReconnectNumber > this.maxWsReconnectNumber) {
            this.$timeout(() => {
                this.loadingStatus = LoadingStatus.ERROR;
                this.wsConnectNumber++;
                this.wsRetrying = false;
                if (this.wsConnectNumber > 0) {
                    this.toastr.error("Не удалось восстановить веб-сокет соединение.");
                } else {
                    this.toastr.error("Не удалось установить веб-сокет соединение.");
                }
                if (errorCallback) {
                    errorCallback();
                }
            })
        } else {
            this.$timeout(() => {
                this.wsConnect(successCallback, errorCallback);
            }, 2000);
        }
    }
    wsConnect(successCallback: (any) => void, errorCallback: () => void) {
        this.$stomp.connect(this.baseUrl + '/websocket', {}, () => {
            this.$scope.$apply(() => {
                this.handleRetry(successCallback, errorCallback);
            });
        }, {transports: ["websocket"]}).then((frame) => {
            this.wsRetrying = false;
            this.handleSuccess();
            successCallback(frame);
            this.wsReconnectNumber = 0;
            this.wsConnectNumber++;
        });
    }
    wsConnectPromise(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.wsConnect((frame) => {
            deferred.resolve(frame);
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }
    applyStatus(status: VotingStatus) {
        this.protocol.question.forEach(q => {
            if (_.has(status.votingResults, q.questionID)) {
                q.resultsVote = status.votingResults[q.questionID];
            }

            if (_.has(status.voters, q.questionID)) {
                this.voters[q.questionID] = status.voters[q.questionID];
            }
            if (this.canViewVotingResults && (_.has(status.votedUsers, q.questionID) || _.has(status.voters, q.questionID))) {
                const notVoted = this.voters[q.questionID].filter(voter => {
                    return status.votedUsers[q.questionID].indexOf(voter.accountName) < 0;
                });
                this.notVoted[q.questionID] = this.$sce.trustAsHtml(notVoted.map(user => `<div>${user.fioFull}</div>`).join(''));
            }
        });
    }
    updateQuestionsVoteStatuses() {
        if (!this.updateAllQuestions && !this.updatedVotesQuestions) {
            return;
        }
        const ids = angular.copy(this.updatedVotesQuestions);
        this.updatedVotesQuestions = null;
        this.updateAllQuestions = false;
        this.$timeout(() => {
            this.votingResource.getVotingStatus({protocolId: this.protocol.protocolID}, ids).$promise.then((status) => {
                this.applyStatus(status);
            })
        });
    }
    initData() {
        Promise.all([
            this.getTaskInfo(
                this.$stateParams.documentId,
                this.$stateParams.formKey,
                this.$stateParams.taskId
            )
        ]).then((responses) => {
            this.setTaskInfoData(responses[0]);
            return this.$q.resolve();
        }).then(() => {
            if (this.canViewVotingResults) {
                const task = this.$interval(() => {
                    this.updateQuestionsVoteStatuses();
                }, 3000);
                this.$scope.$on("$destroy", () => {
                    this.$interval.cancel(task);
                });
            }
            return this.$q.resolve();
        }).then(() => {
            return this.wsConnectPromise();
        }).then(() => {
            return this.votingResource.initBallot({protocolId: this.protocol.protocolID}, {}).$promise;
        }).then((ballotPaper) => {
            this.$timeout(() => {
                this.setBallotPaper(ballotPaper);
                this.loadingStatus = LoadingStatus.SUCCESS;

                this.updateShowActiveButtonVisibility();

                /*window.onscroll = () => { //subscribe to scroll event to display floating button
                    this.updateShowActiveButtonVisibility();
                    this.$scope.$apply();
                };*/
            });

        })
        .catch(() => {
            this.$scope.$apply(() => {
                this.loadingStatus = LoadingStatus.ERROR;
            })
        });
    };
    updateShowActiveButtonVisibility() {
        this.showToActiveButton = this.protocol.question.filter(question => question.activity === true).length > 0;
    }
    setBallotPaper(ballotPaper) {
    	this.ballotPaper = ballotPaper;
    	this.ballotQuestions = {};
    	_.each(ballotPaper.question, (question: any) => {
    		this.ballotQuestions[question.questionID] = question;
    	})
    }
    getTaskInfo(id: string, formKey: string, taskId: number): ng.IPromise<TaskInfo> {
        let deferred: ng.IDeferred<TaskInfo> = this.$q.defer();
        let taskInfo: TaskInfo = {
            document: null,
            task: null,
            taskParams: {
                formKey: formKey,
                documentId: id,
                taskId: taskId
            }
        };
        this.loadDocument(id).then((document) => {
            taskInfo.task = this.taskWrapper.task;
            taskInfo.document = document;
            deferred.resolve(taskInfo);
        });
        return deferred.promise;
    }
    loadDocument(id: string): ng.IPromise<ProtocolDocumentWrapper> {
        let deferred: ng.IDeferred<ProtocolDocumentWrapper> = this.$q.defer();

        this.protocolResource.getById({id: id}, (data: ProtocolDocumentWrapper) => {
            deferred.resolve(data);
        }, () => {
            deferred.reject();
        });

        return deferred.promise;
    }
    setTaskInfoData(taskInfo : TaskInfo) {
    	this.protocol = taskInfo.document.document.protocol;
        this.taskInfo.description = taskInfo.task.description ? taskInfo.task.description : this.taskInfo.description;
        this.taskInfo.meetingDate = taskInfo.document.document.protocol.meetingDate;
    }
    commentChanged(questionID: string) {
        let resultsVoteComment = this.ballotQuestions[questionID].resultsVote.resultsVoteComment;
        resultsVoteComment = resultsVoteComment || '';
    	this.votingResource.setQuestionComment({
    		protocolId: this.protocol.protocolID,
    		questionId: questionID
    	}, resultsVoteComment);
    }
    decisionChanged(questionID: string, resultsVote: string, resultsVoteCode: string) {
        this.ssAlertService.confirm({
            className: 'our-voting-confirm-modal',
            message: 'Ваше решение: ' + resultsVote + '. После сохранения, решение по вопросу нельзя будет изменить. Вы уверены в своем решении?',
            ok: {
                label: 'Да',
                className: 'btn-primary'
            }
        }).then(() => {
            this.blockerInstance = this.ssAlertService.blocker({
                message: 'Подождите, ваш голос обрабатывается'
            });
            return this.votingResource.setQuestionVote({
	    		protocolId: this.protocol.protocolID,
	    		questionId: questionID
	    	}, resultsVoteCode).$promise;
        }).then(() => {
            this.ballotQuestions[questionID].resultsVote.resultsVoteCode = resultsVoteCode;
            this.blockerInstance.close();
        }, () => {
        	this.ballotQuestions[questionID].resultsVote.resultsVoteCode = null;
            this.blockerInstance.close();
        });
    }
    voteOnQuestion(questionID: string) {
        this.$uibModal.open({
            templateUrl: 'app/components/ssmka/my-tasks/gk-voting-form/gkVotingFormQuestionVote.html',
            controller: 'GkVotingFormQuestionVoteController as $ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                voteYesTitle: () => {
                    return this.voteYesTitle;
                },
                voteNoTitle: () => {
                    return this.voteNoTitle;
                }
            }
        }).result.then((resultsVoteCode) => {
            this.blockerInstance = this.ssAlertService.blocker({
                message: 'Подождите, ваш голос обрабатывается'
            });
            this.questionVoting[questionID] = true;
		    this.votingResource.setQuestionVote({
                protocolId: this.protocol.protocolID,
                questionId: questionID
            }, resultsVoteCode).$promise.then(() => {
				this.ballotQuestions[questionID].resultsVote.resultsVoteCode = resultsVoteCode;
				this.questionVoting[questionID] = false;
                this.blockerInstance.close();
	        }, () => {
	        	this.questionVoting[questionID] = false;
                this.blockerInstance.close();
	        });
        });
    }
    scrollToActive() {
        const current = document.getElementsByClassName('current-question')[0] || document.getElementsByClassName('finishing-vote')[0];
        window.scroll(0, $(current).offset().top);
    }
    createFileHandler(sign: boolean) {
        let notVotedQuestions = this.getNotVotedQuestions();
        if ( notVotedQuestions && ( notVotedQuestions.length > 0 ) && !this.votingFinished() ) {
            let questionsNumbers = _.map(_.filter(notVotedQuestions, (question) => {
                return this.ballotQuestions[question.questionID].resultsVote.resultsVoteCode !== ballotPaperForbiddenQuestion;
            }), q => {
                return q.itemNumber;
            });
            if ( questionsNumbers.length ) {
                this.alertService.confirm({
                    message: `По следующим вопросам не приняты решения: ${questionsNumbers.join(', ')}. Продолжить формирование листа голосования?`,
                    okButtonText: 'Да',
                    type: 'btn-primary'
                }).then(() => {
                    this.createFile(sign);
                });
            } else {
                this.createFile(sign);
            }
        } else {
            this.createFile(sign);
        }
    }
    createFile( sign: boolean ) {
        this.creatingFile = !sign;
        this.creatingFileForSign = sign;

        this.votingResource.createBallotPaperFile({
            protocolId: this.protocol.protocolID,
            dsL: sign
        }, {}, (file) => {
            this.creatingFile = false;
            this.creatingFileForSign = false;
            this.ballotPaper.dsL = sign;
            this.ballotPaper.file = file;
        });
    }
    votingFinished() {
        return !!this.protocol.meetingTimeEnd;
    }
    // createFileForSign() {
		// this.creatingFileForSign = true;
		// this.votingResource.createBallotPaperFile({
		// 	protocolId: this.protocol.protocolID,
		// 	dsL: sign
		// }, {}, (file) => {
		// 	this.creatingFileForSign = false;
		// 	this.ballotPaper.dsL = sign;
		// 	this.ballotPaper.file = file;
		// });
    // }

    beforeSign(fileInfo, folderGuid, certEx) {
        return this.prepareBallotPaperSignature({
            dsLastName: certEx.lastName,
            dsName: certEx.firstName,
            dsPosition: certEx.position,
            dsCN: certEx.serialNumber,
            dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
            dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
        }).then((file) => {
            this.ballotPaper.file = file;
            return this.$q.resolve(file);
        });
    }

    private beforeSignBound = this.beforeSign.bind(this);

    afterSign(result: any) {
        if (result.error) {
            this.signingFile = false;
            return;
        }
        this.ballotPaper.file.signed = true;
        return this.uploadBallotPaperSignature().then((file) => {
            this.ballotPaper.file = file;
            return true;
        });
    }

    private afterSignBound = this.afterSign.bind(this);

    signFile() {
        this.signingFile = true;
        this.openSignDialog();
    }
    finishVoting() {
        if (this.ballotPaper.dsL && !this.ballotPaper.ds) {
            this.alertService.message({message: "Необходимо подписать лист голосования электронной подписью."});
            return;
        }
		this.finishingVoting = true;
		let email = this.session.mail();
		if (email) {
            this.votingResource.mailBallotPaperFile({
                protocolId: this.protocol.protocolID
            }, {}).$promise.then(() => {
                return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
            }).then(() => {
                this.finishingVoting = false;
                this.toastr.success('Лист голосования отправлен на email: ' + email)
                location.assign('/oasi/#/app/my-tasks');
            }, () => {
                this.finishingVoting = false;
            });
        } else {
		    this.alertService.message({
                message: "Не задана электронная почта для отправки Листа голосования. Для добавления данных, просьба, обратиться к администратору."
            }).then(() => {}, () => {
		      return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
            }).then(() => {
                this.finishingVoting = false;
                location.assign('/oasi/#/app/my-tasks');
            }, () => {
                this.finishingVoting = false;
            })
        }
    }
    prepareBallotPaperSignature(ds: BallotPaperDS): ng.IPromise<any> {
    	let deferred = this.$q.defer();
		this.votingResource.prepareBallotPaperSignature({
			protocolId: this.protocol.protocolID
		}, ds, (file) => {
		    this.ballotPaper.ds = ds;
			deferred.resolve(file);
		}, () => {
			deferred.reject();
		});
    	return deferred.promise;
    }
    uploadBallotPaperSignature(): ng.IPromise<any> {
    	let deferred = this.$q.defer();
		this.votingResource.uploadBallotPaperSignature({
			protocolId: this.protocol.protocolID
		}, {}, (file) => {
			deferred.resolve(file);
		}, () => {
			deferred.reject();
		});
    	return deferred.promise;
    }
    getNotVotedQuestions() {
    	return _.filter(this.ballotPaper.question, q => {
    		let resultsVoteCode = q.resultsVote.resultsVoteCode;
    		return !q.excluded && (_.isUndefined(resultsVoteCode) || _.isNull(resultsVoteCode) || resultsVoteCode === ballotPaperForbiddenQuestion);
    	})
    }
    isDecisionEnabled(ind: number, questionID: string) {
    	return this.isActiveQuestion(ind) && !this.ballotQuestions[questionID].resultsVote.resultsVoteCode;
    }
    isActiveQuestion(ind: number) {
    	return this.protocol.question[ind].activity === true;
    }
    isPassedQuestion(ind: number) {
    	return this.protocol.question[ind].activity === false;
    }
    isFutureQuestion(ind: number) {
        let activity = this.protocol.question[ind].activity;
        return _.isNull(activity) || _.isUndefined(activity);
    }
    isHasResults(ind: number) {
        let resultsVote = this.protocol.question[ind].resultsVote;
        return resultsVote.yes > 0 || resultsVote.no > 0;
    }
    voteRadioPressHandler(question: Question, resultsVote: string, resultsVoteCode: string) {
        if ( this.ballotQuestions[question.questionID].resultsVote.resultsVoteCode &&
             this.ballotQuestions[question.questionID].resultsVote.resultsVoteCode === ballotPaperForbiddenQuestion ) {
            this.toastr.error('Данный вопрос не относится к Вашей префектуре');
        }
        else {
            this.decisionChanged(question.questionID, resultsVote, resultsVoteCode);
        }
    }
    accountIsInVotersList() {
        let account = this.session.login(),
            voters = this.voters;

        return _.filter(voters, (question) => {
            if (_.find(question, {accountName: account})) {
                return question;
            }
        });
    }
}