class ProtocolAndConclusionController {
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    private origProtocol: Protocol;
    public protocol: Protocol;
    private refusalQuestions: Dictionary<RefusalQuestion>;
    private commentAuthor: string;
    private commentDate: string;
    private comment: string;
    private decisionTypes: Decision[];
    private approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];
    private decisions: Dictionary<Decision>;

    private dzOptions: any;
    private dzProtocolDraftCallbacks: any;
    private dzProtocolDraftMethods: any;
    private dzConclusionDraftCallbacks: any;
    private dzConclusionDraftMethods: any;

    private filesFolderGuid: string;


    private departmentsUsers: User[];
    private supervisingUsers: User[];
    private stroyUsers: User[];

    constructor(public meetingType: string, public generateButtonTitle: string,
                public generateConclusion: boolean, public showConclusion: boolean,
                public voteYesTitle: string, public voteNoTitle: string,
                public editableParameters: boolean,
                private $stateParams: any,
                public protocolResource: IProtocolResource,
                public $q: ng.IQService,
                public $timeout: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private fileResource: IFileResource,
                private protocolTaskService: IProtocolTaskService,
                private $state: any,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private toastr: any,
                private fileHttpService: oasiFileRest.FileHttpService) {
    }

    $onInit() {
        this.initData();
    }

    initData() {
        this.loadDocument(this.$stateParams.documentId).then(() => {
            if (this.commentBlockVisible()) {
                this.setCommentData(this.protocol);
            }
            return this.loadDictionaries();
        }).then(() => {
            this.decisions = {};
            this.protocol.question.forEach(q => {
                this.decisions[q.questionID] = _.find(this.decisionTypes, dt => dt.decisionTypeCode === q.decisionTypeCode);
                q.decisionText = q.decisionText || q.decisionDraft;
            });

            this.refusalQuestions = _.chain(this.protocol.refusalQuestion)
                .map(rq => rq.question).flatten().keyBy(q => q.questionNumber).value();

            this.protocol.approval = this.protocol.approval || new ProtocolApproval(1);
            if (!this.protocol.approval.approvalCycle.agreed.length) {
                let waitApprovalStatus = _.find(this.approvalStatuses, status => status.code === ProtocolApprovalStatus.WAIT);
                return this.initializeProtocolApprovalCycle(waitApprovalStatus).then(agreed => {
                    this.protocol.approval.approvalCycle.agreed = agreed;
                    return this.$q.resolve();
                });
            } else {
                return this.$q.resolve();
            }
        }).then(() => {
            this.initDropzones();
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    };

    loadDocument(id: string): ng.IPromise<void> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => {
            this.protocol = angular.copy(data.document.protocol);
            this.origProtocol = angular.copy(data.document.protocol);
            return this.$q.resolve();
        });
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]}),
            this.nsiRestService.get('Approval'),
            this.nsiRestService.get('OasiStatusResultProtocolPM'),
            this.nsiRestService.ldapUsers('OASI_MEETING_GK_PARTICIPANTS_APPROVED'),
            this.nsiRestService.ldapUsers('OASI_MEETING_GK_SUPERVISIONG_APPROVED'),
            this.nsiRestService.ldapUsers('OASI_MEETING_GK_STROY_APPROVED'),
            this.nsiRestService.get('MeetingFilenetSettings')
        ]).then((result: [Decision[], ApprovalType[], ProtocolApprovalStatus[], User[], User[], User[], MeetingFilenetSettings[]]) => {
            this.decisionTypes = result[0];
            this.approvalTypes = result[1];
            this.approvalStatuses = result[2];
            this.departmentsUsers = _.sortBy(result[3], u => u.displayName);
            this.supervisingUsers = _.sortBy(result[4], u => u.displayName);
            this.stroyUsers = _.sortBy(result[5], u => u.displayName);
            this.filesFolderGuid = _.find(result[6], f => f.meetingType === this.protocol.meetingType).protocolFolderGuid;
            return this.$q.resolve();
        })
    }

    initDropzones() {
        this.dzOptions = {
            autoProcessQueue: false,
            withCredentials: true,
            parallelUploads: 1,
            paramName: "file",
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимое расширение: docx',
            url: "/file"
        };
        this.dzProtocolDraftCallbacks = {
            addedfile: (file) => {
                let fileName: string = file.name;
                let match = /.*\.docx/.exec(fileName.toLowerCase());
                if (!match) {
                    this.toastr.warning("Допустимое расширение: docx", 'Ошибка');
                    this.dzProtocolDraftMethods.removeFile(file);
                    return;
                }

                const fd = new FormData();
                fd.append('file', file);
                fd.append('folderGuid', this.filesFolderGuid);
                fd.append('fileType', 'MkaDocProtPzzProject');
                this.fileHttpService.handleFileUpload(fd).then((resp) => {
                    this.protocol.fileProtocolDraft = {
                        idFile: resp.guid,
                        nameFile: file.name,
                        sizeFile: file.size,
                        signed: false,
                        dateFile: new Date(),
                        typeFile: 'MkaDocProtPzzProject',
                        mimeType: file.type
                    };
                    this.dzProtocolDraftMethods.removeFile(file);
                }, (error) => {
                    console.log(error);
                    this.toastr.warning(error, 'Ошибка');
                });
            }
        };
        this.dzConclusionDraftCallbacks = {
            addedfile: (file) => {
                let fileName: string = file.name;
                let match = /.*\.docx/.exec(fileName.toLowerCase());
                if (!match) {
                    this.toastr.warning("Допустимое расширение: docx", 'Ошибка');
                    this.dzConclusionDraftMethods.removeFile(file);
                    return;
                }

                const fd = new FormData();
                fd.append('file', file);
                fd.append('folderGuid', this.filesFolderGuid);
                fd.append('fileType', 'MkaDocProtPzzProject');
                this.fileHttpService.handleFileUpload(fd).then((resp) => {
                    this.protocol.fileConclusionDraft = {
                        idFile: resp.guid,
                        nameFile: file.name,
                        sizeFile: file.size,
                        signed: false,
                        dateFile: new Date(),
                        typeFile: 'MkaDocProtPzzProject',
                        mimeType: file.type
                    };
                    this.dzConclusionDraftMethods.removeFile(file);
                }, (error) => {
                    console.log(error);
                    this.toastr.warning(error, 'Ошибка');
                });
            }
        };
    }

    cleanFileProtocolDraft() {
        this.protocol.fileProtocolDraft = null;
    }

    cleanFileConclusionDraft() {
        this.protocol.fileConclusionDraft = null;
    }

    initializeProtocolApprovalCycle(approvalStatus: ProtocolApprovalStatus): ng.IPromise<ProtocolApprovalCycleAgreed[]> {
        let agreedApprovalType = _.find(this.approvalTypes, at => at.approvalTypeCode === ApprovalType.AGREED);
        let approvalApprovalType = _.find(this.approvalTypes, at => at.approvalTypeCode === ApprovalType.APPROVAL);

        let agreed = [];
        let departments = _.chain(this.protocol.question).filter(q => !q.excluded).map(q => q.departmentPrepareCode).filter(dep => dep)
            .uniq().filter(dep => dep !== 'MKA_DEP_LAW' && dep !== 'MKA_DEP_GP').concat('MKA_DEP_LAW', 'MKA_DEP_GP').value();
        departments.forEach((dep, index) => {
            let departmentUsers = this.departmentsUsers.filter(u => u.departmentCode === dep);
            if (!departmentUsers.length) {
                return;
            }
            const departmentApprovee = this.getDepartmentApprovee(departmentUsers);
            agreed.push(ProtocolApprovalCycleAgreed.createFrom("1." + (index + 1), agreedApprovalType, approvalStatus, departmentApprovee, dep));
        });
        agreed.push(ProtocolApprovalCycleAgreed.createFrom('2', agreedApprovalType, approvalStatus, this.getSupervisor(this.supervisingUsers)));
        agreed.push(ProtocolApprovalCycleAgreed.createFrom('3', agreedApprovalType, approvalStatus, this.getStroyUser(this.stroyUsers)));

        return this.$q.all([
            this.nsiRestService.ldapUser(this.protocol.secretaryResponsible.accountName),
            this.nsiRestService.ldapUser(this.protocol.approvedBy.accountName)
        ]).then((result: [User, User]) => {
            agreed.push(ProtocolApprovalCycleAgreed.createFrom('4', approvalApprovalType, approvalStatus, result[0]));
            agreed.push(ProtocolApprovalCycleAgreed.createFrom('5', approvalApprovalType, approvalStatus, result[1]));
            return this.$q.resolve(agreed);
        })
    }

    private getDepartmentApprovee(users: User[]): User {
        let chief = _.find(users, u => u.post === 'Начальник управления');
        return chief || _.first(users);
    }

    private getSupervisor(users: User[]): User {
        let suhov = _.find(users, u => u.accountName === 'suhov_au');
        return suhov || _.first(users);
    }

    private getStroyUser(users: User[]): User {
        return _.first(users);
    }

    setCommentData(protocol: Protocol) {
        let commentAuthor: ProtocolPerson = null;
        let secretaryCommentsLength: number = !!protocol.commentSecretaryResponsible ?
            protocol.commentSecretaryResponsible.length : null,
            chairmanCommentsLength: number = !!protocol.commentChairman ?
                protocol.commentChairman.length : null;
        let showSecretaryComment: boolean = false;
        if (secretaryCommentsLength && chairmanCommentsLength) {
            if (protocol.commentSecretaryResponsible[secretaryCommentsLength - 1].commentDate >
                protocol.commentChairman[chairmanCommentsLength - 1].commentDate) {
                commentAuthor = protocol.secretaryResponsible || protocol.secretary;
                showSecretaryComment = true;
            } else {
                commentAuthor = protocol.approvedBy;
            }
        } else if (secretaryCommentsLength) {
            commentAuthor = protocol.secretaryResponsible || protocol.secretary;
            showSecretaryComment = true;
        } else {
            commentAuthor = protocol.approvedBy;
        }
        let comment: ProtocolComment = showSecretaryComment ?
            protocol.commentSecretaryResponsible[secretaryCommentsLength - 1] :
            protocol.commentChairman[chairmanCommentsLength - 1];

        this.commentAuthor = commentAuthor.fioFull;
        this.commentDate = comment.commentDate;
        this.comment = comment.commentText;
    }

    commentBlockVisible() {
        return !!this.protocol.commentSecretaryResponsible ||
            !!this.protocol.commentChairman;
    }

    onAssignHandler() {
        let block = this.blockUI.instances.get('protocolAndConclusion');
        block.start();
        this.saveQuestionsDecisions().then(() => {
            return this.generateProtocolFile();
        }).then(() => {
            if (this.generateConclusion) {
                return this.generateConclusionFile();
            } else {
                return this.$q.resolve();
            }
        }).then(() => {
            block.stop();
        }, () => {
            block.stop();
        });
    }

    generateProtocolFile() {
        return this.protocolResource.formProtocolDraft({id: this.$stateParams.documentId}).$promise.then(fileProtocolDraft => {
            this.protocol.fileProtocolDraft = fileProtocolDraft;
            return this.$q.resolve();
        })
    }

    private generateConclusionFile() {
        return this.protocolResource.formConclusionDraft({id: this.$stateParams.documentId}).$promise.then(fileConclusionDraft => {
            this.protocol.fileConclusionDraft = fileConclusionDraft;
            return this.$q.resolve();
        })
    }

    approvalCycleFilled() {
        return this.protocol.approval && this.protocol.approval.approvalCycle &&
            this.protocol.approval.approvalCycle.agreed && this.protocol.approval.approvalCycle.agreed.length
            && !_.some(this.protocol.approval.approvalCycle.agreed, a => !a.agreedBy);
    }

    submitHandler() {
        let block = this.blockUI.instances.get('protocolAndConclusion');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');

            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, this.getUpdateProcessVars());
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    getUpdateProcessVars() {
        let departmentsApprovals = this.approvalCycleUtils.getDepartmentsApprovees(this.protocol.approval.approvalCycle)
            .map(agreed => agreed.agreedBy.accountName).join(',');
        let supervisors = this.approvalCycleUtils.getSupervisors(this.protocol.approval.approvalCycle)
            .map(agreed => agreed.agreedBy.accountName).join(',');
        let stroyUser = this.approvalCycleUtils.getStroyUser(this.protocol.approval.approvalCycle).agreedBy.accountName;
        return [
            {name: "ApprovalUserVar", value: departmentsApprovals},
            {name: "ApprovalProtocolUserVar", value: supervisors},
            {name: "UkdUserVar", value: stroyUser}
        ];
    }

    saveHandler() {
        let block = this.blockUI.instances.get('protocolAndConclusion');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    saveQuestionsDecisions(): ng.IPromise<any> {
        let protocolWithDecisions = angular.copy(this.origProtocol);
        this.protocol.question.forEach(q => {
            const protocolQuestion = _.find(protocolWithDecisions.question, q1 => q1.questionID === q.questionID);
            if (q.decisionText) {
                protocolQuestion.decisionText = q.decisionText;
            } else {
                delete protocolQuestion.decisionText;
            }
        });
        return this.saveProtocol(this.origProtocol, protocolWithDecisions).then(() => {
            this.origProtocol = protocolWithDecisions;
        });
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.protocol.protocolID}, diff).$promise;
    }

    filesFormed() {
        return this.protocol.fileProtocolDraft && (!this.generateConclusion || this.protocol.fileConclusionDraft);
    }

}