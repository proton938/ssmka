class TableQuestionDecisionController {
    static $inject = [
    ];
    private decision: Decision;
    private refusalQuestion: RefusalQuestion;
    private reasons: RefusalRefuseReason[];
    constructor() {
    }
    $onInit() {
        this.reasons = this.decision.decisionTypeCode === 'REJECT' && this.refusalQuestion ?
            this.refusalQuestion.refuseReason : [];
    }
    imageClass() {
        let code = this.decision.decisionTypeCode;
        return {
            'fa-check-circle question-decision_approved': code === 'APPROVE',
            'fa-times-circle question-decision_declined': code === 'REJECT' || code === 'REWORK'
        };
    }

}

class TableQuestionDecisionComponent {
    public controller: any = TableQuestionDecisionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/protocol-and-conclusion/questionDecision.html';
    public bindings: any = {
        decision: "<",
        refusalQuestion: "<"
    };
}

angular.module('app').component('tableQuestionDecision', new TableQuestionDecisionComponent());
