class GkPzzProtocolAndConclusionController extends ProtocolAndConclusionController {
    static $inject = [
        '$stateParams',
        'protocolResource',
        '$q',
        '$timeout',
        'nsiRestService',
        'fileResource',
        'protocolTaskService',
        '$state',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'toastr',
        'fileHttpService'
    ];

    constructor(private $stateParams: any,
                public protocolResource: IProtocolResource,
                public $q: ng.IQService,
                public $timeout: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private fileResource: IFileResource,
                private protocolTaskService: IProtocolTaskService,
                private $state: any,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private toastr: any,
                private fileHttpService: oasiFileRest.FileHttpService) {
        super('Городская комиссия по вопросам градостроительной деятельности',
            'Сформировать Протокол и Заключение ГК', true, true, 'Рекомендовать', 'Не рекомендовать', true,
            $stateParams, protocolResource, $q, $timeout, nsiRestService, fileResource, protocolTaskService,
            $state, blockUI, dateUtils, approvalCycleUtils, toastr, fileHttpService);
    }

}

class GkPzzProtocolAndConclusion {
    public controller: any = GkPzzProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/protocol-and-conclusion/protocolAndConclusionForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzProtocolAndConclusionForm', new GkPzzProtocolAndConclusion());
