class ApprovalListEditorController {
    static $inject = [
        'approvalCycleUtils',
        'nsiRestService'
    ];

    private protocol: Protocol;
    private approvalCycle: ProtocolApprovalCycle;

    private approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];

    private departmentsUsers: User[];
    private departmentsApprovers: Dictionary<Approvee[]> = {};

    private approvers: _.Dictionary<Approvee[]> = {};
    private searchingApprovers: _.Dictionary<boolean> = {};

    private supervisingUsers: User[];
    private supervisors: Approvee[];

    private stroyUsers: User[];
    private stroyApprovers: Approvee[];

    constructor(private approvalCycleUtils: ApprovalCycleUtils,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.approvalCycle = this.protocol.approval.approvalCycle;
        this.departmentsApprovers = _.chain(this.departmentsUsers)
            .groupBy(u => u.departmentCode)
            .mapValues(users => users.map(u => Approvee.createFromUser(u)))
            .value();

        this.approvalCycleUtils.getDepartmentsApprovees(this.approvalCycle)
            .filter(a => !a.approvalDepartment)
            .forEach(a => {
                this.approvers[a.approvalNum] = [a.agreedBy];
                this.searchingApprovers[a.approvalNum] = false;
            });

        this.supervisors = this.supervisingUsers.map(u => Approvee.createFromUser(u));
        this.stroyApprovers = this.stroyUsers.map(u => Approvee.createFromUser(u));

    }

    delDepartmentApproval(agreed: ProtocolApprovalCycleAgreed) {
        this.approvalCycleUtils.deleteDepartmentApproval(this.approvalCycle, agreed.approvalNum);
    }

    canDelete(agreed: ProtocolApprovalCycleAgreed) {
        if (!this.approvalCycleUtils.isDepartmentApproval(agreed)) {
            return false;
        }
        let parsedApprovalNum = this.approvalCycleUtils.parseApprovalNum(agreed.approvalNum);
        if (parsedApprovalNum.length > 2) {
            return true;
        }
        return this.approvalCycleUtils.getDepartmentsApprovees(this.approvalCycle).filter(a => {
            return a.approvalNum !== agreed.approvalNum && this.approvalCycleUtils.parseApprovalNum(a.approvalNum).length === 2;
        }).length > 0;
    }

    canMoveUp(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isDepartmentApproval(agreed) &&
            this.approvalCycleUtils.hasDepartmentApproval(this.approvalCycle, this.approvalCycleUtils.decreaseApprovalNum(agreed.approvalNum));
    }

    canMoveDown(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isDepartmentApproval(agreed) &&
            this.approvalCycleUtils.hasDepartmentApproval(this.approvalCycle, this.approvalCycleUtils.increaseApprovalNum(agreed.approvalNum));
    }

    moveUp(agreed: ProtocolApprovalCycleAgreed) {
        const prevApprovalNum = this.approvalCycleUtils.decreaseApprovalNum(agreed.approvalNum);
        this.approvalCycleUtils.swapDepartmentApprovals(this.approvalCycle, prevApprovalNum, agreed.approvalNum);
    }

    moveDown(agreed: ProtocolApprovalCycleAgreed) {
        const nextApprovalNum = this.approvalCycleUtils.increaseApprovalNum(agreed.approvalNum);
        this.approvalCycleUtils.swapDepartmentApprovals(this.approvalCycle, agreed.approvalNum, nextApprovalNum);
    }

    addDepartmentApproval() {
        let agreedApprovalType = _.find(this.approvalTypes, at => at.approvalTypeCode === ApprovalType.AGREED);
        let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.WAIT);
        let agreed = this.approvalCycleUtils.addApprovee(this.approvalCycle, agreedApprovalType, waitApprovalStatus);
    }

    onChangeDepartmentApproval(index, approvers: Approvee[]) {
        let agreed = this.approvalCycle.agreed[index];
        let accountName = agreed.agreedBy.accountName;
        if (accountName) {
            _.extend(agreed.agreedBy, _.find(approvers, a => a.accountName === accountName));
        } else {
            agreed.agreedBy = null;
        }
    }

    onChangeSupervisor(index) {
        let accountName = this.approvalCycle.agreed[index].agreedBy.accountName;
        this.approvalCycle.agreed[index].agreedBy = accountName ? _.find(this.supervisors, a => a.accountName === accountName) :
            null;
    }

    onChangeStroyUser(index) {
        let accountName = this.approvalCycle.agreed[index].agreedBy.accountName;
        this.approvalCycle.agreed[index].agreedBy = accountName ? _.find(this.stroyApprovers, a => a.accountName === accountName) :
            null;
    }

    isDepartmentApproval(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isDepartmentApproval(agreed);
    }

    isSupervisor(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isSupervisor(agreed);
    }

    isStroyUser(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isStroyUser(agreed);
    }

    isSecretaryResponsible(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isResponsibleSecretary(agreed);
    }

    isChairman(agreed: ProtocolApprovalCycleAgreed) {
        return this.approvalCycleUtils.isChairman(agreed);
    }

    getLastDepartmentApprovalIndex() {
        return this.approvalCycleUtils.getLastDepartmentApprovalIndex(this.approvalCycle);
    }

    searchApprovers(query: string, approvalNum: string) {
        const params = { fio: query };
        this.searchingApprovers[approvalNum] = true;
        this.nsiRestService.searchUsers(params).then((users: User[]) => {
            this.approvers[approvalNum] = users.map(u => Approvee.createFromUser(u));
            this.searchingApprovers[approvalNum] = false;
        }, () => {
            this.searchingApprovers[approvalNum] = false;
        });
    }

}

class ApprovalListEditorComponent {
    public controller: any = ApprovalListEditorController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/protocol-and-conclusion/approvalListEditor.html';
    public bindings: any = {
        protocol: "=",
        approvalTypes: "<",
        approvalStatuses: "<",
        departmentsUsers: "<",
        supervisingUsers: "<",
        stroyUsers: "<"
    };
}

angular.module('app').component('approvalListEditor', new ApprovalListEditorComponent());
