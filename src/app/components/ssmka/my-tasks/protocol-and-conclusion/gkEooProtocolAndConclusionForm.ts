class GkEooProtocolAndConclusionController extends ProtocolAndConclusionController {
    static $inject = [
        '$stateParams',
        'protocolResource',
        '$q',
        '$timeout',
        'nsiRestService',
        'fileResource',
        'protocolTaskService',
        '$state',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'toastr',
        'fileHttpService'
    ];

    constructor(private $stateParams: any,
                public protocolResource: IProtocolResource,
                public $q: ng.IQService,
                public $timeout: any,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private fileResource: IFileResource,
                private protocolTaskService: IProtocolTaskService,
                private $state: any,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private toastr: any,
                private fileHttpService: oasiFileRest.FileHttpService) {
        super('Городская комиссия по ЭОО', 'Сформировать Протокол', false, false, 'Утвердить', 'Доработать', false,
            $stateParams, protocolResource, $q, $timeout, nsiRestService, fileResource, protocolTaskService,
            $state, blockUI, dateUtils, approvalCycleUtils, toastr, fileHttpService);
    }

    getUpdateProcessVars() {
        let stroyUser = this.approvalCycleUtils.getStroyUser(this.protocol.approval.approvalCycle).agreedBy.accountName;
        return [
            {name: "UkdUserVar", value: stroyUser}
        ];
    }

}

class GkEooProtocolAndConclusion {
    public controller: any = GkEooProtocolAndConclusionController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/protocol-and-conclusion/protocolAndConclusionForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooProtocolAndConclusionForm', new GkEooProtocolAndConclusion());
