class QuestionsParametersFormController {
    static $inject = [
        '$uibModalInstance',
        'questionParameters',
        'nsiRestService',
        '$uibModal',
        '$timeout'
    ];
    private vriZu120Types: VriZu120[] = [];
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    constructor(
        private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
        private questionParameters: QuestionParameters = new QuestionParameters(),
        private nsiRestService: oasiNsiRest.NsiRestService,
        private $uibModal: any,
        private $timeout: any
    ) {
    }
    $onInit() {
        Promise.all([
            this.nsiRestService.get('PzzParameters'),
            this.nsiRestService.get('vriZu120')
        ]).then( (response: any) => {
            this.$timeout(() => {
                this.initTep(response[0]);
                this.vriZu120Types = response[1];
                this.loadingStatus = LoadingStatus.SUCCESS;

                if (this.questionParameters.mainVRI && (!this.questionParameters.mainVRI.recommended || this.questionParameters.mainVRI.recommended.length < 1)) {
                    this.questionParameters.mainVRI.recommended = this.questionParameters.mainVRI.new;
                }
                if (this.questionParameters.relativelyVRI && (!this.questionParameters.relativelyVRI.recommended || this.questionParameters.relativelyVRI.recommended.length < 1)) {
                    this.questionParameters.relativelyVRI.recommended = this.questionParameters.relativelyVRI.new;
                }
                if (this.questionParameters.subVRI && (!this.questionParameters.subVRI.recommended || this.questionParameters.subVRI.recommended.length < 1)) {
                    this.questionParameters.subVRI.recommended = this.questionParameters.subVRI.new;
                }
                if (this.questionParameters.subVRI && (!this.questionParameters.subVRI.recommendedComment && this.questionParameters.subVRI.newComment)) {
                    this.questionParameters.subVRI.recommendedComment = this.questionParameters.subVRI.newComment;
                }
            });
        });
    }
    initTep(pzzParameters: PzzParameter[]) {
        if ( !this.questionParameters.tep.length ) {
            this.questionParameters.tep = [];
            pzzParameters.forEach((pzzParameter) => {
                this.questionParameters.tep.push({
                    tepName: pzzParameter,
                    currentValue: '',
                    newValue: '',
                    recommendedValue: '',
                    sortNumber: '',
                });
            });
        }
        else {
            this.questionParameters.tep.forEach((tep: any) => {
                if (!tep.recommendedValue && tep.newValue) {
                    tep.recommendedValue = tep.newValue;
                }
            });
        }

    }
    onSelectFromReferenceClick(models: any[], customizable: boolean){
        let usedDictionary;

        if ( customizable ) {
            switch (this.questionParameters.vriVersion) {
                case VriVersionEnum.Dictionary_120: usedDictionary = this.vriZu120Types; break;
            }
        } else {
            usedDictionary = this.vriZu120Types;
        }
        let tree = this._buildSelectedTree(usedDictionary, this.questionParameters.vriVersion);

        let modalInstance = this.$uibModal.open({
            component: 'dictionaryModal',
            animation: true,
            controllerAs: "$ctrl",
            windowClass: 'second-modal',
            size: 'lg',
            resolve: {
                multiple: () => {
                    return true;
                },
                name: () => {
                    return name;
                },
                tree: () => {
                    return tree;
                },
                selectedItems: () => {
                    let codes: string[] = models.map( item => item.code );
                    return codes;
                }
            }
        });

        return modalInstance.result.then((codes) => {
            let dicts: any[] = [];
            codes.forEach(code => {
                let dict = this._findDictByCode(usedDictionary, code);

                if ( dict ) {
                    dicts.push(dict);
                }
            });
            return dicts;
        });
    }
    _findDictByCode(dicts: any[], code: string): string {
        let result: any = null;

        dicts.some( dict => {
            if (dict.code === code) {
                dict.name = dict.value;
                result = dict;
            }
            if (dict.children) {
                let subDict = this._findDictByCode(dict.children, code);
                if ( subDict ) {
                    result = subDict;
                }
            }
            return result;
        });
        return result;
    }
    onSelectFromReferenceClick1() {
        this.onSelectFromReferenceClick((this.questionParameters.mainVRI && this.questionParameters.mainVRI.recommended) || [], true)
            .then((codes) => {
                if (!this.questionParameters.mainVRI) {
                    this.questionParameters.mainVRI = new QuestionParameterType();
                }
                this.questionParameters.mainVRI.recommended = codes;
            });
    }
    onSelectFromReferenceClick2() {
        this.onSelectFromReferenceClick((this.questionParameters.relativelyVRI && this.questionParameters.relativelyVRI.recommended) || [], false)
            .then((codes) => {
                if (!this.questionParameters.relativelyVRI) {
                    this.questionParameters.relativelyVRI = new QuestionParameterType();
                }
                this.questionParameters.relativelyVRI.recommended = codes;
            });
    }
    onSelectFromReferenceClick3() {
        this.onSelectFromReferenceClick((this.questionParameters.subVRI && this.questionParameters.subVRI.recommended) || [], true)
            .then((codes) => {
                if (!this.questionParameters.subVRI) {
                    this.questionParameters.subVRI = new QuestionParameterType();
                }
                this.questionParameters.subVRI.recommended = codes;
            });
    }

    _buildSelectedTree(dict: VriZu[] = [], dictName: VriVersionEnum, parent?: oasiWidgets.ITreeViewItemChecked): oasiWidgets.ITreeViewItemChecked[] {
        let tree: oasiWidgets.ITreeViewItemChecked[] = null;
        dict.forEach(item => {
            if ( !tree ) {
                tree = [];
            }
            let template = `${item.value} [${item.code}]`;
            let result: oasiWidgets.ITreeViewItemChecked = {
                parent: parent || null,
                id: item.code,
                template,
                selectable: true,
                children: null,
                expanded: true,
                checked: false,
                disabled: false
            };

            if ( item.children ) {
                let children = this._buildSelectedTree(item.children, dictName, result);
                result.children = children;
            } else {
                result.children = null;
            }
            tree.push(result);
        });
        return tree;
    }
    saveHandler() {
        this.save();
    }
    save() {
        this.$uibModalInstance.close(this.questionParameters);
    }
    cancelHandler() {
        this.cancel();
    }
    cancel() {
        this.$uibModalInstance.dismiss();
    }
    normalize(string: string) {
        return FloatString.normalize(string);
    }

}

angular.module('app').controller('QuestionParametersFormController', QuestionsParametersFormController);
