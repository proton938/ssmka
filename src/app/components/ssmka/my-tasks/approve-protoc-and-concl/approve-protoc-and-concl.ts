class ApproveProtocAndConclController {

    private taskWrapper: any;
    private origProtocol: Protocol;
    private protocol: Protocol;
    private questions: ProtocolQuestion[];

    private approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];
    private decisions: Decision[];

    private addedApprovers: User[] = [];

    private isSupervisor: boolean;
    private isStroyUser: boolean;

    private questionsComments: QuestionCommentToAdd[];
    private comment: string;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(public meetingType: string,
                public showConclusion: boolean,
                public canAddApprovals: boolean,
                private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private activityHttpService: IActivityHttpService) {
    }

    $onInit() {
        let departmentCode = this.session.departmentCode();
        this.isSupervisor = this.$stateParams.isSupervisor;
        this.isStroyUser = this.$stateParams.isStroyUser;
        this.loadDocument(this.$stateParams.documentId).then(() => {
            if (departmentCode === 'MKA_DEP_LAW' || departmentCode === 'MKA_DEP_GP' || this.isSupervisor || this.isStroyUser) {
                this.questions = this.protocol.question.filter(q => !q.excluded);
            } else {
                this.questions = this.protocol.question.filter(q => !q.excluded && q.departmentPrepareCode === departmentCode);
            }
            return this.loadDictionaries();
        }).then(() => {
            let firstQuestion = _.first(this.protocol.question);
            this.questionsComments = [];
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDocument(id: string): ng.IPromise<void> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => {
            this.protocol = angular.copy(data.document.protocol);
            this.origProtocol = angular.copy(data.document.protocol);
            return this.$q.resolve();
        });
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.get('Approval'),
            this.nsiRestService.get('OasiStatusResultProtocolPM'),
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]})
        ]).then((result: [ApprovalType[], ProtocolApprovalStatus[], Decision[]]) => {
            this.approvalTypes = result[0];
            this.approvalStatuses = result[1];
            this.decisions = result[2];
        })
    }

    saveHandler() {
        let block = this.blockUI.instances.get('approveProtocAndConcl');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            block.stop();
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    addApprovee() {
        this.$uibModal.open({
            component: 'add-approval',
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            windowClass: 'our-window-lg',
            resolve: {
                approvalCycle: () => this.protocol.approval.approvalCycle,
                exclude: () => this.approvalCycleUtils.getDepartmentsApprovees(this.protocol.approval.approvalCycle).map(a => a.agreedBy.accountName),
                showComment: () => false
            }
        }).result.then((result: { user: User }[]) => {
            let block = this.blockUI.instances.get('approveProtocAndConcl');
            block.start();

            let agreedApprovalType = _.find(this.approvalTypes, at => at.approvalTypeCode === ApprovalType.AGREED);
            let agreed = this.getAgreed();
            let note = '';

            let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code = ProtocolApprovalStatus.WAIT);
            result.forEach((r, i) => {
                let user = r.user;
                note += user.fioShort + (user.post ? ', ' + user.post : '') + '; ';
                this.approvalCycleUtils.addAdditionalApprovee(this.protocol.approval.approvalCycle, agreedApprovalType, waitApprovalStatus, user, agreed);
                this.addedApprovers.push(user);
            });
            agreed.approvalNote = 'Добавление согласующего: ' + note;

            this.saveProtocol(this.origProtocol, this.protocol).then(() => {
                if (this.isSupervisor) {
                    const logins = this.approvalCycleUtils.getSupervisors(this.protocol.approval.approvalCycle)
                        .map(a => a.agreedBy.accountName);
                    return this.addSequentialApprovals(logins);
                } else {
                    const logins = result.map(u => u.user.accountName);
                    return this.startParallelApprovals(logins);
                }
            }).then(() => {
                this.toastr.info('Задача завершена.');
                block.stop();
                location.assign('/oasi/#/app/my-tasks');
            });
        });
    }

    approve() {
        let block = this.blockUI.instances.get('approveProtocAndConcl');
        block.start();

        let statusCode = ProtocolApprovalStatus.APPROVAL;
        let status = _.find(this.approvalStatuses, st => st.code === statusCode);
        this.applyResult(status, true).then((agreed) => {
            this.toastr.info('Данные сохранены.');
            return this.restoreParentParallelApproval(agreed)
        }).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, [
                {name: "IsApproved", value: true},
            ]);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    reject() {
        let block = this.blockUI.instances.get('approveProtocAndConcl');
        block.start();

        let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.REJECTED);
        this.applyResult(status, false, this.comment, this.questionsComments).then((agreed) => {
            this.toastr.info('Данные сохранены.');
            return this.restoreParentParallelApproval(agreed)
        }).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, [
                {name: "IsApproved", value: false},
            ]);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    private startParallelApprovals(logins: string[]): ng.IPromise<any> {
        if (!logins.length) {
            return this.$q.resolve();
        }
        return this.protocolTaskService.getProcessDefinition('ss_ParallelApproval_gk').then((process: oasiBpmRest.IProcessDefinition) => {
            return this.$q.all(
                _.map(logins, login => this.protocolTaskService.initProcess(process.id, [
                    {"name": "EntityIdVar", "value": this.protocol.protocolID},
                    {"name": "ApprovalUser", "value": login}
                ]))
            ).then(() => {
                return this.protocolTaskService.suspendProcess(this.taskWrapper.task.processInstanceId)
            })
        });
    }

    private addSequentialApprovals(logins: string[]) {
        if (!logins.length) {
            return this.$q.resolve();
        }
        let requestData = {
            taskId: this.$stateParams.taskId,
            usersCollectionVarName: "ApprovalProtocolUserVar",
            durationsCollectionVarName: "DueDatesListForAgreeVar",
            elementVarName: "ApprovalProtocolUser",
            approvalPersons: logins.map(login => {
                return {
                    userName: login,
                    duration: 'PT4H'
                }
            })
        };
        return this.activityHttpService.addApprovers(requestData);
    }

    private applyResult(status: ProtocolApprovalStatus, signOfApproval: boolean, approvalNote?: string, questionsComments?: QuestionCommentToAdd[]): ng.IPromise<ProtocolApprovalCycleAgreed> {
        return this.loadDocument(this.protocol.protocolID).then(() => {
            let agreed = this.getAgreed();
            agreed.approvalResult = status.name;
            agreed.approvalResultCode = status.code;
            agreed.approvalResultColor = status.color;
            if (approvalNote) {
                agreed.approvalNote = approvalNote;
            }
            agreed.approvalFactDate = new Date();

            if (_.isUndefined(this.protocol.approval.approvalCycle.signOfApproval) || this.protocol.approval.approvalCycle.signOfApproval) {
                this.protocol.approval.approvalCycle.signOfApproval = signOfApproval;
            }
            let needToCloseCycle = false;
            if (this.isStroyUser || this.isSupervisor) {
                //Последовательное согласование, закрываем цикл сразу
                needToCloseCycle = status.code === ProtocolApprovalStatus.REJECTED;
            } else {
                //Параллельное согласование, закрываем у последнего пользователя, если были несогласования
                let departmentsApprovees = this.approvalCycleUtils.getDepartmentsApprovees(this.protocol.approval.approvalCycle);
                let lastApprover = departmentsApprovees.filter(a => a.approvalResultCode === ProtocolApprovalStatus.WAIT).length === 0;
                let hasRejects = departmentsApprovees.filter(a => a.approvalResultCode === ProtocolApprovalStatus.REJECTED).length > 0;
                needToCloseCycle = lastApprover && hasRejects;
            }
            if (questionsComments) {
                questionsComments.filter(q => q.questionNumber && q.comment).forEach(comment => {
                    let question = _.find(this.protocol.question, q => q.questionNumber === comment.questionNumber);
                    question.comment = question.comment || [];
                    question.comment.push(this.createComment(comment.comment));
                });
                agreed.approvalComment = questionsComments.map(c => ProtocolApprovalCycleAgreedComment.fromComment(c));
            }
            if (needToCloseCycle) {
                let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code = ProtocolApprovalStatus.WAIT);
                Protocol.closeApprovalCycle(this.protocol, waitApprovalStatus);
            }

            return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
                return this.$q.resolve(agreed);
            });
        })
    }

    private restoreParentParallelApproval(agreed: ProtocolApprovalCycleAgreed): ng.IPromise<any> {
        if (this.isSupervisor || this.isStroyUser) {
            return this.$q.resolve();
        }
        const approvalNum = agreed.approvalNum;
        const added = this.approvalCycleUtils.isAdded(approvalNum);
        if (!added) {
            return this.$q.resolve();
        }
        const parentApproval = this.approvalCycleUtils.getParentApproval(this.protocol.approval.approvalCycle, approvalNum);
        const childApprovals = this.approvalCycleUtils.getChildApprovals(this.protocol.approval.approvalCycle, parentApproval.approvalNum);
        const lastChildApproval = childApprovals.filter(a => a.approvalResultCode === ProtocolApprovalStatus.WAIT).length === 0;
        if (!lastChildApproval) {
            return this.$q.resolve();
        }
        return this.protocolTaskService.getTasksByEntityAndAssignee('approveProtocAndConcl', this.$stateParams.documentId,
            parentApproval.agreedBy.accountName
        ).then(tasks => {
            let task = _.first(tasks.filter(t => t.suspended));
            if (task) {
                return this.protocolTaskService.activateProcess(task.processInstanceId)
            } else {
                return this.$q.resolve();
            }

        });
    }

    private createComment(commentText: string): ProtocolQuestionComment {
        let comment = new ProtocolQuestionComment();
        comment.commentDate = new Date();
        comment.login = this.session.login();
        comment.personFio = this.session.fullName();
        comment.post = this.session.post();
        comment.note = commentText;
        return comment;
    }

    hasQuestionsComments() {
        return this.questionsComments.filter(q => q.questionNumber && q.comment).length > 0;
    }

    private getAgreed() {
        if (this.isSupervisor) {
            const supervisors = this.approvalCycleUtils.getSupervisors(this.protocol.approval.approvalCycle);
            return _.chain(supervisors).filter(a => a.approvalResultCode === ProtocolApprovalStatus.WAIT)
                .filter(a => a.agreedBy.accountName === this.session.login())
                .first()
                .value();
        } else if (this.isStroyUser) {
            return this.approvalCycleUtils.getStroyUser(this.protocol.approval.approvalCycle);
        } else {
            return _.chain(this.approvalCycleUtils.getDepartmentsApprovees(this.protocol.approval.approvalCycle))
                .filter(a => a.approvalResultCode === ProtocolApprovalStatus.WAIT)
                .filter(a => a.agreedBy.accountName === this.session.login())
                .first()
                .value();
        }
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.protocol.protocolID}, diff).$promise;
    }
}

class GkEooApproveProtocAndConclController extends ApproveProtocAndConclController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'protocolTaskService',
        'nsiRestService',
        'session',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'activityHttpService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private activityHttpService: IActivityHttpService) {
        super('Городская комиссия по ЭОО', false, false.
            protocolResource, $stateParams, $q, $uibModal, toastr, protocolTaskService, nsiRestService,
            session, blockUI, dateUtils, approvalCycleUtils, activityHttpService);
    }

}

class GkEooApproveProtocAndConcl {
    public controller: any = GkEooApproveProtocAndConclController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/approve-protoc-and-concl/approve-protoc-and-concl.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkEooApproveProtocAndConcl', new GkEooApproveProtocAndConcl());

class GkPzzApproveProtocAndConclController extends ApproveProtocAndConclController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'protocolTaskService',
        'nsiRestService',
        'session',
        'blockUI',
        'dateUtils',
        'approvalCycleUtils',
        'activityHttpService'
    ];

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils,
                private approvalCycleUtils: ApprovalCycleUtils,
                private activityHttpService: IActivityHttpService) {
        super('Городская комиссия по вопросам градостроительной деятельности', true, true,
            protocolResource, $stateParams, $q, $uibModal, toastr, protocolTaskService, nsiRestService,
            session, blockUI, dateUtils, approvalCycleUtils, activityHttpService);
    }

}

class GkPzzApproveProtocAndConcl {
    public controller: any = GkPzzApproveProtocAndConclController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/approve-protoc-and-concl/approve-protoc-and-concl.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkPzzApproveProtocAndConcl', new GkPzzApproveProtocAndConcl());
