class AbstractProcessController {

    constructor(public $q: ng.IQService,
                public dateUtils: IDateUtils,
                public toastr: any,
                public nsiRestService: oasiNsiRest.NsiRestService,
                public fileHttpService: oasiFileRest.FileHttpService,
                public wordReporterRestService: oasiWordreporterRest.WordReporterRestService,
                public instructionsService: IInstructionsService,
                public protocolResource: IProtocolResource,
                public agendaResource: IAgendaResource) {
    }

    loadProtocol(id: string): ng.IPromise<Protocol> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => data.document.protocol);
    }

    loadAgenda(id: string): ng.IPromise<Agenda> {
        return this.agendaResource.getById({id: id}).$promise.then((data: AgendaDocumentWrapper) => data.document.agenda);
    }

    fillApprovalPlanDate(agreed: ProtocolApprovalCycleAgreed[]): ng.IPromise<any> {
        let approvalTimes = _.uniq(agreed.map(a => a.approvalTime));
        let now = new Date();
        return this.$q.all(
            approvalTimes.map(at => this.calculateApprovalPlanDate(now, at).then(d => [at, d]))
        ).then((result: [string, Date][]) => {
            agreed.forEach(a => {
                a.approvalPlanDate = _.find(result, i => i[0] === a.approvalTime)[1];
            });
            return this.$q.resolve();
        })
    }

    calculateApprovalPlanDate(date: Date, approvalTime: string): ng.IPromise<Date> {
        let dateStr = this.dateUtils.formatDate(date, 'yyyy-MM-ddTHH:mm:ss');
        return this.nsiRestService.businessCalendarAddDuration(dateStr, approvalTime);
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: protocol.protocolID}, diff).$promise;
    }

    initDropzone(folderGuid: string, extensions: string[], getMethods: () => any, callback: (options, callbacks) => void, fileUploaded: (file: FileType) => void) {
        let options = {
            autoProcessQueue: false,
            withCredentials: true,
            parallelUploads: 1,
            paramName: "file",
            dictDefaultMessage: `Загрузить файл. <br/> Допустимое расширение: ${extensions.join(', ')}`,
            url: "/file"
        };
        let callbacks = {
            addedfile: (file) => {
                let fileName: string = file.name;
                let regexp = new RegExp('.*' + extensions.map(e => `\\.${e}`).join('|'));
                let match = fileName.toLowerCase().match(regexp);
                if (!match) {
                    this.toastr.warning(`Допустимое расширение: ${extensions.join(', ')}`, 'Ошибка');
                    getMethods().removeFile(file);
                    return;
                }

                const fd = new FormData();
                fd.append('file', file);
                fd.append('folderGuid', folderGuid);
                fd.append('fileType', 'MkaDocOther');
                this.fileHttpService.handleFileUpload(fd).then((resp) => {
                    fileUploaded({
                        idFile: resp.guid,
                        nameFile: file.name,
                        sizeFile: file.size,
                        signed: false,
                        dateFile: new Date(),
                        typeFile: 'MkaDocOther',
                        classFile: file.type
                    });
                    getMethods().removeFile(file);
                }, (error) => {
                    console.log(error);
                    this.toastr.warning(error, 'Ошибка');
                });
            }
        };
        callback(options, callbacks);
    }

    createProtocolWithoutAtt(folderGuid: string, protocol: Protocol, meetingType: MeetingType,
                                 instructions: ProtocolQuestionInstructions[]): ng.IPromise<FileType> {
        const json = this.instructionsService.getProtocolWithInstructions(protocol, meetingType, instructions);
        const fileName = `Протокол от ${this.dateUtils.formatDate(new Date(), 'dd.MM.yyyy HH:mm:ss')}.docx`;
        return this.wordReporterRestService.nsi2Filenet({
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать протокол с поручениями",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: fileName,
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({
                document: {
                    protocol: json
                }
            }),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'ssProtocolInstr'
            },
            filenetDestination: {
                reportFileName: fileName,
                reportFileType: 'MkaDocOther',
                reportFolderGuid: folderGuid
            }
        }).then((result) => {
            return this.convertToFileType(result);
        });

    }

    createProtocolDraft(folderGuid: string, protocolWithoutAttFile: FileType, attFile?: FileType): ng.IPromise<FileType> {
        return this.convertToPdf(folderGuid, protocolWithoutAttFile).then((protocolWithoutAttPdfFile) => {
            if (attFile) {
                const fileName = `Протокол от ${this.dateUtils.formatDate(new Date(), 'dd.MM.yyyy HH:mm:ss')}.pdf`;
                return this.convertToPdf(folderGuid, attFile).then(attPdfFile => {
                    return this.wordReporterRestService.pdfConcatenateFiles({
                        resFileName: fileName,
                        resFileType: 'MkaDocOther',
                        resFolderGuid: folderGuid,
                        versionSeriesGuids: [].concat(protocolWithoutAttPdfFile.idFile, attPdfFile.idFile)
                    })
                }).then(result => {
                    return this.$q.resolve(this.convertToFileType(result));
                })
            } else {
                return this.$q.resolve(protocolWithoutAttPdfFile);
            }
        });
    }

    private convertToPdf(folderGuid: string, file: FileType): ng.IPromise<FileType> {
        let fileExtension = file.nameFile.substring(file.nameFile.lastIndexOf(".") + 1);
        if (fileExtension !== 'pdf') {
            let convertRequest = {
                pdfFileName: file.nameFile.replace(`.${fileExtension}`, '.pdf'),
                pdfFileType: file.typeFile,
                pdfFolderGuid: folderGuid,
                wordVersionSeriesGuid: file.idFile
            };
            return this.wordReporterRestService.word2pdf(convertRequest).then(result => {
                return this.$q.resolve(this.convertToFileType(result));
            });
        } else {
            return this.$q.resolve(file);
        }
    }


    private convertToFileType(result): FileType {
        return {
            idFile: result.versionSeriesGuid,
            nameFile: result.fileName,
            sizeFile: result.fileSize,
            signed: false,
            dateFile: result.dateLastModified,
            typeFile: result.fileType,
            classFile: result.mimeType
        };
    }

}

class ExpressTaskHeaderController {
    private protocol: Protocol;
    private agenda: Agenda;
}

class ExpressTaskHeader {
    public controller: any = ExpressTaskHeaderController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/task-header/task-header.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    };
    public bindings: any = {
        protocol: '<',
        agenda: '<'
    };
}

angular.module('app').component('expressTaskHeader', new ExpressTaskHeader());