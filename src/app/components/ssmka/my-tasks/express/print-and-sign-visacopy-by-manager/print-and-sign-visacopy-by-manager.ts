class ExpressPrintAndSignVisacopyByManagerController extends AbstractProcessController {
    static $inject = [
        'protocolResource',
        'agendaResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'dateUtils',
        'blockUI',
        'toastr',
        'session',
        'meetingTypesService',
        'instructionsService',
        'protocolTaskService',
        'nsiRestService',
        'fileHttpService',
        'wordReporterRestService',
        'wordReporterPreviewService'
    ];

    protocol: Protocol;
    origProtocol: Protocol;
    agenda: Agenda;

    instructions: { question: Question, instructions: oasiSolarRest.SearchResultDocument[] }[] = [];

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    $onInit() {
        this.loadProtocol(this.$stateParams.documentId).then((protocol) => {
            this.protocol = angular.copy(protocol);
            this.origProtocol = angular.copy(protocol);
            return this.loadAgenda(protocol.agendaID);
        }).then((agenda) => {
            this.agenda = agenda;
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    constructor(protocolResource: IProtocolResource,
                agendaResource: IAgendaResource,
                private $stateParams: any,
                $q: ng.IQService,
                private $uibModal: any,
                dateUtils: IDateUtils,
                private blockUI: any,
                toastr: any,
                private session: oasiSecurity.ISessionStorage,
                private meetingTypesService: MeetingTypesService,
                instructionsService: IInstructionsService,
                private protocolTaskService: IProtocolTaskService,
                nsiRestService: oasiNsiRest.NsiRestService,
                fileHttpService: oasiFileRest.FileHttpService,
                wordReporterRestService: oasiWordreporterRest.WordReporterRestService,
                private wordReporterPreviewService: oasiWordreporterPreview.WordReporterPreviewService) {
        super($q, dateUtils, toastr, nsiRestService, fileHttpService, wordReporterRestService,
            instructionsService, protocolResource, agendaResource);
    }

    createProtocolApprovalListFile() {
        const fileName = `Лист согласования.docx`;
        return this.wordReporterPreviewService.nsi2Preview({
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать лист согласования",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: fileName,
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({
                document: {
                    protocol: this.protocol
                }
            }),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'ssProtocolLsAs'
            }
        });
    }

    documentSigned() {
        let block = this.blockUI.instances.get('printAndSignVisacopyByManager');
        if(!this.protocol.protocolApprovedDate){
            this.toastr.warn('Введите дату подписания');
            return;
        }
        block.start();
        // this.protocol.protocolApprovedDate = new Date();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        })
    }

}

class ExpressPrintAndSignVisacopyByManager {
    public controller: any = ExpressPrintAndSignVisacopyByManagerController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/print-and-sign-visacopy-by-manager/print-and-sign-visacopy-by-manager.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('expressPrintAndSignVisacopyByManager', new ExpressPrintAndSignVisacopyByManager());