class ExpressPrepareDecisionProtocolController extends AbstractProcessController {
    static $inject = [
        'expressResource',
        'protocolResource',
        'agendaResource',
        'questionResource',
        '$stateParams',
        '$q',
        'dateUtils',
        'blockUI',
        'toastr',
        'meetingTypesService',
        'instructionsService',
        'protocolTaskService',
        'alertService',
        'nsiRestService',
        'fileHttpService',
        'wordReporterRestService'
    ];

    protocol: Protocol;
    origProtocol: Protocol;
    agenda: Agenda;
    questions: Question[];
    decisions: string[];

    organizations: ProtocolAttendeeEditorOrganization[];

    private dzProtocolWithoutAttFileOptions: any;
    private dzProtocolWithoutAttFileCallbacks: any;
    private dzProtocolWithoutAttFileMethods: any;
    private dzAttFileOptions: any;
    private dzAttFileCallbacks: any;
    private dzAttFileMethods: any;

    parentFolderGuid: string;
    protocolWithoutAttFile: FileType;

    meetingType: MeetingType;
    approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];
    instructions: ProtocolQuestionInstructions[] = [];

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    $onInit() {
        this.loadProtocol(this.$stateParams.documentId).then((protocol) => {
            this.protocol = angular.copy(protocol);
            this.origProtocol = angular.copy(protocol);
            this.decisions = this.protocol.question.map(q => q.decisionText);
            this.protocol.approval = this.protocol.approval || new ProtocolApproval(1);
            this.protocol.approval.approvalCycle.agreed = this.protocol.approval.approvalCycle.agreed || [];
            return this.loadAgenda(protocol.agendaID);
        }).then((agenda) => {
            this.agenda = agenda;
            return this.loadDictionaries();
        }).then(() => {
            return this.loadIncludedQuestions(this.agenda.agendaID);
        }).then(() => {
            return this.instructionsService.getProtocolInstructions(this.protocol);
        }).then((instructions) => {
            this.instructions = instructions;
            this.initDropzones();
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    constructor(private expressResource: IExpressResource,
                protocolResource: IProtocolResource,
                agendaResource: IAgendaResource,
                private questionResource: IQuestionResource,
                private $stateParams: any,
                $q: ng.IQService,
                dateUtils: IDateUtils,
                private blockUI: any,
                toastr: any,
                private meetingTypesService: MeetingTypesService,
                instructionsService: IInstructionsService,
                private protocolTaskService: IProtocolTaskService,
                private alertService: oasiWidgets.IAlertService,
                nsiRestService: oasiNsiRest.NsiRestService,
                fileHttpService: oasiFileRest.FileHttpService,
                wordReporterRestService: oasiWordreporterRest.WordReporterRestService) {
        super($q, dateUtils, toastr, nsiRestService, fileHttpService, wordReporterRestService,
            instructionsService, protocolResource, agendaResource);
    }

    loadDictionaries(): ng.IPromise<any> {
        return this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('MeetingFilenetSettings'),
            this.nsiRestService.get('Approval'),
            this.nsiRestService.get('OasiStatusResultProtocolPM'),
            this.nsiRestService.get('RolesParticipants')
        ]).then((result: [MeetingType[], MeetingFilenetSettings[], ApprovalType[], ProtocolApprovalStatus[]]) => {
            let meetingTypes: MeetingType[] = result[0];
            this.meetingType = _.find(meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.protocol.meetingType;
            });
            this.parentFolderGuid = _.find(result[1], f => f.meetingType === this.agenda.meetingType).protocolFolderGuid
            this.approvalTypes = result[2];
            this.approvalStatuses = result[3];
            this.organizations = result[4].filter(e => e.LevelRoleCode === 'organization').map(e => {
                return new ProtocolAttendeeEditorOrganization(e.RoleCode, e.RoleName);
            })
        });
    }

    loadIncludedQuestions(agendaId: string): ng.IPromise<any> {
        return this.agendaResource.includedQuestions({ id: agendaId }).$promise.then((included: AgendaQuestion[]) => {
            included = included.filter(question => question.status !== AgendaQuestionStatus.EXCLUDED);
            return this.$q.all(included.map(q => this.questionResource.getById({id: q.questionId}).$promise)).then((result: QuestionDocumentWrapper[]) => {
                if (result.length > 0) {
                    this.questions = result.map(wr => wr.document.question);
                } else {
                    this.questions = [new Question()];
                }
                return this.$q.resolve();
            });

        });
    }

    initDropzones() {
        this.initDropzone(this.parentFolderGuid, ['docx'], () => this.dzProtocolWithoutAttFileMethods, (options, callbacks) => {
            this.dzProtocolWithoutAttFileOptions = options;
            this.dzProtocolWithoutAttFileCallbacks = callbacks;
        }, file => this.protocolWithoutAttFile = file);
        this.initDropzone(this.parentFolderGuid, ['doc', 'docx', 'pdf'], () => this.dzAttFileMethods, (options, callbacks) => {
            this.dzAttFileOptions = options;
            this.dzAttFileCallbacks = callbacks;
        }, file => this.protocol.orderAttach = [file]);
    }

    cleanProtocolWithoutAttFile() {
        this.protocolWithoutAttFile = null;
    }

    cleanAttFile() {
        this.protocol.orderAttach = [];
    }

    cleanProtocolDraftFile() {
        this.protocol.fileProtocolDraft = null;
    }

    createProtocolWithoutAttFile() {
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();
        this.updateAgendaAndReloadProtocol().then(() => {
            return this.createProtocolWithoutAtt(this.parentFolderGuid, this.protocol, this.meetingType, this.instructions);
        }).then(file => {
            this.protocolWithoutAttFile = file;
            block.stop();
        }, () => {
            block.stop();
        });
    }

    beforeAddQuestion(): ng.IPromise {
        if (!this.protocolWithoutAttFile) {
            return this.$q.resolve();
        }
        return this.alertService.confirm({
            message: 'Сформированный протокол будет удалён, в связи с добавлением/удалением вопросов. Продолжить?',
            okButtonText: 'Да',
            type: 'danger'
        });
    }

    beforeDeleteQuestion(question: Question): ng.IPromise {
        if (question.questionID) {
            return this.instructionsService.getQuestionInstructions(question).then((response) => {
                let message = '';
                if (this.protocolWithoutAttFile) {
                    message = 'Сформированный протокол будет удалён, в связи с добавлением/удалением вопросов. ';
                }
                if (response.numFound > 0) {
                    message += 'Удалите поручения, созданные по данному вопросу';
                } else if (message.length) {
                    message += 'Вы действительно хотите удалить данный вопрос?';
                }
                return message.length ? this.alertService.confirm({message: message, okButtonText: 'Ок', type: 'warning'}) :
                    this.$q.resolve();
            });
        } else if (this.protocolWithoutAttFile) {
            return this.alertService.confirm({
                message: 'Сформированный протокол будет удалён, в связи с добавлением/удалением вопросов. Продолжить?',
                okButtonText: 'Ок',
                type: 'warning'
            });
        }
        return this.$q.resolve();
    }

    questionsChanged() {
        this.protocolWithoutAttFile = null;
    }

    createProtocolDraftFile() {
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();
        this.createProtocolDraft(this.parentFolderGuid, this.protocolWithoutAttFile, _.first(this.protocol.orderAttach)).then((fileProtocolDraft) => {
            this.protocol.fileProtocolDraft = fileProtocolDraft;
            block.stop();
        }, () => {
            block.stop();
        });
    }

    approvalCycleFilled() {
        return this.protocol.approval && this.protocol.approval.approvalCycle
            && this.protocol.approval.approvalCycle.agreed && this.protocol.approval.approvalCycle.agreed.length;
    }

    sendToApproval() {
        let notValidApprovals = this.protocol.approval.approvalCycle.agreed.some(a =>
            !a.approvalTime || !a.agreedBy
        );
        if (notValidApprovals) {
            this.toastr.warning('Не заполнены обязательные поля!');
            return;
        }
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();

        return this.updateAgendaAndReloadProtocol().then(() => {
            return this.fillApprovalPlanDate(this.protocol.approval.approvalCycle.agreed);
        }).then(() => {
            this.protocol.approval.approvalCycle.signOfApproval = true;
            return this.saveProtocol(this.origProtocol, this.protocol);
        }).then(() => {
            this.toastr.info('Данные сохранены.');
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        })
    }

    private updateAgendaAndReloadProtocol() {
        if (this.questions.some(q => !q.questionConsider || !q.questionConsider.question)) {
            this.toastr.error('Не заполнен вопрос');
            return;
        }
        return this.expressResource.updateAgenda({ id: this.agenda.agendaID, protocolId: this.protocol.protocolID }, this.getAgendaPayload()).$promise.then(() => {
            return this.loadProtocol(this.protocol.protocolID)
        }).then(protocol => {
            this.origProtocol.question = angular.copy(protocol.question);
            this.protocol.question = protocol.question;
            this.decisions.forEach((decision, index) => {
                this.protocol.question[index].decisionText = decision;
            });
            return this.$q.resolve();
        })
    }

    save() {
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();

        return this.updateAgendaAndReloadProtocol().then(protocol => {
            this.saveProtocol(this.origProtocol, this.protocol);
        }).then(() => {
            this.toastr.info('Данные сохранены.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    private getAgendaPayload(): AgendaPayload {
        const payload: AgendaPayload = {
            document: { agenda: this.agenda },
        };
        if (this.questions.length > 0) {
            payload.questions = this.questions;
        }
        return payload;
    }

}

class ExpressPrepareDecisionProtocol {
    public controller: any = ExpressPrepareDecisionProtocolController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/prepare-decision-protocol/prepare-decision-protocol.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('expressPrepareDecisionProtocol', new ExpressPrepareDecisionProtocol());