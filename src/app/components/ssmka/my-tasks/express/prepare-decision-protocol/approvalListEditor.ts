class ExpressApprovalListEditorController {
    static $inject = [
        'approvalCycleUtils',
        'nsiRestService'
    ];

    private agreed: ProtocolApprovalCycleAgreed[];

    private approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];
    private approvalTimes: { value: string, name: string }[] = [
        {value: 'PT4H', name: '4 часа'},
        {value: 'PT6H', name: '6 часов'},
        {value: 'PT8H', name: '8 часов'},
        {value: 'PT24H', name: '24 часа'},
    ];

    private approvers: _.Dictionary<Approvee[]> = {};
    private searchingApprovers: _.Dictionary<boolean> = {};

    constructor(private approvalCycleUtils: ApprovalCycleUtils,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.agreed.forEach(a => {
            this.approvers[a.approvalNum] = [a.agreedBy];
            this.searchingApprovers[a.approvalNum] = false;
        });
    }

    delApproval(agreed: ProtocolApprovalCycleAgreed) {
        let index = _.findIndex(this.agreed, a => a.approvalNum === agreed.approvalNum);
        this.agreed.splice(index, 1);
        this.setNumeration();
    }

    moveUp(agreed: ProtocolApprovalCycleAgreed) {
        let index = _.findIndex(this.agreed, a => a.approvalNum === agreed.approvalNum);
        let agr = this.agreed[index];
        this.agreed[index] = this.agreed[index - 1];
        this.agreed[index - 1] = agr;
        this.setNumeration();
    }

    moveDown(agreed: ProtocolApprovalCycleAgreed) {
        let index = _.findIndex(this.agreed, a => a.approvalNum === agreed.approvalNum);
        let agr = this.agreed[index];
        this.agreed[index] = this.agreed[index + 1];
        this.agreed[index + 1] = agr;
        this.setNumeration();
    }

    setNumeration() {
        let i = 1;
        this.agreed.forEach(a => {
            a.approvalNum = '' + i++;
        })
    }

    addApproval() {
        let agreedApprovalType = _.find(this.approvalTypes, at => at.approvalTypeCode === ApprovalType.AGREED);
        let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.WAIT);
        let agreed = ProtocolApprovalCycleAgreed.createFrom('' + (this.agreed.length + 1), agreedApprovalType, waitApprovalStatus);
        agreed.approvalTime = 'PT4H';
        this.agreed.push(agreed);
        return agreed;
    }

    onChangeApproval(index, approvers: Approvee[]) {
        let agreed = this.agreed[index];
        let accountName = agreed.agreedBy.accountName;
        if (accountName) {
            _.extend(agreed.agreedBy, _.find(approvers, a => a.accountName === accountName));
        } else {
            agreed.agreedBy = null;
        }
    }

    searchApprovers(query: string, approvalNum: string) {
        const params = {fio: query};
        this.searchingApprovers[approvalNum] = true;
        this.nsiRestService.searchUsers(params).then((users: User[]) => {
            this.approvers[approvalNum] = users.map(u => Approvee.createFromUser(u));
            this.searchingApprovers[approvalNum] = false;
        }, () => {
            this.searchingApprovers[approvalNum] = false;
        });
    }

}

class ExpressApprovalListEditorComponent {
    public controller: any = ExpressApprovalListEditorController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/prepare-decision-protocol/approvalListEditor.html';
    public bindings: any = {
        agreed: "=",
        approvalTypes: "<",
        approvalStatuses: "<"
    };
}

angular.module('app').component('expressApprovalListEditor', new ExpressApprovalListEditorComponent());
