class ExpressApproveProtocolController extends AbstractProcessController {
    static $inject = [
        'protocolResource',
        'agendaResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'dateUtils',
        'blockUI',
        'toastr',
        'session',
        'meetingTypesService',
        'instructionsService',
        'protocolTaskService',
        'nsiRestService',
        'fileHttpService',
        'wordReporterRestService',
        'activityHttpService'
    ];

    protocol: Protocol;
    origProtocol: Protocol;
    agreed: ProtocolApprovalCycleAgreed;
    agenda: Agenda;

    parentFolderGuid: string;

    meetingType: MeetingType;
    approvalTypes: ApprovalType[];
    private approvalStatuses: ProtocolApprovalStatus[];
    instructions: { question: Question, instructions: oasiSolarRest.SearchResultDocument[] }[] = [];

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    $onInit() {
        this.loadProtocol(this.$stateParams.documentId).then((protocol) => {
            this.protocol = angular.copy(protocol);
            this.origProtocol = angular.copy(protocol);
            this.agreed = _.find(this.protocol.approval.approvalCycle.agreed, a =>
                a.approvalResult !== ProtocolApprovalStatus.WAIT && a.agreedBy.accountName === this.session.login());
            this.agreed.fileApproval = this.agreed.fileApproval || [];
            return this.loadAgenda(protocol.agendaID);
        }).then((agenda) => {
            this.agenda = agenda;
            return this.loadDictionaries();
        }).then(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    constructor(protocolResource: IProtocolResource,
                agendaResource: IAgendaResource,
                private $stateParams: any,
                $q: ng.IQService,
                private $uibModal: any,
                dateUtils: IDateUtils,
                private blockUI: any,
                toastr: any,
                private session: oasiSecurity.ISessionStorage,
                private meetingTypesService: MeetingTypesService,
                instructionsService: IInstructionsService,
                private protocolTaskService: IProtocolTaskService,
                nsiRestService: oasiNsiRest.NsiRestService,
                fileHttpService: oasiFileRest.FileHttpService,
                wordReporterRestService: oasiWordreporterRest.WordReporterRestService,
                private activityHttpService: IActivityHttpService) {
        super($q, dateUtils, toastr, nsiRestService, fileHttpService, wordReporterRestService,
            instructionsService, protocolResource, agendaResource);
    }

    loadDictionaries(): ng.IPromise<any> {
        return this.$q.all([
            this.meetingTypesService.getMeetingTypes(),
            this.nsiRestService.get('MeetingFilenetSettings'),
            this.nsiRestService.get('Approval'),
            this.nsiRestService.get('OasiStatusResultProtocolPM')
        ]).then((result: [MeetingType[], MeetingFilenetSettings[], ApprovalType[], ProtocolApprovalStatus[]]) => {
            let meetingTypes: MeetingType[] = result[0];
            this.meetingType = _.find(meetingTypes, (meetingType) => {
                return meetingType.meetingType === this.protocol.meetingType;
            });
            this.parentFolderGuid = _.find(result[1], f => f.meetingType === this.agenda.meetingType).protocolFolderGuid
            this.approvalTypes = result[2];
            this.approvalStatuses = result[3];
        });
    }

    approve() {
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();
        let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.APPROVAL);
        return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []).then(() => {
            this.toastr.success('Задача завершена!');
            return this.applyResult(status);
        }).then(() => {
            this.toastr.success('Документ сохранен!');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    sendBackToWork() {
        if (!this.agreed.approvalNote) {
            this.toastr.error('Не заполнены обязательные поля!');
            return;
        }
        let block = this.blockUI.instances.get('prepareDecisionProtocol');
        block.start();
        this.protocol.approval.approvalCycle.signOfApproval = false;
        return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            let status = _.find(this.approvalStatuses, st => st.code === ProtocolApprovalStatus.REJECTED);
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []).then(() => {
                this.toastr.success('Задача завершена!');
                return this.applyResult(status, false);
            }).then(() => {
                this.toastr.success('Документ сохранен!');
                location.assign('/oasi/#/app/my-tasks');
            }, () => {
                block.stop();
            });
        });    
    }

    formatTime(str: string = ''): string {
        const time = str.split(':');
        return time.length > 1 ? `${time[0]}:${time[1]}` : '';
    }

    private applyResult(status: ProtocolApprovalStatus, signOfApproval?: boolean) {
        this.agreed.approvalResult = status.name;
        this.agreed.approvalResultCode = status.code;
        this.agreed.approvalResultColor = status.color;
        this.agreed.approvalFactDate = new Date();

        this.agreed.fileApproval.forEach(f => {
            if (!_.isDate(f.dateFile)) {
                f.dateFile = new Date(<string>f.dateFile);
            }
        });

        if (!_.isUndefined(signOfApproval)) {
            this.protocol.approval.approvalCycle.signOfApproval = signOfApproval;
        }

        return this.saveProtocol(this.origProtocol, this.protocol);
    }

    addApproval() {
        this.$uibModal.open({
            component: 'add-approval',
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            windowClass: 'our-window-lg',
            resolve: {
                approvalCycle: () => this.protocol.approval.approvalCycle,
                exclude: () => this.protocol.approval.approvalCycle.agreed.map(a => a.agreedBy.accountName),
                approvalTypes: () => this.approvalTypes,
                showComment: () => true
            }
        }).result.then((result: { approvalType: ApprovalType, user: User, comment: string }[]) => {
            let block = this.blockUI.instances.get('prepareDecisionProtocol');
            block.start();

            let waitApprovalStatus = _.find(this.approvalStatuses, st => st.code = ProtocolApprovalStatus.WAIT);
            this.agreed.added = this.agreed.added || [];
            let approvalCycle = this.protocol.approval.approvalCycle;

            let approvalTimes = _.uniq(this.protocol.approval.approvalCycle.agreed.map(a => a.approvalTime));
            return this.calculateApprovalPlanDate(new Date(), 'PT3H').then((date: Date) => {
                result.forEach((r, i) => {
                    this.agreed.added.push(ProtocolApprovalCycleAgreedAdded.fromUser(r.user, r.comment));
                    let approvalNum = "" + (approvalCycle.agreed.length + 1);
                    let agreed = ProtocolApprovalCycleAgreed.createFrom(approvalNum, r.approvalType, waitApprovalStatus, r.user);
                    agreed.approvalPlanDate = date;
                    approvalCycle.agreed.push(agreed);
                });
                return this.saveProtocol(this.origProtocol, this.protocol);
            }).then(() => {
                return this.addApprovers([...result.map(r => r.user.accountName), this.session.login()], 'PT2H');
            }).then(() => {
                location.assign('/oasi/#/app/my-tasks');
            }, () => {
                block.stop();
            })
        });
    }

    private startParallelApprovals(logins: string[], approvalDuration: string): ng.IPromise<any> {
        if (!logins.length) {
            return this.$q.resolve();
        }
        return this.protocolTaskService.getProcessDefinition('ss_approvalProtocolDraftParallel').then((process: oasiBpmRest.IProcessDefinition) => {
            return this.$q.all(
                _.map(logins, login => this.protocolTaskService.initProcess(process.id, [
                    {"name": "EntityIdVar", "value": this.protocol.protocolID},
                    {"name": "EntityDescriptionVar", "value": this.protocol.protocolID},
                    {"name": "ApprovalUserVar", "value": login},
                    {"name": "ApprovalDuration", "value": approvalDuration},
                ]))
            )
        });
    }

    private addApprovers(logins: string[], approvalDuration: string) {
        if (!logins.length) {
            return this.$q.resolve();
        }
        let requestData = {
            taskId: this.$stateParams.taskId,
            usersCollectionVarName: "ApprovalUserVar",
            durationsCollectionVarName: "ApprovalDuration",
            elementVarName: "ApprovalUser",
            approvalPersons: logins.map(login => {
                return {
                    userName: login,
                    duration: approvalDuration
                }
            })
        };

        return this.activityHttpService.addApprovers(requestData);
    }

}

class ExpressApproveProtocol {
    public controller: any = ExpressApproveProtocolController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/approve-protocol/approve-protocol.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('expressApproveProtocol', new ExpressApproveProtocol());