class ExpressScanAndAttachSignedProtocolController extends AbstractProcessController {
    static $inject = [
        'protocolResource',
        'agendaResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'dateUtils',
        'blockUI',
        'toastr',
        'session',
        'meetingTypesService',
        'instructionsService',
        'protocolTaskService',
        'nsiRestService',
        'fileHttpService',
        'wordReporterRestService',
        'wordReporterPreviewService',
        'mggtService'
    ];

    protocol: Protocol;
    origProtocol: Protocol;
    agenda: Agenda;

    private dzFileProtocolOptions: any;
    private dzFileProtocolCallbacks: any;
    private dzFileProtocolMethods: any;

    parentFolderGuid: string;

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    $onInit() {
        this.loadProtocol(this.$stateParams.documentId).then((protocol) => {
            this.protocol = angular.copy(protocol);
            this.origProtocol = angular.copy(protocol);
            this.protocol.fileProtocol = this.protocol.fileProtocol || [];
            return this.loadAgenda(protocol.agendaID);
        }).then((agenda) => {
            this.agenda = agenda;
            return this.loadDictionaries();
        }).then(() => {
            this.initDropzones();
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }



    constructor(protocolResource: IProtocolResource,
                agendaResource: IAgendaResource,
                private $stateParams: any,
                $q: ng.IQService,
                private $uibModal: any,
                dateUtils: IDateUtils,
                private blockUI: any,
                toastr: any,
                private session: oasiSecurity.ISessionStorage,
                private meetingTypesService: MeetingTypesService,
                instructionsService: IInstructionsService,
                private protocolTaskService: IProtocolTaskService,
                nsiRestService: oasiNsiRest.NsiRestService,
                fileHttpService: oasiFileRest.FileHttpService,
                wordReporterRestService: oasiWordreporterRest.WordReporterRestService,
                private wordReporterPreviewService: oasiWordreporterPreview.WordReporterPreviewService,
                private mggtService: MggtService) {
        super($q, dateUtils, toastr, nsiRestService, fileHttpService, wordReporterRestService,
            instructionsService, protocolResource, agendaResource);
    }

    loadDictionaries(): ng.IPromise<any> {
        return this.$q.all([
            this.nsiRestService.get('MeetingFilenetSettings')
        ]).then((result: [MeetingFilenetSettings[]]) => {
            this.parentFolderGuid = _.find(result[0], f => f.meetingType === this.agenda.meetingType).protocolFolderGuid
        });
    }

    initDropzones() {
        this.initDropzone(this.parentFolderGuid, ['pdf'], () => this.dzFileProtocolMethods, (options, callbacks) => {
            this.dzFileProtocolOptions = options;
            this.dzFileProtocolCallbacks = callbacks;
        }, file => this.protocol.fileProtocol.push(file));
    }

    deleteFileProtocol(idFile: string) {
        this.protocol.fileProtocol = this.protocol.fileProtocol.filter(f => f.idFile !== idFile);
    }

    finishTask() {
        let block = this.blockUI.instances.get('scanAndAttachSignedProtocol');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            return this.mggtService.sendProtocol(this.protocol, this.agenda);
        }).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        })
    }

}

class ExpressScanAndAttachSignedProtocol {
    public controller: any = ExpressScanAndAttachSignedProtocolController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/express/scan-and-attach-signed-protocol/scan-and-attach-signed-protocol.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('expressScanAndAttachSignedProtocol', new ExpressScanAndAttachSignedProtocol());