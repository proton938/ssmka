import Dictionary = _.Dictionary;

class DetermineRefusalCasesByPZZAddReasonController {
    static $inject = [
        '$q',
        '$uibModalInstance',
        'nsiRestService',
        'selected'
    ];

    private reasons: Dictionary<Reason>;
    private reasonSelected: Dictionary<boolean> = {};
    private result: Reason[];
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(private $q: ng.IQService,
                private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private selected: string[]) {
    }

    $onInit() {
        this.selected.forEach(code => this.reasonSelected[code] = true);
        this.nsiRestService.get('PzzRefusalBase').then(reasons => {
            this.reasons = <Dictionary<Reason>>_.keyBy(reasons, 'code');
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });

    }

    changed() {
        this.result = _.keys(this.reasonSelected).filter(code => this.reasonSelected[code])
            .map(code => this.reasons[code]);
    }

    add() {
        this.$uibModalInstance.close(this.result);
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }

}

class Reason {
    code: string;
    name: string;
}

angular.module('app')
    .controller('DetermineRefusalCasesByPZZAddReasonController', DetermineRefusalCasesByPZZAddReasonController);