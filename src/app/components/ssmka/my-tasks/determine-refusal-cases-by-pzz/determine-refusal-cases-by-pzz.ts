class DetermineRefusalCasesByPZZController {
    static $inject = [
        'protocolResource',
        '$stateParams',
        '$q',
        '$uibModal',
        'toastr',
        'protocolTaskService',
        'nsiRestService',
        'session',
        'blockUI',
        'dateUtils'
    ];

    private taskWrapper: any;
    private origProtocol: Protocol;
    private protocol: Protocol;
    private decisions: Decision[];
    private questionsDecisions: Dictionary<Decision>;
    private departmentFullName: string;
    private questions: { protocolQuestion: ProtocolQuestion, refusalQuestion: RefusalQuestion }[] = [];
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(private protocolResource: IProtocolResource,
                private $stateParams: any,
                private $q: any,
                private $uibModal: any,
                private toastr: any,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private session: oasiSecurity.ISessionStorage,
                private blockUI: any,
                private dateUtils: IDateUtils) {
    }

    $onInit() {
        this.loadDocument(this.$stateParams.documentId).then(() => {
            this.departmentFullName = this.session.departmentFullName();
            return this.updateRefusalQuestion();
        }).then((refusal) => {
            this.questions = refusal.question.map(q => {
                return {
                    protocolQuestion: _.find(this.protocol.question, pq => pq.questionNumber === q.questionNumber),
                    refusalQuestion: q
                }
            });
            return this.loadDictionaries();
        }).then(() => {
            this.questionsDecisions = {};
            this.questions.map(q => q.protocolQuestion).forEach(q => {
                this.questionsDecisions[q.questionNumber] = _.find(this.decisions, d => d.decisionTypeCode === q.decisionTypeCode);
            });
            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    updateRefusalQuestion(): ng.IPromise<Refusal> {
        let accountName = this.session.login();
        let departmentCode = this.session.departmentCode();
        let refusal = _.find(this.protocol.refusalQuestion, ref => ref.executorDivisionCode === departmentCode);
        let executor = _.find(refusal.executor, ex => ex.executorsLogin = accountName);
        if (executor.executorCheck) {
            return this.$q.resolve(refusal);
        } else {
            executor.executorCheck = true;
            refusal.executor.filter(ex => ex.executorsLogin !== accountName).forEach(ex => ex.executorCheck = false);
            return this.saveProtocol(this.origProtocol, this.protocol).then(() => {
                this.origProtocol = angular.copy(this.protocol);
                return this.$q.resolve(refusal);
            })
        }

    }

    loadDocument(id: string): ng.IPromise<void> {
        return this.protocolResource.getById({id: id}).$promise.then((data: ProtocolDocumentWrapper) => {
            this.protocol = angular.copy(data.document.protocol);
            this.origProtocol = angular.copy(data.document.protocol);
            return this.$q.resolve();
        });
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]})
        ]).then((result: [Decision[]]) => {
            this.decisions = result[0];
        })
    }

    addReason(question: RefusalQuestion) {
        question.refuseReason = question.refuseReason || [];
        this.$uibModal.open({
            templateUrl: 'app/components/ssmka/my-tasks/determine-refusal-cases-by-pzz/addReason.html',
            controller: 'DetermineRefusalCasesByPZZAddReasonController as $ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            windowClass: '',
            resolve: {
                selected: () => {
                    return question.refuseReason.map(reason => reason.code);
                }
            }
        }).result.then((result: Reason[]) => {
            question.refuseReason = result.map(reason => {
                let result = new RefusalRefuseReason();
                result.code = reason.code;
                result.name = reason.name;
                return result;
            });
        });
    }

    submitHandler() {
        let block = this.blockUI.instances.get('determineRefusalCases');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, []);
        }).then(() => {
            this.toastr.info('Задача завершена.');
            block.stop();
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    saveHandler() {
        let block = this.blockUI.instances.get('determineRefusalCases');
        block.start();
        this.saveProtocol(this.origProtocol, this.protocol).then(() => {
            this.toastr.info('Данные сохранены.');
            block.stop();
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            block.stop();
        });
    }

    saveProtocol(origProtocol: Protocol, protocol: Protocol): ng.IPromise<any> {
        let formattedOrigProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(origProtocol)}});
        let formattedProtocol = this.dateUtils.formatDates({document: {protocol: angular.copy(protocol)}});
        let diff = jsonpatch.compare(formattedOrigProtocol, formattedProtocol);
        return this.protocolResource.patch({id: this.protocol.protocolID}, diff).$promise;
    }

    imageClass(code) {
        return {
            'fa-check-circle question-decision_approved': code === 'APPROVE',
            'fa-times-circle question-decision_declined': code === 'REJECT' || code === 'REWORK'
        };
    }
}

class DetermineRefusalCasesByPZZ {
    public controller: any = DetermineRefusalCasesByPZZController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/determine-refusal-cases-by-pzz/determine-refusal-cases-by-pzz.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('determineRefusalCasesByPzz', new DetermineRefusalCasesByPZZ());
