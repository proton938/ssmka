class GkSecretaryVotingFormController {
    votingResource: IVotingResource;

    private taskWrapper: any;
    private taskInfo: any = {
        description: '',
        meetingDate: null
    };
    private protocol: Protocol;
    private ballotPaper: BallotPaper;
    private notVoted: { [key: string]: string[] } = {};
    private voters: { [key: string]: any[] } = {};
    public decisionTypes: Decision[];
    private loadingStatus = LoadingStatus.LOADING;

    private wsConnectNumber = 0;
    private maxWsReconnectNumber = 5;
    private wsReconnectNumber = 0;
    private wsRetrying = false;

    private updateAllQuestions: boolean = false;
    private updatedVotesQuestions: string[] = null;

    private finishingVoting: boolean = false;
    private addingAttended: boolean = false;
    private completingQuestion: boolean = false;
    private excludingQuestion: boolean = false;

    private currentQuestionElemTop: number;
    private showToActiveButton: boolean = false;

    constructor(public meetingType: string,
                public votingResourceId: string,
                public participantProcessId: string,
                public participantTaskId: string,
                public $q: ng.IQService,
                private $stateParams: any,
                private $scope: any,
                private $stomp: any,
                private $state: any,
                private $sce: any,
                private $uibModal: any,
                private $injector: any,
                public protocolResource: IProtocolResource,
                public activityRestService: oasiBpmRest.ActivityRestService,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private alertService: oasiWidgets.IAlertService,
                public $timeout: any,
                public $interval: any,
                private toastr: Toastr,
                private baseUrl: string) {
        this.votingResource = this.$injector.get(this.votingResourceId);
    }

    $onInit() {
        this.initData();
    }

    applyActiveQuestionId(activeQuestionId?: string) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.protocol.question.forEach(q => {
                    if (_.isBoolean(q.activity) && q.activity) {
                        q.activity = false;
                    }
                    if (activeQuestionId && activeQuestionId === q.questionID) {
                        q.activity = true;
                    }
                });
                let currentQuestion = document.getElementsByClassName('current-question')[0];
                if (currentQuestion) {
                    this.currentQuestionElemTop = $(currentQuestion).offset().top;
                    this.updateShowActiveButtonVisibility();
                }
                else { //in case of element render takes too long (for too long lists on slow devices)
                    setTimeout(() => {
                        currentQuestion = document.getElementsByClassName('current-question')[0];
                        if (currentQuestion) {
                            this.currentQuestionElemTop = $(currentQuestion).offset().top;
                        }
                        this.updateShowActiveButtonVisibility();
                    }, 300);
                }
            });
        });
        this.$scope.$apply(() => {
            this.$timeout(() => {
            });
        });
    };

    applyExcluded(exludedQuestionsIds: string[]) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.protocol.question.forEach(q => {
                    if (_.indexOf(exludedQuestionsIds, q.questionID) >= 0) {
                        q.excluded = true;
                    }
                });
            });
        });
    };

    applyMeetingTimeEnd(meetingTimeEnd: Date) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                this.protocol.meetingTimeEnd = meetingTimeEnd;
            });
        });
    };

    applyVotesUpdated(questionsIds: string[]) {
        this.$scope.$apply(() => {
            this.$timeout(() => {
                if (questionsIds.length) {
                    if (!this.updateAllQuestions) {
                        this.updatedVotesQuestions = _.union(this.updatedVotesQuestions ? this.updatedVotesQuestions : [], questionsIds);
                    }
                } else {
                    this.updatedVotesQuestions = [];
                    this.updateAllQuestions = true;
                }
            });
        })
    }

    handleSuccess() {
        let subscriptions = [];

        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/active', (result: any) => {
            this.applyActiveQuestionId(result.active);
        }, {}));

        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/excluded', (exludedQuestionsIds: string[]) => {
            this.applyExcluded(exludedQuestionsIds);
        }, {}));

        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/end', (meetingTimeEnd: Date) => {
            this.applyMeetingTimeEnd(meetingTimeEnd);
        }, {}));

        subscriptions.push(this.$stomp.subscribe('/topic/voting/' + this.protocol.protocolID + '/protocol/votesUpdated', (questionsIds: string[]) => {
            this.applyVotesUpdated(questionsIds);
        }, {}));

        this.$scope.$on("$destroy", () => {
            subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
            this.$stomp.disconnect();
        });
        this.updateAllQuestions = true;
        this.updatedVotesQuestions = [];
        if (this.wsConnectNumber > 0) {
            this.loadDocument(this.protocol.protocolID).then((wrapper) => {
                const protocol = wrapper.document.protocol;
                this.protocol.meetingTimeEnd = protocol.meetingTimeEnd;
                for (let i = 0; i < this.protocol.question.length; i++) {
                    this.protocol.question[i] = _.extend(this.protocol.question[i],
                        _.pick(protocol.question[i], ['activity', 'excluded']));
                }
            })
        }
    }

    handleRetry(successCallback: (any) => void, errorCallback?: () => void) {
        if (this.wsConnectNumber > 0 && !this.wsRetrying) {
            this.toastr.warning("Произошла ошибка веб-сокет соединения, происходит попытка переподключения.");
        }
        this.wsRetrying = true;
        if (++this.wsReconnectNumber > this.maxWsReconnectNumber) {
            this.$timeout(() => {
                this.loadingStatus = LoadingStatus.ERROR;
                this.wsConnectNumber++;
                this.wsRetrying = false;
                if (this.wsConnectNumber > 0) {
                    this.toastr.error("Не удалось восстановить веб-сокет соединение.");
                } else {
                    this.toastr.error("Не удалось установить веб-сокет соединение.");
                }
                if (errorCallback) {
                    errorCallback();
                }
            })
        } else {
            this.$timeout(() => {
                this.wsConnect(successCallback, errorCallback);
            }, 2000);
        }
    }

    wsConnect(successCallback: (any) => void, errorCallback: () => void) {
        this.$stomp.connect(this.baseUrl + '/websocket', {}, () => {
            this.$scope.$apply(() => {
                this.handleRetry(successCallback, errorCallback);
            });
        }, {transports: ["websocket"]}).then((frame) => {
            this.wsRetrying = false;
            this.handleSuccess();
            successCallback(frame);
            this.wsReconnectNumber = 0;
            this.wsConnectNumber++;
        });
    }

    wsConnectPromise(): ng.IPromise<any> {
        let deferred = this.$q.defer();
        this.wsConnect((frame) => {
            deferred.resolve(frame);
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    applyStatus(status: VotingStatus) {
        this.protocol.question.forEach(q => {
            if (_.has(status.votingResults, q.questionID)) {
                q.resultsVote = status.votingResults[q.questionID];
            }

            if (_.has(status.voters, q.questionID)) {
                this.voters[q.questionID] = status.voters[q.questionID];
            }
            if (_.has(status.votedUsers, q.questionID) || _.has(status.voters, q.questionID)) {
                const notVoted = this.voters[q.questionID].filter(voter => {
                    return status.votedUsers[q.questionID].indexOf(voter.accountName) < 0;
                });
                this.notVoted[q.questionID] = this.$sce.trustAsHtml(notVoted.map(user => `<div>${user.fioFull}</div>`).join(''));
            }
        });
    }

    updateQuestionsVoteStatuses() {
        if (!this.updateAllQuestions && !this.updatedVotesQuestions) {
            return;
        }
        const ids = angular.copy(this.updatedVotesQuestions);
        this.updatedVotesQuestions = null;
        this.updateAllQuestions = false;
        this.$timeout(() => {
            this.votingResource.getVotingStatus({protocolId: this.protocol.protocolID}, ids).$promise.then((status) => {
                this.applyStatus(status);
            })
        });
    }

    initData() {
        Promise.all([
            this.getTaskInfo(
                this.$stateParams.documentId,
                this.$stateParams.formKey,
                this.$stateParams.taskId
            )
        ]).then((responses) => {
            this.setTaskInfoData(responses[0]);
            return this.$q.resolve();
        }).then(() => {
            return this.loadDictionaries();
        }).then(() => {
            const task = this.$interval(() => {
                this.updateQuestionsVoteStatuses();
            }, 3000);
            this.$scope.$on("$destroy", () => {
                this.$interval.cancel(task);
            });
            return this.$q.resolve();
        }).then(() => {
            return this.wsConnectPromise();
        }).then(() => {
            return this.votingResource.initVoting({
                protocolId: this.protocol.protocolID
            }, {}).$promise
        }).then(() => {
            this.$timeout(() => {
                this.loadingStatus = LoadingStatus.SUCCESS;
            })
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    };

    getTaskInfo(id: string, formKey: string, taskId: number): ng.IPromise<any> {
        let deferred: ng.IDeferred<TaskInfo> = this.$q.defer();
        let taskInfo: TaskInfo = {
            document: null,
            task: null,
            taskParams: {
                formKey: formKey,
                documentId: id,
                taskId: taskId
            }
        };
        this.loadDocument(id).then((document) => {
            taskInfo.task = this.taskWrapper.task;
            taskInfo.document = document;
            deferred.resolve(taskInfo);
        });
        return deferred.promise;
    }

    loadDocument(id: string): ng.IPromise<ProtocolDocumentWrapper> {
        let deferred: ng.IDeferred<ProtocolDocumentWrapper> = this.$q.defer();

        this.protocolResource.getById({id: id}, (data: ProtocolDocumentWrapper) => {
            deferred.resolve(data);
        }, () => {
            deferred.reject();
        });

        return deferred.promise;
    }

    loadDictionaries() {
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {nickAttr: 'meetingTypeCode', values: [this.protocol.meetingType]})
        ]).then(result => {
            this.decisionTypes = result[0];
        })
    }

    setTaskInfoData(taskInfo: TaskInfo) {
        this.protocol = taskInfo.document.document.protocol;
        this.taskInfo.description = taskInfo.task.description ? taskInfo.task.description : this.taskInfo.description;
        this.taskInfo.meetingDate = taskInfo.document.document.protocol.meetingDate;
    }

    completeQuestion(ind: number) {
        let questionID = this.protocol.question[ind].questionID;
        const question = _.find(this.protocol.question, q => q.questionID === questionID);
        if (GkSecretaryVotingFormController.isInconsistent(question)) {
            this.alertService.message({
                message: 'По рассматриваемому вопросу не принято решение. Необходимо продолжить голосование или снять вопрос с рассмотрения.'
            });
            return;
        }
        this.completingQuestion = true;
        this.votingResource.completeQuestionVoting({
            protocolId: this.protocol.protocolID,
            questionId: questionID
        }, {}, () => {
            this.completingQuestion = false;
        })
    }

    reopenQuestion(ind: number) {
        const activeQuestion = _.find(this.protocol.question, q => q.activity);
        if (activeQuestion) {
            let confirmPromise = GkSecretaryVotingFormController.hasVotes(activeQuestion) && GkSecretaryVotingFormController.isInconsistent(activeQuestion) ?
                this.alertService.confirm({
                    okButtonText: 'Да',
                    type: 'default',
                    message: 'По рассматриваемому вопросу не принято решение. Подтвердите переход к рассмотрению следующего вопроса'
                }) : this.alertService.confirm({
                    message: 'Направить на повторное рассмотрение?',
                    okButtonText: 'Да',
                    type: 'primary'
                });
            confirmPromise.then(() => {
                this.votingResource.reopenQuestionVoting({
                    protocolId: this.protocol.protocolID,
                    questionId: this.protocol.question[ind].questionID
                }, {})
            });
        }
        else {
            this.votingResource.reopenQuestionVoting({
                protocolId: this.protocol.protocolID,
                questionId: this.protocol.question[ind].questionID
            }, {});
        }
    }

    excludeQuestion(ind: number) {
        let questionID = this.protocol.question[ind].questionID;
        this.alertService.confirm({
            message: 'Снять данный вопрос с рассмотрения?',
            okButtonText: 'Да',
            type: 'danger'
        }).then(() => {
            this.excludingQuestion = true;
            this.votingResource.excludeQuestion({
                protocolId: this.protocol.protocolID,
                questionId: questionID
            }, {}, () => {
                this.excludingQuestion = false;
            }, () => {
                this.excludingQuestion = false;
            })
        });
    }

    updateProtocolFinish(): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.votingResource.finishVoting({
            protocolId: this.protocol.protocolID
        }, {}, () => {
            deferred.resolve();
        }, () => {
            deferred.reject();
        });
        return deferred.promise;
    }

    getInconsistent() {
        return this.protocol.question.filter(question => {
            if (question.excluded) return false;
            return !question.excluded && GkSecretaryVotingFormController.isInconsistent(question);
        });
    }

    static isInconsistent(question: ProtocolQuestion) {
        return !(Math.max(question.resultsVote.yes, question.resultsVote.no) / question.resultsVote.total >= 2 / 3);
    }

    static isQuestionApproved(question: ProtocolQuestion) {
        let votesCount = question.resultsVote.yes + question.resultsVote.no;
        return question.resultsVote.yes >= ((votesCount * 2) / 3);
    }

    finishVoting() {
        this.finishingVoting = true;

        let inconsistent = this.getInconsistent();
        if (inconsistent.length > 0) {
            let numbers = inconsistent[0].itemNumber;
            for (let i = 1; i < inconsistent.length; i++) {
                numbers += `, ${inconsistent[i].itemNumber}`;
            }
            this.toastr.error(`По вопрос${inconsistent.length === 1 ? 'у' : 'ам'} ${numbers} не принято решение. Необходимо вернуть вопрос${inconsistent.length === 1 ? '' : 'ы'} на голосование или снять с рассмотрения.`);
            this.finishingVoting = false;
            return;
        }
        this.protocol.question.forEach(q => {
            if (q.excluded) {
                return;
            }
            this.processQuestionOnFinishVoting(q, GkSecretaryVotingFormController.isQuestionApproved(q));
        });

        let isReject = this.protocol.question.some(q => !GkSecretaryVotingFormController.isQuestionApproved(q));
        this.protocolResource.saveVotingDecision({id: this.$stateParams.documentId}, this.protocol.question).$promise.then(() => {
            return this.updateProtocolFinish();
        }).then(() => {
            return this.protocolTaskService.updateProcess(this.$stateParams.taskId, [{
                name: "IsRejectVar",
                value: isReject
            }])
        }).then(() => {
            this.finishingVoting = false;
            location.assign('/oasi/#/app/my-tasks');
        }, () => {
            this.finishingVoting = false;
        })

    }

    processQuestionOnFinishVoting(question: ProtocolQuestion, questionApproved: boolean) {
    }

    addAttended() {
        this.$uibModal.open({
            templateUrl: 'app/components/ssmka/my-tasks/gk-secretary-voting-form/addAttendee.html',
            controller: 'GkSecretaryVotingFormAddAttendeeController as $ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            windowClass: 'our-edit-attendee-modal',
            resolve: {
                attended: () => {
                    return this.protocol.attended;
                },
                addParticipant: () => {
                    return (participant: ProtocolAttended) => {
                        this.addParticipants([participant]);
                    }
                },
                deleteParticipant: () => {
                    return this.deleteParticipant.bind(this);
                }
            }
        });
    }

    addParticipants(newAttended: ProtocolAttended[]) {
        this.addingAttended = true;
        return this.votingResource.addAttended({
            protocolId: this.protocol.protocolID
        }, newAttended).$promise.then(result => {
            return this.initProcesses(newAttended).then(() => {
                this.protocol.attended = result;
                this.addingAttended = false;
            });
        }).catch(() => {
            this.addingAttended = false;
        });
    }

    deleteParticipant(accountName: string) {
        this.addingAttended = true;
        return this.votingResource.deleteAttended({
                protocolId: this.protocol.protocolID
            },
            [accountName]).$promise.then((response: any[]) => {
            this.protocol.attended = response;
            return this.protocolTaskService.getTasksByEntityAndAssignee(this.participantTaskId, this.$stateParams.documentId, accountName);
        }).then((tasks: any[]) => {
            return this.$q.all(
                tasks.filter(t => t.processInstanceId).map(t => this.activityRestService.suspendProcess(t.processInstanceId))
            )
        }).then(() => {
            this.addingAttended = false;
        }).catch(() => {
            this.addingAttended = false;
        });
    }

    initProcesses(newAttended: ProtocolAttended[]): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.protocolTaskService.getProcessDefinition(this.participantProcessId).then((process: oasiBpmRest.IProcessDefinition) => {
            return this.$q.all(
                _.map(newAttended, attended => {
                    return this.protocolTaskService.initProcess(
                        process.id,
                        this.protocolTaskService.getInitProcessProperties(this.protocol, attended.accountName)
                    )
                })
            )
        }).then(() => {
            deferred.resolve();
        }, () => {
            deferred.resolve();
        });
        return deferred.promise;
    }

    setActiveQuestion(ind: number) {
        const question = _.find(this.protocol.question, q => q.activity);
        let confirmPromise = GkSecretaryVotingFormController.hasVotes(question) && GkSecretaryVotingFormController.isInconsistent(question) ?
            this.alertService.confirm({
                okButtonText: 'Да',
                type: 'default',
                message: 'По рассматриваемому вопросу не принято решение. Подтвердите переход к рассмотрению следующего вопроса'
            }) : this.$q.resolve();
        confirmPromise.then(() => {
            this.votingResource.setQuestionActive({
                protocolId: this.protocol.protocolID,
                questionId: this.protocol.question[ind].questionID
            }, {});
        });
    }

    static hasVotes(question: ProtocolQuestion) {
        return question.resultsVote.yes + question.resultsVote.no > 0;
    }

    getNotVotedQuestions() {
        return _.filter(this.ballotPaper.question, q => {
            let resultsVoteCode = q.resultsVote.resultsVoteCode;
            return _.isUndefined(resultsVoteCode) || _.isNull(resultsVoteCode);
        })
    }

    isActiveQuestion(ind: number) {
        return this.protocol.question[ind].activity === true;
    }

    isPassedQuestion(ind: number) {
        return this.protocol.question[ind].activity === false;
    }

    isFutureQuestion(ind: number) {
        let activity = this.protocol.question[ind].activity;
        return _.isNull(activity) || _.isUndefined(activity);
    }

    updateShowActiveButtonVisibility() {
        this.showToActiveButton = this.protocol.question.filter(question => question.activity === true).length > 0;
    }

    scrollToActive() {
        const current = document.getElementsByClassName('current-question')[0] || document.getElementsByClassName('finishing-vote')[0];
        window.scroll(0, $(current).offset().top);
    }

}