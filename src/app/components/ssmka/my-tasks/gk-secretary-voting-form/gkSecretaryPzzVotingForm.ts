class GkSecretaryPzzVotingFormController extends GkSecretaryVotingFormController {
    static $inject = [
        '$q',
        '$stateParams',
        '$scope',
        '$stomp',
        '$state',
        '$sce',
        '$uibModal',
        '$injector',
        'protocolResource',
        'activityRestService',
        'protocolTaskService',
        'nsiRestService',
        'alertService',
        '$timeout',
        '$interval',
        'toastr',
        'base_url'
    ];

    constructor(public $q: ng.IQService,
                private $stateParams: any,
                private $scope: any,
                private $stomp: any,
                private $state: any,
                private $sce: any,
                private $uibModal: any,
                private $injector: any,
                public protocolResource: IProtocolResource,
                public activityRestService: oasiBpmRest.ActivityRestService,
                private protocolTaskService: IProtocolTaskService,
                private nsiRestService: oasiNsiRest.NsiRestService,
                private alertService: oasiWidgets.IAlertService,
                public $timeout: any,
                public $interval: any,
                private toastr: Toastr,
                private baseUrl: string) {
        super('Городская комиссия по вопросам градостроительной деятельности', 'pzzVotingResource',
            'ss_quiz_vote_prc', 'ss_quiz_vote_ID',
            $q, $stateParams, $scope, $stomp, $state, $sce, $uibModal, $injector, protocolResource, activityRestService,
            protocolTaskService, nsiRestService, alertService, $timeout, $interval, toastr, baseUrl);
    }

    processQuestionOnFinishVoting(question: ProtocolQuestion, questionApproved: boolean) {
        let searchCode: string = questionApproved ? 'APPROVE' : 'REJECT',
            decisionType: Decision = _.find(this.decisionTypes, (decisionType) => {
                return decisionType.decisionTypeCode === searchCode;
            });
        question.decisionType = decisionType.decisionType;
        question.decisionTypeCode = decisionType.decisionTypeCode;
    }

}

class GkSecretaryPzzVotingFormComponent {
    public controller: any = GkSecretaryPzzVotingFormController;
    public templateUrl: string = 'app/components/ssmka/my-tasks/gk-secretary-voting-form/gkSecretaryVotingForm.html';
    public require: any = {
        taskWrapper: '^taskWrapper'
    }
}

angular.module('app').component('gkSecretaryPzzVotingForm', new GkSecretaryPzzVotingFormComponent());
