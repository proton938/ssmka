class AttendeeModalController {
    static $inject = [
        '$uibModalInstance',
        'attendee'
    ];

    constructor(
        private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance, private attendee: string
    ) {
    }

    save() {
        this.$uibModalInstance.close();
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }

}

angular.module('app').controller('AttendeeModalController', AttendeeModalController);