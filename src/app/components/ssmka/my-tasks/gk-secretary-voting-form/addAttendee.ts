class GkSecretaryVotingFormAddAttendeeController {
    static $inject = [
        '$q',
    	'$uibModalInstance',
    	'nsiRestService',
        'attended',
        '$uibModal',
        'addParticipant',
        'deleteParticipant'
    ];

    tableData: any = {};
    private attended: ProtocolAttended[];
    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;

    constructor(
        private $q: ng.IQService,
    	private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
    	private nsiRestService: oasiNsiRest.NsiRestService,
        private alreadyAttended: ProtocolAttended[],
        private $uibModal: any,
        private addParticipant: Function,
        private deleteParticipant: Function
    ) {
    }
    
    $onInit() {
        this.attended = this.alreadyAttended;
        this.nsiRestService.get('RolesParticipants').then((rolesParticipants: RoleParticipant[]) => {
            this.normalizeParticipantList(rolesParticipants).then((normalizedParticipantList) => {
                this.tableData = normalizedParticipantList;
                this.loadingStatus = LoadingStatus.SUCCESS;
            });
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    addParticipantHandler(selectedParticipant: NormalizedParticipant) {
        if (!selectedParticipant.accountName) {
            return;
        }
        this.addParticipant(this.normalizedToAttended(selectedParticipant));
    }

    deleteParticipantHandler(key: number) {
        this.$uibModal.open({
            templateUrl: 'app/components/ssmka/my-tasks/gk-secretary-voting-form/attendeeModal.html',
            controller: 'AttendeeModalController as $ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            windowClass: 'second-modal',
            resolve: {
                attendee: () => {
                    return this.tableData[key].selectedParticipant.shortFullName;
                }
            }
        }).result.then(() => {
            return this.deleteParticipant(this.tableData[key].selectedParticipant.accountName);
        }).then(() => {
            this.tableData[key].selectedParticipant = {
                shortFullName: '',
                accountName: null
            };
        }).catch(() => {});
    }

    async normalizeParticipantList(participantsList: RoleParticipant[]) {
        let attendedParticipants: ProtocolAttended[] = this.alreadyAttended || [],
            normalizedParticipantList: any = {};

        for (let participant of participantsList) {
            if (!participant.account_name) {
                continue;
            }

            let participantAccount: oasiNsiRest.UserBean;

            await this.nsiRestService.ldapUser(participant.account_name).then((user) => {
                participantAccount = user;
            }).catch((error) => {
                //console.error(error);
            });

            if ( participantAccount ) {
                let normalizedParticipant = {
                    shortFullName: participantAccount.fioShort || participantAccount.displayName,
                    fullName: participantAccount.displayName,
                    role: participant.RoleName,
                    checked: attendedParticipants.some((attendedParticipant) => {
                        return attendedParticipant.role.roleCode === participant.RoleCode;
                    }),
                    levelRoleCode: participant.LevelRoleCode,
                    mask: participant.mask,
                    post: participant.PostName,
                    accountName: participant.account_name,
                    roleCode: participant.RoleCode,
                    quorum: participant.quorum
                };
                let participanKey = participant.groupRoles + participant.LevelRoleCode;
                if ( !normalizedParticipantList.hasOwnProperty(participanKey) ) {
                    let emptyCase = {
                        shortFullName: '',
                        accountName: null
                    };
                    normalizedParticipantList[participanKey] = {
                        participantsList: {
                            emptyCase,
                            [normalizedParticipant.accountName]: normalizedParticipant
                        },
                        selectedParticipant: normalizedParticipant.checked ? normalizedParticipant : emptyCase,
                        roleName: normalizedParticipant.role,
                        roleKey: participanKey
                    };
                } else {
                    normalizedParticipantList[participanKey].participantsList[normalizedParticipant.accountName] = normalizedParticipant;
                    if (normalizedParticipant.checked) {
                        normalizedParticipantList[participanKey].selectedParticipant = normalizedParticipant;
                    }
                }
            }
        }
        return normalizedParticipantList;
    }

    attendParticipants() {
        this.attended = [];
        for (let roleCode in this.tableData) {
            if (this.tableData[roleCode].selectedParticipant.accountName !== null) {
                let selectedParticipant  = this.tableData[roleCode].selectedParticipant;
                this.attended.push(this.normalizedToAttended(selectedParticipant));
            }
        }
    }

    normalizedToAttended(normalizedParticipant: NormalizedParticipant): ProtocolAttended {
        return {
            accountName: normalizedParticipant.accountName,
            post: normalizedParticipant.post,
            iofShort: normalizedParticipant.shortFullName,
            fioFull: normalizedParticipant.fullName,
            role: {
                roleCode: normalizedParticipant.roleCode,
                roleName: normalizedParticipant.role
            },
            prefect: normalizedParticipant.mask
        }
    }
    
    save() {
	    this.$uibModalInstance.close();
    }
    
    cancel() {
        this.$uibModalInstance.dismiss();
    }    
    
}

angular.module('app').controller('GkSecretaryVotingFormAddAttendeeController', GkSecretaryVotingFormAddAttendeeController);