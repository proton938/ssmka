class ProtocolsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolsController;
        this.templateUrl = 'app/components/ssmka/protocols/protocols.html';
        this.bindings = {};
    }
}

class ProtocolsSearchResult {
    count: number;
    protocols: Protocol[];
}

class SortingProtocols {
    static PROTOCOL_APPROVED: string = 'protocolApproved';
    static MEETING_DATE: string = 'meetingDate';
    static MEETING_TYPE_ORDER: string = 'meetingTypeOrder';
    static QUESTIONS_NUM: string = 'questionsNum';
}

class ProtocolsController {
    static $inject = ['$q', 'protocolResource', 'meetingTypesService', 'ssAuthorizationService'];
    
    private hasViewPermission: boolean;

    private searchResult: SearchResult<Protocol>;
    private meetingTypes: { [key: string]: String };
    private loadingStatus: LoadingStatus;
    private loadingDataStatus: LoadingStatus;
    private viewExtSearch: boolean = false;
    private extSearchFilter: ProtocolSearchFilter = new ProtocolSearchFilter();
    
    private sortingAsc: boolean;
    private sorting: SortingAll = SortingProtocols.MEETING_DATE;
    private pagination: Pagination;
    private meetingTypesData: any[];

    constructor(private $q: any, private protocolResource: IProtocolResource, private meetingTypesService: IMeetingTypesService, private authorizationService: IAuthorizationService) {
    }
    
    reload() {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingDataStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            }            
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingDataStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_PROTOCOL');

        let sort = this.sorting;
        if (sort === SortingAll.MEETING_TYPE_ORDER) {
            sort = [
                `${SortingAll.MEETING_TYPE_ORDER},${this.sortingAsc ? 'asc' : 'desc'}`,
                `${SortingAll.MEETING_NUMBER},${this.sortingAsc ? 'desc' : 'asc'}`
            ]
        }
        else {
            sort += this.sortingAsc ? ',asc' : ',desc';
        }
        
        let params: any = {
            sort: sort,
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };        
        let error = () => {
            this.loadingDataStatus = LoadingStatus.ERROR;
        };

        if (!this.extSearchFilter.meetingTypes) {
            this.extSearchFilter.meetingTypes = availableMeetingTypes;
        }

        this.protocolResource.search(params, this.extSearchFilter, (result: any) => {
            this.searchResult = {
                data: _.map(result.content, (wrapper: ProtocolDocumentWrapper) => {
                    return wrapper.document.protocol;
                }),
                total: result.totalElements
            }
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            }
            this.loadingDataStatus = LoadingStatus.SUCCESS;           
        }, error);
    }

    toggleSorting(sorting: SortingAll) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }
        this.reload();
    }

    isSorted(field: SortingAll): boolean {
        return field === this.sorting;
    }

    $onInit() {
        this.meetingTypes = {};
        this.loadingStatus = LoadingStatus.LOADING;
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            _.each(meetingTypes, (meetingType) => {
                this.meetingTypes[meetingType.meetingType] = meetingType.meetingShortName;
            });
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.reload();
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });

    }

    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }

    setFilter(filter: ProtocolSearchFilter) {
        this.extSearchFilter = filter;
        this.reload();
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key];
        });
        this.reload();
    }
}

angular.module('app').component('protocols', new ProtocolsComponent());
