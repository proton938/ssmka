class ProtocolsExtSearchComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolsExtSearchController;
        this.templateUrl = 'app/components/ssmka/protocols/protocols-ext-search.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
    }
}

class ProtocolsExtSearchController {
    static $inject = ['$q', 'meetingTypesService', 'nsiRestService'];

    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: ProtocolSearchFilter;

    private loadingAppStatus: LoadingStatus;

    private meetingTypes: MeetingType[];
    private protocolApprovedOptions: string[] = ['Утвержден', 'Не утвержден'];
    private approvers: any[];

    constructor(
        private $q: ng.IQService,
        private meetingTypesService: IMeetingTypesService,
        private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypes = meetingTypes.filter((el) => !el.Express);
            this.loadingAppStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingAppStatus = LoadingStatus.ERROR;
        });
        this.$q.all([
            this.nsiRestService.searchUsers({group: ["OASI_MEETING_GK_PZZ_APPROVED"]}),
            this.nsiRestService.searchUsers({group: ["OASI_MEETING_PROTOCOLAPPROVE"]})
        ]).then((response: oasiNsiRest.UserBean[][]) => {
            this.approvers = response[0];
            for (let user of response[1]) {
                if (!_.find(this.approvers,u => u.accountName === user.accountName)) {
                    this.approvers.push(user);
                }
            }
        });
    }

    setFilter(extSearchFilter: ProtocolSearchFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    apply() {
        let filter = _.clone(this.searchFilter);
        if (filter.protocolApproved) {
            filter.protocolApproved = filter.protocolApproved === 'Утвержден';
        }
        if (filter.meetingTypes && filter.meetingTypes.length < 1) delete filter.meetingTypes;
        if (filter.approvedBy && filter.approvedBy.length < 1) delete filter.approvedBy;
        if (filter.meetingDate) {
            if (!filter.meetingDate.startDate) delete filter.meetingDate.startDate;
            if (!filter.meetingDate.endDate) delete filter.meetingDate.endDate;
        }
        this.applyFilter({
            value: filter
        });
    }

    cancel() {
        _.each(_.keys(this.searchFilter), (key) => {
            delete this.searchFilter[key];
        });
        this.clearFilter();
    }
}

angular.module('app').component('protocolsExtSearch', new ProtocolsExtSearchComponent());
