class QuestionsExtSearchComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionsExtSearchController;
        this.templateUrl = 'app/components/ssmka/questions/questions-ext-search.html';
        this.bindings = {
            applyFilter: '&',
            clearFilter: '&'
        };
        this.require = {
            parent: '^questions'
        }        
    }
}

class QuestionsExtSearchController implements IQuestionsFilterController {
    static $inject = ['$q', 'nsiRestService'];

    private parent: QuestionsController;
    
    private applyFilter: (value: any) => {};
    private clearFilter: () => {};
    private searchFilter: QuestionSearchFilter;

    private loadingAppStatus: LoadingStatus;

    private prefects: Prefect[];
    private responsibleDepartments: ResponsibleDepartment[];
    private meetingResponsibles: oasiNsiRest.UserBean[];
    private questionCategories: CategoryQuestion[];
    private decisionTypes: Decision[];

    private updatingUsersListInProgress: boolean = false;

    constructor(private $q: ng.IQService, private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.$q.all([
            this.nsiRestService.get('Prefect'),
            this.nsiRestService.get('ResponsibleDepartments'),
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE'),
            this.nsiRestService.get('CategoryQuestion'),
            this.nsiRestService.get('Decisions')
        ]).then((values: any[]) => {
            this.prefects = values[0];
            this.responsibleDepartments = values[1];
            this.meetingResponsibles = values[2];
            this.questionCategories = values[3];
            this.decisionTypes = values[4];
            this.loadingAppStatus = LoadingStatus.SUCCESS;
        });
        this.parent.setFilterController(this);
    }
    
    setFilter(extSearchFilter: QuestionSearchFilter) {
        this.searchFilter = angular.copy(extSearchFilter);
    }

    departmentChanged() {
        let departmentPrepareShort: string = this.searchFilter.departmentPrepare;
        this.updatingUsersListInProgress = true;
        if (departmentPrepareShort) {
            let department = _.find(this.responsibleDepartments, (department: ResponsibleDepartment) => {
                return department.departmentPrepareShort === departmentPrepareShort;
            })
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE', department.departmentPrepareCode).then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });
        } else {
            this.nsiRestService.ldapUsers('OASI_MEETING_RESPONSIBLE').then((users: any[]) => {
                this.meetingResponsibles = users;
                this.updatingUsersListInProgress = false;
            }).catch((err) => {
                this.updatingUsersListInProgress = false;
            });
        }
    }
    
    questionCategoryChanged() {
        let questionCategoryName: string = this.searchFilter.questionCategory; 
        if (questionCategoryName) {
            let questionCategory = _.find(this.questionCategories, (questionCategory: CategoryQuestion) => {
                return questionCategory.questionCategory === questionCategoryName;
            })
            this.nsiRestService.getBy('Decisions', {
                nickAttr: 'questionCode',
                values: [questionCategory.questionCode]
            }).then((decisionTypes: Decision[]) => {
                this.decisionTypes = decisionTypes;
            })
        } else {
            this.nsiRestService.get('Decisions').then((decisionTypes: Decision[]) => {
                this.decisionTypes = decisionTypes;
            })
        }
    }
    
    apply() {
        this.applyFilter({
            value: this.searchFilter
        });
    }
    
    cancel() {
        this.clearFilter();
    }

}

angular.module('app').component('questionsExtSearch', new QuestionsExtSearchComponent());
