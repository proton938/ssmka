namespace questions {
    export class Sorting {
        static DATE: string = "considerationDate";
        static ADDRESS: string = "address";
    }
    angular.module("app").constant("Sorting", {
        DATE: Sorting.DATE,
        ADDRESS: Sorting.ADDRESS
    });
}