class QuestionsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionsController;
        this.templateUrl = 'app/components/ssmka/questions/questions.html';
        this.bindings = {
        };
    }
}

class QuestionsController {
    static $inject = ['$state', '$stateParams'];

    private viewMode: ViewMode;
    private viewExtSearch: boolean;

    private query: string;
    private filter: QuestionSearchFilter;
    private filterController: IQuestionsFilterController;
    private listController: IChildQuesionsController;

    constructor(private $state: any, private $stateParams: any) {
    }

    $onInit() {
        this.query = this.$stateParams.query;
        this.viewMode = ViewMode.LIST;
        this.viewExtSearch = false;
        if (this.$stateParams.filter) {
            this.filter = this.$stateParams.filter;
            this.viewExtSearch = true;
        } else {
            this.filter = new QuestionSearchFilter();
            this.viewExtSearch = false;            
        }
    }

    isListViewMode(): boolean {
        return this.viewMode === ViewMode.LIST;
    }

    isMeetingsViewMode(): boolean {
        return this.viewMode === ViewMode.MEETINGS;
    }

    switchViewMode(viewMode: ViewMode) {
        this.viewMode = viewMode;
    }

    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }
    
    setFilterController(filterController: IQuestionsFilterController) {
        this.filterController = filterController;
        this.filterController.setFilter(this.filter);
    }

    setListController(listController: IChildQuesionsController) {
        this.listController = listController;
        this.listController.reload(this.query ? this.query : this.filter, true);
    }

    setFilter(filter: QuestionSearchFilter) {
        _.extend(this.filter, filter);
        if (this.query) {
            this.$state.go('app.ssmka.questions', {filter: this.filter, query: null});
        } else {
            this.filterController.setFilter(this.filter);
            this.listController.reload(this.filter, true);            
        }
    }

    clearFilter() {
        _.each(_.keys(this.filter), (key) => {
            delete this.filter[key]; 
        });
        if (this.query) {
            this.$state.go('app.ssmka.questions', {filter: this.filter, query: null});
        } else {
            this.filterController.setFilter(this.filter);
            this.listController.reload(this.filter, true);            
        }
    }

}

interface IQuestionsFilterController {
    setFilter(extSearchFilter: QuestionSearchFilter): void;
}
interface IChildQuesionsController {
    reload(filter: QuestionSearchFilter | string, reset: boolean): void;
}

angular.module('app').component('questions', new QuestionsComponent());
