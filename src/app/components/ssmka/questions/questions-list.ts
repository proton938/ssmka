class QuestionsListComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionsListController;
        this.templateUrl = 'app/components/ssmka/questions/questions-list.html';
        this.bindings = {
            switchViewMode: '&'
        };
        this.require = {
            parent: '^questions'
        }
    }
}

class QuestionsListController implements IChildQuesionsController {
    static $inject = ['questionResource', 'ssAuthorizationService', 'Sorting', 'Consideration', 'meetingTypesService'];

    private hasViewPermission: boolean;

    private parent: QuestionsController;

    private sorting: questions.Sorting;
    private sortingAsc: boolean;
    
    private pagination: Pagination;

    private loadingAppStatus: LoadingStatus;
    private searchResult: SearchResult<Question>;

	private filter: QuestionSearchFilter | string;
    private switchViewMode: (value: any) => {};
    private meetingTypesData: any[];

    constructor(private questionResource: IQuestionResource, private authorizationService: IAuthorizationService, private Sorting: any, private Consideration: any,
                private meetingTypesService: IMeetingTypesService) {
    }

    reload(filter: QuestionSearchFilter | string, reset?: boolean) {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingAppStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            };
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingAppStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
        
        if (reset) {
            this.reset();
        }

        this.filter = filter;

        if (!this.filter.meetingTypes) {
            this.filter.meetingTypes = availableMeetingTypes;
        }

        let params: any = {
            sort: this.sorting + ',' + (this.sortingAsc ? 'asc' : 'desc'),
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };
        let error = () => {
            this.loadingAppStatus = LoadingStatus.ERROR;
        };
        let success = (result: PageData<QuestionDocumentWrapper>) => {
            this.searchResult = {
                data: _.map(result.content, (wrapper: QuestionDocumentWrapper) => {
                    return wrapper.document.question;
                }),
                total: result.totalElements
            };
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            };
            this.loadingAppStatus = LoadingStatus.SUCCESS;            
        };
        if (_.isString(filter)) {
            _.extend(params, {query: filter});
            this.questionResource.searchQuery(params, success, error);            
        } else {
            this.questionResource.search(params, this.filter, success, error);            
        }        

    }
    
    reset() {
        this.sorting = questions.Sorting.DATE;
        this.sortingAsc = false;
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };        
    }
    
    $onInit() {
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            this.parent.setListController(this);
        }).catch(() => {
        });
    }

    isDateSorting(): boolean {
        return this.sorting === questions.Sorting.DATE;
    }

    isAddressSorting(): boolean {
        return this.sorting === questions.Sorting.ADDRESS;
    }

    switchSorting(sorting: questions.Sorting) {
        if (this.sorting === sorting) {
            this.sortingAsc = !this.sortingAsc;
        } else {
            this.sorting = sorting;
        }
        this.reload(this.filter, false);
    }

    switchToMeetings() {
        this.switchViewMode({ value: ViewMode.MEETINGS });
    }

}

angular.module('app').component('questionsList', new QuestionsListComponent());
