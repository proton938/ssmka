class QuestionsMeetingsComponent {
    public bindings: any;
    public require: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = QuestionsMeetingsController;
        this.templateUrl = 'app/components/ssmka/questions/questions-meetings.html';
        this.bindings = {
            switchViewMode: '&'
        };
        this.require = {
            parent: '^questions'
        }        
    }
}

class QuestionsMeetingsSearchResult {
    count: number;
    meetings: any[];
}

class QuestionsMeetingsController implements IChildQuesionsController {
    static $inject = ['questionResource', 'ssAuthorizationService', '$q', 'meetingTypesService'];
    
    private hasViewPermission: boolean;
    
    private parent: QuestionsController;
    
    private pagination: Pagination;
    
    private loadingStatus: LoadingStatus;
    private searchResult: SearchResult<MeetingGroup<DecisionGroup>>;
    
    private questions: { [key: string]: Question };
    
    private expanded: any;
    
    private filter: QuestionSearchFilter;
    private switchViewMode: (value: any) => {};
    private meetingTypesData: any[];

    constructor(private questionResource: IQuestionResource, private authorizationService: IAuthorizationService, private $q: ng.IQService,
                private meetingTypesService: IMeetingTypesService) {
    }
    
    reload(filter?: QuestionSearchFilter, reset?: boolean) {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            };
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
            
        if (reset) {
            this.reset();
        } 
            
        this.filter = filter;

        if (!this.filter.meetingTypes) {
            this.filter.meetingTypes = availableMeetingTypes;
        }

        let params: any = {
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };            
            
        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };  
        this.expanded = {};    
        let success = (result: PageData<MeetingGroup<DecisionGroup>>) => {
            this.searchResult = {
                data: result.content,
                total: result.totalElements
            };
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            };
            let questionsIds: string[] = _.chain(result.content).map((meetingGroup: MeetingGroup<DecisionGroup>) => {
                return meetingGroup.content;
            }).flatten().map((decisionGroup: DecisionGroup) => {
                return decisionGroup.content;
            }).flatten().value();       
            this.$q.all(
                _.map(questionsIds, (questionId: string) => {
                    return this.questionResource.getById({
                        id: questionId
                    }).$promise
                })
            ).then((result) => {
                _.each(result, (wrapper: QuestionDocumentWrapper) => {
                    let question: Question = wrapper.document.question;
                    this.questions[question.questionID] = question;
                });
                this.loadingStatus = LoadingStatus.SUCCESS;
            }, error)
        };    
        if (_.isString(filter)) {
            _.extend(params, {query: filter});
            this.questionResource.searchGroupQuery(params, success, error);
        } else {
            this.questionResource.searchGroup(params, this.filter, success, error);            
        }         
    } 
        
    reset() {
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };                 
    }

    $onInit() {
        this.questions = {};
        this.expanded = {};

        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            this.parent.setListController(this);
        }).catch(() => { });
    }
    
    switchToList() {
        this.switchViewMode({value: ViewMode.LIST});
    }
    
    toggle(index: number) {
        this.expanded[index] = !this.expanded[index];  
    }

}

angular.module('app').component('questionsMeetings', new QuestionsMeetingsComponent());
