class ProtocolEditDataAsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolEditDataAsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-edit-data-as.html';
        this.bindings = {
            protocol: "="
        };
    }
}

class ProtocolEditDataAsController {
    static $inject = ['nsiRestService'];

    private protocol: Protocol;

    private searchingCommissionMember = {};
    private searchingSecretaryResponsible: boolean;

    private commissionMembers: {};
    private secretaryResponsibles: SecretaryResponsible[];
    private inviteeOrgs: { code: string, name: string }[];

    constructor(private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    $onInit() {
        if (!this.protocol.secretaryResponsible) {
            this.protocol.secretaryResponsible = new SecretaryResponsible();
        }
        this.commissionMembers = {};
        if (this.protocol.commissionMember) {
            this.protocol.commissionMember.forEach((m, i) => {
                this.commissionMembers[i] = [m];
            });
        }
        this.secretaryResponsibles = this.protocol.secretaryResponsible ? [this.protocol.secretaryResponsible] : [];

        this.inviteeOrgs = this.protocol.attended.filter(att => !att.internal).map(att => {
            return {code: att.code, name: att.name};
        })

    }

    addCommissionMember() {
        this.protocol.commissionMember = this.protocol.commissionMember || [];
        this.protocol.commissionMember.push(new ProtocolCommissionMember());
    }

    removeCommissionMember(index) {
        this.protocol.commissionMember.splice(index, 1);
    }

    addInvitee() {
        this.protocol.invitee = this.protocol.invitee || [];
        this.protocol.invitee.push(new ProtocolInvitee());
    }

    removeInvitee(index) {
        this.protocol.invitee.splice(index, 1);
    }

    searchCommissionMembers(index: number, query: string) {
        const params = {fio: query};
        this.searchingCommissionMember[index] = true;
        this.searchMkaUsers(query).then(users => {
            this.commissionMembers[index] = users.map(this.convertToCommissionMember);
            this.searchingCommissionMember[index] = false;
        }, () => {
            this.searchingCommissionMember[index] = false;
        });
    }

    searchSecretaryResponsibles(query: string) {
        const params = {fio: query};
        this.searchingSecretaryResponsible = true;
        this.searchMkaUsers(query).then(users => {
            this.secretaryResponsibles = users.map(this.convertToSecretaryResponsible);
            this.searchingSecretaryResponsible = false;
        }, () => {
            this.searchingSecretaryResponsible = false;
        });
    }

    onChangeCommissionMember(index: number) {
        let commissionMember = this.protocol.commissionMember[index];
        let accountName = commissionMember.accountName;
        if (accountName) {
            this.protocol.commissionMember[index] = this.commissionMembers[index].find(m => m.accountName === accountName);
        } else {
            this.protocol.commissionMember[index] = new ProtocolCommissionMember();
        }
    }

    onChangeSecretaryResponsible() {
        let accountName = this.protocol.secretaryResponsible.accountName;
        if (accountName) {
            this.protocol.secretaryResponsible = this.secretaryResponsibles.find(m => m.accountName === accountName);
        } else {
            this.protocol.secretaryResponsible = new SecretaryResponsible();
        }
    }

    onChangeInviteeOrg(index: number) {
        let invitee = this.protocol.invitee[index];
        let orgCode = invitee.code;
        if (orgCode) {
            this.protocol.invitee[index].name = this.inviteeOrgs.find(m => m.code === orgCode).name;
        } else {
            this.protocol.invitee[index].name = null;
        }
    }

    convertToCommissionMember(user: User) {
        let res = new ProtocolCommissionMember();
        res.post = user.post;
        res.iofShort = user.iofShort;
        res.fioFull = user.displayName;
        res.accountName = user.accountName;
        return res;
    }

    convertToSecretaryResponsible(user: User) {
        let res = new SecretaryResponsible();
        res.post = user.post;
        res.iofShort = user.iofShort;
        res.fioFull = user.displayName;
        res.accountName = user.accountName;
        return res;
    }

    convertToInvitee(user: User) {
        let res = new ProtocolInvitee();
        res.post = user.post;
        res.iofShort = user.iofShort;
        res.fioFull = user.displayName;
        res.accountName = user.accountName;
        res.code = user.departmentCode;
        res.name = user.departmentFullName;
        return res;
    }

    searchMkaUsers(query: string): ng.IPromise<User[]> {
        const params = {fio: query, department: ['MKA']};
        return this.nsiRestService.searchUsers(params);
    }

    searchUsers(query: string): ng.IPromise<User[]> {
        const params = {fio: query};
        return this.nsiRestService.searchUsers(params);
    }

}

angular.module('app').component('protocolEditDataAs', new ProtocolEditDataAsComponent());
