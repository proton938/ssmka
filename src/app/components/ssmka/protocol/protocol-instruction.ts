class ProtocolInstructionComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolInstructionController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-instruction.html';
        this.bindings = {
            statuses: "<",
            instructions: "<"
        };
    }
}

class ProtocolInstructionController {
    static $inject = [
        'dateUtils'
    ];
    
    constructor(
        private dateUtils: IDateUtils
    ) {
    }

    $onInit() {
    }

    getDate(val: any) {
        let date: Date = null;
        if (typeof val === 'string') {
            date = new Date(val);
        } else if (_.isDate(val)) {
            date = <Date> val;
        }
        return date;
    }

    formatDate(val: any) {
        let date = this.getDate(val);
        if (date) {
            return this.dateUtils.formatDate(date, 'dd.MM.yyyy');
        }
    }

    getNextDay() {
        let current = new Date();
        let dif = (23 - current.getHours()) * 60 * 60 * 1000
                + (59 - current.getMinutes()) * 60 * 1000
                + (60 - current.getSeconds()) * 1000;
        let nextDay = new Date(current.getTime() + dif);
        return nextDay;
    }

    isOutdated(instruction: oasiSolarRest.SearchResultDocument) {
        let date = this.getDate(instruction.planDateInst);
        let currentDay = new Date();
        let nextDay = this.getNextDay();

        return !instruction.reportDateInst &&
               (date < nextDay) &&
               (date.getDate() !== currentDay.getDate());
    }

}

angular.module('app').component('protocolInstruction', new ProtocolInstructionComponent());
