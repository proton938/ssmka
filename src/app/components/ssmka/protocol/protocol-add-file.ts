class ProtocolAddFileComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolAddFileController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-add-file.html';
        this.bindings = {
            resolve: '<',
            close: '&',
            dismiss: '&'
        };
    }
}

class ProtocolAddFileController implements ng.ui.bootstrap.IModalServiceInstance {
    static $inject = ['toastr', 'protocolResource'];
    
    public close:(result?: any) => {};
    public dismiss:(reason?: any) => {};
    public result: ng.IPromise<any>;
    public opened: ng.IPromise<any>;
    public rendered: ng.IPromise<any>;
    public closed: ng.IPromise<any>;
    
    private resolve: any;
    private protocolID: string;
    
    private dzOptions: any;
    private dzCallbacks: any;
    private dzMethods: any;

    constructor(private toastr: Toastr, private protocolResource: IProtocolResource) {
    }

    $onInit() {
        this.protocolID = this.resolve.protocolID;
        this.dzOptions = {
            autoProcessQueue: false,
            withCredentials: true,
            parallelUploads: 1,
            paramName: "file",
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимое расширение: pdf',
            url: this.protocolResource.getAddFileUrl(this.protocolID),
            accept: (file, done) => {
                let fileName: string = file.name;
                let match = /.*\.pdf/.exec(fileName.toLowerCase());
                if (match) {
                    done();
                } else {
                    done("Допустимое расширение: pdf");
                }                    
            }
        };
        this.dzCallbacks = {
            error: (file, errorMessage) => {
                this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                this.dzMethods.removeFile(file);                
            },
            success: () => {
               this.close();
            }
        }
    }
    
    save() {
        this.dzMethods.processQueue();
    }
    
    cancel() {
        this.dismiss();
    }    
    
}

angular.module('app').component('protocolAddFile', new ProtocolAddFileComponent());
