class ApprovalSheetController {
    static $inject = [
        '$stateParams',
        'protocolResource',
        'pzzVotingResource',
        '$timeout',
        'nsiRestService'
    ];

    private loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    private questionsCount: number = 0;
    private tableData: any = [];
    constructor(
        private $stateParams: any,
        private protocolResource: IProtocolResource,
        private votingResource: IVotingResource,
        private $timeout: any,
        private nsiRestService: oasiNsiRest.NsiRestService
    ) {
    }
    $onInit() {
        Promise.all([
            this.initTableData(),
            this.nsiRestService.get('RolesParticipants')
        ]).then((responses: [BallotPaper[], RoleParticipant[]]) => {
            this.$timeout(() => {
                this.tableData = this.sortTableData(this.normalizeTableData(responses[0]), responses[1]);
                this.loadingStatus = LoadingStatus.SUCCESS;
            });
        }, (error) => {
            console.log(error);
        });
    }
    initTableData () {
        return this.votingResource.getProtocolBallot({protocolId: this.$stateParams.id}).$promise;
    }
    normalizeTableData (tableData: BallotPaper[]) {
        this.questionsCount = tableData.length;
        return tableData.map((ballotPapaer) => {
            return {
                iofShort: ballotPapaer.voting.iofShort,
                role: ballotPapaer.voting.role,
                file: ballotPapaer.file || null
            };
        });
    }
    sortTableData (tableData: any[], roles: RoleParticipant[]) {
        let sorted = [];
        let rolesNames = [];
        _.forEach(roles, role => {
           if (_.indexOf(rolesNames, role.RoleName) < 0) {
               rolesNames.push(role.RoleName);
           }
        });
        rolesNames.forEach(role => {
            tableData.forEach(row => {
                if (row.role && role === row.role) {
                    sorted.push(row);
                }
            })
        });
        tableData.forEach(row => { //workaround for participants having no role
            if (!row.role) {
                sorted.push(row);
            }
        });
        return sorted;
    }
}

angular.module('app').component('approvalSheet', {
    controller: ApprovalSheetController,
    templateUrl: 'app/components/ssmka/protocol/approval-sheet/approvalSheet.html',
    bindings: {
        protocolPromise: "<",
    }
});
