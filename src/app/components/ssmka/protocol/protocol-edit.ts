class ProtocolEditComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolEditController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-edit.html';
        this.bindings = {
            protocolPromise: "<",
            edit: '&'
        };
    }
}

class ProtocolEditController {
    static $inject = [
        '$timeout', '$state', '$scope', '$q', '$uibModal', 'protocolResource',
        'questionResource', 'nsiRestService', 'mggtService', 'dateUtils', 'toastr', 'ssAuthorizationService', 'Departments', 'MeetingType'
    ];

    private loadingStatus: LoadingStatus;

    private protocolPromise: ng.IPromise<Protocol>;
    private protocol: Protocol;
    private questions: { [key: string]: Question };
    private origMeetingAttendees: RoleParticipant[];
    private meetingAttendees: ProtocolAttended[];
    private decisionTypes: Decision[];
    private redactorGroupMemberInvest: ProtocolSigner[];
    private redactorGroupMemberMKA: ProtocolSigner[];
    private protocolGroupMember: ProtocolSigner[];   
    private protocolApprovers: ProtocolApprovedBy[];
    private questionCategories: CategoryQuestion[];
    public edit: (val: any) => {};

    private departments: any[];
    private internalAttendees: any[];
    private updatingUsersListInProgress: boolean = false;
    private externalAttendees: any[];

    constructor(
        private $timeout: any, private $state: any, private $scope: any, private $q: ng.IQService, private $uibModal: any, private protocolResource: IProtocolResource,
        private questionResource: IQuestionResource, private nsiRestService: oasiNsiRest.NsiRestService, private mggtService: MggtService,
        private dateUtils: IDateUtils, private toastr: Toastr, private authorizationService: IAuthorizationService, private Departments: any, private MeetingType: any) {
    }

    $onInit() {
        this.protocolPromise.then((data: any) => {
            if (this.authorizationService.check('OASI_MEETING_FORM_PROTOCOL')) {

                this.protocol = angular.copy(data.protocol);

                this.loadQuestions().then(() => {
                    this.loadDictionaries();
                }, () => {
                    this.loadingStatus = LoadingStatus.ERROR; 
                });

            } else {
                this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
                this.loadingStatus = LoadingStatus.ERROR;   
            }                
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;   
        });
    }
    
    loadQuestions() {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.questions = {};
        this.$q.all(
            _.map(this.protocol.question, (protocolQuestion: ProtocolQuestion) => {
                return this.questionResource.getById({
                    id: protocolQuestion.questionID
                }).$promise
            })
        ).then((result) => {
            _.each(result, (wrapper: QuestionDocumentWrapper) => {
                let question: Question = wrapper.document.question;
                this.questions[question.questionID] = question;
            });
            deferred.resolve();
        }, () => {
            deferred.reject();
        });
        return deferred.promise;  
    }
    
    loadDictionaries() {
        this.loadingStatus = LoadingStatus.LOADING; 
        let convertSigner = (user: oasiNsiRest.UserBean): ProtocolSigner => {
            return {
                iofShort: user.iofShort,
                post: user.post,
                fioShort: user.fioShort,
                accountName: user.accountName
            }            
        };
        let convertApprover = (user: oasiNsiRest.UserBean): ProtocolApprovedBy => {
            return {
                iofShort: user.iofShort,
                post: user.post,
                fioFull: user.displayName,
                accountName: user.accountName
            }            
        };
        this.$q.all([
            this.nsiRestService.get('RolesParticipants'),
            this.nsiRestService.getBy('Decisions', {
                nickAttr: 'meetingTypeCode',
                values: [this.protocol.meetingType]
            }),
            this.nsiRestService.ldapUsers('OASI_MEETING_REDACTORGROUP', this.Departments.MKA_OTHER_INVEST),
            this.nsiRestService.ldapUsers('OASI_MEETING_REDACTORGROUP', this.Departments.MKA),
            this.nsiRestService.ldapUsers('OASI_MEETING_PROTOCOLGROUP'),
            this.nsiRestService.ldapUsers('OASI_MEETING_PROTOCOLAPPROVE'),
            this.nsiRestService.get('CategoryQuestion'),
            this.nsiRestService.departments()
        ]).then((values: any[]) => {
            this.origMeetingAttendees = values[0].filter((el)=>{
                if (el.LevelRoleCode === 'organization') {
                    return el;
                }
            });
            this.meetingAttendees = _.map(this.origMeetingAttendees, (val: RoleParticipant) => {
                const result = new ProtocolAttended();
                result.name = val.RoleName;
                result.code = val.RoleCode;
                result.role = {
                    roleCode: val.RoleCode,
                    roleName: val.RoleName
                };
                return result;
            });
            this.decisionTypes = values[1];
            this.redactorGroupMemberInvest = _.map(values[2], convertSigner);
            this.redactorGroupMemberMKA = _.map(values[3], convertSigner);
            this.protocolGroupMember = _.map(values[4], convertSigner);
            this.protocolApprovers = _.map(values[5], convertApprover);
            this.questionCategories = values[6];
            this.departments = values[7];
            this.prepareAttendees();

            this.loadingStatus = LoadingStatus.SUCCESS;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;   
        });        
    }
    
    attendedAdded() {
        this.externalAttendees = _.sortBy(this.externalAttendees, (att) => {
            return _.findIndex(this.origMeetingAttendees, (orig) => {
                return orig.RoleCode === att.code;
            });
        })
    }

    private createProtocol(protocol: Protocol): ng.IPromise<string> {
        let deferred = this.$q.defer<string>();
        this.protocolResource.create({
            document: {
                protocol: protocol
            }
        }, (data: ProtocolCreateResult) => {
            deferred.resolve(data.id);
        });
        return deferred.promise;
    }

    private updateProtocol(protocol: Protocol): ng.IPromise<boolean> {
        let id: string = protocol.protocolID;
        let deferred = this.$q.defer<boolean>();
        this.protocolResource.update({ id: id }, {
            document: {
                protocol: protocol
            }
        }, function() {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    private saveProtocol(protocol: Protocol): void {
        let p: Protocol = angular.copy(this.protocol);
        _.extend(p, protocol);
        let newProtocol = !p.protocolID;
        let promise = newProtocol ? this.createProtocol(p) : this.updateProtocol(p);
        promise.then(result => {
            if (newProtocol) {
                p.protocolID = result;
            }
            return p.protocolApproved ? this.mggtService.sendProtocolAgendaAndQuestions(p, p.agendaID, _.keys(this.questions)) : this.$q.resolve();
        }).then(() => {
            this.$state.go('app.ssmka.protocol', {id: p.protocolID, editing: false}, {reload: true});
        });
    }

    save() {
        this.protocol.attended = [];
        if (this.internalAttendees.length && this.internalAttendees[0].result) {
            this.internalAttendees.map(el => {
                this.protocol.attended.push(el.result);
            });
        }
        if (this.externalAttendees.length) {
            this.protocol.attended = this.protocol.attended.concat(this.externalAttendees);
        }
        this.saveProtocol(this.protocol);
    }
    
    cancelEdit() {
        this.edit({val: false})
    }

    clearAttendees() {
        this.externalAttendees = [];
    }

    departmentsChange(department, index) {
        this.updatingUsersListInProgress = true;
        this.internalAttendees[index].users = [];
        this.nsiRestService.ldapUsers(null, department.description).then((users: any) => {
            this.internalAttendees[index].users = users;
            this.internalAttendees[index].result = {};
            this.updatingUsersListInProgress = false;
        });
    }

    prepareAttendees() {
        this.internalAttendees = [];
        this.externalAttendees = [];

        if (this.protocol.attended && this.protocol.attended.length) {

            this.protocol.attended.map((el) => {
                if (el.internal && el.internal === true) {
                    this.internalAttendees.push({result: el, department: el.name});
                }
                else {
                    this.externalAttendees.push(el);
                }
            });

            if (!this.internalAttendees.length) {
                this.internalAttendees.push({});
            }

            if (!this.externalAttendees.length && !this.protocol.agendaID) {
                this.externalAttendees = this.meetingAttendees;
            }

        }
        else {
            this.internalAttendees.push({});
            if (!this.protocol.agendaID) {
                this.externalAttendees = this.meetingAttendees;
            }
        }


    }

    internalAttendeeChange(attendee, index) {
        attendee.name = attendee.departmentFullName;
        attendee.code = attendee.departmentCode;
        attendee.internal = true;
        attendee.fioFull = attendee.displayName;
        this.internalAttendees[index].result = attendee;
    }

    addInternalAttendee() {
        this.internalAttendees.push({});
    }

    removeInternalAttendee(index) {
        if (this.internalAttendees.length === 1) {
            this.internalAttendees[0] = {};
            return;
        }
        this.internalAttendees.splice(index, 1);
    }

}

angular.module('app').component('protocolEdit', new ProtocolEditComponent());
