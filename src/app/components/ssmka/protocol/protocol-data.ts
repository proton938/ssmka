class ProtocolDataController {
    static $inject = [];

    constructor() {
    }

    $onInit() {
    }

}

class ProtocolDataComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolDataController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-data.html';
        this.bindings = {
            protocol: "<"
        };
    }
}

angular.module('app').component('protocolData', new ProtocolDataComponent());
