class ProtocolApprovalsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolApprovalsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-approvals.html';
        this.bindings = {
            protocol: "<"
        };
    }
}

class ProtocolApprovalsController {
    static $inject = [
    ];

    approvalHistoryListExpanded: _.Dictionary<boolean> = {};

    constructor() {
    }

    $onInit() {
    }
}

angular.module('app').component('protocolApprovals', new ProtocolApprovalsComponent());
