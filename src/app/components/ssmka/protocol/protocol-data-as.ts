class ProtocolDataAsController {
    static $inject = [];

    constructor() {
    }

    $onInit() {
    }

}

class ProtocolDataAsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolDataAsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-data-as.html';
        this.bindings = {
            protocol: "<"
        };
    }
}

angular.module('app').component('protocolDataAs', new ProtocolDataAsComponent());
