class ProtocolEditDecisionsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolEditDecisionsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-edit-decisions.html';
        this.bindings = {
            protocol: "=",
            questions: '<',
            decisionTypes: '<'
        };
    }
}

class ProtocolEditDecisionsController {
    static $inject = [
        'ssAuthorizationService', 'MeetingType'
    ];

    private hasQuestionViewPermission: boolean;

    private protocol: Protocol;
    private questions: { [key: string]: Question };
    private decisionTypes: Decision[];

    constructor(private authorizationService: IAuthorizationService, private MeetingType: any) {
    }

    $onInit() {
        this.hasQuestionViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
    }

    decisionTypeChanged(ind: number) {
        let decisionType: Decision = _.find(this.decisionTypes, (decisionType: Decision) => {
            return decisionType.decisionTypeCode === this.protocol.question[ind].decisionTypeCode;
        });
        this.protocol.question[ind].decisionType = decisionType.decisionType;
    }

    getDecisionTypes(protocolQuestion: ProtocolQuestion) {
        let question = this.questions[protocolQuestion.questionID];
        return _.filter(this.decisionTypes, (decisionType: Decision) => {
            return decisionType.questionCode === question.questionConsider.questionCategoryCode;
        });
    }

}

angular.module('app').component('protocolEditDecisions', new ProtocolEditDecisionsComponent());
