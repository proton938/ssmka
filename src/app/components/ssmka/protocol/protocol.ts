class ProtocolComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol.html';
        this.bindings = {};
    }
}

class ProtocolController {
    static $inject = ['$q', '$state', '$stateParams', 'protocolResource', 'agendaResource', 'ssAuthorizationService', 'toastr',
        'meetingTypesService'
    ];

    private protocolPromise: ng.IPromise<Protocol>;
    private protocol: Protocol;
    private loadingStatus: LoadingStatus;
    
    private editing: boolean;

    constructor(private $q: ng.IQService, private $state: any, private $stateParams: any, private protocolResource: IProtocolResource, private agendaResource: IAgendaResource,
        private authorizationService: IAuthorizationService, private toastr: Toastr,
                private meetingTypesService: IMeetingTypesService) {
    }

    $onInit() {
        
        this.editing = this.$stateParams.editing;
        let agendaID: string = this.$stateParams.agendaID;

        let id: string = this.$stateParams.id;
        if (!id) {            
            this.protocolPromise = this.newProtocol(agendaID);
            this.editing = true;
        } else {
            this.protocolPromise = this.loadProtocol(id);
        }

    }
    
    reload(): ng.IPromise<any> {
        this.protocolPromise = this.loadProtocol(this.protocol.protocolID);
        return this.protocolPromise; 
    }

    newProtocol(agendaID: string): ng.IPromise<any> {
        let success = (protocol) => {
            this.protocol = protocol;
            deferred.resolve({
                protocol: protocol
            });
            this.loadingStatus = LoadingStatus.SUCCESS;            
        }
        let deferred: ng.IDeferred<any> = this.$q.defer();
        if (agendaID) {
            this.agendaResource.createProtocol({
                id: agendaID
            }, (wrapper: ProtocolDocumentWrapper) => {
                success(wrapper.document.protocol);
            })
        } else {
            success(new Protocol());
        }
        return deferred.promise;
    }

    loadProtocol(id: string): ng.IPromise<any> {
        this.loadingStatus = LoadingStatus.LOADING;
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.protocolResource.getById({ id: id }, (data: ProtocolDocumentWrapper) => {
            if (this.meetingTypesService.checkMeetingType(data.document.protocol.meetingType)) {
                this.protocol = data.document.protocol;
                deferred.resolve({
                    protocol: this.protocol
                });
                this.loadingStatus = LoadingStatus.SUCCESS;
            } else {
                deferred.reject();
                this.loadingStatus = LoadingStatus.ERROR;
                this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");                
            }
        })
        return deferred.promise;
    }
    
    edit(edit: boolean) {
        this.editing = edit;
    }     
}

angular.module('app').component('protocol', new ProtocolComponent());
