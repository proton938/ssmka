class ProtocolEditAsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolEditAsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-edit-as.html';
        this.bindings = {
            protocolPromise: "<",
            edit: '&'
        };
    }
}

class ProtocolEditAsController {
    static $inject = [
        '$timeout', '$state', '$scope', '$q', '$uibModal', 'protocolResource',
        'questionResource', 'nsiRestService', 'mggtService', 'dateUtils', 'toastr', 'ssAuthorizationService', 'Departments', 'MeetingType'
    ];

    private loadingStatus: LoadingStatus;

    private protocolPromise: ng.IPromise<Protocol>;
    private protocol: Protocol;
    private questions: { [key: string]: Question };
    private decisionTypes: Decision[];
    private folderGuid: string;
    private meetingProtocolProjectClass: string;
    private fileProtocolProject: any[];
    public edit: (val: any) => {};

    constructor(private $timeout: any, private $state: any, private $scope: any, private $q: ng.IQService, private $uibModal: any, private protocolResource: IProtocolResource,
                private questionResource: IQuestionResource, private nsiRestService: oasiNsiRest.NsiRestService, private mggtService: MggtService,
                private dateUtils: IDateUtils, private toastr: Toastr, private authorizationService: IAuthorizationService, private Departments: any, private MeetingType: any) {
    }

    $onInit() {
        this.protocolPromise.then((data: any) => {
            if (!this.authorizationService.check('OASI_MEETING_FORM_PROTOCOL')) {
                this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
                return this.$q.reject();
            }

            this.protocol = angular.copy(data.protocol);
            return this.loadQuestions();
        }).then(() => {
            return this.loadDictionaries();
        }).then(() => {
            let fileProtocolProject = _.find(this.protocol.fileProtocol, file => file.classFile === this.meetingProtocolProjectClass);
            this.fileProtocolProject = fileProtocolProject ? [fileProtocolProject] : [];
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadQuestions() {
        this.questions = {};
        return this.$q.all(
            _.map(this.protocol.question, (protocolQuestion: ProtocolQuestion) => {
                return this.questionResource.getById({
                    id: protocolQuestion.questionID
                }).$promise
            })
        ).then((result) => {
            _.each(result, (wrapper: QuestionDocumentWrapper) => {
                let question: Question = wrapper.document.question;
                this.questions[question.questionID] = question;
            });
        });
    }

    loadDictionaries() {
        let convertSigner = (user: oasiNsiRest.UserBean): ProtocolSigner => {
            return {
                iofShort: user.iofShort,
                post: user.post,
                fioShort: user.fioShort,
                accountName: user.accountName
            }
        };
        let convertApprover = (user: oasiNsiRest.UserBean): ProtocolApprovedBy => {
            return {
                iofShort: user.iofShort,
                post: user.post,
                fioFull: user.displayName,
                accountName: user.accountName
            }
        };
        return this.$q.all([
            this.nsiRestService.getBy('Decisions', {
                nickAttr: 'meetingTypeCode',
                values: [this.protocol.meetingType]
            }),
            this.nsiRestService.getBy('MeetingFilenetSettings', {
                nickAttr: 'meetingType',
                values: [this.protocol.meetingType]
            }),
            this.nsiRestService.get('Templates')
        ]).then((values: any[]) => {
            this.decisionTypes = values[0];
            this.folderGuid = values[1][0].protocolFolderGuid;
            const template = _.find(values[2], (t: Template) => _.some(t.meetingType, mt => mt.meetingType === this.protocol.meetingType));
            this.meetingProtocolProjectClass = template.meetingProtocolProjectClass;
        });
    }

    private createProtocol(protocol: Protocol): ng.IPromise<string> {
        let deferred = this.$q.defer<string>();
        this.protocolResource.create({
            document: {
                protocol: protocol
            }
        }, (data: ProtocolCreateResult) => {
            deferred.resolve(data.id);
        });
        return deferred.promise;
    }

    private updateProtocol(protocol: Protocol): ng.IPromise<boolean> {
        let id: string = protocol.protocolID;
        let deferred = this.$q.defer<boolean>();
        this.protocolResource.update({id: id}, {
            document: {
                protocol: protocol
            }
        }, function () {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    private saveProtocol(protocol: Protocol): void {
        let p: Protocol = angular.copy(this.protocol);
        _.extend(p, protocol);
        let newProtocol = !p.protocolID;
        let promise = newProtocol ? this.createProtocol(p) : this.updateProtocol(p);
        promise.then(result => {
            if (newProtocol) {
                p.protocolID = result;
            }
            return p.protocolApproved ? this.mggtService.sendProtocolAgendaAndQuestions(p, p.agendaID, _.keys(this.questions)) : this.$q.resolve();
        }).then(() => {
            this.$state.go('app.ssmka.protocol', {id: p.protocolID, editing: false}, {reload: true});
        });
    }

    save() {
        this.protocol.fileProtocol = _.filter(this.protocol.fileProtocol, file => file.classFile !== this.meetingProtocolProjectClass);
        if (this.fileProtocolProject.length) {
            let file = this.fileProtocolProject[0];
            let fileProtocolProject = file.mimeType ? FileType.create(file.idFile, file.nameFile, file.sizeFile, file.signed, new Date(file.dateFile),
                file.mimeType, file.typeFile) : file;
            this.protocol.fileProtocol = this.protocol.fileProtocol || [];
            this.protocol.fileProtocol.push(fileProtocolProject);
        }
        this.saveProtocol(this.protocol);
    }

    cancelEdit() {
        this.edit({val: false})
    }

}

angular.module('app').component('protocolEditAs', new ProtocolEditAsComponent());
