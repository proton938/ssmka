class ProtocolViewComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolViewController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-view.html';
        this.bindings = {
            protocolPromise: "<",
            edit: '&',
            reload: '&'
        };
    }
}

class ProtocolViewController {
    static $inject = [
        '$q', '$uibModal', 'protocolResource', 'instructionsService',
        'meetingTypesService', 'nsiRestService', 'fileResource',
        'ssAuthorizationService', 'authorizationService', 'alertService', 'wordReporterPreviewService',
        'dateFilter', 'MeetingType', 'session'
    ];
    
    private hasQuestionViewPermission: boolean;
    private hasJsonViewPermission: boolean;
    private hasHistoryViewPermission: boolean;
    private hasApprovalSheetViewPermission: boolean;

    private loadingStatus: LoadingStatus;
    
    private protocolPromise: ng.IPromise<Protocol>;
    private protocol: Protocol;
    private instructions: ProtocolQuestionInstructions[];
    private fileProtocolProject: FileType;
    private fileProtocol: FileType;
    private meetingType: MeetingType;
    private canViewProjectFiles: boolean;
    private canViewFiles: boolean;
    public edit: (val: any) => {};
    public reload: () => ng.IPromise<any>;

    constructor(
        private $q: any, private $uibModal: any, private protocolResource: IProtocolResource, private instructionsService: IInstructionsService,
        private meetingTypesService: IMeetingTypesService, private nsiRestService: oasiNsiRest.NsiRestService, private fileResource: IFileResource,
        private ssAuthorizationService: IAuthorizationService, private authorizationService: oasiSecurity.IAuthorizationService, private alertService: oasiWidgets.IAlertService,
        private wordReporterPreviewService: oasiWordreporterRest.WordReporterPreviewService,
        private dateFilter: any, private MeetingType: any,
        private session: oasiSecurity.ISessionStorage
    ) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;

        const isCardAdmin = this.authorizationService.check('PROTOCOL_CARD_ADMIN');

        this.hasQuestionViewPermission = this.ssAuthorizationService.check('OASI_MEETING_CARD_QUESTION');

        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };
        this.protocolPromise.then((data: any) => {
            this.setProtocol(data.protocol).then(() => {
                this.hasJsonViewPermission = isCardAdmin &&
                    (this.protocol.meetingType === 'PZZ' || this.protocol.meetingType === 'GK_EOO');
                this.hasHistoryViewPermission = isCardAdmin &&
                    this.protocol.meetingType === 'GK_EOO';
                this.hasApprovalSheetViewPermission = (this.protocol.meetingType === 'PZZ' && this.authorizationService.check('OASI_MEETING_GK_PZZ_SECRETARY')) ||
                    (this.protocol.meetingType === 'GK_EOO' && this.authorizationService.check('OASI_MEETING_GK_EOO_SECRETARY'));

                if (this.protocol.meetingType === this.MeetingType.PZZ) {
                    const groups = this.session.groups();
                    this.canViewProjectFiles = groups.some(gr => gr === 'OASI_MEETING_GK_PZZ_ACCESS_PROJECT_FILES');
                    this.canViewFiles = groups.some(gr => gr === 'OASI_MEETING_GK_PZZ_ACCESS_FILES');
                } else {
                    this.canViewProjectFiles = true;
                    this.canViewFiles = true;
                }
                this.$q.all([
                    this.meetingTypesService.getMeetingTypes(),
                    this.instructionsService.getProtocolInstructions(data.protocol)
                ]).then((result: any) => {
                    let meetingTypes: MeetingType[] = result[0];
                    this.meetingType = _.find(meetingTypes, (meetingType) => {
                        return meetingType.meetingType === this.protocol.meetingType;
                    });
                    this.instructions = result[1];
                    this.loadingStatus = LoadingStatus.SUCCESS;
                }, error);
            })
        }, error);
    }

    setProtocol(protocol: Protocol): ng.IPromise<any> {
        let deferred: ng.IDeferred<any> = this.$q.defer();
        this.protocol = protocol;

        this.nsiRestService.get('Templates').then((templates: Template[]) => {
            const template = _.find(templates, (t: Template) => _.some(t.meetingType, mt => mt.meetingType === protocol.meetingType));
            this.fileProtocolProject = _.find(this.protocol.fileProtocol, (file: FileType) => {
                return file.classFile === template.meetingProtocolProjectClass;
            });
            this.fileProtocol = _.find(this.protocol.fileProtocol, (file: FileType) => {
                return file.classFile === template.meetingProtocolClass;
            });
            deferred.resolve();
        });
        return deferred.promise;
    }

    editProtocol() {
        this.edit({ val: true });
    }

    createReport() {
        if (this.protocol.meetingType === this.MeetingType.AS) {
            let dataNotFilled = this.protocol.question.some(q => !q.decisionText || !q.decisionType);
            if (dataNotFilled) {
                this.alertService.message({
                    message: 'Необходимо внести решение по всем вопросам протокола',
                    type: 'warning'
                });
                return;
            }
        }
        this.protocolResource.createReport({}, {
            id: this.protocol.protocolID 
        }, () => {
            this.reload().then((data: any) => {
                this.setProtocol(data.protocol)
            });            
        });
    }
    
    sendDecision() {
        this.protocolResource.sendDecision({
            id: this.protocol.protocolID
        }, () => {
            this.alertService.message({
                message: 'Решение успешно отправлено'
            });            
        });
    } 
    
    showAddFileDialog() {
        this.$uibModal.open({
            component: 'protocol-add-file',
            resolve: {
                protocolID: () => {
                    return this.protocol.protocolID
                }
            },
            size: 'lg'
        }).result.then(() => {
            this.reload().then((data: any) => {
                this.setProtocol(data.protocol)
            }); 
        }); 
    }

    createProtocolWithInstructionsPrintForm() {
        const json = this.instructionsService.getProtocolWithInstructions(this.protocol, this.meetingType, this.instructions);
        this.wordReporterPreviewService.nsi2Preview({
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать протокол с поручениями",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: "Протокол с поручениями.docx",
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({
                document: {
                    protocol: json
                }
            }),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'ssProtocolInstr'
            }
        });
    }

    
}

angular.module('app').component('protocolView', new ProtocolViewComponent())
    .filter('notExcludedQuestions', () => {
        return function (questions: ProtocolQuestion[]) {
            return _.filter(questions, question => {
                return !question.excluded;
            })            
        };
    });
