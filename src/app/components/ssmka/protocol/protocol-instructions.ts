class ProtocolInstructionsComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolInstructionsController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-instructions.html';
        this.bindings = {
            data: "<"
        };
    }
}

class ProtocolInstructionsController {
    static $inject = [
        'nsiRestService', 'ssAuthorizationService'
    ];
    
    private loadingStatus: LoadingStatus;

    private hasQuestionViewPermission: boolean;

    private statuses: any[];
    private data: InstrQuestion[];

    constructor(
        private nsiRestService: oasiNsiRest.NsiRestService,
        private authorizationService: IAuthorizationService
    ) {
    }

    $onInit() {
        this.loadingStatus = LoadingStatus.LOADING;

        this.hasQuestionViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');

        this.nsiRestService.get('OasiInstructionStatus').then((statuses) => {
            this.statuses = statuses;
        }).then(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        })
    }
    

}

angular.module('app').component('protocolInstructions', new ProtocolInstructionsComponent());
