class ProtocolJsonEditorController {
    static $inject = ['toastr', '$state', 'protocolResource', 'dateUtils'];

    constructor(private toastr: Toastr,
                private $state,
                private protocolResource: IProtocolResource,
                private dateUtils: IDateUtils) {
    }

    private origDocument: any;
    private document: any;

    private jsonEditorOptions: {
        mode: string
    };

    $onInit() {
        this.origDocument = this.dateUtils.formatDates(angular.copy(this.document));
        this.document = this.dateUtils.formatDates(angular.copy(this.document));
        this.jsonEditorOptions = {
            mode: 'tree'
        };
    }

    save() {
        let id = this.document.protocolID;
        let diff = jsonpatch.compare({document: {protocol: this.origDocument}}, {document: {protocol: this.document}});
        this.protocolResource.patch({id: id}, diff).$promise.then(() => {
            this.$state.reload();
        });

    }

}

class ProtocolJsonEditorComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = ProtocolJsonEditorController;
        this.templateUrl = 'app/components/ssmka/protocol/protocol-json-editor.html';
        this.bindings = {
            document: "<"
        };
    }
}

angular.module('app').component('protocolJsonEditor', new ProtocolJsonEditorComponent());
