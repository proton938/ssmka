class PresentationPrepareComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = PresentationPrepareController;
        this.templateUrl = 'app/components/ssmka/presentation_prepare/presentation_prepare.html';
        this.bindings = {};
    }
}

class PresentationPrepareController {
    static $inject = ['$q', 'questionResource', 'ssAuthorizationService', 'fileResource', 'MaterialType', 'meetingTypesService'];
    
    private hasViewPermission: boolean;

    private viewExtSearch: boolean;

    private extSearchFilter: QuestionSearchFilter;
    
    private pagination: Pagination;
    
    private loadingStatus: LoadingStatus;
    private searchResult: SearchResult<PresentationPrepareGroup>;
    
    private questions: { [key: string]: Question };
    
    private expanded: any;

    private meetingTypesData: any[];

    constructor(private $q: ng.IQService, private questionResource: IQuestionResource, private authorizationService: IAuthorizationService, private fileResource: IFileResource, private MaterialType: any,
                private meetingTypesService: IMeetingTypesService) {
    }
    
    reload() {
        
        let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
        if (!meetingTypes || meetingTypes.length === 0) {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.searchResult = {
                data: [],
                total: 0
            };
            return;
        }

        let availableMeetingTypes = this.meetingTypesData.filter((el) => {
            if (meetingTypes.indexOf(el.meetingType) !== -1 && !el.Express) {
                return el;
            }
        }).map((el) => el.meetingType);
        
        this.loadingStatus = LoadingStatus.LOADING;
        this.hasViewPermission = this.authorizationService.check('OASI_MEETING_CARD_QUESTION');
        
        let error = () => {
            this.loadingStatus = LoadingStatus.ERROR;
        };  
        this.expanded = {};
        const params: any = {
            page: this.pagination.pageNumber - 1,
            size: this.pagination.pageSize
        };

        if (!this.extSearchFilter.meetingTypes) {
            this.extSearchFilter.meetingTypes = availableMeetingTypes;
        }

        this.questionResource.searchPrepare(params, this.extSearchFilter, (result: PageData<PresentationPrepareGroup>) => {
            this.searchResult = {
                data: result.content,
                total: result.totalElements
            }            
            this.pagination = {
                pageNumber: result.pageNumber + 1,
                pageSize: result.pageSize,
                totalPages: result.totalPages                
            }   
            let questionsIds: string[] = _.chain(result.content).map((presentationPrepareGroup: PresentationPrepareGroup) => {
                return presentationPrepareGroup.content;
            }).flatten().map((meetingGroup: MeetingGroup<string>) => {
                return meetingGroup.content;
            }).flatten().value();       
            this.$q.all(
                _.map(questionsIds, (questionId: string) => {
                    return this.questionResource.getById({
                        id: questionId
                    }).$promise
                })
            ).then((result) => {
                _.each(result, (wrapper: QuestionDocumentWrapper) => {
                    let question: Question = wrapper.document.question;
                    this.questions[question.questionID] = question;
                });
                this.loadingStatus = LoadingStatus.SUCCESS;
            }, error)   
            this.loadingStatus = LoadingStatus.SUCCESS;      
        }, error);
    }

    $onInit() {
        this.viewExtSearch = false;
        this.extSearchFilter = new QuestionSearchFilter();
        this.expanded = {};
        this.questions = {};
        this.pagination = {
            totalPages: 0,
            pageNumber: 1,
            pageSize: 10
        };
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            this.reload();
        }).catch(() => { });
    }
    
    toggleViewExtSearch() {
        this.viewExtSearch = !this.viewExtSearch;
    }
    
    toggle(id: number) {
        this.expanded[id] = !this.expanded[id];
    }  
    
    setFilter(filter: QuestionSearchFilter) {
        _.extend(this.extSearchFilter, filter);
        this.reload();
    }

    clearFilter() {
        _.each(_.keys(this.extSearchFilter), (key) => {
            delete this.extSearchFilter[key]; 
        });
        this.reload();
    }  
    
    getMaterialGroup(question: Question, materialType: string) {
        return _.find(question.questionConsider.materials, (materialGroup) => {
            return materialGroup.materialType === materialType;
        });
    }
    
    getFileLink(id) {
        return this.fileResource.getDownloadUrl(id);
    }    

}

angular.module('app').component('presentationPrepare', new PresentationPrepareComponent());
