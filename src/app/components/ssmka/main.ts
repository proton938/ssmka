class MainComponent {
  public bindings: any;
  public controller: any;
  public templateUrl: string;

  constructor() {
    this.controller = MainController;
    this.templateUrl = 'app/components/ssmka/main.html';
    this.bindings = {
    };
  }
}

class MainController {
}

angular.module('app').component('main', new MainComponent());
