class CalendarComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = CalendarController;
        this.templateUrl = 'app/components/ssmka/calendar/calendar.html';
        this.bindings = {};
    }
}

class CalendarEvent {
    id: string;
    title: string;
    start: any;
    end?: any;
    url: string;
    className?: string;
    backgroundColor?: string;
    textColor?: string;
}

class CalendarController {
    static $inject = ['$q', '$state', '$stateParams', '$rootScope', '$timeout', 'agendaResource', 'protocolResource', 'meetingTypesService', 'dateUtils', 'timeUtils',
        'session'];

    private meetingTypes: { [key: string]: MeetingType };
    private meetingTypesData: MeetingType[];
    private loadingStatus: LoadingStatus;

    constructor(private $q: ng.IQService, private $state: any, private $stateParams: any, private $rootScope: any, private $timeout: any,
        private agendaResource: IAgendaResource, private protocolResource: IProtocolResource, private meetingTypesService: IMeetingTypesService,
        private dateUtils: IDateUtils, private timeUtils: ITimeUtils, private session: oasiSecurity.ISessionStorage) {
    }

    $onInit() {
        this.meetingTypes = {};
        this.meetingTypesService.getMeetingTypes().then((meetingTypes: MeetingType[]) => {
            this.meetingTypesData = meetingTypes;
            _.each(meetingTypes, (meetingType: MeetingType) => {
                this.meetingTypes[meetingType.meetingType] = meetingType;
            });
            this.initCalendar();
            this.loadingStatus = LoadingStatus.SUCCESS;
        }).catch(() => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    createEvent(meetingType: MeetingType, agenda: Agenda, protocol: Protocol): CalendarEvent {
        let url: string;
        if (protocol && !meetingType.Express) {
            url = this.$state.href('app.ssmka.protocol', {id: protocol.protocolID});
        } else {
            url = this.$state.href('app.ssmka.agenda', {id: agenda.agendaID});
        }
        const { backgroundColor, textColor } = this.getEventColor(agenda);
        let result: CalendarEvent = {
            id: agenda.agendaID,
            title: meetingType? meetingType.meetingShortName : agenda.meetingType,
            start: moment(this.getDateTime(agenda.meetingDate, agenda.meetingTime)),
            url: url,
            backgroundColor,
            textColor,
        };
        if (agenda.meetingTimeEnd) {
            result.end = moment(this.getDateTime(agenda.meetingDate, agenda.meetingTimeEnd));
        }
        return result;
    }

    getDateTime(date: Date, time: string) {
        let startTime: Date = this.timeUtils.parse(time);
        let dateTime: Date = new Date(date.getTime());
        dateTime.setHours(startTime.getHours());
        dateTime.setMinutes(startTime.getMinutes());
        return dateTime;
    }

    getEventColor(agenda: Agenda): {backgroundColor: string, textColor: string} {
        const meetingTypeData: MeetingType = this.meetingTypesData.find(mtd => mtd.meetingType === agenda.meetingType);
        if( meetingTypeData && meetingTypeData.meetingColor ) {
            return {
                backgroundColor: meetingTypeData.meetingColor,
                textColor: '#FFF',
            };
        }
        return {
            backgroundColor: '#DCDCDC',
            textColor: '#000',
        };
    }

    loadProtocol(agenda: Agenda): ng.IPromise<Protocol> {
        let deferred: ng.IDeferred<Protocol> = this.$q.defer();
        if (agenda.meetingNumber) {
            this.protocolResource.getByMeetingNumber({
                meetingType: agenda.meetingType,
                meetingNumber: agenda.meetingNumber,
                year: agenda.meetingDate.getFullYear()
            }, (wrapper: ProtocolDocumentWrapper) => {
                deferred.resolve(wrapper.document.protocol);
            });
        } else {
            deferred.resolve(null);
        }
        return deferred.promise;
    }

    initCalendar() {
        this.$timeout(() => {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay',
                },

                defaultDate: new Date(),
                aspectRatio: 2.5,
//                height: 'auto',
//                contentHeight: 'auto',
                fixedWeekCount: false,
                locale: 'ru',
                buttonIcons: false, // show the prev/next text
                weekNumbers: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                timeFormat: 'HH:mm',
                events: (start, end, timezone, callback) => {

                    let meetingTypes = this.meetingTypesService.getUserMeetingTypes();
                    const dateFormat = 'YYYY-MM-DD';

                    if (!meetingTypes || meetingTypes.length === 0) {
                        return;
                    }

                    let availableMeetingTypes = this.meetingTypesData.map((el) => el.meetingType);

                    let params: any = {
                        page: 0,
                        size: 1000,
                        sort: 'meetingDate,asc'
                    };

                    let filter: any = {
                        meetingDate: {
                            startDate: start.format(dateFormat),
                            endDate: end.format(dateFormat)
                        },
                        meetingTypes: availableMeetingTypes
                    };

                    const login = this.session.login();

                    this.agendaResource.searchPublished(params, filter, (result:any) => {

                        let agendasWrappers:AgendaDocumentWrapper[] = 
                            (result.content)
                            ? result.content.filter((wrapper: AgendaDocumentWrapper) => {
                                const agenda = wrapper.document.agenda;
                                let meetingType = this.meetingTypes[agenda.meetingType];
                                return !meetingType.Express ||
                                    (agenda.attended && agenda.attended.some(att => att.accountName === login)) ||
                                    (agenda.authorCreate && agenda.authorCreate.accountName === login) ||
                                    (agenda.initiator && agenda.initiator.accountName === login)
                            }) 
                            : [];

                        this.$q.all(
                            _.chain(agendasWrappers).filter((wrapper: AgendaDocumentWrapper) => {
                                return !this.meetingTypes[wrapper.document.agenda.meetingType].Express;
                            }).map((wrapper: AgendaDocumentWrapper) => {
                                return this.loadProtocol(wrapper.document.agenda);
                            }).value()
                        ).then((result: Protocol[]) => {
                            let protocols: { [key: string]: Protocol } = {};
                            _.forEach(result, (protocol: Protocol) => {
                                if (protocol) {
                                    protocols[protocol.agendaID] = protocol;
                                }
                            });
                            callback(_.map(agendasWrappers, (wrapper: AgendaDocumentWrapper): CalendarEvent => {
                                let agenda = wrapper.document.agenda;
                                let meetingType = this.meetingTypes[wrapper.document.agenda.meetingType];
                                return this.createEvent(meetingType, agenda, protocols[agenda.agendaID]);
                            }));
                        });

                        this.updateBreadcrumbs();

                    });
                }
            });
        });
    }

    updateBreadcrumbs() {

        let newBreadcrumbs = [];
        let navigatorCode = 'calendar';
        let sidebar = this.$rootScope["sidebar"];

        if (sidebar) {

            sidebar.forEach(item => {
                if (item["code"] === navigatorCode) {
                    newBreadcrumbs.push({
                        title: item["name"],
                        url: item["url"]
                    });
                }
            });

            if (!this.$rootScope["breadcrumbs"]) {
                this.$rootScope["breadcrumbs"] = [];
            }

            this.$rootScope["breadcrumbs"].length = 0;
            this.$rootScope["breadcrumbs"] = this.$rootScope["breadcrumbs"].concat(newBreadcrumbs);
        }

    }

}

angular.module('app').component('calendar', new CalendarComponent());
