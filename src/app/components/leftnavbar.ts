class LeftNavBarComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = LeftNavBarController;
        this.templateUrl = 'app/components/leftnavbar.html';
        this.bindings = {};
    }
}

class LeftNavBarController {
    static $inject = ['$state'];
    constructor(private $state: any) {
    }
}

angular.module('app').component('leftnavbar', new LeftNavBarComponent());
