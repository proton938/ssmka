class AppComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = AppController;
        this.templateUrl = 'app/components/app.html';
        this.bindings = {};
    }
}

class AppController {
    static $inject = ['$stateParams', '$rootScope'];

    private disableNavigator: any;
    
    constructor(private $stateParams: any, private $rootScope: any) {
        this.permissionFailed = this.$stateParams.permissionFailed;
        delete this.$stateParams.permissionFailed;
        delete this.$stateParams.permissionChecked;
        this.disableNavigator = this.$stateParams.disableNavigator ? true : false;
    }
    
    permissionFailed: boolean;
    
}

angular.module('app').component('app', new AppComponent());