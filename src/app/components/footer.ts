class FooterComponent {
  public bindings: any;
  public controller: any;
  public templateUrl: string;

  constructor() {
    this.controller = FooterController;
    this.templateUrl = 'app/components/footer.html';
    this.bindings = {};
  }
}

class FooterController {
  static $inject = ['configResource'];
  private version: string = "";

  constructor(private configResource: IConfigResource) {
    this.configResource.getVersion((result) => {
        this.version = result.version;
    })
  }
}

angular.module('app').component('footer', new FooterComponent()); 