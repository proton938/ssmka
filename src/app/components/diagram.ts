class DiagramComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = DiagramController;
        this.templateUrl = 'app/components/diagram.html';
        this.bindings = {};
    }
}

class DiagramController {
    static $inject = ['$stateParams'];
    private src: string;
    constructor(
        private $stateParams: TopNavBarStateParams,
    ) {
    }

    $onInit() {
        this.src = this.$stateParams.image;
    }

}

angular.module('app').component('diagram', new DiagramComponent());
