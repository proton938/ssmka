angular.module('app').service('tokenInterceptor', ['$q', '$injector', function ($q, $injector) {
    let requestInterceptor = {
        request: function (config) {
            let deferred = $q.defer();
            if (/activiti-rest/.test(config.url)) {
                deferred.resolve(config);
            } else {
                deferred.resolve(config);
            }
            return deferred.promise;
        }
    };
    return requestInterceptor;
}]);

angular.module('app').config(['$httpProvider', function ($httpProvider) {
    // $httpProvider.interceptors.push('tokenInterceptor');
}]);
