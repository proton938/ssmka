/**
 * модуль обеспечения серверного логгирования
 */
let logModule = angular.module("app");

//server writer
logModule.run([function () {
    ServerLogger.init('ssmka');
}]);

//js errors
logModule.config(globalExceptionHandlerConfig);

//http layer interceptors
logModule.factory('httpErrorInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new SSMKAServerErrorHandler($q);
}]);

logModule.factory('loginInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    //два похожих запроса - поиск по АД и авторизация
    return new LoginInterceptor(new UrlPatternMatcher('/auth/user', ['users'], null));
}]);
logModule.factory('authenticationInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new LogoutInterceptor(new UrlPatternMatcher('/auth/logout', null, null));
}]);
logModule.factory('activityTaskInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new ActivityTaskInterceptor($stateParams, new UrlPatternMatcher('/service/runtime/tasks', null, null));
}]);
logModule.factory('documentsSearchInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new DocumentsSearchInterceptor(new UrlPatternMatcher('/questions?query', null, null));
}]);
logModule.factory('documentUpdateInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new DocumentUpdateInterceptor($stateParams, new UrlPatternMatcher('/questions/\\w+\\/update', null, null));
}]);
logModule.factory('fileUploadInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new FileUploadInterceptor($stateParams, new UrlPatternMatcher('/questions/\\w+\\/materials/add',
        ['createFolder', 'changeFolderInfo', 'sign', 'hashhex', 'folderinfo', 'delete', 'folderContent', 'fileinfo', 'verify'],
        null));
}]);
logModule.factory('fileDownloadInterceptor', ['$stateParams', '$q', function ($stateParams, $q) {
    return new FileDownloadInterceptor($stateParams, new UrlPatternMatcher('/files/\\w+\\',
        ['createFolder', 'changeFolderInfo', 'sign', 'hashhex', 'folderinfo', 'delete', 'folderContent', 'fileinfo', 'verify'],
        null));
}]);

logModule.config(['$httpProvider', function ($httpProvider: ng.IHttpProvider) {
    $httpProvider.interceptors.push('httpErrorInterceptor');
    $httpProvider.interceptors.push('loginInterceptor');
    $httpProvider.interceptors.push('authenticationInterceptor');
    $httpProvider.interceptors.push('activityTaskInterceptor');
    $httpProvider.interceptors.push('documentsSearchInterceptor');
    $httpProvider.interceptors.push('documentUpdateInterceptor');
    $httpProvider.interceptors.push('fileUploadInterceptor');
    $httpProvider.interceptors.push('fileDownloadInterceptor');
}]);

//app transitions monitor
logModule.run(['$stateParams', '$state', '$rootScope', function ($stateParams, $state, $rootScope) {
    let monitor = new RoutesTransitionMonitor($stateParams, $rootScope);

    //showcases
    monitor.statesMap.push(['app.ssmka.main', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Главная')))
    }]);

    monitor.statesMap.push(['app.ssmka.planned_questions', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Планируемые вопросы заседаний')));
    }]);

    monitor.statesMap.push(['app.ssmka.questions', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Вопросы заседаний')));
    }]);

    monitor.statesMap.push(['app.ssmka.question', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Вопрос СС МКА', $stateParams['id'])));
    }]);

    //card
    monitor.statesMap.push(['app.ssmka.agendas.published', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Повестки заседаний')));
    }]);

    monitor.statesMap.push(['app.ssmka.agendas.projects', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Проекты повесток заседаний')));
    }]);

    monitor.statesMap.push(['app.ssmka.agenda', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Повестка заседания', $stateParams['id'])));
    }]);

    monitor.statesMap.push(['app.ssmka.protocols', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Протоколы заседаний')));
    }]);

    monitor.statesMap.push(['app.ssmka.protocol', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Протокол заседания', $stateParams['id'])));
    }]);

    monitor.statesMap.push(['app.ssmka.calendar', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Календарь')));
    }]);

    monitor.statesMap.push(['app.ssmka.presentation_prepare', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Подготовка презентаций')));
    }]);

    monitor.statesMap.push(['app.ssmka.my-tasks', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Витрина Мои задачи')));
    }]);

    monitor.statesMap.push(['app.ssmka.process.gkPzzRegisterSS', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Регистрация участников ГК', $stateParams['documentId'])));
    }]);

    monitor.statesMap.push(['app.ssmka.process.VoteSS', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Электронное голосование', $stateParams['documentId'])));
    }]);

    monitor.statesMap.push(['app.ssmka.process.gkEooRegisterSS', ($stateParams) => {
        serverLog(new Action(ActionType.OPEN_FORM, new OpenFormMessage('Карточка Регистрация участников ГК ЭОО', $stateParams['documentId'])));
    }]);

    monitor.start();
}]);
