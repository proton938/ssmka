//TODO: перенести в oasi-widgets как dateSelectorSs или расширить существующий dateSelector
class DateSelectorController {
    static $inject = ['$state', '$scope'];
    public value: any;
    public validatable: boolean;
    public validate: boolean;
    public disabled: boolean;
    public myWidth: string;
    public myChange: (val: any) => {};
    public style: any;

    private dateOpened: boolean;
    private options: any;  
    
    private dateOpen() {
        this.dateOpened = true;
    } 

    $onInit() {
        this.myWidth = this.myWidth || '200';
        this.style = {
            width: this.myWidth + 'px'
        }
    }
    
    ngChange() {
        this.myChange({value: this.value});
    }
}

class DateSelectorComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = DateSelectorController;
        this.templateUrl = 'app/widgets/dateSelector.html';
        this.bindings = {
            value: '=ngModel',
            validatable: '=',
            validate: '=',
            disabled: '=',
            myWidth: '=',
            myChange: '&'
        };
    }
}

angular.module('app').component('dateSelectorSs', new DateSelectorComponent());
