class MultiInputController {
    public items: string[];
    public onChange: any;

    /** @ngInject */
    static $inject = ['$scope'];

    constructor(public $scope) {
        this.items = null;
    }

    $onChanges(props) {
        if (props.items && typeof props.items.currentValue !== "undefined") {
            // чтобы не изменить внешний массив
            this.items = [].concat(props.items.currentValue);
            this.initView();
        }
        if (props.onChange && typeof props.onChange.currentValue !== "undefined") {
            this.onChange = props.onChange.currentValue;
        }
    }

    initView() {
        // если пустых нет
        if (!this.items.some(item => !item)) {
            this.addEmpty()
        }
    }

    onInputChange(value: string, valueIndex: number) {
        this.items[valueIndex] = value;
        const removeOtherEmpty = () => {
            // ищем второй с конца пустой и удаляем, если такой есть
            let emptyIndex;
            this.items.map((element, index, array) => {
                // именно пустой и не текущий
                if (!element && index !== valueIndex) {
                    emptyIndex = index;
                }
            });
            // взять именно последний и удалить
            if (typeof emptyIndex !== "undefined") {
                this.items.splice(emptyIndex, 1)
            }
        };
        // и если значение не пустое (срабатывает 1 раз)
        if (!value) {
            // найти пустой другой и удалить
            removeOtherEmpty();
            // после удаления пустого
            this.callback();
        } else {
            // перед созданием пустого
            this.callback();
            // и пустых нет
            if (!this.items.some(item => !item)) {
                this.addEmpty()
            }
        }
    }

    callback() {
        if (this.onChange) {
            this.onChange(this.items.filter(i => !!i));
        }
    }

    addEmpty() {
        this.items.push('')
    }
}

class MultiInputComponent {
    public bindings: any;
    public controller: any;
    public templateUrl: string;

    constructor() {
        this.controller = MultiInputController;
        this.templateUrl = 'app/widgets/multi-input/multi-input.html';
        this.bindings = {
            items: '<',
            onChange: '<'
        };
    }
}

angular.module('app').component('multiInput', new MultiInputComponent());
