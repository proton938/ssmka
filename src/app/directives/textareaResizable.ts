class TextareaResizable implements ng.IDirective {
    restrict: string;

    static factory(): ng.IDirectiveFactory {
        const directive = () => new TextareaResizable();
        directive.$inject = [];
        return directive;
    }

    constructor() {
        this.restrict = 'A';
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery) => {
        element.on('change keyup keydown paste cut', function (){
            $(this).height(0).height(this.scrollHeight);
        });
    }
}

angular.module('app').directive('textareaResizable', TextareaResizable.factory());
