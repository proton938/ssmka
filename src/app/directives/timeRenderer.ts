interface TimeRendererScope extends ng.IScope {
    model: Date;
    ngModel: string;
    format: string;
}

class TimeRenderer implements ng.IDirective {
    restrict: string;
    require: string;
    templateUrl: string;
    scope: any;

    static factory(): ng.IDirectiveFactory {
        const directive = (dateFilter: any, timeUtils: ITimeUtils) => new TimeRenderer(dateFilter, timeUtils);
        directive.$inject = ['dateFilter', 'timeUtils'];
        return directive;
    }

    constructor(private dateFilter: any, private timeUtils: ITimeUtils) {
        this.restrict = 'E';
        this.require = 'ngModel';
        this.templateUrl = 'app/directives/timeRenderer.html';
        this.scope = {
            ngModel: "<",
            format: "@"
        };
    }

    link = ($scope: TimeRendererScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        this.initModel($scope, $scope.ngModel);
        $scope.format = $scope.format || 'HH:mm'; 
        ngModel.$render = () => {
            this.initModel($scope, ngModel.$modelValue);
        }        
    }

    initModel($scope: TimeRendererScope, value: string) {
        $scope.model = this.timeUtils.parse(value);
    }
}

angular.module('app').directive('timeRenderer', TimeRenderer.factory());