class MyAuthLink implements ng.IDirective {
    restrict = 'E';
    transclude = true;

    static factory(): ng.IDirectiveFactory {
        const directive = ($rootScope: ng.IRootScopeService, authorizationService: IAuthorizationService) => new MyAuthLink($rootScope, authorizationService);
        directive.$inject = ['$rootScope', 'ssAuthorizationService'];
        return directive;
    }

    constructor(private $rootScope: ng.IRootScopeService, private authorizationService: IAuthorizationService) {
    }
    
    defineVisibility(element: ng.IAugmentedJQuery, subject: string, href: string, $transclude: any) {
        if (this.authorizationService.check(subject)) {
            let el: any = $('<a>').attr('href', href).append($transclude());
            element.replaceWith(el);
        } else {
            element.replaceWith($transclude());
        }
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: any, ctrls: any, $transclude: any) => {
        let subject: string = attrs.myAuth.replace(/\s+/g, '');
        let href: string = attrs.href;
        if (subject.length > 0) {
            this.defineVisibility(element, subject, href, $transclude);
        }
    }
}

angular.module('app').directive('myAuthLink', MyAuthLink.factory());
