class MyAuth implements ng.IDirective {
    restrict = 'A';

    static factory(): ng.IDirectiveFactory {
        const directive = ($rootScope: ng.IRootScopeService, authorizationService: IAuthorizationService) => new MyAuth($rootScope, authorizationService);
        directive.$inject = ['$rootScope', 'ssAuthorizationService'];
        return directive;
    }

    constructor(private $rootScope: ng.IRootScopeService, private authorizationService: IAuthorizationService) {
    }
    
    setVisible(element: ng.IAugmentedJQuery, visible: boolean) {
        if (visible) {
            element.removeClass('hidden');
        } else {
            element.addClass('hidden');
        }        
    }
    
    defineVisibility(element: ng.IAugmentedJQuery, subject: string) {
        this.setVisible(element, this.authorizationService.check(subject));                    
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: any) => {
        let subject: string = attrs.myAuth.replace(/\s+/g, '');
        if (subject.length > 0) {
            this.setVisible(element, false);
            this.defineVisibility(element, subject);
        }
    }
}

angular.module('app').directive('myAuth', MyAuth.factory());
