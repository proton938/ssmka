interface IFileNameScope extends ng.IScope {
    fileName: string;
    maxLength: number;
}

class FileName implements ng.IDirective {
    restrict: string;
    scope: any;


    static factory(): ng.IDirectiveFactory {
        const directive = () => new FileName();
        directive.$inject = [];
        return directive;
    }

    constructor() {
        this.restrict = 'E';
        this.scope = {
            fileName: "=",
            maxLength: "="
        };
    }

    link = ($scope: IFileNameScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        var ind = $scope.fileName.lastIndexOf(".");
        if (ind > $scope.maxLength) {
            var name = $scope.fileName.substr(0, ind);
            var ext = $scope.fileName.substr(ind + 1, $scope.fileName.length);
            element.text(name.substr(0, $scope.maxLength) + "... ." + ext);
        } else {
            element.text($scope.fileName);
        }
    }
}

angular.module('app').directive('fileName', FileName.factory());