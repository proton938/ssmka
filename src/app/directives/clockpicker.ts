class ClockPicker implements ng.IDirective {
    restrict: string;

    static factory(): ng.IDirectiveFactory {
        const directive = () => new ClockPicker();
        directive.$inject = [];
        return directive;
    }

    constructor() {
        this.restrict = 'A';
    }

    link = ($scope: ng.IScope, element: ng.IAugmentedJQuery) => {
        element.clockpicker();
    }
}

angular.module('app').directive('clockPicker', ClockPicker.factory());