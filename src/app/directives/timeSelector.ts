interface TimeSelectorScope extends ng.IScope {
    model: any;
    ngModel: string;
    changed: () => void;
}

class TimeSelector implements ng.IDirective {
    restrict: string;
    require: string;
    templateUrl: string;
    scope: any;

    static factory(): ng.IDirectiveFactory {
        const directive = (dateFilter: any, timeUtils: ITimeUtils) => new TimeSelector(dateFilter, timeUtils);
        directive.$inject = ['dateFilter', 'timeUtils'];
        return directive;
    }

    constructor(private dateFilter: any, private timeUtils: ITimeUtils) {
        this.restrict = 'E';
        this.require = 'ngModel';
        this.templateUrl = 'app/directives/timeSelector.html';
        this.scope = {
            ngModel: "=",
            ngBlur: "&",
        };
    }

    link = ($scope: TimeSelectorScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        $scope.model = this.timeUtils.parse($scope.ngModel);
        $scope.$watch(() => {
            return $scope.model;
        }, (date) => {
            $scope.ngModel = this.dateFilter(date, 'HH:mm:ss');
        });
        // Проверка потери фокуса директивой, если фокус попадает не в одно из двух полей ввода.
        // В таком случае вызываем назначенный метод.
        if ($attrs.ngBlur) {
            element.on('focusout', (e: JQueryEventObject) => {
                const relatedTarget: Element = e.relatedTarget;
                if (!relatedTarget || angular.element(relatedTarget).parentsUntil(element).length > 5) {
                    $scope.ngBlur();
                }
            });
        }
    };

    initModel($scope: TimeSelectorScope, value: string) {
        $scope.model = this.timeUtils.parse(value);
    };
}

angular.module('app').directive('timeSelector', TimeSelector.factory());
