interface QuestionAddressScope extends ng.IScope {
	address: string;
	prefect: string[];
	district: string[];
	prefectDistrictString: string;
}

class QuestionAddress implements ng.IDirective {
    restrict: string;
    templateUrl: string;
    scope: any;

    static factory(): ng.IDirectiveFactory {
        const directive = () => new QuestionAddress();
        directive.$inject = [];
        return directive;
    }    

    constructor() {
        this.restrict = 'E';
        this.templateUrl = 'app/directives/questionAddress.html';
        this.scope = {
            address: "=",
            prefect: "=",
            district: "=" 
        };
    }
    
    link = ($scope: QuestionAddressScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        let arr: string[] = new Array();
        if ($scope.prefect) {
            arr = arr.concat($scope.prefect);
        }
        if ($scope.district) {
            arr = arr.concat($scope.district);
        }
        $scope.prefectDistrictString = arr.length > 0 ? '(' + arr.join(', ') + ')' : '';        
    }
    
}

angular.module('app').directive('questionAddress', QuestionAddress.factory());