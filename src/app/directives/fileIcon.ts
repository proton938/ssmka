interface IFileIconScope extends ng.IScope {
    fileName: string;
    format: string;
    small: boolean;
}

class FileIcon implements ng.IDirective {
    restrict: string;
    templateUrl: string;
    scope: any;
    formats: string[];
    fileName: string;

    static factory(): ng.IDirectiveFactory {
        const directive = () => new FileIcon();
        directive.$inject = [];
        return directive;
    }

    constructor() {
        this.restrict = 'E';
        this.scope = {
            fileName: "<",
            small: "="
        };
        this.formats = ['csv', 'doc', 'docx', 'dwg', 'pdf', 'ppt', 'pptx', 'rar', 'txt', 'xml', 'zip'];
    }

    link = ($scope: IFileIconScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) => {
        var ind = $scope.fileName.lastIndexOf(".");
        var ext = $scope.fileName.substr(ind + 1, $scope.fileName.length);
        let format: string = _.find(this.formats, function(f) {
            return f === ext;
        });
        if (!format) {
            format = 'unknown';           
        }
        let size = $scope.small ? 'small' : 'md';
        $('<img>').attr('src', 'images/icons/' + format + '.png').addClass('our-icon-file-' + size).appendTo(element);
    }
}

angular.module('app').directive('fileIcon', FileIcon.factory());