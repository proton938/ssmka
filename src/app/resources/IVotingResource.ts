class VotingStatus {
    votingResults: { [key: string]: ProtocolQuestionVoteResult };
    voters?: { [key: string]: any[] };
    votedUsers?: { [key: string]: any[] };
}

interface IVotingResource extends ng.resource.IResourceClass<any> {
	initVoting: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
	finishVoting: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
	initBallot: ng.resource.IResourceMethod<any>;
    addAttended: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolAttended[]>>;
    deleteAttended: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolAttended[]>>;
	setQuestionComment: ng.resource.IResourceMethod<any>;
	setQuestionVote: ng.resource.IResourceMethod<any>;
	completeQuestionVoting: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
	reopenQuestionVoting: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
    setQuestionActive: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
	createBallotPaperFile: ng.resource.IResourceMethod<any>;
	prepareBallotPaperSignature: ng.resource.IResourceMethod<any>;
	uploadBallotPaperSignature: ng.resource.IResourceMethod<any>;
    mailBallotPaperFile: ng.resource.IResourceMethod<any>;
    excludeQuestion: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
    getVotingStatus: ng.resource.IResourceMethod<ng.resource.IResource<VotingStatus>>;
    getProtocolBallot: ng.resource.IResourceMethod<BallotPaper[]>;
}

function PzzVotingResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils): IVotingResource {
    return VotingResource($resource, baseUrl, 'pzz', dateUtils);
}

function EooVotingResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils): IVotingResource {
    return VotingResource($resource, baseUrl, 'eoo', dateUtils);
}

function VotingResource($resource: ng.resource.IResourceService, baseUrl: string, prefix: string, dateUtils: IDateUtils): IVotingResource {

    let initVoting: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/init`,
        method: 'POST'
    };

    let finishVoting: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/finish`,
        method: 'POST'
    };    
    
    let initBallot: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballot`,
        method: 'POST'
    };

    let addAttended: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/addAttended`,
        method: 'POST',
        isArray: true
    };

    let deleteAttended: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/deleteAttended`,
        method: 'POST',
        isArray: true
    };

    let setQuestionComment: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/comment`,
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain'
        }
    };    
    
    let setQuestionVote: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/vote`,
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain'
        }        
    };
    
    let completeQuestionVoting: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/complete`,
        method: 'POST'
    };  
    
    let reopenQuestionVoting: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/reopen`,
        method: 'POST'
    };

    let setQuestionActive: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/active`,
        method: 'POST'
    };

    let createBallotPaperFile: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballot/file/create`,
        method: 'POST'
    }; 
    
    let prepareBallotPaperSignature: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballot/file/sing/prepare`,
        method: 'POST'
    };      
    
    let uploadBallotPaperSignature: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballot/file/sing`,
        method: 'POST'
    };

    let mailBallotPaperFile: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballot/file/mail`,
        method: 'POST'
    };

    let excludeQuestion: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/:questionId/exclude`,
        method: 'POST'
    };

    let getVotingStatus: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/status`,
        method: 'POST'
    };

    let getProtocolBallot: ng.resource.IActionDescriptor = {
        url: baseUrl + `/voting/${prefix}/:protocolId/ballots`,
        method: 'GET',
        isArray: true
    };

    return <IVotingResource>$resource(null, {}, {
        initVoting: initVoting,
        finishVoting: finishVoting,
        initBallot: initBallot,
        addAttended: addAttended,
        setQuestionComment: setQuestionComment,
        setQuestionVote: setQuestionVote,
        completeQuestionVoting: completeQuestionVoting,
        reopenQuestionVoting: reopenQuestionVoting,
        setQuestionActive: setQuestionActive,
        createBallotPaperFile: createBallotPaperFile,
        prepareBallotPaperSignature: prepareBallotPaperSignature,
        uploadBallotPaperSignature: uploadBallotPaperSignature,
        mailBallotPaperFile: mailBallotPaperFile,
        excludeQuestion: excludeQuestion,
        getVotingStatus: getVotingStatus,
        deleteAttended,
        getProtocolBallot
    });
}

PzzVotingResource.$inject = ['$resource', 'base_url', 'dateUtils'];
EooVotingResource.$inject = ['$resource', 'base_url', 'dateUtils'];

angular.module('app').service('pzzVotingResource', PzzVotingResource);
angular.module('app').service('eooVotingResource', EooVotingResource);