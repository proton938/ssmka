interface IInstructionResource extends ng.resource.IResourceClass<any> {
    getInstruction: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    updateInstruction: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function InstructionResource($resource: ng.resource.IResourceService): IInstructionResource {

    let getInstruction: ng.resource.IActionDescriptor = {
        url: '/instruction-rest/instruction/:id',
        method: 'GET'
    };

    let updateInstruction: ng.resource.IActionDescriptor = {
        url: '/instruction-rest/instruction/:id',
        method: 'PATCH'
    };

    return <IInstructionResource>$resource(null, { id: '@id' }, {
        getInstruction,
        updateInstruction
    });
}

InstructionResource.$inject = ['$resource'];

angular.module('app').service('instructionResource', InstructionResource);
