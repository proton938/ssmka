interface IExpressResource extends ng.resource.IResourceClass<any> {
    createAgenda: ng.resource.IResourceMethod<ng.resource.IResource<{id: string}>>;
    updateAgenda: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    markPassed: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    generateProtocol: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function ExpressResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils): IExpressResource {

    let createAgenda: ng.resource.IActionDescriptor = {
        url: baseUrl + '/express/agenda/create',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let updateAgenda: ng.resource.IActionDescriptor = {
        url: baseUrl + '/express/agenda/:id/update',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let markPassed: ng.resource.IActionDescriptor = {
        url: baseUrl + '/express/agenda/:id/mark/passed',
        method: 'POST'
    };

    let generateProtocol: ng.resource.IActionDescriptor = {
        url: baseUrl + '/express/agenda/:id/protocol/generate',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    return <IExpressResource>$resource(null, { id: '@id' }, {
        createAgenda,
        updateAgenda,
        markPassed,
        generateProtocol
    });
}

ExpressResource.$inject = ['$resource', 'base_url', 'dateUtils'];

angular.module('app').service('expressResource', ExpressResource);
