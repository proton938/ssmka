interface IAuthenticationResource extends ng.resource.IResourceClass<any> {
    logout: ng.resource.IResourceMethod<any>;
    user: ng.resource.IResourceMethod<AuthUser>;
    meetingTypes: ng.resource.IResourceMethod<string[]>;
    token: ng.resource.IResourceMethod<any>;
}

function AuthenticationResource($resource: ng.resource.IResourceService, baseUrl: string): IAuthenticationResource {

    let logout: ng.resource.IActionDescriptor = {
        url: '/auth/logout',
        method: 'POST'
    };

    let user: ng.resource.IActionDescriptor = {
        url: baseUrl + '/auth/user',
        method: 'GET'
    };

    let meetingTypes: ng.resource.IActionDescriptor = {
        url: baseUrl + '/auth/user/meetingTypes',
        method: 'GET',
        isArray: true
    };

    let token: ng.resource.IActionDescriptor = {
        url: baseUrl + '/auth/token',
        method: 'GET'
    };

    return <IAuthenticationResource>$resource(null, {}, {
        logout: logout,
        user: user,
        meetingTypes: meetingTypes,
        token: token
    });

}

AuthenticationResource.$inject = ['$resource', 'base_url'];

angular.module('app').service('authenticationResource', AuthenticationResource);
