class QuestionCreateResult {
    id: string;
}

interface IQuestionResource extends ng.resource.IResourceClass<any> {
    all: ng.resource.IResourceMethod<Question[]>;
    search: ng.resource.IResourceMethod<PageData<Question>>;
    searchQuery: ng.resource.IResourceMethod<PageData<Question>>;
    searchGroup: ng.resource.IResourceMethod<PageData<MeetingGroup<DecisionGroup>>>;
    searchGroupQuery: ng.resource.IResourceMethod<PageData<MeetingGroup<DecisionGroup>>>;
    searchPlan: ng.resource.IResourceMethod<PageData<MeetingGroup<String>>>;
    searchPrepare: ng.resource.IResourceMethod<PageData<PresentationPrepareGroup>>;
    getById: ng.resource.IResourceMethod<ng.resource.IResource<QuestionDocumentWrapper>>;
    copy: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    create: ng.resource.IResourceMethod<ng.resource.IResource<QuestionCreateResult>>;
    update: ng.resource.IResourceMethod<void>;
    materials: ng.resource.IResourceMethod<QuestionMaterialGroup[]>;
    getUploadMaterialUrl(questionID: string, fileType: string): string;
    deleteMaterial: ng.resource.IResourceMethod<void>;
    copyMaterials: ng.resource.IResourceMethod<ng.IPromise<any>>;
    isPlanDateEditable:ng.resource.IResourceMethod<ng.IPromise<any>>;
    getQuestionsIdsWithHistoryContaining:ng.resource.IResourceMethod<ng.IPromise<any>>;
    sendToMggt: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function QuestionResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils): IQuestionResource {

    function parseQuestions(response: any): any {
        let questions: Question[] = _.map(response.data, function(str: string) {
            let question: Question = JSON.parse(str).document.question;
            dateUtils.parseDates(question);
            return question;
        });
        return questions;
    }     
    
    let all: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/all',
        method: 'GET',
        isArray: true,
        interceptor: { response: parseQuestions }
    };
    
    let search: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/list',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: { response: dateUtils.parseResponseDates }
    };
    
    let searchQuery: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/list',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let searchGroup: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/group',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: { response: dateUtils.parseResponseDates }
    };
    
    let searchGroupQuery: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/group',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let searchPlan: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/plan',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: { response: dateUtils.parseResponseDates }
    };    
    
    let searchPrepare: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/search/prepare',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates,
        interceptor: { response: dateUtils.parseResponseDates }
    };    
    
    let getById: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let copy: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/copy',
        method: 'POST',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let create: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/create',
        transformRequest: dateUtils.formatRequestDates,
        method: 'POST'
    };

    let update: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/update',
        transformRequest: dateUtils.formatRequestDates,
        method: 'POST'
    };

    let materials: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/materials',
        method: 'GET',
        isArray: true,
        interceptor: { response: dateUtils.parseResponseDates }
    };    
    
    let deleteMaterial: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/materials/delete',
        method: 'POST'
    };
    
    let copyMaterials: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/materials/copy',
        method: 'POST',
        transformResponse: function(response: any) {
            return response;
        }
    };

    let isPlanDateEditable: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/planDate/editable',
        method: 'GET'
    };

    let getQuestionsIdsWithHistoryContaining: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/contain/history/:id',
        method: 'GET',
        isArray: true
    };

    let sendToMggt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/questions/:id/send/mggt',
        method: 'POST'
    };

    let result = <IQuestionResource>$resource(null, { id: '@id', fileGuid: '@fileGuid', sort: '@sort' }, {
        all: all,
        search: search,
        searchQuery: searchQuery,
        searchGroup: searchGroup,
        searchGroupQuery: searchGroupQuery,
        searchPlan: searchPlan,
        searchPrepare: searchPrepare,
        getById: getById,
        create: create,
        update: update,
        materials: materials,
        deleteMaterial: deleteMaterial,
        copyMaterials: copyMaterials,
        copy: copy,
        isPlanDateEditable: isPlanDateEditable,
        getQuestionsIdsWithHistoryContaining: getQuestionsIdsWithHistoryContaining,
        sendToMggt
    });
    result.getUploadMaterialUrl = (questionID: string, fileType: string) => {
        return baseUrl + '/questions/' + questionID + '/materials/add?type=' + fileType;
    };
    return result;
}

QuestionResource.$inject = ['$resource', 'base_url', 'dateUtils'];

angular.module('app').service('questionResource', QuestionResource);