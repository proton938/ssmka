interface IConfigResource extends ng.resource.IResourceClass<any> {
    getVersion: ng.resource.IResourceMethod<any>;
}

function ConfigResource($resource: ng.resource.IResourceService): IConfigResource {
    var getVersion: ng.resource.IActionDescriptor = {
        url: "data/version.txt",
        method: 'GET',
        headers: {'Content-Type': undefined},
        transformRequest: angular.identity,
        responseType: "text",
        transformResponse: (data, headersGetter, status): any => {
            if (200 <= status && status < 300) {
                return {version: data};
            }

            return data;
        } 
    };
    
    return <IConfigResource>$resource(null, null, {
        getVersion: getVersion
    });
}

ConfigResource.$inject = ['$resource'];

angular.module('app').service('configResource', ConfigResource);
