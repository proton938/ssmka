class AgendaCreateResult {
    id: string;
}

class LastHeldQuestion {
    agendaId: string;
    questionId: string;
    itemNumber: string;
    question: string;
    questionCategory: string;
    questionPrimaryId: string;
}

interface IAgendaResource extends ng.resource.IResourceClass<any> {
    all: ng.resource.IResourceMethod<Agenda[]>;
    searchPublished: ng.resource.IResourceMethod<ng.resource.IResource<PageData<AgendaDocumentWrapper>>>;
    searchProjects: ng.resource.IResourceMethod<PageData<AgendaDocumentWrapper[]>>;
    searchCalendar: ng.resource.IResourceMethod<AgendaDocumentWrapper[]>;
    getById: ng.resource.IResourceMethod<ng.resource.IResource<AgendaDocumentWrapper>>;
    getByIdFull: ng.resource.IResourceMethod<ng.resource.IResource<AgendaDocumentWrapper>>;
    create: ng.resource.IResourceMethod<QuestionCreateResult>;
    update: ng.resource.IResourceMethod<void>;
    searchIncludedQuestions: ng.resource.IResourceMethod<QuestionDocumentWrapper[]>;
    searchCandidateQuestions: ng.resource.IResourceMethod<QuestionDocumentWrapper[]>;
    includedQuestions: ng.resource.IResourceMethod<AgendaQuestion[]>;
    includedQuestionsCount: ng.resource.IResourceMethod<any>;
    candidateQuestions: ng.resource.IResourceMethod<QuestionDocumentWrapper[]>;
    changes: ng.resource.IResourceMethod<AgendaQuestion[]>;
    publish: ng.resource.IResourceMethod<void>;
    createProtocol: ng.resource.IResourceMethod<ProtocolDocumentWrapper>;
    isRestorable: ng.resource.IResourceMethod<any>;
    createReport: ng.resource.IResourceMethod<void>;
    renumberQuestions: ng.resource.IResourceMethod<void>;
    generateGkVoting: ng.resource.IResourceMethod<void>;
    getLastHeldQuestion: ng.resource.IResourceMethod<ng.resource.IResource<LastHeldQuestion[]>>;
    mailNotification: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    sendToMggt: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function AgendaResource($resource: ng.resource.IResourceService, baseUrl: string, $timeout: any, AgendaQuestionStatus: any, dateUtils: IDateUtils): IAgendaResource {

    let all: ng.resource.IActionDescriptor = {
        url: '/app/data/agendas.json',
        method: 'GET',
        isArray: true
    };

    let searchPublished: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/search/published',
        method: 'POST',
        interceptor: { response: dateUtils.parseResponseDates },
        transformRequest: dateUtils.formatRequestDates
    };

    let searchProjects: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/search/project',
        method: 'POST',
        interceptor: {response: dateUtils.parseResponseDates}
    };

    let searchCalendar: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/search/calendar',
        method: 'GET',
        isArray: true,
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let getById: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let getByIdFull: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/full',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let create: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/create',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let update: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/update',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let searchIncludedQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/questions/included',
        method: 'GET',
        isArray: true,
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let searchCandidateQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/questions/candidate',
        method: 'GET',
        isArray: true,
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let includedQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/included',
        method: 'GET',
        isArray: true
    };

    let includedQuestionsCount: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/included/count',
        method: 'GET'
    };

    let candidateQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/candidate',
        method: 'GET',
        isArray: true,
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let changes: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/changes',
        method: 'GET',
        isArray: true
    };

    let publish: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/publish',
        method: 'POST'
    };

    let createProtocol: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/protocol/generate',
        method: 'GET',
        interceptor: { response: dateUtils.parseResponseDates }
    };

    let isRestorable: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/available',
        method: 'GET'
    };

    let createReport: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/report/create',
        method: 'POST'
    };

    let renumberQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/questions/renumber',
        method: 'POST'
    };

    let generateGkVoting: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/protocol/voting/generate',
        method: 'GET'
    };

    let getLastHeldQuestion: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/lastHeld/questions',
        method: 'GET',
        isArray: true
    };

    let mailNotification: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/notification/mail',
        method: 'GET'
    };

    let sendToMggt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/agenda/:id/send/mggt',
        method: 'POST'
    };

    let result = <IAgendaResource>$resource(null, { id: '@id' }, {
        all: all,
        searchPublished: searchPublished,
        searchProjects: searchProjects,
        searchCalendar: searchCalendar,
        getById: getById,
        getByIdFull: getByIdFull,
        create: create,
        update: update,
        searchIncludedQuestions: searchIncludedQuestions,
        searchCandidateQuestions: searchCandidateQuestions,
        includedQuestions: includedQuestions,
        includedQuestionsCount: includedQuestionsCount,
        candidateQuestions: candidateQuestions,
        changes: changes,
        publish: publish,
        createProtocol: createProtocol,
        isRestorable: isRestorable,
        createReport: createReport,
        renumberQuestions: renumberQuestions,
        generateGkVoting,
        getLastHeldQuestion,
        mailNotification,
        sendToMggt
    });
    return result;
}

AgendaResource.$inject = ['$resource', 'base_url', '$timeout', 'AgendaQuestionStatus', 'dateUtils'];

angular.module('app').service('agendaResource', AgendaResource);
