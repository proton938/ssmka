interface IFileResource extends ng.resource.IResourceClass<any> {
    getDownloadUrl(id: string): string;
}

function FileResource($resource: ng.resource.IResourceService, baseUrl: string, filenetUrl: string): IFileResource {

    let result = <IFileResource>$resource(null, {}, {
    });
    result.getDownloadUrl = (id: string) => {
        return filenetUrl + '/files/' + id;
    };
    return result;
    
}

FileResource.$inject = ['$resource', 'base_url', 'filenet_url'];

angular.module('app').service('fileResource', FileResource);
