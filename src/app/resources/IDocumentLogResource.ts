interface IDocumentLogResource extends ng.resource.IResourceClass<any> {
    get: ng.resource.IResourceMethod<ng.resource.IResource<any[]>>;
}

function DocumentLogResource($resource: ng.resource.IResourceService, baseUrl: string): IVotingResource {

    let get: ng.resource.IActionDescriptor = {
        url: baseUrl + `/log/:id/asc`,
        method: 'GET',
        isArray: true
    };


    return <IVotingResource>$resource(null, {}, {
        get: get
    });
}

DocumentLogResource.$inject = ['$resource', 'base_url'];

angular.module('app').service('documentLogResource', DocumentLogResource);