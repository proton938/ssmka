interface IDateUtils {
    formatDate(date: Date, format?: string): string;
    formatDates(input: any): any;
    formatRequestDates(request:any): void;
    parseResponseDates(response:any): any;
    parseDates(input: any): void;
}

let DATE_TIME_FIELDS: RegExp[] = [
    /document\.protocol\.approval\.approvalCycle\.agreed\..\.approvalFactDate/,
    /document\.protocol\.approval\.approvalCycle\.agreed\..\.approvalPlanDate/,
    /document\.protocol\.approval\.approvalCycle\.agreed\..\.approvalComment\..\.commentDate/,
    /document\.protocol\.approvalHistory\.approvalCycle\..\.agreed\..\.approvalFactDate/,
    /document\.protocol\.approvalHistory\.approvalCycle\..\.agreed\..\.approvalPlanDate/,
    /document\.protocol\.approvalHistory\.approvalCycle\.agreed\..\.approvalComment\..\.commentDate/,
];

function DateUtils(dateFilter: any): IDateUtils {
    
    function formatDate(date: Date, format?: string): string {
        if (!format) {
            format = 'yyyy-MM-dd';
        }
        return dateFilter(date, format);
    }
    
    function _formatDates(input: any, path?: string): void {
        for (let key in input) {
            if (!input.hasOwnProperty(key)) continue;

            let propPath = path ? path + '.' + key : key;

            if (angular.isDate(input[key])) {
                let date: Date = input[key];
                if (_.some(DATE_TIME_FIELDS, f => propPath.match(f))) {
                    input[key] = formatDate(date, 'yyyy-MM-ddTHH:mm:ss');
                } else {
                    input[key] = formatDate(date, 'yyyy-MM-dd');
                }
            } else if (typeof input[key] === "object") {
                _formatDates(input[key], propPath);
            }
        }
    }    
    
    function parseDates(input: any) {
        for (let key in input) {
            if (!input.hasOwnProperty(key)) continue;

            if (typeof input[key] === "object") {
                parseDates(input[key]);
            } else {
                let match = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*$/.exec(input[key]);
                if (typeof input[key] === "string" && match) {
                    input[key] = new Date(input[key]);
                }
                match = /^\d{4}-\d{2}-\d{2}$/.exec(input[key]);
                if (typeof input[key] === "string" && match) {
                    input[key] = new Date(input[key]);
                }
            }
        }
    }  
    
    function parseResponseDates(response: any): any {
        parseDates(response.data);
        return response.data;
    }
    
    function formatDates(request:any) {
        const input = angular.copy(request);
        _formatDates(input);
        return input;
    }

    function formatRequestDates(request:any) {
        return angular.toJson(formatDates(request));
    }

    return {
        formatDate: formatDate,
        formatDates: formatDates,
        formatRequestDates: formatRequestDates,
        parseResponseDates: parseResponseDates,
        parseDates: parseDates
    };
    
}

DateUtils.$inject = ['dateFilter'];

angular.module('app').service('dateUtils', DateUtils);