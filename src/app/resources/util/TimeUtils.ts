interface ITimeUtils {
    parse(value: string): Date;
}

function TimeUtils(): ITimeUtils {
    
    function parse(value: string) {
        let match = /^(\d{2}):(\d{2}):(\d{2})$/.exec(value);
        if (match) {
            return new Date(0, 0, 0, parseInt(match[1]), parseInt(match[2]), parseInt(match[3]));
        }
        match = /^(\d{2}):(\d{2})$/.exec(value);
        if (match) {
            return new Date(0, 0, 0, parseInt(match[1]), parseInt(match[2]), 0);
        }        
    }

    return {
        parse: parse
    };
    
}

TimeUtils.$inject = [];

angular.module('app').service('timeUtils', TimeUtils);