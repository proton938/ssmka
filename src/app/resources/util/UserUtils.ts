interface IUserUtils {
    toQuestionUserType(user: oasiNsiRest.UserBean, organizationCode?: string): QuestionUserType;
}

function UserUtils(): IUserUtils {
    
    function toQuestionUserType(user: oasiNsiRest.UserBean, organizationCode?: string) {
        let result: QuestionUserType = {
            accountName: user.accountName,
            fioFull: user.displayName,
            fioShort: user.fioShort,
            phone: user.telephoneNumber,
            email: user.mail,
            post: user.post,
            organization: user.departmentFullName,
            organizationShort: null,
            organizationCode: organizationCode
        };
        return result;        
    }

    return {
        toQuestionUserType: toQuestionUserType
    };
    
}

UserUtils.$inject = [];

angular.module('app').service('userUtils', UserUtils);
