class ProtocolCreateResult {
    id: string;
}

interface IProtocolResource extends ng.resource.IResourceClass<any> {
    search: ng.resource.IResourceMethod<ng.resource.IResource<PageData<ProtocolDocumentWrapper>>>;
    getById: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolDocumentWrapper>>;
    getResultsVote: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolQuestionVoteResult>>;
    getByAgendaId: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolDocumentWrapper>>;
    getByMeetingNumber: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolDocumentWrapper>>;
    create: ng.resource.IResourceMethod<ProtocolCreateResult>;
    update: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
    patch: ng.resource.IResourceMethod<ng.resource.IResource<ProtocolDocumentWrapper>>;
    sendDecision: ng.resource.IResourceMethod<void>;
    createReport: ng.resource.IResourceMethod<ng.resource.IResource<FileType>>;
    addQuestionToProtocol: ng.resource.IResourceMethod<ng.resource.IResource<void>>;

    getAddFileUrl(protocolID: string): string;

    attendParticipants: ng.resource.IResourceMethod<ProtocolAttended[]>;
    questionUpdate: ng.resource.IResourceMethod<QuestionParameters>;
    saveVotingDecision: ng.resource.IResourceMethod<ng.resource.IResource<void>>;
    formProtocolDraft: ng.resource.IResourceMethod<any>;
    formConclusionDraft: ng.resource.IResourceMethod<any>;
    getPrimaryQuestions: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
    sendToMggt: ng.resource.IResourceMethod<ng.resource.IResource<any>>;
}

function ProtocolResource($resource: ng.resource.IResourceService, baseUrl: string, dateUtils: IDateUtils): IProtocolResource {

    let search: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/search',
        method: 'POST',
        interceptor: {response: dateUtils.parseResponseDates},
        transformRequest: dateUtils.formatRequestDates
    };

    let getById: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id',
        method: 'GET',
        interceptor: {response: dateUtils.parseResponseDates}
    };

    let getResultsVote: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/:questionId/resultsVote',
        method: 'GET'
    };

    let getByAgendaId: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/byAgenda/:agendaId',
        method: 'GET',
        interceptor: {response: dateUtils.parseResponseDates}
    };


    let getByMeetingNumber: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol',
        method: 'GET',
        interceptor: {response: dateUtils.parseResponseDates}
    };

    let create: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/create',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let update: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/update',
        method: 'POST',
        transformRequest: dateUtils.formatRequestDates
    };

    let patch: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id',
        method: 'PATCH',
        transformRequest: dateUtils.formatRequestDates
    };

    let sendDecision: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/decision/send',
        method: 'POST'
    };

    let createReport: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/report/create',
        method: 'POST'
    };

    let addQuestionToProtocol: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:protocolId/voting/question/:questionId/add',
        method: 'POST'
    };

    let attendParticipants: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/voting/attended',
        method: 'POST',
        isArray: true
    };

    let questionUpdate: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/question/:questionId/parameters',
        method: 'POST'
    };

    let saveVotingDecision: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/voting/decision',
        method: 'POST'
    };

    let formProtocolDraft: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/voting/protocolDraft/create',
        method: 'POST',
        interceptor: { response: val => val.data }
    };

    let formConclusionDraft: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/voting/conclusionDraft/create',
        method: 'POST',
        interceptor: { response: val => val.data }
    };
    let getPrimaryQuestions: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/questions/primary',
        method: 'GET'
    };
    let sendToMggt: ng.resource.IActionDescriptor = {
        url: baseUrl + '/protocol/:id/send/mggt',
        method: 'POST'
    };
    let result = <IProtocolResource>$resource(null, {
        id: '@id', protocolId: '@protocolId', questionId: '@questionId'
    }, {
        search: search,
        getById: getById,
        getResultsVote: getResultsVote,
        getByAgendaId: getByAgendaId,
        getByMeetingNumber: getByMeetingNumber,
        create: create,
        update: update,
        patch: patch,
        sendDecision: sendDecision,
        createReport: createReport,
        addQuestionToProtocol,
        attendParticipants,
        questionUpdate,
        saveVotingDecision,
        formProtocolDraft,
        formConclusionDraft,
        getPrimaryQuestions,
        sendToMggt
    });
    result.getAddFileUrl = (protocolID: string) => {
        return baseUrl + '/protocol/' + protocolID + '/files/add';
    };
    return result;
}

ProtocolResource.$inject = ['$resource', 'base_url', 'dateUtils'];

angular.module('app').service('protocolResource', ProtocolResource);