class ErrorHandlerInterceptor {
    static $inject = ['$q', '$injector', 'nsi_url'];

    constructor(private $q: ng.IQService, private $injector: any, private nsiUrl: string) {
    }

    handleError(httpResponse: any): any {
        const toastr: Toastr = this.$injector.get('toastr');
        switch (httpResponse.status) {
            case 401:
                let currentLocation = window.location.pathname + window.location.search + window.location.hash;
                // # решетку нужно заменить иначе роутер обрубит остаток урла после
                window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
                break;
            case 413:
                toastr.warning("Превышен доступный размер файла.", "Внимание");
                break;
            default:
                if (httpResponse.data && httpResponse.data.userMessages) {
                    httpResponse.data.userMessages.forEach(message => {
                        toastr.warning(message, "Внимание");
                    })
                } else if (httpResponse.data && httpResponse.data.message) {
                    toastr.warning(httpResponse.data.message, "Внимание");
                } else if (~httpResponse.data.indexOf('File already exists with name')) {
                    // Не делаем ничего если это запрос в filenet-rest с ответом, о существовании файла.
                    // Эта ситуация обрабатывается в *express-agenda-edit*
                } else {
                    toastr.warning("Произошла ошибка. Обратитесь к администратору.", "Внимание");
                }
                break;
        }
        return this.$q.reject(httpResponse);
    }

    responseError = this.handleError.bind(this);
}

angular.module('app').service('errorHandlerInterceptor', ErrorHandlerInterceptor);
