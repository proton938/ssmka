interface IMeetingTypesService {
    setUserMeetingTypes(meetingTypes: string[]);
    getUserMeetingTypes(): string[];
    getMeetingTypes(): ng.IPromise<MeetingType[]>;
    checkMeetingType(meetingType: string): boolean;
}

class MeetingTypesService implements IMeetingTypesService {
    static $inject = [
        '$q',
        '$sessionStorage',
        'nsiRestService'
    ];

    constructor(private $q: ng.IQService,
                private $sessionStorage: angular.storage.IStorageService,
                private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    setUserMeetingTypes(meetingTypes: string[]) {
        this.$sessionStorage["ss-meetingTypes"] = meetingTypes;
    }

    getUserMeetingTypes(): string[] {
        return this.$sessionStorage["ss-meetingTypes"];
    }

    getMeetingTypes(): ng.IPromise<MeetingType[]> {
        const userMeetingTypes = this.getUserMeetingTypes();
        return this.nsiRestService.get('MeetingTypes').then((meetingTypes: MeetingType[]) => {
            return _.filter(meetingTypes, (meetingType: MeetingType) => {
                return _.find(userMeetingTypes, (mt) => mt === meetingType.meetingType);
            });
        });
    }

    checkMeetingType(meetingType: string): boolean {
        let meetingTypes = this.getUserMeetingTypes();
        return meetingTypes ? meetingTypes.indexOf(meetingType) >= 0 : false;
    }

}

angular.module('app').service('meetingTypesService', MeetingTypesService);
