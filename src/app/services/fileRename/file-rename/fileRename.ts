class RenameFileController {
    newName: string = '';
    extension: string;
    static $inject = ['$uibModalInstance', 'oldName'];

    constructor(private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
        private oldName: string) {
        let arr = oldName.split('.');
        this.extension = arr[arr.length - 1];
    }

    rename() {
        this.$uibModalInstance.close(this.newName + '.' + this.extension);
    }

    cancel() {
        this.$uibModalInstance.dismiss('cancel');
    }
}

angular.module('app').controller('RenameFileController', RenameFileController);
