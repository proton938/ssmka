interface IFileRenameService {
    renameFile(oldName: string): ng.IPromise<any>;
}

class FileRenameService {
    static $inject = ['$q', '$uibModal'];
    constructor(private $q: ng.IQService, private $uibModal: ng.ui.bootstrap.IModalService) { }
    renameFile(oldName: string): ng.IPromise<string> {
        let defer: ng.IDeferred<string> = this.$q.defer<string>();
        let modalInstance = this.$uibModal.open({
            templateUrl: 'app/services/fileRename/file-rename/fileRename.html',
            controller: 'RenameFileController as $ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            windowClass: 'file-rename',
            resolve: {
                oldName: () => {
                    return oldName;
                }
            }
        });
        modalInstance.result.then(response => {
            defer.resolve(response);
        }).catch(error => {
            defer.reject();
        });
        return defer.promise;
    }
}

angular.module('app').service('ssFileRenameService', FileRenameService);
