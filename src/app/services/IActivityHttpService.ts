interface IActivityHttpService {
    addApprovers(requestData: AddApproversData): ng.IPromise<any>;
}

class AddApproversData {
    taskId: string;
    usersCollectionVarName: string;
    durationsCollectionVarName: string;
    elementVarName: string;
    approvalPersons: {userName: string; duration: string}[]
}

class ActivityHttpService implements IActivityHttpService {
    static $inject = ['$http', 'activiti_rest_url', '$q'];

    constructor(private $http: ng.IHttpService, private activitiRestUrl: string, private $q: ng.IQService) {
    }

    addApprovers(requestData: AddApproversData): ng.IPromise<any> {
        let url: string = this.activitiRestUrl + '/approval/changeApprovalPersons';

        let defer: ng.IDeferred<any> = this.$q.defer();

        this.$http.post(url, requestData).then(_ => {
            defer.resolve(<any>_.data);
        }).catch(error => {
            defer.reject(error);
        });

        return defer.promise;
    }


}

angular.module('app').service('activityHttpService', ActivityHttpService);
