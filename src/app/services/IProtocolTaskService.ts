interface IProtocolTaskService {
    getProcessDefinition(key: string): ng.IPromise<oasiBpmRest.IProcessDefinition>;

    initProcess(processDefinitionId: string, variables: oasiBpmRest.ITaskVariable[]): ng.IPromise<string>;

    getTasksByEntityAndAssignee(taskDefinitionKey: string, entityID: string, assignee: string): ng.IPromise<IProtocolTask[]>;

    updateProcess(taskId: string, vars: oasiBpmRest.ITaskVariable[]): ng.IPromise<string>;

    suspendProcess(processId: string): ng.IPromise<void>;

    activateProcess(processId: string): ng.IPromise<void>;

    getInitProcessProperties(protocol: Protocol, accountName: string): oasiBpmRest.ITaskVariable[];
}

class ProtocolTaskService implements IProtocolTaskService {
    static $inject = [
        '$q',
        'activityRestService',
        'dateFilter'
    ];

    constructor(private $q: ng.IQService,
                private activityRestService: oasiBpmRest.ActivityRestService,
                private dateFilter: any) {
    }

    getProcessDefinition(key: string): ng.IPromise<oasiBpmRest.IProcessDefinition> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.getProcessDefinitions({
            key: key,
            latest: true
        }).then((response) => {
            let processes = response.data;
            if (processes.length <= 0) {
                defer.reject("Процесс " + key + " не найден");
            } else {
                defer.resolve(processes[0]);
            }
        }).catch((error) => {
            defer.reject(error);
        });

        return defer.promise;
    }

    initProcess(processDefinitionId: string, variables: oasiBpmRest.ITaskVariable[]): ng.IPromise<string> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.initProcess({
            processDefinitionId: processDefinitionId,
            variables: variables
        }).then(id => {
            defer.resolve(id);
        }).catch((error) => {
            defer.reject(error);
        });

        return defer.promise;
    }

    updateProcess(taskId: string, vars: oasiBpmRest.ITaskVariable[]): ng.IPromise<string> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.finishTask(parseInt(taskId), vars).then(id => {
            defer.resolve(id);
        }).catch((error) => {
            defer.reject(error);
        });

        return defer.promise;
    }

    getTasksByEntityAndAssignee(taskDefinitionKey: string, entityID: string, assignee: string): ng.IPromise<IProtocolTask[]> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.queryTasks({
            assignee: assignee,
            requestParams: true,
            order: "desc",
            taskDefinitionKey: taskDefinitionKey,
            processInstanceVariables: [{
                name: "EntityIdVar",
                value: entityID,
                operation: "equals"
            }],
            size: 1000000
        }).then((_: any) => {
            defer.resolve(_.data);
        }).catch(error => {
            defer.reject(error)
        });

        return defer.promise;
    }

    suspendProcess(processId: string): ng.IPromise<string> {
        return this.activityRestService.suspendProcess(processId);
    }

    activateProcess(processId: string): ng.IPromise<string> {
        return this.activityRestService.activateProcess(processId);
    }

    getInitProcessProperties(protocol: Protocol, accountName: string): oasiBpmRest.ITaskVariable[] {
        return [{
            "name": "EntityIdVar",
            "value": protocol.protocolID
        }, {
            "name": "EntityDescriptionVar",
            "value": `${protocol.meetingFullName}` +
            ` №${protocol.meetingNumber} от ${this.dateFilter(protocol.meetingDate, 'dd.MM.yyyy')}`
        }, {
            "name": "ContractorVar",
            "value": accountName
        }];
    }

}

angular.module('app').service('protocolTaskService', ProtocolTaskService);