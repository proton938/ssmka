class ApprovalCycleUtils {
    static $inject = [];

    constructor() {
    }

    getApprovalIndex(approvalNum: string, depth: number) {
        return this.parseApprovalNum(approvalNum)[depth];
    }

    isAdded(approvalNum) {
        return this.parseApprovalNum(approvalNum).length > 2;
    }

    getLastDepartmentApprovalIndex(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.findLastIndex(approvalCycle.agreed, a => this.isDepartmentApproval(a));
    }

    decreaseApprovalNum(approvalNum: string, depth?: number): string {
        const parsedApprovalNum = this.parseApprovalNum(approvalNum);
        if (!depth) {
            depth = parsedApprovalNum.length - 1;
        }
        parsedApprovalNum[depth]--;
        return this.joinApprovalNum(parsedApprovalNum);
    }

    increaseApprovalNum(approvalNum: string, segment?: number): string {
        const parsedApprovalNum = this.parseApprovalNum(approvalNum);
        if (!segment) {
            segment = parsedApprovalNum.length - 1;
        }
        parsedApprovalNum[segment]++;
        return this.joinApprovalNum(parsedApprovalNum);
    }

    parseApprovalNum(approvalNum: string): number[] {
        return approvalNum.split('.').map(i => parseInt(i));
    }

    joinApprovalNum(numbers: number[]): string {
        return numbers.join('.');
    }

    getParentApproval(approvalCycle: ProtocolApprovalCycle, approvalNum: string): ProtocolApprovalCycleAgreed {
        const parsedApprovalNum = this.parseApprovalNum(approvalNum);
        if (parsedApprovalNum.length <= 2) {
            return null;
        }
        const parentApprovalNum = this.joinApprovalNum(parsedApprovalNum.slice(0, parsedApprovalNum.length - 1));
        return _.find(approvalCycle.agreed, a => a.approvalNum === parentApprovalNum);
    }

    getChildApprovals(approvalCycle: ProtocolApprovalCycle, approvalNum: string) {
        return approvalCycle.agreed.filter(a => {
            return _.startsWith(a.approvalNum, approvalNum) &&
                this.parseApprovalNum(a.approvalNum).length === this.parseApprovalNum(approvalNum).length + 1
        })
    }

    deleteDepartmentApproval(approvalCycle: ProtocolApprovalCycle, approvalNum: string) {
        const parsedApprovalNum = this.parseApprovalNum(approvalNum);
        const depth = parsedApprovalNum.length - 1;
        const lastNumber = _.last(parsedApprovalNum);
        approvalCycle.agreed = approvalCycle.agreed.filter(a => !_.startsWith(a.approvalNum, approvalNum));
        approvalCycle.agreed.filter(a => this.isDepartmentApproval(a)).forEach(a => {
            if (this.getApprovalIndex(a.approvalNum, depth) > lastNumber) {
                a.approvalNum = this.decreaseApprovalNum(a.approvalNum, depth);
            }
        })
    }

    swapDepartmentApprovals(approvalCycle: ProtocolApprovalCycle, approvalNum1: string, approvalNum2: string) {
        let depth = this.parseApprovalNum(approvalNum1).length - 1;
        let agreeds1 = approvalCycle.agreed.filter(a => _.startsWith(a.approvalNum, approvalNum1));
        let agreeds2 = approvalCycle.agreed.filter(a => _.startsWith(a.approvalNum, approvalNum2));
        let index = _.findIndex(approvalCycle.agreed, a => a.approvalNum === agreeds1[0].approvalNum);
        approvalCycle.agreed.splice(index, agreeds1.length + agreeds2.length);
        agreeds2.forEach(a => {
            approvalCycle.agreed.splice(index++, 0, a);
            a.approvalNum = this.decreaseApprovalNum(a.approvalNum, depth)
        });
        agreeds1.forEach(a => {
            approvalCycle.agreed.splice(index++, 0, a);
            a.approvalNum = this.increaseApprovalNum(a.approvalNum, depth)
        });
    }

    hasDepartmentApproval(approvalCycle: ProtocolApprovalCycle, approvalNum: string) {
        return _.some(approvalCycle.agreed, a => a.approvalNum === approvalNum);
    }

    isDepartmentApproval(agreed: ProtocolApprovalCycleAgreed | ProtocolApprovalHistoryCycleAgreed) {
        return _.startsWith(agreed.approvalNum, '1');
    }

    isSupervisor(agreed: ProtocolApprovalCycleAgreed | ProtocolApprovalHistoryCycleAgreed) {
        return _.startsWith(agreed.approvalNum, '2');
    }

    isStroyUser(agreed: ProtocolApprovalCycleAgreed | ProtocolApprovalHistoryCycleAgreed) {
        return _.startsWith(agreed.approvalNum, '3');
    }

    isResponsibleSecretary(agreed: ProtocolApprovalCycleAgreed | ProtocolApprovalHistoryCycleAgreed) {
        return _.startsWith(agreed.approvalNum, '4');
    }

    isChairman(agreed: ProtocolApprovalCycleAgreed | ProtocolApprovalHistoryCycleAgreed) {
        return _.startsWith(agreed.approvalNum, '5');
    }

    getDepartmentsApprovees(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.filter(approvalCycle.agreed, a => this.isDepartmentApproval(a));
    }

    getSupervisors(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.filter(approvalCycle.agreed, a => this.isSupervisor(a));
    }

    getStroyUser(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.find(approvalCycle.agreed, a => this.isStroyUser(a));
    }

    getResponsibleSecretary(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.find(approvalCycle.agreed, a => this.isResponsibleSecretary(a));
    }

    getChairman(approvalCycle: ProtocolApprovalCycle | ProtocolApprovalHistoryCycle) {
        return _.find(approvalCycle.agreed, a => this.isChairman(a));
    }

    addAdditionalApprovee(approvalCycle: ProtocolApprovalCycle, approvalType: ApprovalType, status: ProtocolApprovalStatus, user: User, addedBy: ProtocolApprovalCycleAgreed) {
        const approvalNum = addedBy.approvalNum;
        const parsedApprovalNum = this.parseApprovalNum(approvalNum);
        const addedDepartmentApprovals = this.getChildApprovals(approvalCycle, approvalNum);
        let newApprovalNum = this.joinApprovalNum(parsedApprovalNum.concat([addedDepartmentApprovals.length + 1]));
        let index;
        if (addedDepartmentApprovals.length) {
            let lastApprovalNum = _.last(addedDepartmentApprovals).approvalNum;
            index = _.findIndex(approvalCycle.agreed, a => a.approvalNum === lastApprovalNum) + 1;
        } else {
            index = _.findIndex(approvalCycle.agreed, a => a.approvalNum === approvalNum)
        }
        approvalCycle.agreed.splice(index, 0, ProtocolApprovalCycleAgreed.createFrom(newApprovalNum, approvalType, status, user));
    }

    addApprovee(approvalCycle: ProtocolApprovalCycle, approvalType: ApprovalType, approvalStatus: ProtocolApprovalStatus, user?: User): ProtocolApprovalCycleAgreed {
        const lastDepartmentApprovalIndex = this.getLastDepartmentApprovalIndex(approvalCycle);
        const approvalNum = approvalCycle.agreed[lastDepartmentApprovalIndex].approvalNum;
        const newApprovalNum = this.joinApprovalNum([1, this.getApprovalIndex(approvalNum, 1) + 1]);
        const agreed = ProtocolApprovalCycleAgreed.createFrom(newApprovalNum, approvalType, approvalStatus, user);
        approvalCycle.agreed.splice(lastDepartmentApprovalIndex + 1, 0, agreed);
        return agreed;
    }

}

angular.module('app').service('approvalCycleUtils', ApprovalCycleUtils);
