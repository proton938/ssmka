class FileService {

   static $inject = ['$q'];

   constructor(private $q: ng.IQService) {}

   public getContentFromBlobAsBinaryString(blob: Blob): ng.IPromise<any> {
      let deferred = this.$q.defer();

      if (!FileReader.prototype.readAsBinaryString) {
         FileReader.prototype.readAsBinaryString = function (fileData) {
            let binary = "";
            let pt = this;
            let reader = new FileReader();
            reader.onload = function (e) {
               let bytes = new Uint8Array(reader.result);
               let length = bytes.byteLength;
               for (let i = 0; i < length; i++) {
                  binary += String.fromCharCode(bytes[i]);
               }
               pt.content = binary;
               $(pt).trigger('onload');
            };

            reader.readAsArrayBuffer(fileData);
         };
      }

      let fileReader = new FileReader();

      fileReader.onload = function (e: any) {
         let data = (!e) ? (<any>this).content : e.target.result;
         deferred.resolve(data);
      };

      fileReader.onerror = function (error) {
         deferred.reject(error);
      };

      fileReader.readAsBinaryString(blob);

      return deferred.promise;
   }
}
