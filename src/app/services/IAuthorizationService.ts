interface IAuthorizationService {
    setUserPermissions(permissions: string[]);
    getUserPermissions(): string[];
    check(subject: string): boolean;
}

class AuthorizationService implements IAuthorizationService {
    static $inject = ['$http', '$base64', '$q', 'toastr', '$sessionStorage'];
    
    constructor(private $http: ng.IHttpService, private $base64: any, private $q: ng.IQService, private toastr: Toastr, private $sessionStorage: angular.storage.IStorageService) {
    }

    setUserPermissions(permissions: string[]) {
        this.$sessionStorage["ss-permissions"] = permissions;
    }

    getUserPermissions(): string[] {
        return this.$sessionStorage["ss-permissions"];
    }
    
    check(resource: string): boolean {
        let permissions = this.getUserPermissions();
        return permissions ? permissions.indexOf(resource) >= 0 : false;  
    }
    
}

angular.module('app').service('ssAuthorizationService', AuthorizationService);