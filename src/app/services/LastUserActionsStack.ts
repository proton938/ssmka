class UserActionsStack {
    static $inject = ['_'];

    actionList: any[] = [];

    constructor(private _: any) {
    }

    register(actionName: string): void {
        this.actionList.push(actionName);
    }

    getLast(): string {
        return this._.last(this.actionList);
    }
}

angular.module('app').service('userActionsStack', UserActionsStack);
