interface IAuthenticationService {
    newSession(model: any): ng.IPromise<AuthUser>;
    logoutThenNewSession(model: any): ng.IPromise<AuthUser>;
    checkSession(onSuccess: any, onError: any);
    destroySession(onSuccess: any, onError: any);
}

class AuthenticationService implements IAuthenticationService {
    static $inject = ['$http', '$base64', '$q', 'toastr', 'ssAuthorizationService', 'authenticationResource', 'MeetingType', 'nsi_url', 'nsiRestService'];

    constructor(private $http: ng.IHttpService, private $base64: any, private $q: ng.IQService, private toastr: Toastr,
        private authorizationService: IAuthorizationService, private authenticationResource: IAuthenticationResource,
        private MeetingType: any, private nsiUrl: string,
        private nsiRestService: oasiNsiRest.NsiRestService) {
    }

    public newSession(model: any): ng.IPromise<AuthUser> {
        let defered: ng.IDeferred<AuthUser> = this.$q.defer();
        let error = function(reason: any) {
            defered.reject(reason);
        }
        this.checkSessionWithAuth((authUser: AuthUser) => {
            defered.resolve(authUser);
        }, error, model.login, model.password);
        return defered.promise;
    }

    public logoutThenNewSession(model: any): ng.IPromise<AuthUser> {
        let defered: ng.IDeferred<AuthUser> = this.$q.defer();
        this.authenticationResource.logout(() => {
            this.newSession(model).then(function(authUser) {
                defered.resolve(authUser);
            })
        }, (error) => {
            console.error(error);
            this.toastr.warning("Произошла ошибка. Обратитесь к администратору. Код Ошибки " + error.status +
                " Описание " + error.statusText + "!", "Внимание");
        });
        return defered.promise;
    }

    public checkSession(onSuccess: any, onError: any) {
        this.checkSessionWithAuth(onSuccess, onError, null, null);
    }

    private checkSessionWithAuth(onSuccess: any, onError: any, login: string, password: string) {
        const $http = this.$http;
        const nsiUrl = this.nsiUrl;
        let error = function (reason) {
            let currentLocation = window.location.pathname + window.location.search + window.location.hash;
            // # решетку нужно заменить иначе роутер обрубит остаток урла после
            window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
        };
        if (login && password) {
            // this.$http.defaults.headers.common['Authorization'] = 'Basic ' + this.$base64.encode(login + ':' + password);
        }
        this.authenticationResource.user((authUser: AuthUser) => {
            console.log(authUser);

            this.nsiRestService.ldapUser(authUser.login).then((user) => {
                authUser.user = user;
                this.authenticationResource.meetingTypes((meetingTypes) => {
                    authUser.meetingTypes = meetingTypes;
                    try {
                        onSuccess(authUser);
                    } catch (exc) {
                        console.error(exc);
                    }
                }, error);
            }).catch((error) => {
                console.error(error);
            });

        }, error);
    }

    public destroySession(onSuccess: any, onError: any) {
        this.authenticationResource.logout(function() {
            try {
                onSuccess();
            } catch (exc) {
                console.error(exc);
            }
        }, function(reason) {
            console.error(reason);
            try {
                onError(reason);
            } catch (exc) {
                console.error(exc);
            }
        });
    }
}

angular.module('app').service('authenticationService', AuthenticationService);
