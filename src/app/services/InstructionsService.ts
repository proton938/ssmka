class ProtocolQuestionInstructions {
    questionId: string;
    instructions: oasiSolarRest.SearchResultDocument[];
}

class ProtocolWithInstructionsUser {
    login: string;
    fioFull: string;
}

class ProtocolWithInstructionsAttended {
    name: string;
    code: string;
    attendedPerson?: string;
    attendedPersonFIO?: {
        post: string;
        iofShort: string;
        fioFull: string;
    }[]
}

class ProtocolInstruction {
    documentId: string;
    instructionsNumber: string;
    statusId: {
        code: string;
        name: string;
        color: string;
    };
    instruction: {
        number: string;
        beginDate: Date;
        creator: ProtocolWithInstructionsUser;
        content: string;
        description: string;
        planDate: Date;
        factDate: Date;
        executor: ProtocolWithInstructionsUser;
        coExecutor: ProtocolWithInstructionsUser[];
        questionId: string;
        questionPrimaryID: string;
        reportContent: string;
    };
    reportExecutor: ProtocolWithInstructionsUser
}

class ProtocolWithInstructions {
    protocolID: string;
    meetingType: string;
    meetingFullName: string;
    meetingDate: Date;
    meetingNumber: string;
    meetingTime: string;
    meetingTimeEnd: string;
    meetingPlace: string;
    protocolApproved: boolean;
    protocolApprovedDate: Date;
    attendedNum: number;
    attended: ProtocolWithInstructionsAttended[];
    questionsNum: number;
    approvedBy: ProtocolPerson;
    secretary: ProtocolPerson;
    protocolInfo: ProtocolInfo;
    question: {
        questionID: string;
        questionNumber: string;
        itemNumber: string;
        excluded: boolean;
        decisionType: string;
        decisionTypeCode: string;
        decisionText: string;
        question: string;
        activity: boolean;
        instructions?: ProtocolInstruction[];
    }[]
}

interface IInstructionsService {
    getQuestionInstructions(question: Question): ng.IPromise<oasiSolarRest.SearchResult>;

    getProtocolInstructions(protocol: Protocol): ng.IPromise<ProtocolQuestionInstructions[]>;

    getAgendaInstructions(agenda: Agenda, meetingType: MeetingType, primaryQuestionsIds: string[]): ng.IPromise<oasiSolarRest.SearchResult>;

    getProtocolWithInstructions(protocol: Protocol, meetingType: MeetingType, instructions: ProtocolQuestionInstructions[]): ProtocolWithInstructions;
}

class InstructionsService implements IInstructionsService {
    static $inject = [
        '$q',
        'timeUtils',
        'oasiSolarRestService',
        'protocolResource',
    ];

    constructor(private $q: ng.IQService,
                private timeUtils: ITimeUtils,
                private oasiSolarRestService: oasiSolarRest.OasiSolarRestService,
                private protocolResource: IProtocolResource) {
    }

    getQuestionInstructions(question: Question) {
        return question.questionPrimaryID ? this.oasiSolarRestService.searchExt({
            type: 'INSTRUCTION',
            pageSize: 200,
            sort: 'docDateInst desc',
            fields: [{
                name: 'questionPrimaryID',
                value: question.questionPrimaryID
            }]
        }) : this.$q.resolve(new oasiSolarRest.SearchResult());
    }

    getProtocolInstructions(protocol: Protocol): ng.IPromise<ProtocolQuestionInstructions[]> {
        const deferred = this.$q.defer<ProtocolQuestionInstructions[]>();
        let questionMap = {};
        let solrFieldsFilter = [];
        this.protocolResource.getPrimaryQuestions({id: protocol.protocolID}).$promise.then((result: any) => {
            questionMap = _.invert(result);
            const primaryQuestionsIds = _.keys(questionMap);
            if (primaryQuestionsIds && primaryQuestionsIds.length) {
                solrFieldsFilter.push({
                    name: 'questionPrimaryID',
                    value: primaryQuestionsIds
                });
                return true;
            } else {
                deferred.resolve({questionMap: questionMap, instructions: new oasiSolarRest.SearchResult()});
            }
        }).then(() => {
            return this.getNextProtocolMeetingDate(protocol.meetingDate, protocol.meetingType);
        }).then((nextProtocolMeetingDate: Date) => {
            if (nextProtocolMeetingDate) {
                solrFieldsFilter.push({name: 'docDateInst', value: {to: nextProtocolMeetingDate}})
            }
            return this.oasiSolarRestService.searchExt({
                type: 'INSTRUCTION',
                pageSize: 200,
                fields: solrFieldsFilter
            });
        }).then((searchResult: any) => {
            let result = _.chain(searchResult.docs).groupBy((doc: any) => doc.questionPrimaryID).toPairs().map((el: any) => {
                const primaryQuestionID = el[0];
                const questionId = questionMap[primaryQuestionID];
                return {
                    questionId: questionId,
                    instructions: el[1]
                };
            }).value();
            deferred.resolve(result);
        });
        return deferred.promise;
    }

    getNextProtocolMeetingDate(meetingDate: Date, meetingType: string): ng.IPromise<Date> {
        let deferred = this.$q.defer<Date>();
        const nextMeetingFrom = new Date(meetingDate.getTime());
        nextMeetingFrom.setDate(nextMeetingFrom.getDate() + 1);
        this.oasiSolarRestService.searchExt({
            type: "SS_PROTOCOL",
            pageSize: 1,
            sort: 'meetingDateSs asc',
            fields: [
                {name: "meetingTypeSS", value: meetingType},
                {name: "meetingDateSs", value: {from: nextMeetingFrom}}
                ]
        }).then((result: oasiSolarRest.SearchResult) => {
            if (result.numFound > 0) {

                let futureMeetingDates: Date[] = _.map(result.docs, (doc: any) => {
                    return new Date(doc.meetingDateSs);
                });

                let dateInst = _.min(futureMeetingDates);
                dateInst.setDate(dateInst.getDate() - 1);
                dateInst.setHours(23, 59, 59, 999);
                deferred.resolve(dateInst);
            } else {
                deferred.resolve();
            }
        });
        return deferred.promise;
    }

    getAgendaInstructions(agenda: Agenda, meetingType: MeetingType, primaryQuestionsIds: string[]) {
        let solrFieldsFilter = [];
        solrFieldsFilter.push({
            name: 'questionPrimaryID',
            value: primaryQuestionsIds
        });

        if (meetingType.Express) {
            return this.oasiSolarRestService.searchExt({
                type: 'INSTRUCTION',
                pageSize: 200,
                fields: solrFieldsFilter
            });
        }

        const nextMeetingFrom = this.getMeetingDateTime(agenda.meetingDate, agenda.meetingTime);
        nextMeetingFrom.setMinutes(nextMeetingFrom.getMinutes() + 1, 0, 0);
        return this.getNextAgendaMeetingDate(agenda.meetingDate, agenda.meetingTime, agenda.meetingType)
            .then((nextAgendaMeetingDate: Date) => {
            if (nextAgendaMeetingDate) {
                solrFieldsFilter.push({name: 'docDateInst', value: {to: nextAgendaMeetingDate}})
            }
            return this.oasiSolarRestService.searchExt({
                type: 'INSTRUCTION',
                pageSize: 200,
                fields: solrFieldsFilter
            });
        })
    }

    getNextAgendaMeetingDate(meetingDate: Date, meetingTime: string, meetingType: string): ng.IPromise<Date> {
        let deferred = this.$q.defer<Date>();
        const nextMeetingFrom = this.getMeetingDateTime(meetingDate, meetingTime);
        nextMeetingFrom.setMinutes(nextMeetingFrom.getMinutes() + 1, 0, 0);
        this.oasiSolarRestService.searchExt({
            type: "SS_AGENDA",
            pageSize: 1,
            sort: 'meetingDateTimeSs asc',
            fields: [
                {name: "meetingTypeSS", value: meetingType},
                {name: "meetingDateTimeSs", value: {from: nextMeetingFrom}}
                ]
        }).then((result: oasiSolarRest.SearchResult) => {
            if (result.numFound > 0) {

                let futureMeetingDates: Date[] = _.map(result.docs, (doc: any) => {
                    return new Date(doc.meetingDateTimeSs);
                });

                let dateInst = _.min(futureMeetingDates);
                dateInst.setMinutes(dateInst.getMinutes() - 1, 59, 999);
                deferred.resolve(dateInst);
            } else {
                deferred.resolve();
            }
        });
        return deferred.promise;
    }

    getProtocolWithInstructions(protocol: Protocol, meetingType: MeetingType, instructions: ProtocolQuestionInstructions[]): ProtocolWithInstructions {
        let result = {
            protocolID: protocol.protocolID,
            meetingType: meetingType.meetingType,
            meetingFullName: meetingType.meetingFullName,
            meetingDate: protocol.meetingDate,
            meetingNumber: protocol.meetingNumber,
            meetingTime: protocol.meetingTime,
            meetingTimeEnd: protocol.meetingTimeEnd,
            meetingPlace: protocol.meetingPlace,
            protocolApproved: protocol.protocolApproved,
            protocolApprovedDate: protocol.protocolApprovedDate,
            attendedNum: protocol.attendedNum,
            protocolInfo: protocol.protocolInfo,
            attended: _.chain(protocol.attended).groupBy(att => att.code).map((atts: ProtocolAttended[]) => {
                let result: ProtocolWithInstructionsAttended = {
                    name: atts[0].name,
                    code: atts[0].code
                };
                let hasPerson = _.some(atts, att => att.fioFull || att.iofShort || att.post);
                if (hasPerson) {
                    result.attendedPerson = atts.map(att => att.iofShort).join(', ');
                    result.attendedPersonFIO = atts.map(att => {
                        return {
                            post: att.post,
                            iofShort: att.iofShort,
                            fioFull: att.fioFull
                        }
                    });
                }
                return result;
            }).value(),
            questionsNum: protocol.questionsNum,
            approvedBy: protocol.approvedBy,
            secretary: protocol.secretary,
            question: protocol.question.map((q, index) => {
                return {
                    questionID: q.questionID,
                    questionNumber: (index + 1).toString(),
                    itemNumber: q.itemNumber,
                    excluded: q.excluded,
                    decisionType: q.decisionType,
                    decisionTypeCode: q.decisionTypeCode,
                    decisionText: q.decisionText,
                    question: q.question,
                    activity: q.activity
                };
            })
        };
        instructions.forEach(instr => {
            let question: any = _.find(result.question,q => q.questionID === instr.questionId);
            question.instructions = instr.instructions.map((i, index) => this.convert(i, index + 1));
        });
        return result;
    }

    convert(solrDoc: oasiSolarRest.SearchResultDocument, index: number) {
        return {
            documentId: solrDoc.documentId,
            instructionsNumber: index,
            statusId: {
                code: solrDoc.statusIdInst,
                name: solrDoc.condition,
            },
            instruction: {
                number: solrDoc.docNumberInst,
                beginDate: solrDoc.docDateInst,
                creator: {
                    login: solrDoc.creatorLogInst,
                    fioFull: solrDoc.creatorInst
                },
                content: solrDoc.contentInst,
                description: solrDoc.descriptionInst,
                planDate: solrDoc.planDateInst,
                factDate: solrDoc.factDateInst,
                executor: {
                    login: solrDoc.executorLogInst,
                    fioFull: solrDoc.executorInst
                },
                coExecutor: {
                    login: solrDoc.coExecutorsLogInst,
                    fioFull: solrDoc.coExecutorsInst
                },
                questionId: solrDoc.questionId,
                questionPrimaryID: solrDoc.questionPrimaryID,
                reportContent: solrDoc.reportContentInst
            }
        };
    }

    private getMeetingDateTime(meetingDate: Date, meetingTimeStr: string): Date {
        const meetingDateTime = new Date(meetingDate.getTime());
        const meetingTime = this.timeUtils.parse(meetingTimeStr);
        meetingDateTime.setHours(meetingTime.getHours(), meetingTime.getMinutes(), 0, 0);
        return meetingDateTime;
    }

}

angular.module('app').service('instructionsService', InstructionsService);
