class MyTasksService {
    static $inject = [
        '$q',
        '$http',
        'nsiRestService',
        'nsi_url',
        'activityRestService'
    ];

    constructor(
        private $q: ng.IQService,
        private $http: ng.IHttpService,
        private nsiRestService: oasiNsiRest.NsiRestService,
        private nsiUrl: string,
        private activityRestService: oasiBpmRest.ActivityRestService
    ) {
    }

    getAllTaskWithDelegated(account: string): ng.IPromise<any> {
        let defer: any = this.$q.defer();

        let delegatedTaskRules: DelegatedTask[];
        let allMyTasks: IProtocolTask[] = [];
        let allDelegatorTasks: IProtocolTask[] = [];

        if (!account || account === '') {
            defer.reject('Account name is null or empty');
        } else {


            let promises: any = [];
            promises.push(this.getMyTasks(null, account));
            promises[0]
                .then((result) => {
                    result.forEach(t => t.foreign = false);
                    allMyTasks = allMyTasks.concat(result);
                    // return this.$http.get(this.nsiUrl + '/delegate/tasks/sysprefix/rd/account/' + account);
                    return { data: [] };
                })
                .then((response: any) => {
                    delegatedTaskRules = response.data;

                    delegatedTaskRules.forEach((taskRule: DelegatedTask) => {
                        // delayed serial promises func/ then push only unique tasks
                        let promisesFn = [];
                        promisesFn.push(() => {
                            let deferred = this.$q.defer();

                            this.getMyTasks(null, taskRule.delegationRule.fromUser).then(result => {
                                result.forEach(dt => {
                                    dt.foreign = true;
                                    dt.delegationRuleId = taskRule.delegationRule.id;
                                    let isNew = true;
                                    let uniqueTasks = allMyTasks.concat(allDelegatorTasks);
                                    uniqueTasks.forEach(mt => {
                                        if (mt.id === dt.id) isNew = false;
                                    });
                                    if (isNew) allDelegatorTasks.push(dt);
                                });

                                deferred.resolve();

                            }).catch(e => deferred.reject(e));

                            return deferred.promise;
                        });

                        promises.push(this.serial(promisesFn));
                    });

                    this.$q.all(promises).then(() => {

                        let filteredByRulesTasks = [];
                        allDelegatorTasks.forEach((task) => {
                            let passed = false;
                            let currentTime = new Date().getTime();

                            delegatedTaskRules.forEach((dt: DelegatedTask) => {
                                if (dt.delegationRule.id !== task.delegationRuleId)
                                    return;

                                // check if passed by rule
                                if (task.assignee && dt.delegationRule.fromUser === task.assignee &&
                                    (dt.name === task.formKey || (dt.name === '' || dt.name === null)))
                                    passed = true;

                                // candidate
                                if (!task.assignee && (dt.name === task.formKey || (dt.name === '' || dt.name === null)))
                                    passed = true;

                                // check time border
                                if (passed && task.foreign && (currentTime < dt.delegationRule.startDate ||
                                        (dt.delegationRule.endDate && currentTime > dt.delegationRule.endDate)))
                                    passed = false;

                                if (passed) {
                                    filteredByRulesTasks.push(task);
                                } else {
                                    console.debug('delegate rejected task');
                                    console.debug(task)
                                }

                            });

                        });


                        let accounts = filteredByRulesTasks.map(val => {
                            return val.assignee
                        });
                        accounts = _.uniqBy(accounts, (e) => {
                            return e;
                        });

                        if(accounts.length > 0) {
                            this.$q.all(
                                _.map(accounts, account => {
                                    return this.nsiRestService.ldapUser(account);
                                })
                            ).then((response: oasiNsiRest.UserBean[]) => {
                                filteredByRulesTasks.forEach((t) => {
                                    if (t.assignee && t.assignee !== account) {
                                        t.isDelegated = true;

                                        response.forEach((val) => {
                                            if(val.accountName === t.assignee) { t.delegatorFio = val.displayName; }
                                        });
                                    }

                                    if (!t.assignee && t.foreign) {
                                        t.isDelegated = true;
                                        t.delegatorFio = 'группы исполнителей (кандидатская задача)';
                                    }
                                });

                                filteredByRulesTasks = filteredByRulesTasks.concat(allMyTasks);
                                defer.resolve(filteredByRulesTasks);
                            })
                        } else {
                            defer.resolve(allMyTasks);
                        }


                    })

                })
                .catch(e => defer.reject(e));

        }

        return defer.promise;
    }

    getMyTasks(processDefinitionKeys: string[], login: string): ng.IPromise<IProtocolTask[]> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        let promises: ng.IPromise<any>[] = [];

        if (!processDefinitionKeys)
            processDefinitionKeys = ['ss_approval_gk', 'ss_quiz_vote_prc'];

        processDefinitionKeys.forEach(key => {
            promises.push(this.getTasks(key, login));
        });

        this.$q.all(promises).then((result: IProtocolTask[][]) => {
            let tasks: IProtocolTask[] = [];

            result.forEach(t => {
                tasks = tasks.concat(t);
            });

            defer.resolve(tasks);
        }).catch(error => {
            defer.reject(error);
        });

        return defer.promise;
    }

    getTasks(processDefinitionKey: string, login: string): ng.IPromise<IProtocolTask[]> {
        let defer: ng.IDeferred<any> = this.$q.defer();

        this.activityRestService.getTasks({
            candidateOrAssigned: login,
            includeProcessVariables: true,
            order: "desc",
            processDefinitionKey: processDefinitionKey,
            size: 1000000,
            sort: "createTime"
        }).then(_ => {
            defer.resolve(_.data);
        }).catch(error => {
            defer.reject(error)
        });

        return defer.promise;
    }

    serial(promisesFn: any[]): ng.IPromise<any> {
        let prevPromise = null;
        angular.forEach(promisesFn, (pFn) => {
            if (prevPromise === null) {
                prevPromise = pFn();
            } else {
                prevPromise = prevPromise.then(pFn);
            }
        });

        return prevPromise;
    }
}

angular.module('app').service('myTasksService', MyTasksService);
