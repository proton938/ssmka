interface IAlertService {
    confirm(settings: ConfirmSettings): ng.IPromise<any>;
    blocker(settings: MessageSettings): any;
}

class ButtonSettings {
    label: string;
    className: string;
}

class ConfirmSettings {
    className?: string;
    ok: ButtonSettings;
    message: string;
}

class MessageSettings {
    message: string;
}

class AlertService implements IAlertService {
    static $inject = ['$uibModal'];

    constructor(private $uibModal: any) {
    }

    confirm(settings: ConfirmSettings): ng.IPromise<any> {

        let controller = function($uibModalInstance, $scope) {

            $scope.message = settings.message;
            $scope.ok = settings.ok || settings;

            $scope.cancel = function() {
                $uibModalInstance.dismiss();
            }

            $scope.submit = function() {
                $uibModalInstance.close();
            }

            return {}
        };
        controller.$inject = ['$uibModalInstance', '$scope'];

        return this.$uibModal.open({
            templateUrl: 'app/services/alert/confirm.html',
            windowClass: settings.className,
            controller: controller
        }).result;
    }

    blocker(settings: MessageSettings): ng.IPromise<any> {

        let controller = function($uibModalInstance, $scope) {

            $scope.message = settings.message;

            $scope.cancel = function() {
                $uibModalInstance.dismiss();
            }

            return {}
        };
        controller.$inject = ['$uibModalInstance', '$scope'];

        return this.$uibModal.open({
            templateUrl: 'app/services/alert/blocker.html',
            controller: controller,
            backdrop  : 'static',
            keyboard  : false
        });
    }

}

angular.module('app').service('ssAlertService', AlertService);