class MggtService {
    static $inject = [
        '$q',
        'questionResource',
        'agendaResource',
        'protocolResource',
    ];

    constructor(private $q: ng.IQService,
                private questionResource: IQuestionResource,
                private agendaResource: IAgendaResource,
                private protocolResource: IProtocolResource) {
    }

    private needToSendAgenda(agenda: Agenda) {
        return agenda.attended && _.find(agenda.attended, att => att.code === 'MKA_MGGT' || att.code === 'MGGT');
    }

    private needToSendProtocol(protocol: Protocol) {
        return protocol.attended && _.find(protocol.attended, att => att.code === 'MKA_MGGT' || att.code === 'MGGT');
    }

    public sendAgenda(agenda: Agenda, questionsIds: string[]): ng.IPromise<any> {
        if (this.needToSendAgenda(agenda)) {
            return this.sendQuestions(questionsIds).then(() => {
                return this.agendaResource.sendToMggt({id: agenda.agendaID}).$promise;
            })
        }
        return this.$q.resolve();
    }

    private sendQuestions(questionsIds: string[]): ng.IPromise<any> {
        if (questionsIds && questionsIds.length) {
            return this.$q.all(questionsIds.map(id => this.questionResource.sendToMggt({id: id}).$promise));
        }
        return this.$q.resolve();
    }

    public sendProtocol(protocol: Protocol, agenda?: Agenda): ng.IPromise<any> {
        let needToSend = agenda ? this.needToSendAgenda(agenda) : this.needToSendProtocol(protocol);
        if (needToSend) {
            return this.protocolResource.sendToMggt({id: protocol.protocolID}).$promise;
        }
        return this.$q.resolve();
    }

    public sendProtocolAgendaAndQuestions(protocol: Protocol, agendaId: string, questionsIds: string[]): ng.IPromise<any> {
        if (this.needToSendProtocol(protocol)) {
            return this.sendQuestions(questionsIds).then(() => {
                return this.agendaResource.sendToMggt({id: agendaId}).$promise;
            }).then(() => {
                return this.protocolResource.sendToMggt({id: protocol.protocolID}).$promise;
            })
        }
        return this.$q.resolve();
    }

}

angular.module('app').service('mggtService', MggtService);
