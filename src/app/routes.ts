angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'toastrConfig', routesConfig])
    .run(['$rootScope', '$state', '$stateParams', 'meetingTypesService', 'authenticationService', 'authRestService', 'ssAuthorizationService', 'session',
        ($rootScope: ng.IRootScopeService, $state: ng.ui.IStateService,
         $stateParams: ng.ui.IStateParamsService,
         meetingTypesService: IMeetingTypesService,
         authService: IAuthenticationService,
         authRestService: oasiAuthRest.AuthRestService,
         authorizationService: IAuthorizationService,
         session: oasiSecurity.ISessionStorage) => {
            $rootScope.$on('$stateChangeStart', function (event: ng.IAngularEvent, toState: ToStateWithPermissions, toStateParams: ToStateParamsWithPermissions, fromState: any) {

                if (!session.isAuthenticated()) {
                    event.preventDefault();
                    authService.checkSession(
                        (response: AuthUser) => {
                            authorizationService.setUserPermissions(response.permissions);
                            meetingTypesService.setUserMeetingTypes(response.meetingTypes);
                            authRestService.user().then(user => {
                                session.setAuthenticationResult(user);
                                $state.go(toState, toStateParams);
                            });
                        },
                        (reason: any) => {
                        }
                    );
                } else if (toState.permission && !toStateParams.permissionChecked) {
                    event.preventDefault();
                    _.extend(toStateParams, {
                        permissionChecked: true,
                        permissionFailed: !authorizationService.check(toState.permission)
                    });
                    $state.go(toState, toStateParams);
                }

            });
        }
    ]);

interface ToStateWithPermissions extends ng.ui.IState {
    permission: string;
}

interface ToStateParamsWithPermissions extends ng.ui.IStateParamsService {
    permissionChecked: boolean;
}

/** @ngInject */
function routesConfig($stateProvider: angular.ui.IStateProvider,
                      $urlRouterProvider: angular.ui.IUrlRouterProvider,
                      $locationProvider: angular.ILocationProvider,
                      $httpProvider: ng.IHttpProvider,
                      toastrConfig: any) {

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.interceptors.push('errorHandlerInterceptor');

    $urlRouterProvider.otherwise(($injector, $location) => {
        var $state = $injector.get("$state");
        $state.go('app.ssmka.calendar');
    });

    angular.extend(toastrConfig, {
        timeOut: 8000,
        extendedTimeOut: 2000
    });

    $stateProvider.state('app', {
        abstract: true,
        url: '/app',
        params: {
            permissionChecked: false,
            permissionFailed: false
        },
        template: '<app></app>'
    }).state('app.ssmka', {
        abstract: true,
        url: '/ssmka',
        template: '<ssmka-app></ssmka-app>'
    }).state('app.ssmka.main', {
        url: '/main',
        template: '<main></main>',
        data: {title: 'Главная'}
    }).state('app.ssmka.planned_questions', <angular.ui.IState> {
        url: '/planned_questions',
        template: '<planned-questions></planned-questions>',
        data: {title: 'Планируемые вопросы заседаний'},
        permission: 'OASI_MEETING_VIEW_QUESTIONPLAN'
    }).state('app.ssmka.questions', <angular.ui.IState> {
        url: '/questions?query',
        params: {
            query: null,
            filter: null
        },
        template: '<questions></questions>',
        data: {title: 'Вопросы заседаний'},
        permission: 'OASI_MEETING_VIEW_QUESTION'
    }).state('app.ssmka.question', {
        url: '/question/:id',
        params: {
            id: null,
            tab: 0,
            editingCard: false
        },
        template: '<question></question>',
        data: {title: 'Вопрос СС МКА'}
    }).state('app.ssmka.agendas', {
        url: '/agendas',
        template: '<div ui-view></div>',
        abstract: true
    }).state('app.ssmka.agendas.published', <angular.ui.IState> {
        url: '/published',
        template: '<agendas-published></agendas-published>',
        data: {title: 'Повестки заседаний'},
        permission: 'OASI_MEETING_VIEW_AGENDA'
    }).state('app.ssmka.agendas.projects', <angular.ui.IState> {
        url: '/projects',
        template: '<agendas-projects></agendas-projects>',
        data: {title: 'Проекты повесток заседаний'},
        permission: 'OASI_MEETING_VIEW_AGENDAPROJECT'
    }).state('app.ssmka.agenda', {
        url: '/agenda/:id',
        params: {
            id: null,
            editing: false,
            disableNavigator: true,
            activeTab: null
        },
        template: '<agenda></agenda>',
        data: {title: 'Повестка заседания'}
    }).state('app.ssmka.express-agenda-edit', {
        url: '/express/agenda/edit/:id',
        params: {
            id: null,
            copy: false,
            payload: null
        },
        template: '<express-agenda-edit></express-agenda-edit>',
        data: {title: 'Повестка заседания'}
    }).state('app.ssmka.protocols', <angular.ui.IState> {
        url: '/protocols',
        template: '<protocols></protocols>',
        data: {title: 'Протоколы заседаний'},
        permission: 'OASI_MEETING_VIEW_PROTOCOL'
    }).state('app.ssmka.protocol', {
        url: '/protocol/:id',
        params: {
            id: null,
            agendaID: null,
            editing: false
        },
        template: '<protocol></protocol>',
        data: {title: 'Протокол заседания'}
    }).state('app.ssmka.calendar', {
        url: '/calendar',
        params: {
            disableNavigator: true
        },
        template: '<calendar></calendar>',
        data: {title: 'Календарь'}
    }).state('app.ssmka.presentation_prepare', <angular.ui.IState> {
        url: '/presentation_prepare',
        template: '<presentation_prepare></presentation_prepare>',
        data: {title: 'Подготовка презентаций'},
        permission: 'OASI_MEETING_VIEW_PREZENT'
    }).state('app.ssmka.my-tasks', {
        url: '/my-tasks?query',
        params: {},
        template: '<my-tasks></my-tasks>',
        data: {title: 'Мои задачи'}
    }).state('app.ssmka.process', {
        url: '/process',
        abstract: true,
        template: '<process></process>',
        params: {
            taskId: null,
            formKey: null,
            documentId: null
        }
    }).state('app.ssmka.process.gkPzzRegisterSS', {
        url: '/RegisterSS/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-registration-form></gk-pzz-registration-form>'
            }
        },
        data: {title: 'Регистрация участников ГК'}
    }).state('app.ssmka.process.gkEooRegMembersAddQuestions', {
        url: '/regMembersAddQuestions_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-registration-form></gk-eoo-registration-form>'
            }
        },
        data: {title: 'Регистрация участников ГК'}
    }).state('app.ssmka.process.addQuestion', {
        url: '/addQuestion/:meetingType/:protocolId/:taskId',
        params: {
            protocolId: null
        },
        views: {
            'body': {
                template: '<add-question-dialog></add-question-dialog>'
            }
        },
        data: {title: 'Добавление вопроса к голосованию'}
    }).state('app.ssmka.process.GkPzzPrepareProtocolAndConclusion', {
        url: '/PrepareProtocolAndConclusion/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-protocol-and-conclusion-form></gk-pzz-protocol-and-conclusion-form>'
            }
        },
        data: {title: 'Формирование Протокола и Заключения ГК'}
    }).state('app.ssmka.process.GkEooPrepareProtocolAndConclusion', {
        url: '/prepareProtocolGkSec_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-protocol-and-conclusion-form></gk-eoo-protocol-and-conclusion-form>'
            }
        },
        data: {title: 'Формирование Протокола'}
    }).state('app.ssmka.process.gkPzzVoting', {
        url: '/ss_quiz_vote/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-voting-form></gk-pzz-voting-form>'
            }
        },
        data: {title: 'Электронное голосование'}
    }).state('app.ssmka.process.gkEooQuizVote', {
        url: '/quizVote_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-voting-form></gk-eoo-voting-form>'
            }
        },
        data: {title: 'Электронное голосование'}
    }).state('app.ssmka.process.gkPzzSecretaryVoting', {
        url: '/transitionOnQuiz/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-secretary-pzz-voting-form></gk-secretary-pzz-voting-form>'
            }
        },
        data: {title: 'Секретарь: Электронное голосование'}
    }).state('app.ssmka.process.gkEooSecretaryVoting', {
        url: '/transOnQuis_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-secretary-eoo-voting-form></gk-secretary-eoo-voting-form>'
            }
        },
        data: {title: 'Секретарь: Электронное голосование'}
    }).state('app.ssmka.process.DetermineRefusalCasesByPZZ', {
        url: '/determineRefusalCasesByPZZ_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<determine-refusal-cases-by-pzz></determine-refusal-cases-by-pzz>'
            }
        },
        data: {title: 'Установить причины отказов по ПЗЗ'}
    }).state('app.ssmka.process.approveProtocAndConcl_frm', {
        url: '/approveProtocAndConcl_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-approve-protoc-and-concl></gk-pzz-approve-protoc-and-concl>'
            }
        },
        data: {title: 'Согласовать Протокол и Заключение ГК'}
    }).state('app.ssmka.process.approveProtocol_frm', {
        url: '/approveProtocol_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-approve-protoc-and-concl></gk-eoo-approve-protoc-and-concl>'
            }
        },
        data: {title: 'Согласовать Протокол'}
    }).state('app.ssmka.process.ApproveProtocAndConcl', {
        url: '/ApproveProtocAndConcl/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-approve-protoc-and-concl></gk-pzz-approve-protoc-and-concl>'
            }
        },
        params: {
            isSupervisor: true
        },
        data: {title: 'Согласовать Протокол и Заключение ГК'}
    }).state('app.ssmka.process.approveProtocolFirstZam_frm', {
        url: '/approveProtocolFirstZam_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-approve-protoc-and-concl></gk-eoo-approve-protoc-and-concl>'
            }
        },
        params: {
            isSupervisor: true
        },
        data: {title: 'Согласовать Протокол'}
    }).state('app.ssmka.process.ApproveProtocolGkUkd', {
        url: '/ApproveProtocolGkUkd/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-approve-protoc-and-concl></gk-pzz-approve-protoc-and-concl>'
            }
        },
        params: {
            isStroyUser: true
        },
        data: {title: 'Согласовать Протокол и Заключение ГК'}
    }).state('app.ssmka.process.approveProtocolUkdSk_frm', {
        url: '/approveProtocolUkdSk_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-approve-protoc-and-concl></gk-eoo-approve-protoc-and-concl>'
            }
        },
        params: {
            isStroyUser: true
        },
        data: {title: 'Согласовать Протокол и Заключение ГК'}
    }).state('app.ssmka.process.GkPzzSignProtocolAndConclusionRS', {
        url: '/SignProtocolAndConclusionRS/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-approval-protocol-and-conclusion-form></gk-pzz-approval-protocol-and-conclusion-form>'
            }
        },
        data: {title: 'Утверждение Протокола и Заключения ГК'}
    }).state('app.ssmka.process.GkEooSignProtocolAndConclusionRS', {
        url: '/signProtocol_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-approval-protocol-and-conclusion-form></gk-eoo-approval-protocol-and-conclusion-form>'
            }
        },
        data: {title: 'Утверждение Протокола'}
    }).state('app.ssmka.process.GkPzzSignProtocolAndConclusionPC', {
        url: '/SignProtocolAndConclusionPC/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-chairman-approve-form></gk-pzz-chairman-approve-form>'
            }
        },
        data: {title: 'Утверждение Протокола и Заключения ГК'}
    }).state('app.ssmka.process.GkEooSignProtocolAndConclusionPC', {
        url: '/signProtocolByZampred_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-chairman-approve-form></gk-eoo-chairman-approve-form>'
            }
        },
        data: {title: 'Утверждение Протокола'}
    }).state('app.ssmka.process.GkPzzReworkProtocolAndConclusion', {
        url: '/ReworkProtocolAndConclusion/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-pzz-rework-protocol-and-conclusion></gk-pzz-rework-protocol-and-conclusion>'
            }
        },
        data: {title: 'Доработка Протокола и Заключения ГК'}
    }).state('app.ssmka.process.GkEooReworkProtocolAndConclusion', {
        url: '/reworkProtocol_frm/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<gk-eoo-rework-protocol-and-conclusion></gk-eoo-rework-protocol-and-conclusion>'
            }
        },
        data: {title: 'Доработка Протокола'}
    }).state('app.ssmka.process.PrepareDecisionProtocol', {
        url: '/PrepareDecisionProtocol/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<express-prepare-decision-protocol></express-prepare-decision-protocol>'
            }
        },
        data: {title: 'Сформировать протокол решения'}
    }).state('app.ssmka.process.ProcessApprovalResults', {
        url: '/ProcessApprovalResults/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<express-process-approval-results></express-process-approval-results>'
            }
        },
        data: {title: 'Обработать результаты согласования'}
    }).state('app.ssmka.process.ApproveProtocol', {
        url: '/approveProtocol/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<express-approve-protocol></express-approve-protocol>'
            }
        },
        data: {title: 'Согласовать протокол'}
    }).state('app.ssmka.process.PrintAndSignVisacopyByManager', {
        url: '/PrintAndSignVisacopyByManager/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<express-print-and-sign-visacopy-by-manager></express-print-and-sign-visacopy-by-manager>'
            }
        },
        data: {title: 'Распечатать и подписать визовый экземпляр у руководителя'}
    }).state('app.ssmka.process.ScanAndAttachSignedProtocol', {
        url: '/ScanAndAttachSignedProtocol/:documentId/:taskId',
        parent: 'app.ssmka.process',
        views: {
            'body': {
                template: '<express-scan-and-attach-signed-protocol></express-scan-and-attach-signed-protocol>'
            }
        },
        data: {title: 'Отсканировать и приложить подписанный протокол'}
    }).state('diagram', {
        url: '/diagram/:title/:image',
        template: '<diagram></diagram>',
        params: {
            image: "",
            title: ""
        }
    });
}
