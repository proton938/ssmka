Dropzone.autoDiscover = false;
var app = angular
    .module('app', [
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.validate',
        'ui.sortable',
        'toastr',
        'ivh.treeview',
        'ngFileUpload',
        'base64',
        'ngStorage',
        'nk.touchspin',
        'angular-ladda',
        'ui.select',
        'datePicker',
        "datatables",
        "datatables.bootstrap",
        "localytics.directives",
        "datatables.bootstrap",
        'ngCollection',
        'ngStomp',
        'thatisuday.dropzone',
        'oasi-security',
        'oasi.auth.rest',
        'oasi.nsi.rest',
        'angular.filter',
        'oasi.bpm.history',
        'oasi.bpm.rest',
        'oasi.solar.rest',
        'oasi.task.wrapper.component',
        'oasi.footer',
        'oasi.navbar',
        'oasi.sidebar',
        'oasi.breadcrumbs',
        'oasi-config-box',
        'oasi.cryptopro',
        'oasi.file.rest',
        'oasi.file.manager',
        'oasi.wordreporter.rest',
        'oasi.document.logs',
        'angular-jsoneditor'
    ])
    .config(['blockUIConfig', function (blockUIConfig: angular.blockUI.BlockUIConfig) {
        blockUIConfig.cssClass = 'block-ui blockUI';
        blockUIConfig.autoBlock = false;
        blockUIConfig.autoInjectBodyBlock = false;
        blockUIConfig.message = undefined;
    }])
    .config(["activityRestServiceProvider", "activiti_rest_url", (activityRestServiceProvider: oasiBpmRest.ActivityRestServiceProvider, activitiRestUrl: string) => {
        activityRestServiceProvider.root = activitiRestUrl;
    }]).config(["nsiRestServiceProvider", "nsi_url", (nsiRestServiceProvider: oasiNsiRest.NsiRestServiceProvider, nsiUrl: string) => {
        nsiRestServiceProvider.root = nsiUrl;
    }]).config(["oasiSolarRestServiceProvider", "solr_url", (oasiSolarRestServiceProvider: oasiSolarRest.OasiSolarRestServiceProvider, solrUrl: string) => {
        oasiSolarRestServiceProvider.root = solrUrl;
    }]).config(["fileHttpServiceProvider", "filenet_url", (fileHttpServiceProvider: oasiFileRest.FileHttpServiceProvider, filenetUrl: string) => {
        fileHttpServiceProvider.root = filenetUrl;
    }]).config(["wordReporterRestServiceProvider", "reporter_url", (wordReporterRestService: oasiWordreporterRest.WordReporterRestServiceProvider, reporterUrl: string) => {
        wordReporterRestService.rootReporter = reporterUrl;
    }]).config(["wordReporterPreviewServiceProvider", "reporter_url", (wordReporterPreviewService: oasiWordreporterRest.WordReporterPreviewServiceProvider, reporterUrl: string) => {
        wordReporterPreviewService.rootReporter = reporterUrl;
    }]);

// oasi-security component
app.constant('authServerApi', '/oasi/auth')
    .constant('nsiApi', '/nsi/nsi-api/v1');


class AuthenticationFailureInterceptor {

    constructor(private $q: angular.IQService) {
    }

    responseError = (response) => {
        if (response.status === 401) {
            const currentLocation = window.location.pathname + window.location.search + window.location.hash;
            window.location.href = '/?from=' + currentLocation.replace(/#/, '^');
        }
        return this.$q.reject(response);
    }
}

app.factory('authenticationFailureInterceptor', ['$q', function ($q) {
    return new AuthenticationFailureInterceptor($q);
}]);
app.factory('cdpAuthenticationFailureInterceptor', ['$q', function ($q) {
    return new AuthenticationFailureInterceptor($q);
}]);

// переопределить на верхнем уровне для удобства переноса - не нужно ждать правильной версии oasi-security
app.config(['$httpProvider', function ($httpProvider: any) {
    $httpProvider.interceptors.push('authenticationFailureInterceptor');
    $httpProvider.interceptors.push('cdpAuthenticationFailureInterceptor');
}]);
