## Перед установкой
Скорее всего в процессе установки придется собирать модуль `sass` под текущую платформу разработчика. Для этого
понадобятся `python 2.7` и компилятор. Проще всего взять инструкцию к [node-gyp](https://github.com/nodejs/node-gyp),
который, собственно, и используется для сборки.
Дополнительно надо поставить глобально `gulp` и `bower`:
```
$ npm i -g gulp-cli
$ npm i -g bower
```
## Установка
Для начала, надо установить зависимости для разработки:
```
$ npm i
```
Если эта команда вызывает ошибку установки sass - предварительно проделать: `$ npm i node-sass`, а потом `$ npm i`

Затем зависимости для фронтенда:
```
$ bower install
```
Если в процессе установки модулей для фронта `bower` ругается, что не найден реестр на heroku, значит надо прописать
путь к новому реестру в `.bowerrc` добавив настройку:
```
"registry": "https://registry.bower.io"
```
Затем надо собрать проект в основном для подгрузки typings
```
$ npm run-script build -- --env=polikom-dev
```

При ошибках `ENOENT` при установке можно поробовать насильно почистить кеш npm:
```
$ npm cache clean --force
```
## Запуск

Для запуска необходимо: 

1)   установить web-сервер nginx, в настройках конфигурации (файл: `nginx.conf`) блок `server {}` заменить на:
````
server {
        listen 8081;
		location /browser-sync/ {
		proxy_pass http://127.0.0.1:4200/browser-sync/;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		}
		location /ssmka {
		proxy_pass http://127.0.0.1:4200/;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		}
		location /bower_components {
		proxy_pass http://127.0.0.1:4200;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		}
		location /ssmka/api/ {
		proxy_pass http://192.168.100.42:5310/ssmka/api/;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		}
		location /ssmka/api/websocket/ {
		proxy_pass http://192.168.100.42:5310/ssmka/api/websocket/;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		}
		location /ssmka/logs {
		return 200 'OK';
		}
		location / {
		proxy_pass http://192.168.100.42:5310/;
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		}
    }
````

2)   настройки gatekeeper (файл: `config.yml`):

````
    discovery-url: https://smartid.reinform.ru/iam/auth/realms/smart
    client-id: smart-app
    client-secret: 509f3a87-dea7-49ce-a111-8b40672dc56f
    
    listen: 0.0.0.0:3000
    redirection-url: http://localhost:3000
    upstream-url: http://localhost:8081/
    preserve-host: true
    
    enable-default-deny: true
    
    secure-cookie: false # -> for https in redirection-url
    
    enable-encrypted-token: true
    enable-refresh-tokens: true
    encryption-key: 12161666666666765dg66666666sfd23
    
    enable-logging: true
    enable-json-logging: true
    
    resources:
    - uri: /*
    - uri: /public/*
    white-listed: true
    
````

3) в файл проекта ...\ssmka\conf\browsersync.conf.js вставить код:
````
    const conf = require('./gulp.conf');

    module.exports = function () {
    	return {
    		server: {
    			baseDir: [
    			conf.paths.tmp,
    			conf.paths.src
    			],
    			routes: {
    			'/bower_components': 'bower_components'
    			}
    		},
    		port: 4200,
    		open: false
    	};
    };

````

В дирректории nginx:
````
$ start nginx
````

В разработке лучше применять `serve` задачу, которая будет пересобирать измененные файлы и перезагружать браузер. Для
этого в отдельном окне надо запустить:
```
$ npm run-script serve -- --env=polikom-dev
```
Параметр окружения обязателен, так как он имеет нестандартные значения, весь список которых можно посмотреть в папке
`./env/`.

Теперь открываем приложение на `http://localhost:3000/ssmka/#/app/ssmka/agenda/`
